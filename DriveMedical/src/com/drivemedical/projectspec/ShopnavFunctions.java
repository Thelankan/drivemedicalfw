package com.drivemedical.projectspec;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.Locators;



public class ShopnavFunctions {
	
   public List<String> prodnames=new ArrayList<String>();
   public List<String> prodPrice=new ArrayList<String>();
   public ArrayList<String> materialId = new ArrayList<String>();
   int RemoveIcon[];
   int leftOutProductsInLastPage;
   int totPages;
   public String savedCartNameFromExcel;
   

   public void navigateToPLP() throws Exception
   {
	   if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
	   {
		   BaseTest.log.info("Click on Hamburgar menu");
		   BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
		   BaseTest.log.info("Click on arrow icon");
		   BaseTest.l1.getWebElement("MegaMenu_Products_Cat_Arrow_Mobile", "Shopnav//header.properties").click();
		   BaseTest.log.info("click on sub category");
		   BaseTest.l1.getWebElement("MegaMenu_Canes_SubCat_Mobile", "Shopnav//header.properties").click();
	   } 
	   else 
	   {
		 /*  BaseTest.log.info("Hover on root category");
		   BaseTest.act.moveToElement(BaseTest.l1.getWebElement("MegaMenu_Products_Cat","Shopnav//header.properties")).perform();
		   BaseTest.log.info("click on sub category");
		   BaseTest.l1.getWebElement("MegaMenu_Canes_SubCat", "Shopnav//header.properties").click();*/
		   
		   String url = GetData.getDataFromProperties("//data//config.properties", "URL");
		   String subCAtUrl = "Products/Mobility/Canes/c/Canes";
		   url = url+subCAtUrl;
			BaseTest.driver.get(url);
	   }
   }
   
	public void searchProduct(String searchKeyWord) throws Exception
	{
		if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile"))
		{
			BaseTest.log.info("Mobile View");
			BaseTest.l1.getWebElement("Header_Mob_Search_Text", "Shopnav//header.properties").click();
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").sendKeys(searchKeyWord);
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").sendKeys(Keys.ENTER);
//			BaseTest.l1.getWebElement("Header_Mob_Search_Icon", "Shopnav//header.properties").click();
		}
		else if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("tablet")) 
		{
			BaseTest.log.info("Tab View");
			BaseTest.l1.getWebElement("Header_Mob_Search_Text", "Shopnav//header.properties").click();
			BaseTest.gVar.waitForElement("Header_SearchBox_Tab", "Shopnav//header.properties", 8);
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").sendKeys(searchKeyWord);
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").sendKeys(Keys.ENTER);
		} 
		else 
		{
			BaseTest.log.info("Desktop View");
			Thread.sleep(3000);
			BaseTest.l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").sendKeys(searchKeyWord);
			BaseTest.l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").sendKeys(Keys.ENTER);
//			BaseTest.l1.getWebElement("Header_Search_Icon", "Shopnav//header.properties").click();
		
		}
	}
	
	public void navigateToPDP_Search(String searchKeyWord) throws Exception
	{
		searchProduct(searchKeyWord);
		//Click on Product name in PLP (if it navigate to PLP)
		if (BaseTest.gVar.assertVisible("ProductNames", "Shopnav//PLP.properties"))
		{
			BaseTest.l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
		}
		
		//Collect the product name in PDP
		prodnames.add(BaseTest.l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText());
		if (BaseTest.gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties")) 
		{
			prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
		}
		
	}
	
	public void selectSwatch() throws Exception
	{

		try
		{
			if(BaseTest.gVar.assertVisible("PDP_ColorText", "Shopnav//PDP.properties"))
			{
				try
				{
					WebElement SelectedColor=BaseTest.l1.getWebElement("PDP_SelectedColorSwatch", "Shopnav//PDP.properties");
				}
				catch (Exception e)
				{
					BaseTest. l1.getWebElement("PDP_SelectedColorSwatch", "Shopnav//PDP.properties").click();
				}
			}
		}
		catch(Exception e)
		{
			BaseTest.log.info("color is not there");
		}
		try
		{
			if(BaseTest.l1.getWebElement("PDP_Fit_Swatch", "Shopnav//PDP.properties").isDisplayed())
			{
				BaseTest.gVar.selectObj(BaseTest.l1.getWebElement("PDP_Fit_DropDown", "Shopnav//PDP.properties")).selectByIndex(1);
			}
		}
		catch(Exception e)
		{
			BaseTest.log.info("Fit is not there");
		}
		try
		{
			if(BaseTest.l1.getWebElement("PDP_Size_Swatch_Text", "Shopnav//PDP.properties").isDisplayed())
			{
				BaseTest.gVar.selectObj(BaseTest.l1.getWebElement("PDP_Size_Dropdown", "Shopnav//PDP.properties")).selectByIndex(1);
			}
		}
		catch(Exception e)
		{
			BaseTest.log.info("Fit is not there");
		}
    }

// ################## Commenting below script is becouse we are adding multiple products from PLP. If the product is OUT OF STOCK then the script will fail. To avoid that will add the products which are InStock(these product ID fetched from excel sheet)
	
//		public void addItemToCart(String searchKeyWord,int numOfItems,int qty) throws Exception
//		{
//			for(int i=0; i< numOfItems; i++)
//			{
//				BaseTest.log.info("LOOP:- "+ i);
//				
//				if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
//				{
//					//Mini cart popup is not displaying in mobile view. Hence Home page navigation is not required
//				}
//				else
//				{
//					BaseTest.log.info("Click on Home PAge for Desktop view");
//					//After clicking on ADDTOCArt, minicart popup is displaying. If Minicart popup is displaying unable to search.Hence Home page navigation(Refreshing the page) is required
//					BaseTest.l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
//					Thread.sleep(2000);
//				}
//				
//				BaseTest.log.info("Navigate to PLP");
//				searchProduct(searchKeyWord);
//				BaseTest.log.info("fetch product names");
//				String proNAmeInPLP = BaseTest.l1.getWebElements("ProductNames_Text", "Shopnav//PLP.properties").get(i).getText();
//				BaseTest.log.info("proNAmeInPLP:- "+ proNAmeInPLP);
//				prodnames.add(proNAmeInPLP);
//				BaseTest.log.info("prodnames:- "+ prodnames);
//				BaseTest.l1.getWebElements("ProductNames", "Shopnav//PLP.properties").get(i).click();
//				BaseTest.log.info("fetch product price from PDP");
//				prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
//				materialId.add(BaseTest.l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText().split("#")[1].trim());
//				
//				BaseTest.log.info("Select Swath");
//				//selectSwatch();
//				BaseTest.log.info("Enter quantity");
//				BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
//				BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(qty+"");
//				BaseTest.log.info("Click on Add to cart");
//				BaseTest.l1.getWebElement("PDP_AddToCart_Btn", "Shopnav//PDP.properties").click();
//			}
//		}
	
	public void addItemToCart(String searchKeyWord,int numOfItems,int qty) throws Exception
	{
		for(int i=0; i< numOfItems; i++)
		{
			BaseTest.log.info("LOOP:- "+ i);
			
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				//Mini cart popup is not displaying in mobile view. Hence Home page navigation is not required
			}
			else
			{
				BaseTest.log.info("Navigate to Home PAge for Desktop view");
				//After clicking on ADDTOCArt, minicart popup is displaying. If Minicart popup is displaying unable to search.Hence Home page navigation(Refreshing the page) is required
//				BaseTest.l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
				BaseTest.gVar.navigateToSite(BaseTest.xmlTest);
				Thread.sleep(2000);
			}
			
			BaseTest.log.info("Navigate to PLP");
			int rowNum = 1+i;
//			searchProduct(searchProduct);
//			BaseTest.l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
//			navigateToPDP_Search(searchProduct);
			if (numOfItems>1)
			{
				searchKeyWord = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", rowNum, 3);
			}
			BaseTest.log.info("searchKeyWord:-"+ searchKeyWord);
			pdpNavigationThroughURL(searchKeyWord);
			BaseTest.log.info("fetch product name and price from PDP");
			String proNAmeInPDP = BaseTest.l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
			BaseTest.log.info("proNAmeInPLP:- "+ proNAmeInPDP);
			prodnames.add(proNAmeInPDP);
			BaseTest.log.info("prodnames:- "+ prodnames);
			prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
			materialId.add(BaseTest.l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText().split("#")[1].trim());
			
			BaseTest.log.info("Select Swath");
			//selectSwatch();
//			BaseTest.log.info("Enter quantity");
			BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
			BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(qty+"");
			BaseTest.log.info("Click on Add to cart");
			try
			{
				BaseTest.l1.getWebElement("PDP_AddToCart_Btn", "Shopnav//PDP.properties").click();
			}
			catch (Exception e)
			{
				WebElement element = BaseTest.l1.getWebElement("PDP_AddToCart_Btn", "Shopnav//PDP.properties");
				JavascriptExecutor exe=(JavascriptExecutor)BaseTest.driver;
				exe.executeScript("arguments[0].click();", element);
			}
		}
	}
	
	public void addItemToCart(int numOfItems,int qty) throws Exception
	{
		for(int i=0; i< numOfItems; i++)
		{
			BaseTest.log.info("LOOP:- "+ i);
			
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				//Mini cart popup is not displaying in mobile view. Hence Home page navigation is not required
			}
			else
			{
				BaseTest.log.info("Navigate to Home PAge for Desktop view");
				//After clicking on ADDTOCArt, minicart popup is displaying. If Minicart popup is displaying unable to search.Hence Home page navigation(Refreshing the page) is required
//				BaseTest.l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
				BaseTest.gVar.navigateToSite(BaseTest.xmlTest);
				Thread.sleep(2000);
			}
			
			BaseTest.log.info("Navigate to PLP");
			int rowNum = 1+i;
			String searchProduct = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", rowNum, 3);
//			searchProduct(searchProduct);
//			BaseTest.l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
			navigateToPDP_Search(searchProduct);
			BaseTest.log.info("fetch product name and price from PDP");
			String proNAmeInPDP = BaseTest.l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
			BaseTest.log.info("proNAmeInPLP:- "+ proNAmeInPDP);
			prodnames.add(proNAmeInPDP);
			BaseTest.log.info("prodnames:- "+ prodnames);
			prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
			materialId.add(BaseTest.l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText().split("#")[1].trim());
			
			BaseTest.log.info("Select Swath");
			//selectSwatch();
//			BaseTest.log.info("Enter quantity");
//			BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
//			BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(qty+"");
			BaseTest.log.info("Click on Add to cart");
			BaseTest.l1.getWebElement("PDP_AddToCart_Btn", "Shopnav//PDP.properties").click();
		}
	}
	
	public void navigateToCartPage() throws Exception
	{
		BaseTest.log.info("Navigate to cart");
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"cart";
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("MOBILE VIEW");
			BaseTest.gVar.waitForElement("Header_Cart_Link_Mobile", "Shopnav//header.properties",8);
			if (BaseTest.gVar.assertVisible("Header_Cart_Link_Mobile", "Shopnav//header.properties"))
			{
				BaseTest.l1.getWebElement("Header_Cart_Link_Mobile", "Shopnav//header.properties").sendKeys(Keys.ENTER);
			}
			else if (BaseTest.gVar.assertVisible("Header_Cart_Link_GuestUser_Mobile", "Shopnav//header.properties")) 
			{
				BaseTest.l1.getWebElement("Header_Cart_Link_GuestUser_Mobile", "Shopnav//header.properties").click();
			}
			else 
			{
				WebElement cartHeading = BaseTest.l1.getWebElement("Cart_Heading", "Cart//Cart.properties");
				WebDriverWait wait = new WebDriverWait(BaseTest.driver, 8);
				wait.until(ExpectedConditions.elementToBeClickable(cartHeading));
				BaseTest.driver.get(cartPageUrl);
			}
		} 
		else 
		{
			BaseTest.log.info("DESKTOP view");
			
			if (BaseTest.gVar.assertVisible("Header_Cart_Link", "Shopnav//header.properties"))
			{
				BaseTest.l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
			}
			else 
			{
				BaseTest.driver.get(cartPageUrl);
			}
		}
		
		//Verify the CArt page
		if (BaseTest.gVar.assertNotVisible("Cart_Heading", "Cart//Cart.properties"))
		{
			BaseTest.driver.get(cartPageUrl);
		}
		
		Thread.sleep(3000);
	}
	public void navigateToCart(String searchKeyWord,int numOfItems,int qty) throws Exception
	{
		BaseTest.log.info("Add Item to Cart");
		addItemToCart(searchKeyWord,numOfItems,qty);
		navigateToCartPage();
	}
	
	public void openHamburgarMenu() throws Exception
	{
		//Check whether the Hamburger menu is opened or not
		if (BaseTest.gVar.assertNotVisible("Hamburger_Expanded", "Shopnav\\header.properties"))
		{
			BaseTest.log.info("Click on Hamburgar menu");
			WebElement hamburgarElement = BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties");
			WebDriverWait wait = new WebDriverWait(BaseTest.driver, 8);
			wait.until(ExpectedConditions.elementToBeClickable(hamburgarElement));
//			hamburgarElement.click();
			hamburgarElement.sendKeys(Keys.ENTER);
		}
		else 
		{
			BaseTest.log.info("Click on Hamburgar menu is opened already");
		}
	}
	public void expandMyAccountSection() throws Exception
	{
		BaseTest.log.info("Expand the My account section");
		Thread.sleep(2500);
		BaseTest.gVar.waitForElement("Hamburger_MyAccount_ArrowDown", "Shopnav\\header.properties", 6);
		if (BaseTest.gVar.assertVisible("Hamburger_MyAccount_ArrowDown", "Shopnav\\header.properties"))
		{
			BaseTest.l1.getWebElement("Hamburger_MyAccount_ArrowDown", "Shopnav\\header.properties").click();
			Thread.sleep(500);
		}
	}
	
	public void navigateToSavedCart() throws Exception
	{
		/*String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/saved-carts";
		Thread.sleep(2000);
		BaseTest.driver.get(cartPageUrl);
		Thread.sleep(1000);
		BaseTest.log.info("Verify the Saved cart page");
		BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("SavedCart_Heading", "Profile//Savedcart.properties"),"Verify the SAVED CART heading");*/
		try
		{
			if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie")) 
			{
				Thread.sleep(3000);
			}
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				openHamburgarMenu();
				expandMyAccountSection();
				BaseTest.log.info("Click on Saved Carts link");
				BaseTest.l1.getWebElement("Hamburger_SavedCarts_Link", "Shopnav\\header.properties").click();
			}
			else 
			{
				WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
				BaseTest.gVar.waitForElement("Header_Reg_MyAct_Link", "Shopnav//header.properties", 10000);
				BaseTest.log.info("Mouse hover on My Account link in header");
				Thread.sleep(2000);
				BaseTest.gVar.mouseHover(myAccountLink);
				Thread.sleep(1000);
				if (BaseTest.gVar.assertVisible("Header_Saved_Carts_Link", "Shopnav//header.properties")) 
				{
					//If My Account Overlay is visible, Click on Saved Cart link link 
					BaseTest.l1.getWebElement("Header_Saved_Carts_Link", "Shopnav//header.properties").click();
				} 
			}
		}
		catch (Exception e)
		{
			System.out.println("navigateToSavedCart() CATCH block executing");
			
			String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
			String cartPageUrl = homePagUrl+"my-account/saved-carts";
			Thread.sleep(2000);
			BaseTest.driver.get(cartPageUrl);
			Thread.sleep(1000);
			BaseTest.log.info("Verify the Saved cart page");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("SavedCart_Heading", "Profile//Savedcart.properties"),"Verify the SAVED CART heading");
		}
		
	}
	
	public void navigateToOrderHistoryPage() throws Exception
	{
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/orders";
		try
		{
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				BaseTest.log.info("Click on Hamburgar menu");
				BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
				expandMyAccountSection();
				Thread.sleep(1500);
				BaseTest.log.info("Click on Order History link");
				BaseTest.l1.getWebElement("Hamburger_OrderHistory_Link", "Shopnav\\header.properties").click();
				
			}
			else 
			{
				
				WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
				BaseTest.gVar.waitForElement("Header_Reg_MyAct_Link", "Shopnav//header.properties", 10000);
				BaseTest.log.info("Mouse hover on My Account link in header");
				BaseTest.gVar.mouseHover(myAccountLink);
				BaseTest.l1.getWebElement("Header_OrderHistory_Link", "Shopnav//header.properties").click();
				
			}
		}
		catch (Exception e)
		{
			System.out.println("navigateToOrderHistoryPage CATCH blocking executing");
			BaseTest.driver.get(cartPageUrl);
		}
		BaseTest.log.info("Verify the Order History page");
		String orderHistoryText = GetData.getDataFromExcel("//data//GenericData_US.xls", "OrderHistory", 1, 0);
		BaseTest.sa.assertEquals(BaseTest.gVar.assertEqual("OrderHistory_Section_Header", "Profile//OrderHistory.properties"), 
				orderHistoryText,"Order History Page Heading");
	}
	
	public void navigateToQuotesPage() throws Exception
	{
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/orders";
		
		try
		{
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				BaseTest.log.info("Click on Hamburgar menu");
				BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
				expandMyAccountSection();
				Thread.sleep(1500);
				BaseTest.log.info("Click on Quotes link");
				BaseTest.l1.getWebElement("Hamburger_Quotes_Link", "Shopnav\\header.properties").click();
			}
			else 
			{
				WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
				BaseTest.gVar.waitForElement("Header_Reg_MyAct_Link", "Shopnav//header.properties", 10000);
				BaseTest.log.info("Mouse hover on My Account link in header");
				BaseTest.gVar.mouseHover(myAccountLink);
				BaseTest.l1.getWebElement("Header_Quotes_Link", "Shopnav//header.properties").click();
			}
		}
		catch (Exception e)
		{
			System.out.println("navigateToQuotesPage CATCH block is executing");
			BaseTest.driver.get(cartPageUrl);
		}
		
		BaseTest.log.info("Verify the Order History page");
		String quotesHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 9);
		BaseTest.sa.assertEquals(BaseTest.gVar.assertEqual("Quotes_Heading", "Profile//Quotes.properties"), 
				quotesHeading,"Quotes Page Heading");
	}

	public void navigateToPersonalDetailsPAge() throws Exception
	{
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/update-profile";
		
		try
		{
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				BaseTest.log.info("Click on hamburger menu and click on contact details button");
				BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav//header.properties").click();
				expandMyAccountSection();
				BaseTest.l1.getWebElement("Hamburger_ContactDetails_Link", "Shopnav//header.properties").click();
			}
			else 
			{
				WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
				BaseTest.gVar.waitForElement("Header_Reg_MyAct_Link", "Shopnav//header.properties", 10000);
				BaseTest.log.info("Mouse hover on My Account link in header");
				BaseTest.gVar.mouseHover(myAccountLink);
				//Click on link in my account overlay
				BaseTest.l1.getWebElement("Header_ContactDetails_Link", "Shopnav//header.properties").click();
				
				
			}
			
		}
		catch (Exception e)
		{
			BaseTest.log.info("NAvigating to Contact Details page through URL"); 
			BaseTest.driver.get(cartPageUrl);
		}
	}
	
	public void navigateToFrequentlyOrderedItemsPage() throws Exception
	{
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on Hamburgar menu");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
			expandMyAccountSection();
			BaseTest.log.info("Click on Frequently Ordered link");
			BaseTest.l1.getWebElement("Hamburger_FrequentlyOrdered_Link", "Shopnav\\header.properties").click();
		}
		else 
		{
			WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
//			BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();		
			BaseTest.log.info("Mouse hover on My Account link in header");
			BaseTest.gVar.mouseHover(myAccountLink);
			if (BaseTest.gVar.assertVisible("Header_FrequentlyOrders_Link", "Shopnav//header.properties")) 
			{
				//If My Account Overlay is visible, Click on Frequently Orders link 
				BaseTest.l1.getWebElement("Header_FrequentlyOrders_Link", "Shopnav//header.properties").click();
			} 
			else 
			{
				//Else Click on link present left navigation section in My Account page 
				myAccountLink.click();
				BaseTest.log.info("Click on Frequently Ordered Link in left navigation section");
				BaseTest.l1.getWebElement("AccountOverview_LeftNavFrequentlyOrderedLink", "Profile//AccountOverview.properties").click();
			}
		}
		
	}
	
	public void navigateToShippingAddressPage() throws Exception
	{
		try
		{
			if (BaseTest.gVar.mobileView()||BaseTest.gVar.tabletView())
			{
//				BaseTest.log.info("click on site logo");
//				BaseTest.l1.getWebElement("Header_Mob_Logo", "Shopnav//header.properties").click();
				BaseTest.log.info("click on hamburger menu");
				BaseTest.l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
				expandMyAccountSection();
				BaseTest.log.info("click on Shipping address");
				BaseTest.l1.getWebElement("Hamburger_ShipAddr_Link", "Shopnav//header.properties").click();
			}
			else
			{
				BaseTest.log.info("click on site logo");
				BaseTest.log.info("hover on my account link from header");
//				BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
				BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties").click();
				if (BaseTest.gVar.assertVisible("Header_ShipAddr_Link", "Shopnav//header.properties"))
				{
					BaseTest.log.info("click on shipping address");
					BaseTest.l1.getWebElement("Header_ShipAddr_Link", "Shopnav//header.properties").click();
				}
				else
				{
					BaseTest.log.info("Click on Shipping address link in left nav section");
					BaseTest.l1.getWebElement("AccountOverview_LeftNavShippingAddresses","Profile//AccountOverview.properties").click();
				}
			}
		}
		catch (Exception e)
		{
			String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
			String cartPageUrl = homePagUrl+"my-company/shipping-address";
			BaseTest.driver.get(cartPageUrl);
			
		}
	}
	
	public void navigateToDropShipAddressPage() throws Exception
	{
		
		
		try
		{
			if (BaseTest.gVar.mobileView()||BaseTest.gVar.tabletView())
			{
				BaseTest.log.info("click on hamburger menu");
				BaseTest.l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
				expandMyAccountSection();
				BaseTest.log.info("click on DropShipp address");
				BaseTest.l1.getWebElement("Hamburger_DropShip_Link", "Shopnav//header.properties").click();
			}
			else
			{
				WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
				BaseTest.log.info("Mouse hover on My Account link in header");
				BaseTest.gVar.mouseHover(myAccountLink);
				//If My Account Overlay is visible, Click on Drop Ship link 
				if (BaseTest.gVar.assertVisible("Header_DropShip_Link", "Shopnav//header.properties"))
				{
					BaseTest.log.info("click on shipping address");
					BaseTest.l1.getWebElement("Header_DropShip_Link", "Shopnav//header.properties").click();
				}
				else
				{
					//Else Click on link present left navigation section in My Account page
					myAccountLink.click();
					BaseTest.log.info("Click on DropShip address link in left nav section");
					BaseTest.l1.getWebElement("AccountOverview_LeftNavDropShipAddresses","Profile//AccountOverview.properties").click();
				}
			}
		}
		catch (Exception e)
		{
			String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
			String cartPageUrl = homePagUrl+"my-account/address-book";
			BaseTest.driver.get(cartPageUrl);
		}
	}
	
	
	public void navigateToAccountOverviewPage() throws Exception
	{
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/account-landing";
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on hamburger menu and click on Account Overview link");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav//header.properties").click();
			BaseTest.s.expandMyAccountSection();
			BaseTest.l1.getWebElement("Hamburger_AccountOverview_Link", "Shopnav//header.properties").click();
		}
		else 
		{
			WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
//			BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();		
			BaseTest.log.info("Mouse hover on My Account link in header");
			BaseTest.gVar.mouseHover(myAccountLink);
			if (BaseTest.gVar.assertVisible("Header_AccountOverview_Link", "Shopnav//header.properties")) 
			{
				//If My Account Overlay is visible, Click on Account Overview link 
				BaseTest.l1.getWebElement("Header_AccountOverview_Link", "Shopnav//header.properties").click();
			} 
			else 
			{
				//Else Click on link present left navigation section in My Account page 
				myAccountLink.click();
			}
		}
//		BaseTest.driver.get(cartPageUrl);
	}
	
	public void navigateToAccountPaymentsPage() throws Exception
	{
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/payment-details";
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on hamburger menu and click on Account Payments link");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav//header.properties").click();
			BaseTest.s.expandMyAccountSection();
			BaseTest.l1.getWebElement("Hamburger_AccountPayments_Link", "Shopnav//header.properties").click();
		}
		else 
		{
			WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
//			BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();		
			BaseTest.log.info("Mouse hover on My Account link in header");
			BaseTest.gVar.mouseHover(myAccountLink);
			if (BaseTest.gVar.assertVisible("Header_AccountPayments_Link", "Shopnav//header.properties")) 
			{
				//If My Account Overlay is visible, Click on Account Payments link 
				BaseTest.l1.getWebElement("Header_AccountPayments_Link", "Shopnav//header.properties").click();
			} 
			else 
			{
				//Else Click on link present left navigation section in My Account page 
				myAccountLink.click();
				BaseTest.driver.get(cartPageUrl);
			}
		}
		
	}
	
	
	public void navigateToUsersPage() throws Exception
	{
		Thread.sleep(3000);
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/account-landing";
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on hamburger menu");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav//header.properties").click();
			expandMyAccountSection();
			BaseTest.log.info("Click on User link in Hamburgar menu");
			BaseTest.l1.getWebElement("Hamburger_User_Link", "Shopnav//header.properties").click();
		}
		else
		{
			WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
			BaseTest.log.info("Mouse hover on My Account link in header");
			BaseTest.gVar.mouseHover(myAccountLink);
			//If My Account Overlay is visible, Click on User link 
			if (BaseTest.gVar.assertVisible("Header_User_Link", "Shopnav//header.properties"))
			{
				BaseTest.log.info("click on users link");
				BaseTest.l1.getWebElement("Header_User_Link", "Shopnav//header.properties").click();
			}
			else
			{
				myAccountLink.click();
				BaseTest.log.info("Click on USER link in leftNAv section in MyAccount page");
				BaseTest.l1.getWebElement("AccountOverview_LeftNavUserLink", "Profile//AccountOverview.properties").click();
			}
			
		}
		
//		BaseTest.driver.get(cartPageUrl);
	}
	
	public void navigateToCommunicationPreferencesPage() throws Exception
	{
		String homePagUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String cartPageUrl = homePagUrl+"my-account/communication-preference";
		if (BaseTest.gVar.mobileView() || BaseTest.gVar.tabletView())
		{
			BaseTest.log.info("Click on HAmburgar menu");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav//header.properties").click();
			expandMyAccountSection();
			BaseTest.log.info("Click on Communication Preferences link in Hamburgar menu");
			BaseTest.l1.getWebElement("Hamburger_CommunicationPreferences_Link", "Shopnav//header.properties").click();
		}
		else
		{
			WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
			BaseTest.log.info("Mouse hover on My Account link in header");
			BaseTest.gVar.mouseHover(myAccountLink);
			//If My Account Overlay is visible, Click on Communication Preferences link 
			if (BaseTest.gVar.assertVisible("Header_CommunicationPreferences_Link", "Shopnav//header.properties"))
			{
				BaseTest.log.info("click on users link");
				BaseTest.l1.getWebElement("Header_CommunicationPreferences_Link", "Shopnav//header.properties").click();
			}
			else
			{
				myAccountLink.click();
				BaseTest.log.info("Click on USER link in leftNAv section in MyAccount page");
				BaseTest.l1.getWebElement("AccountOverview_LeftNavCommunicationPreferencesLink", "Profile//AccountOverview.properties").click();
			}
			
		}
		
	}
	
	public void navigateToMyAccountPage() throws Exception
	{
		BaseTest.log.info("Click on My Account link in header");
		if (BaseTest.gVar.mobileView()||BaseTest.gVar.tabletView())
		{
			navigateToAccountOverviewPage();
		}
		else
		{
			BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties").click();
		}
	}
	
	public void removeSavedcarts() throws Exception
	{
		
		BaseTest.log.info("navigate to saved cart");
		navigateToSavedCart();
		String removeCartLink;
		try
		{
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				removeCartLink = "SavedCart_Remove_link_Mobile";
			}
			else
			{
				removeCartLink = "SavedCart_Remove_link";
			}
			List<WebElement> removelinkcount=BaseTest.l1.getWebElements(removeCartLink, "Profile//Savedcart.properties");
			BaseTest.log.info("removelinkcount:- "+ removelinkcount.size());
			for(int i=0; i<=removelinkcount.size(); i++)
			{ 
				BaseTest.log.info("LOOP:- "+i);
				//			   BaseTest.l1.getWebElements("SavedCart_Remove_link", "Profile//Savedcart.properties").get(i).click();
				BaseTest.l1.getWebElement(removeCartLink, "Profile//Savedcart.properties").click();
				
				BaseTest.l1.getWebElement("SavedCart_Delete_btn", "Profile//Savedcart.properties").click();		
				Thread.sleep(5000);
			}
		}
	   
	   catch(Exception e)
	   {
		   BaseTest.log.info("No Saved Carts are present");
	   }
	}
	
   public void removeSingleSavedcart(int numberofremovesavedcart) throws Exception
   {
	   for(int i=0; i<numberofremovesavedcart;i++)
	   {
		   BaseTest.log.info("LOOP:-"+i);
		   try 
		   {
			   BaseTest.l1.getWebElements("SavedCart_Remove_link", "Profile//Savedcart.properties").get(i).click();
			   BaseTest.l1.getWebElement("SavedCart_Delete_btn", "Profile//Savedcart.properties").click();
		   } 
		   catch (Exception e) 
		   {
			   BaseTest.log.info("CATCH BLOCKING EXECUTING No Saved Carts are present");
		   }
		   
	   }
	   
   }

   
	public void savedCartDetails(int row) throws Exception
	{
		BaseTest.log.info("Entering Name field");
		savedCartNameFromExcel = GetData.getDataFromExcel("//data//GenericData_US.xls", "SavedCartdetails", row,0);
		BaseTest.log.info("savedCartNameFromExcel:-"+savedCartNameFromExcel);
		BaseTest.l1.getWebElement("Cart_SaveCart_Name_txtBox", "Cart//Cart.properties").sendKeys(savedCartNameFromExcel);
		BaseTest.log.info("Entering discriptions for saved Carts");
		BaseTest.l1.getWebElement("Cart_SaveCart_Discription_txtbox", "Cart//Cart.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData_US.xls", "SavedCartdetails", row,1));
	}
	
	public void verifyNavToPDP(String pName) throws Exception
	{
		BaseTest.log.info("verify product name");
		BaseTest.sa.assertEquals(BaseTest.gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				pName, "Verify the product name");
		
		BaseTest.sa.assertAll();
	}
	
	public void pagination() throws Exception
	{
		String totProducts=BaseTest.l1.getWebElement("PLP_Tot_Prods", "Shopnav//PLP.properties").getText();	
	}
	
	public void paginationAndItemPerPage(int itemsPerPage) throws Exception
	{
		BaseTest.log.info("fetch total products");
		String totProducts;
		if (BaseTest.gVar.assertVisible("PLP_ContantSlot", "Shopnav//PLP.properties"))
		{
			//For PLP
			totProducts=BaseTest.l1.getWebElement("PLP_Tot_Prods", "Shopnav//PLP.properties").getText();	
			totProducts=totProducts.replaceAll("[^0-9]", "");
		}
		else
		{
			//For Search result page
			totProducts=BaseTest.l1.getWebElement("Search_ProductsCount", "Shopnav//SearchResultPage.properties").getText();
			int lastSpace = totProducts.lastIndexOf(" ");
			totProducts = totProducts.substring(totProducts.indexOf(lastSpace), totProducts.length()).trim();
		}

		BaseTest.log.info("totProducts:- "+totProducts);
		int totProdsInInteger=new Integer(totProducts);
		BaseTest.log.info("calculate left out products at last page");
		leftOutProductsInLastPage=totProdsInInteger%itemsPerPage;
		BaseTest.log.info("leftOutProductsInLastPage:- "+ leftOutProductsInLastPage);
		totPages=(totProdsInInteger/itemsPerPage)+1;
		BaseTest.log.info("totPages:- "+ totPages);

		if(BaseTest.gVar.assertVisible("PLP_Pagination", "Shopnav//PLP.properties")) 
		{

			BaseTest.log.info("verify next link");
			for(int j=0;j<totPages;j++)
			{
				if(j==0) 
				{
					BaseTest.log.info("verify products in first page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
					BaseTest.log.info("Previous link should be disabled");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Prev_disable", "Shopnav//PLP.properties"));

				} 
				else if(j==totPages-1)
				{
					BaseTest.log.info("click on next link");
					BaseTest.l1.getWebElement("PLP_Pagination_Next", "Shopnav//PLP.properties").click();
					BaseTest.log.info("verify products in last page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), leftOutProductsInLastPage);
					BaseTest.log.info("next link should be disabled");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next_disable", "Shopnav//PLP.properties"));

				}
				else 
				{
					BaseTest.log.info("click on next link");
					BaseTest.l1.getWebElement("PLP_Pagination_Next", "Shopnav//PLP.properties").click();
					BaseTest.log.info("verify products in page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
					BaseTest.log.info("both next and previous links should display");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next", "Shopnav//PLP.properties"));
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Previous", "Shopnav//PLP.properties"));
				}
			}

			BaseTest.log.info("verify previous link");
			for(int j=0;j<totPages;j++)
			{
				if(j==0) 
				{
					BaseTest.log.info("verify products in last page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), leftOutProductsInLastPage);	
					BaseTest.log.info("next link should be disabled");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next_disable", "Shopnav//PLP.properties"));

				} 
				else if(j==totPages-1)
				{
					BaseTest.log.info("click on next link");
					BaseTest.l1.getWebElement("PLP_Pagination_Previous", "Shopnav//PLP.properties").click();
					BaseTest.log.info("verify products in last page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
					BaseTest.log.info("Previous link should be disabled");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Prev_disable", "Shopnav//PLP.properties"));

				} 
				else 
				{
					BaseTest.log.info("click on previous link");
					BaseTest.l1.getWebElement("PLP_Pagination_Previous", "Shopnav//PLP.properties").click();
					BaseTest.log.info("verify products in page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
					BaseTest.log.info("both next and previous links should display");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next", "Shopnav//PLP.properties"));
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Previous", "Shopnav//PLP.properties"));
				}
			}

			BaseTest.log.info("verify page number links");
			for(int j=1;j<=totPages;j++)
			{
				if(j==1) 
				{
					BaseTest.log.info("verify products in last page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);	
					BaseTest.log.info("Previous link should be disabled");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Prev_disable", "Shopnav//PLP.properties"));

				} 
				else if(j==totPages)
				{
					BaseTest.log.info("click on next link");
					BaseTest.driver.findElement(By.xpath("//ul[@class='pagination']/descendant::a[contains(text(),'"+j+"')]")).click();
					BaseTest.log.info("verify products in last page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), leftOutProductsInLastPage);
					BaseTest.log.info("next link should be disabled");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next_disable", "Shopnav//PLP.properties"));

				} 
				else 
				{
					BaseTest.log.info("click on previous link");
					BaseTest.driver.findElement(By.xpath("//ul[@class='pagination']/descendant::a[contains(text(),'"+j+"')]")).click();;
					BaseTest.log.info("verify products in page");
					BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
					BaseTest.log.info("both next and previous links should display");
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next", "Shopnav//PLP.properties"));
					BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Previous", "Shopnav//PLP.properties"));
				}
			}

		}
		else 
		{

			BaseTest.log.info("verify products in one page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), totProducts);
		}

		BaseTest.log.info("validate all asserts");
		BaseTest.sa.assertAll();

	}
	
	public void pdpNavigationThroughURL(String productID) throws Exception
	{
		//First navigate to Any PDP
//		String productId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
//		######### Below Code is taking time so commented. alternate option is upend product id with site URL
//		navigateToPDP_Search(searchKeyWord);
//		//COllect the URL
//		String currentUrl = BaseTest.driver.getCurrentUrl();
//		BaseTest.log.info("currentUrl:- "+ currentUrl);
//		String temp = currentUrl;
//		String id = currentUrl.substring(currentUrl.lastIndexOf("/")+1, currentUrl.length());
//		BaseTest.log.info("id:- "+ id);
//		
//		String updatedURL = temp.replace(id, productID);
//		BaseTest.log.info("updatedURL:- "+ updatedURL);
		
		String siteUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String constructedUrl = siteUrl+"p/"+productID;
		BaseTest.driver.get(constructedUrl);
	}
	
	
	public void navigateToHomePage() throws Exception
	{
//		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
//		{
//			BaseTest.log.info("Mobile View");
//			BaseTest.l1.getWebElement("Header_Mob_Logo", "Shopnav//header.properties").click();
//		} 
//		else 
//		{
//			BaseTest.log.info("Desktop view");
//			BaseTest.l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
//		}
		BaseTest.gVar.navigateToSite(BaseTest.xmlTest);
	}
	
	public void homecareProvidersPage() throws Exception
	{
		String siteUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
		String constructedUrl = siteUrl+"homecareProvidersPage";
		BaseTest.driver.get(constructedUrl);
	}
	
	
}
