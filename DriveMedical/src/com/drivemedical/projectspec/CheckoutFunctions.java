package com.drivemedical.projectspec;

import java.util.List;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CheckoutFunctions {
	
	public static String PONumberVal;

	
	public void navigateToPaymentTypePage() throws Exception
	{
		BaseTest.log.info("navigate to Payment type page");
		navigateToShippingMethodPage();
		Thread.sleep(4000);
		BaseTest.log.info("Select the radio button in SHIPPING METHODS section");
		List<WebElement> shippingMethodRadioButton = BaseTest.l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		int shippingMethod = 0;
		shippingMethodRadioButton.get(shippingMethod).click();
		BaseTest.log.info("click on next button");
		BaseTest.l1.getWebElement("ShippingMtd_Next_Btn","Checkout//ShippingMethod.properties").click();
	}
	
	public void navigateToShipToPage() throws Exception
	{
		BaseTest.log.info("add item to cart");
		String excelDAta = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		BaseTest.log.info("excelDAta:- "+ excelDAta);
		BaseTest.s.navigateToCart(excelDAta, 1, 1);
		
		BaseTest.log.info("click on proceed to check out button");
		BaseTest.l1.getWebElement("Cart_CheckoutButton","Cart//Cart.properties").click();
		Thread.sleep(2000);
	}
	
	public void navigateToShipToPage(String data, int numOfItems, int qty) throws Exception
	{
		BaseTest.log.info("add item to cart");
		BaseTest.s.navigateToCart(data, numOfItems, qty);
		
		BaseTest.log.info("click on proceed to check out button");
		if (BaseTest.gVar.ieBrowser())
		{
			Thread.sleep(2000);
			JavascriptExecutor exe=(JavascriptExecutor)BaseTest.driver;
			exe.executeScript("arguments[0].click();", BaseTest.l1.getWebElement("Cart_CheckoutButton","Cart//Cart.properties"));
		}
		else 
		{
			BaseTest.l1.getWebElement("Cart_CheckoutButton","Cart//Cart.properties").click();
		}
		
		Thread.sleep(2000);
	}
	
	public void navigateToShippingMethodPage() throws Exception
	{
		BaseTest.log.info("navigate to Ship To page");
		navigateToShipToPage();
		Thread.sleep(4000);
		BaseTest.log.info("Select first saved address");
		BaseTest.l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		BaseTest.log.info("Click on CONTINUE button in SHipTo Section");
		BaseTest.gVar.waitForElement("ShipTo_Continue_button", "Checkout//ShipTo.properties", 8000);
//		BaseTest.l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
		WebElement radioButtonElement = BaseTest.l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties");
		JavascriptExecutor exe=(JavascriptExecutor)BaseTest.driver;
		exe.executeScript("arguments[0].click();", radioButtonElement);
		
	}
	
	public void navigateToShippingMethodPage(String product, int numOfItems, int qty) throws Exception
	{
		BaseTest.log.info("navigate to Ship To page");
		navigateToShipToPage(product, numOfItems, qty);
		Thread.sleep(4000);
		BaseTest.log.info("Select first saved address");
//		BaseTest.l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		BaseTest.l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties").get(2).click();
		BaseTest.log.info("Click on CONTINUE button in SHipTo Section");
		BaseTest.l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
	}
	
	public void navigateToPaymentTypePage(String product, int numOfItems, int qty) throws Exception
	{
		BaseTest.log.info("navigate to Payment type page");
		navigateToShippingMethodPage(product, numOfItems, qty);
		Thread.sleep(4000);
		BaseTest.log.info("Select the radio button in SHIPPING METHODS section");
		List<WebElement> shippingMethodRadioButton = BaseTest.l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		int shippingMethod = 0;
		Thread.sleep(5000);
//		shippingMethodRadioButton.get(shippingMethod).click();
		WebElement radioButtonElement = shippingMethodRadioButton.get(shippingMethod);
		JavascriptExecutor exe=(JavascriptExecutor)BaseTest.driver;
		exe.executeScript("arguments[0].click();", radioButtonElement);
		BaseTest.gVar.waitForElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties", 10000);
		BaseTest.log.info("click on CONTINUE button in Shipping Method section");
		BaseTest.l1.getWebElement("ShippingMtd_Next_Btn","Checkout//ShippingMethod.properties").click();
	}
	
	
	public void navigateToOrderReviewPage() throws Exception
	{
		BaseTest.log.info("navigate to Order Review page");
		navigateToPaymentTypePage();
		BaseTest.log.info("enter PO boc number");
		int randomNum = BaseTest.gVar.generateRandomNum();
		PONumberVal =Integer.toString(randomNum);
		System.out.println("Random PO NUMBER:-  "+ PONumberVal);
//		PONumberVal=GetData.getDataFromProperties("//POM//Checkout//PaymentType.properties", "POBoxNumber");
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		BaseTest.log.info("click on next button");
		BaseTest.l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
	}
	
	public void navigateToOrderReviewPage(String product, int numOfItems, int qty) throws Exception
	{
		BaseTest.log.info("navigate to Order Review page");
		navigateToPaymentTypePage(product, numOfItems, qty);
		BaseTest.log.info("enter PO boc number");
		int randomNum = BaseTest.gVar.generateRandomNum();
		PONumberVal =Integer.toString(randomNum);
		System.out.println("Random PO NUMBER:-  "+ PONumberVal);
//		PONumberVal=GetData.getDataFromProperties("//POM//Checkout//PaymentType.properties", "POBoxNumber");
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		BaseTest.log.info("click on next button");
		BaseTest.l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
	}
	
	public void addAddress(int rowNum) throws Exception
	{
		BaseTest.log.info("enter name");
		String fNmae = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 1);
		if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(1500);
		}
		BaseTest.l1.getWebElement("ShipTo_Name_Textbox","Checkout//ShipTo.properties").sendKeys(fNmae);
		BaseTest.log.info("enter address 1");
		String address1 = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 2);
		if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(1500);
		}
		BaseTest.l1.getWebElement("ShipTo_Addr1_Textbox","Checkout//ShipTo.properties").sendKeys(address1);
		BaseTest.log.info("enter city");
		String city = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 4);
		if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(1500);
		}
		BaseTest.l1.getWebElement("ShipTo_city_Textbox","Checkout//ShipTo.properties").sendKeys(city);
		BaseTest.log.info("enter state");
		String state = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 5);
		if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(1500);
		}
		new Select(BaseTest.l1.getWebElement("ShipTo_State_Textbox","Checkout//ShipTo.properties")).selectByValue(state);
		BaseTest.log.info("enter zip code");
		String zipCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 6);
		if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(1500);
		}
		BaseTest.l1.getWebElement("ShipTo_Zip_Textbox","Checkout//ShipTo.properties").sendKeys(zipCode);
		BaseTest.log.info("enter phone");
		String phNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum,7);
		if(BaseTest.xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(1500);
		}
		BaseTest.l1.getWebElement("ShipTo_Phone_Textbox","Checkout//ShipTo.properties").sendKeys(phNum);
	}
	
	public void navigateToCartFromCheckoutPage() throws Exception
	{
		if (BaseTest.gVar.mobileView()||BaseTest.gVar.tabletView())
		{
			BaseTest.l1.getWebElement("Checkout_ReturnToCart_Link_Mobile", "Checkout//Checkout.properties").click();
		}
		else
		{
			BaseTest.l1.getWebElement("Checkout_ReturnToCart_Link", "Checkout//Checkout.properties").click();
		}
		
	}
	
	
	
}
