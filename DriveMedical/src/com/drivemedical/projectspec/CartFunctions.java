package com.drivemedical.projectspec;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.drivemedical.utilgeneric.BaseTest;



public class CartFunctions 
{

	public void clearCart() throws Exception
	{
		BaseTest.log.info("Navigate to CArt page");
		BaseTest.s.navigateToCartPage();
		Thread.sleep(3000);
		
		BaseTest.log.info("Count the number of rows");
		try 
		{
			List<WebElement> dotButtons = BaseTest.l1.getWebElements("Cart_DotButtons", "Cart//Cart.properties");
			System.out.println("Number of rows/dot buttons count in cart page:-  " + dotButtons.size());	
			int cnt=0;
			if (dotButtons.size()!=0)
			{
				while (BaseTest.gVar.assertVisible("Cart_DotButtons", "Cart//Cart.properties"))
				{
					BaseTest.log.info("LOOP:- "+cnt);
					System.out.println("Click Close icon button in cart page");
					BaseTest.l1.getWebElement("Cart_DotButtons", "Cart//Cart.properties").click();
					Thread.sleep(5000);
					BaseTest.driver.navigate().refresh();
					cnt++;
				}
			}
		} 
		catch (Exception e) 
		{
			BaseTest.sa.assertTrue(false,"Clear cart function is not completed");
		}
	}
}
