package com.drivemedical.projectspec;

import net.sourceforge.htmlunit.corejs.javascript.ast.CatchClause;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.Locators;

public class ProfileFunctions 
{
	public void navigateToLoginPage() throws Exception
	{
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on Hamburger menu");
			BaseTest.s.openHamburgarMenu();
			Thread.sleep(2000);
			
			BaseTest.log.info("Click on sign in link");
			WebElement signInLinkElement = BaseTest.l1.getWebElement("Hamburger_SignIn_Link","Shopnav\\header.properties");
			WebDriverWait wait=new WebDriverWait(BaseTest.driver, 8);
			wait.until(ExpectedConditions.elementToBeClickable(signInLinkElement));
//			signInLinkElement.click();
			signInLinkElement.sendKeys(Keys.ENTER);
			if (BaseTest.gVar.assertNotVisible("Login_Heading", "Profile//login.properties"))
			{
				String loginUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
				loginUrl = loginUrl+"login";
				BaseTest.driver.get(loginUrl);
			}
		}
		else
		{
			BaseTest.log.info("Desktop View");
			BaseTest.l1.getWebElement("Header_SignIn_LINK","Shopnav\\header.properties").click();
		}
	}
	
	public void logIn(XmlTest xmlTest) throws Exception
	{
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie")) 
		{
			Thread.sleep(2000);
		}
		String email = xmlTest.getParameter("email");
		BaseTest.l1.getWebElement("Login_Email_TextBox", "Profile//login.properties").sendKeys(email);
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(2000);
		}
		
		String pwd = xmlTest.getParameter("password");
		BaseTest.l1.getWebElement("Login_Password_Box", "Profile//login.properties").sendKeys(pwd);
		
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(3000);
		}
		WebElement loginButtonElement = BaseTest.l1.getWebElement("Login_Login_Btn", "Profile//login.properties");
		WebDriverWait wait = new WebDriverWait(BaseTest.driver, 8);
		wait.until(ExpectedConditions.elementToBeClickable(loginButtonElement));
//		loginButtonElement.click();
		loginButtonElement.sendKeys(Keys.ENTER);
		Thread.sleep(3000);
//		BaseTest.driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD");
		
	}
	
	public void logInWithCredentials(String un, String pwd) throws Exception
	{
		Thread.sleep(3000);
		BaseTest.l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys(un);
		BaseTest.l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys(pwd);
		BaseTest.l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
	}
	
	public void logout(XmlTest xmlTest) throws Exception
	{
		Thread.sleep(5000);
		try 
		{
			if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
			{
				BaseTest.log.info("LOGOUT MOBILE VIEW");
				BaseTest.log.info("Click on HAMBURGAR");
				BaseTest.l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
				Thread.sleep(2000);
				BaseTest.log.info("Verify the signout link");
				if (BaseTest.gVar.assertVisible("Hamburger_MyAccount_ArrowDown", "Shopnav//header.properties"))
				{
					BaseTest.l1.getWebElement("Hamburger_MyAccount_ArrowDown", "Shopnav//header.properties").click();
					Thread.sleep(1000);
				}
				if (BaseTest.gVar.assertVisible("Hamburger_SignOut_Link", "Shopnav//header.properties")) 
				{
					BaseTest.log.info("Click on logout link");
					BaseTest.l1.getWebElement("Hamburger_SignOut_Link", "Shopnav//header.properties").click();
				}
				else 
				{
					BaseTest.log.info("USER IS NOT SIGNED IN");
					BaseTest.log.info("Click on Close link in Hamburger");
					BaseTest.l1.getWebElement("Hamburger_Close_Button", "Shopnav//header.properties").click();
				}
				
				
			} 
			else 
			{
				BaseTest.log.info("LOGOUT DESKTOP VIEW");
				if (BaseTest.gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav//header.properties")) 
				{
					BaseTest.log.info("My Account link is visible in Header");
					WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
					BaseTest.log.info("MOuse hover on MyAccount link in Header");	
					BaseTest.gVar.mouseHover(myAccountLink);
					
					BaseTest.log.info("Click on Signout link");
					if (BaseTest.gVar.assertVisible("Header_SignOut_Link", "Shopnav//header.properties"))
					{
						//If My ACcount overlay is visible then Click on SIGNOUT link in My ACcount overlay. 
						BaseTest.l1.getWebElement("Header_SignOut_Link", "Shopnav//header.properties").click();
					}
					else 
					{
						//Navigate to My Account page
						myAccountLink.click();
						//Click on SIGNOUT link in My Account page
						BaseTest.l1.getWebElement("AccountOverview_SignoutLink", "Profile//AccountOverview.properties").click();
						BaseTest.log.info("Signout link is clicked in ACCOUNT OVER PAGE");
					}
					BaseTest.log.info("Logout Done");
				}
			}
		} 
		catch(Exception e) 
		{
			BaseTest.log.info("It's a guest user");
		}
		
	}
	
	
}
