package com.drivemedical.utilgeneric;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;


public class Test{
	
	/*String name;
	WebDriver driver;*/
	
/*	@org.testng.annotations.Test
	public void ExtentTest_Test_1() throws Exception
	{
		
		
		l1.getWebElement("signIn", "login.properties").click();
		SoftAssert sa=new SoftAssert();
		sa.assertTrue(l1.getWebElement("loginReturnHeading", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("loginText", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("emailLabel", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("emailTextBox", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("pwdLabel", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("pwdTextBox", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("submitBtn", "login.properties").isDisplayed());
		sa.assertAll();
		DesiredCapabilities des=DesiredCapabilities.firefox();
		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		des.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");	
		WebDriver driver=new FirefoxDriver(des);
		
		driver.get("https://stage.medicaldepot.com:9002/drivestorefront/?site=driveUS");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//div[contains(@class,'nav-search')]/descendant::input[@id='js-site-search-input']")).sendKeys("50009000",Keys.ENTER);
		Thread.sleep(10000);
		driver.findElements(By.className("name")).get(0).click();
		Thread.sleep(5000);
		Select sel=new Select(driver.findElement(By.xpath("//select[@id='priority2']")));
		sel.selectByIndex(1);
		
	}
	@org.testng.annotations.Test
	public void ExtentTest() throws Exception
	{
		l1.getWebElement("forGotPwdLink", "login.properties").click();
		Set<String> s=driver.getWindowHandles();
		Iterator i1=s.iterator();
		String childWin=(String) i1.next();
		driver.switchTo().window(childWin);
		Assert.assertTrue(l1.getWebElement("forgotPwdHeading", "login.properties").isDisplayed());
		l1.getWebElement("closeBtn", "login.properties").click();
		System.out.println("closed fgt");
		//navigate back to base window
		driver.switchTo().defaultContent();
		Assert.assertTrue(l1.getWebElement("loginReturnHeading", "login.properties").isDisplayed());
		//System.out.println("hello");
	}*/
	
/*	@org.testng.annotations.Test
	public void test3() throws Exception
	{
		Properties p=new Properties();
		p.load(new FileInputStream(new File(System.getProperty("user.dir")+"//data//sitedata.properties")));
		Iterator<Object> i=p.keySet().iterator();
		Enumeration e=p.propertyNames();
		
		while (e.hasMoreElements()) {
			String str1=(String)e.nextElement();
			if(p.getProperty(str1).equalsIgnoreCase("adi")) {
				name=str1;
				System.out.println(name);
			}
		}
		
	Properties p1=new Properties();
	p1.load(new FileInputStream(new File(System.getProperty("user.dir")+"//data//Validations.properties")));
	System.out.println(p1.getProperty(name));
	
	}*/
	
/*	@org.testng.annotations.Test
	public void app() throws Exception
	{
		DesiredCapabilities des=DesiredCapabilities.chrome();
		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
		//des.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR,ElementScrollBehavior.BOTTOM);
		des.setPlatform(Platform.VISTA);
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		driver=new ChromeDriver(des);
		driver.get("https://med-d-www.ms.ycs.io/drivestorefront/driveUS/en/USD/Products/Mobility/Wheelchairs/Cruiser-III-Wheelchair/p/1078");
		Thread.sleep(5000);
		driver.findElement(By.id("pdpAddtoCartInput")).clear();
		driver.findElement(By.id("pdpAddtoCartInput")).sendKeys(12+"");
		System.out.println("select canada francis");
		driver.findElement(By.xpath("//div[@class='current-country']/span")).click();
		driver.findElement(By.xpath("//a[@data-locale='fr_CA']")).click();
		Thread.sleep(3000);
		//if(driver.findElement(By.xpath("//div[@class='current-country']/span")).getText().contains("FRAN�AIS")) {
		System.out.println("fetching by");
			By by=By.xpath("//a[contains(text(),'My Account')]");
			String byref=by.toString();
			int start=byref.indexOf("'")+1;
			int end=byref.indexOf("')");
			System.out.println("fetching string");
			String replacedString=byref.substring(start,end);
			System.out.println(replacedString);
			System.out.println("fetching string of other language");
			String testingInPRogress=Test2.multiLanguage("C:\\Users\\areddy2\\Desktop\\Resources1\\Resources\\SK\\account_en_CA.properties", "C:\\Users\\areddy2\\Desktop\\Resources1\\Resources\\SK\\account_fr_CA.properties", replacedString);
			System.out.println(testingInPRogress);
			byref=byref.replace(replacedString, testingInPRogress).replace("By.xpath: ", "");
			System.out.println(byref);
			Thread.sleep(3000);
			driver.findElement(By.xpath(byref)).click();
		} else {
		System.out.println(driver.findElement(By.xpath("//h1[contains(text(),'My Account Log in')]")).isDisplayed());
		}
		test();
		
	}*/
	@org.testng.annotations.Test
	public void test00()
	{
	//	System.out.println((char)92+""+'A');
/*		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 90); 
        System.out.println(cal.getTime());*/
	//Assert.assertTrue(true);
	
		DesiredCapabilities des=DesiredCapabilities.chrome();
		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
		//des.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR,ElementScrollBehavior.BOTTOM);
		des.setPlatform(Platform.VISTA);
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(des);
	}
	
/*	@org.testng.annotations.Test(dependsOnMethods="test00")
	public void test01()
	{
	//	System.out.println((char)92+""+'A');
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 90); 
        System.out.println(cal.getTime());
	System.out.println("test00 is passed");
	
		
	}*/
	
}
