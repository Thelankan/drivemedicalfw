package com.drivemedical.cart;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;

public class CartGuest extends BaseTest {

	@Test(groups={"reg"},description="OOTB_096_1_2")
	public void TC00_startShop_Test() throws Exception
	{
		log.info("Navigate to empty cart page");
		l1.getWebElement("Header_Cart_Link", "Shopnav\\header.properties").click();
		log.info("verify empty cart message");
		sa.assertTrue(l1.getWebElement("Cart_EmptyCart_Msg", "Cart\\Cart.properties").isDisplayed());
		log.info("Click on Start Shopping button");
		l1.getWebElement("Cart_Start_Shopping_btn", "Cart\\Cart.properties").click();
		log.info("verify navigation back to home page");
		sa.assertTrue(l1.getWebElement("Home_Identifier", "Shopnav\\header.properties").isDisplayed());
		sa.assertAll();
	}

	
}
