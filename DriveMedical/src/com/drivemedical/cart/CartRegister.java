package com.drivemedical.cart;

import java.security.Key;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.ShopnavFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CartRegister extends BaseTest 
{

	String proColorInPDP;
	String proNameINPDP;
	String PDPProperties = "Shopnav//PDP.properties";
	String cartProperties = "Cart//Cart.properties";
	
	String configuredProName;
	String configuredProPrice;
	String similarProductName;
	String similarProductPrice;
	
	static String numberOfSimilarItems;

	@Test(groups={"reg"}, description="OOTB-097_DRM-642,643,644")
	public void TC00_verify_Previous_Prods_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		cart.clearCart();
		
		log.info("add item to cart");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.navigateToCart(searchData,2, 1);
		
		log.info("verify cart navigation");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Cart Heading");
		s.navigateToHomePage();
		log.info("log out from application");
		p.logout(xmlTest);
		log.info("log in to the application again");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("verify cart quantity");
		if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			sa.assertEquals(gVar.assertEqual("Header_Cart_Qty_Mobile", "Shopnav//header.properties"),"2", "Verify the mini cart quantity in header");	
		}
		else
		{
			sa.assertEquals(gVar.assertEqual("Header_Cart_Qty", "Shopnav//header.properties"),"2", "Verify the quantity");
		}
		
		log.info("Navigate to cart page");
		s.navigateToCartPage();
		log.info("verify products in cart page");
		sa.assertEquals(l1.getWebElements("Cart_Line_Items", "Cart//Cart.properties").size(),2,"Verify the line Items");
		for(int i=0;i<2;i++)
		{
			sa.assertEquals(s.prodnames.get(i),l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties").get(i).getText());
		}
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-712 DRM-713 DRM-714")
	public void TC01_uiOfCartPage(XmlTest xmlTest) throws Exception
	{
		
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.pdpNavigationThroughURL(searchData);
		
		proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("proNameINPDP:-   "+ proNameINPDP);
		
		String proPriceInPDP = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		log.info("proPriceInPDP:-  "+ proPriceInPDP);
		
		String proQty = l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").getAttribute("value");
		log.info("Product quantity in PDP:- "+proQty);
		
//		String proColorInPDP;
		if (gVar.assertVisible("PDP_ColorSwatchesList", "Shopnav//PDP.properties")) 
		{
			proColorInPDP = l1.getWebElement("PDP_SelectedColorSwatchImg", "Shopnav//PDP.properties").getAttribute("title");
			log.info("Product color in PDP:-  "+proColorInPDP);
		}
		log.info("Cick on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		Thread.sleep(3000);
		log.info("Navigate to CART page");
		s.navigateToCartPage();
		
		log.info("Verify the CART page ");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"), "Verify the Cart Heading");
		sa.assertEquals(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), "Cart");
		sa.assertEquals(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), "Home Cart");
		sa.assertTrue(gVar.assertVisible("Cart_Banner", "Cart//Cart.properties"), "Cart page Banner");
		
		sa.assertEquals(gVar.assertEqual("Cart_ExportCSV_Button", "Cart//Cart.properties"), "Export CSV");
		sa.assertEquals(gVar.assertEqual("Cart_SaveCart_SavedCart_txt", "Cart//Cart.properties"), "Save Cart");
		
		log.info("Verify the CArt item headings");
		sa.assertTrue(gVar.assertVisible("Cart_ItemLIst_Header", "Cart//Cart.properties"), "Cart page item headings");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderItem", "Cart//Cart.properties"), "Item");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderPrice", "Cart//Cart.properties"), "Price");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderQuantity", "Cart//Cart.properties"), "Qty");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderTotal", "Cart//Cart.properties"), "Unit");
		
		log.info("Verify the product details in cart page");
		sa.assertEquals(gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"), proNameINPDP);
		sa.assertEquals(gVar.assertEqual("Cart_Prod_ID", "Cart//Cart.properties"), proColorInPDP);
		sa.assertEquals(gVar.assertEqual("Cart_ItemQuantity", "Cart//Cart.properties"), proQty);
		sa.assertEquals(gVar.assertEqual("Cart_ItemPrice", "Cart//Cart.properties"), proPriceInPDP);
		sa.assertEquals(gVar.assertEqual("Cart_ItemTotal", "Cart//Cart.properties"), proPriceInPDP);
		sa.assertTrue(gVar.assertVisible("Cart_DotButtons", "Cart//Cart.properties"), "remove item button");
		
		log.info("Verify the Promo section in cart page");
		sa.assertTrue(gVar.assertVisible("Cart_PromoSection", "Cart//Cart.properties"), "Promo section");
		sa.assertTrue(gVar.assertVisible("Cart_PromoSection", "Cart//Cart.properties"), "Promo section");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_link", "Cart//Cart.properties", "aria-expanded"), "true");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_Label", "Cart//Cart.properties"), "Promo code");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_Textbox", "Cart//Cart.properties", "placeholder"), "Enter promo code");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_ApplyButton", "Cart//Cart.properties"), "Apply");
		
		sa.assertEquals(gVar.assertEqual("Cart_Subtotal_label", "Cart//Cart.properties"), "Subtotal:");
		sa.assertEquals(gVar.assertEqual("Cart_OrderTotal_label", "Cart//Cart.properties"), "Order Total");
		sa.assertEquals(gVar.assertEqual("Cart_Subtotal_Amount", "Cart//Cart.properties"), proPriceInPDP);
		sa.assertEquals(gVar.assertEqual("Cart_OrderTotal_Amount", "Cart//Cart.properties"), proPriceInPDP);
		
		log.info("Verify the Checkout button in cart page");
		sa.assertEquals(gVar.assertEqual("Cart_CheckoutButton", "Cart//Cart.properties"), " Proceed to CheckOut");
		
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-719")
	public void TC02_cartBannerImage()
	{
		log.info("Verify the banner image in cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Banner_Image", "Cart//Cart.properties"),"Verify the banner image in Cart page");
		
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-724")
	public void TC03_verify_Product_Availability_MessageIn_Cart()
	{
		log.info("Verify the in stock message in cart page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Cart_ProductAvailabilityMsg", "Cart//Cart.properties"),"In Stock", "Verify the in stock message in cart page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-712")
	public void TC04_clickOnProductImageInCartPAge(XmlTest xmlTest) throws Exception
	{
		Thread.sleep(5000);
		log.info("Click on Product image");
		l1.getWebElement("Cart_LineItem_ProdImg", "Cart//Cart.properties").click();
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"));
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), proNameINPDP);
		
		//###############################
		log.info("Navigate to Cart page");
		s.navigateToCartPage();
		log.info("Verify the Cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		
		log.info("Click on Product name");
		l1.getWebElement("Cart_Prod_Name", "Cart//Cart.properties").click();
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"));
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), proNameINPDP);

	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-725 DRM-726")
	public void TC05_saveCartFunctionality(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to cart page");
		s.navigateToCartPage();
		String cartId = l1.getWebElement("Cart_ID", "Cart//Cart.properties").getText();
		log.info("Cart Id:-  " + cartId);
		log.info("Click on SAVE CART button");
//		l1.getWebElement("Cart_SaveCart_SavedCart_txt", "Cart//Cart.properties").click();
	    log.info("Click on Save Cart Button");
	    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
	    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
		}
		else
		{
			l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
		}
		
		log.info("Verify the Save cart popup");
		sa.assertTrue(gVar.assertVisible("Cart_SavedCart_Popup_text", "Cart//Cart.properties"),"Verify the save cart pop headline");
		
		log.info("Enter valid details in popup");
		String saveCardName = "TestSaveCard";
		String saveCardDisc = "TestSaveCard Discription";
		l1.getWebElement("Cart_SaveCart_Name_txtBox", "Cart//Cart.properties").sendKeys(saveCardName);
		l1.getWebElement("Cart_SaveCart_Discription_txtbox", "Cart//Cart.properties").sendKeys(saveCardDisc);
		l1.getWebElement("Cart_SaveCart_Save_btn", "Cart//Cart.properties").click();
		
		log.info("Verify the Successfull message");
		sa.assertTrue(gVar.assertVisible("Cart_SaveCart_SuccessMsg", "Cart//Cart.properties"),"Verify the success message");
		String exptMsg = "Cart"+" "+saveCardName+" "+"was successfully saved";
		String temp = gVar.assertEqual("Cart_SaveCart_SuccessMsg_CloseLink", "Cart//Cart.properties");
		log.info("temp:- "+ temp);
		String actualSuccessMsg = gVar.assertEqual("Cart_SaveCart_SuccessMsg", "Cart//Cart.properties").replace(temp, "").trim();
		log.info("actualSuccessMsg:- "+ actualSuccessMsg);
		sa.assertEquals(actualSuccessMsg, exptMsg,"Verify the success message Text");
		
		log.info("Verify the Empty cart page");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"),"Verify Empty Cart");
		
		log.info("Verify the number of saved carts link in Cart page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Cart_SaveCart_SavedCart_txt", "Cart//Cart.properties"), "You Have 1 Saved Carts","Verify the saved cart link in cart page");
		
		log.info("Saved cart Link verification in cart");
		sa.assertTrue(gVar.assertVisible("Cart_SaveCart_SavedCart_txt", "Cart\\Cart.properties"),"Verify the SAVED CART LINK");
		
		log.info("Verify the EMPTY cart page");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart\\Cart.properties"),"Verify the EMPTY cart message");
		
		log.info("Navigate to SAVE CART page");
		s.navigateToSavedCart();
		
		log.info("Verify the product in Saved cart page");
		sa.assertEquals(gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"), saveCardName);
		sa.assertEquals(gVar.assertEqual("SavedCart_CartID", "Profile//Savedcart.properties"), cartId);
		sa.assertEquals(gVar.assertEqual("SavedCart_Product_Description", "Profile//Savedcart.properties"), saveCardDisc);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-729")
	public void TC06_updateProductQuantity() throws Exception
	{
		log.info("Navigate to Cart page");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.navigateToCart(searchData, 1, 1);
		String proPriceInPDP = s.prodPrice.get(0);
		log.info("Product price in PDP:-  "+proPriceInPDP);

		Double proPrice = new Double(proPriceInPDP.replace("$", ""));
		log.info("Product price double type:-  "+ proPrice);
		
		log.info("Update cart quantity");
		int qty = 10;
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").clear();
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(Integer.toString(qty));
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(Keys.TAB);	//Remove focus from the quantity box
		
		Thread.sleep(10000);
		
		log.info("Verify the updated quantity");
		sa.assertEquals(l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").getAttribute("value"), qty+"");
		
		double exptTotPrice = proPrice*qty;
		log.info("Total price :- "+exptTotPrice);
		DecimalFormat df = new DecimalFormat("##.00");
		
		log.info("Verify the updated price in cart page");
		String actualTotPrice = l1.getWebElement("Cart_ItemTotal", "Cart//Cart.properties").getText().replace("$", "").replaceAll(",", "");
		System.err.println("Actual Total price:-  " + actualTotPrice);
		sa.assertEquals(actualTotPrice, df.format(exptTotPrice), "Verify the total line item price");
		
		sa.assertAll();
	}
	
	@Test(groups ={"reg"}, description="OOTB-096 DRM-715")
	public void TC07_continueShoppingFunctionality() throws Exception
	{
		log.info("NAvigate to home page");
		s.navigateToHomePage();
		
		log.info("Navigate to CArt page");
		s.navigateToCartPage();
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		log.info("Click on CONTINUE SHOPPING link in cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link", "Cart//Cart.properties").click();
		
		log.info("Verify the home page");
		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav//header.properties"),"Verify the HOME page");
		
		//####################
		log.info("NAvigate to PLP");
		String normalProduct = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.searchProduct(normalProduct);
		
//		String plpHeader = l1.getWebElement("PLP_Heading", "Shopnav//PLP.properties").getText();
		String plpHeader = gVar.assertEqual("PLP_Heading", "Shopnav//PLP.properties");
		log.info("plpHeader:- "+ plpHeader);
		log.info("Navigate to CArt page");
		s.navigateToCartPage();
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");

		log.info("Click on CONTINUE SHOPPING link in cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link", "Cart//Cart.properties").click();
		
		log.info("Verify the NAvigated page");
		sa.assertTrue(gVar.assertVisible("PLP_Grid_Div", "Shopnav//PLP.properties"),"Verify the PLP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_Heading", "Shopnav//PLP.properties"), plpHeader,"PLP heading");
		
		//####################
		log.info("NAvigate to PDP");
		s.pdpNavigationThroughURL(normalProduct);
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
//		String proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		String proNameINPDP = gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties");
		
		log.info("Navigate to CArt page");
		s.navigateToCartPage();
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");

		log.info("Click on CONTINUE SHOPPING link in cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link", "Cart//Cart.properties").click();
		
		log.info("Verify the Navigated PAGE");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), proNameINPDP,"Verify the PDP name");
		
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="OOTB-096 DRM-730")
	public void TC09_verifyUIofEmptyCartPage() throws Exception
	{
		log.info("Clear the cart items");
		cart.clearCart();
		log.info("Verify the Empty cart page heading");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"), "Verify the Empty cart page heading");
		
		log.info("Verify the START shopping button in empty cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Start_Shopping_btn", "Cart//Cart.properties"),"Verify the START SHOPPING button");
		
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page HEADING");
		sa.assertTrue(gVar.assertVisible("Cart_Continue_Shopping_Link_EmptyCart", "Cart//Cart.properties"),"Verify the Continue Shopping link");
		gVar.assertEqual(gVar.assertEqual("Cart_Continue_Shopping_Link_EmptyCart", "Cart//Cart.properties"), "Continue Shopping", "Verify the Continue Shopping link text");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-731")
	public void TC10_startShoppingInEmptyCart() throws Exception
	{
		
		log.info("Click on START SHOPPING button");
		l1.getWebElement("Cart_Start_Shopping_btn", "Cart//Cart.properties").click();
		
		log.info("Verify the navigated Home page");
		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav//header.properties"),"Verify navigated HOME PAGE");
		
		log.info("Navigate to Cart page");
		s.navigateToCartPage();
		log.info("Clikc on Continue Shopping button in Empty cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link_EmptyCart", "Cart//Cart.properties").click();
		log.info("Verify the navigated Home page");
		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav//header.properties"),"Verify navigated HOME PAGE");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-716")
	public void TC11_navigateToCheckout(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Cart page");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.navigateToCart(searchData, 1, 1);
		String proPriceInPDP = s.prodPrice.get(0);
		log.info("Product price in PDP:-  "+proPriceInPDP);

		Double proPrice = new Double(proPriceInPDP.replace("$", ""));
		log.info("Product price double type:-  "+ proPrice);
		
		log.info("Click on Checkout button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Click on RETURN TO CART link");
		if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			l1.getWebElement("Checkout_ReturnToCart_Link_Mobile", "Checkout//Checkout.properties").click();
		}
		else 
		{
			l1.getWebElement("Checkout_ReturnToCart_Link", "Checkout//Checkout.properties").click();
		}
		
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		cart.clearCart();
		sa.assertAll();
	}
	
	//Precondition:- Product should contain multiple UOM option
	@Test(groups={"reg"}, description="OOTB-096 DRM-721")
	public void TC12_modifyLineItemUOMinCart() throws Exception
	{
		int uomOptionsCount=0;
		boolean uomTrue = false;
		List<String> proPrice = new ArrayList<String>();
		
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 8, 1);
		s.pdpNavigationThroughURL(searchData);
		proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("proNameINPDP:-   "+ proNameINPDP);
		
		
		log.info("Verify the UOM in PDP");
		Thread.sleep(2000);
		if (gVar.assertVisible("PDP_UOM_SelectBox", "Shopnav//PDP.properties")) 
		{
			Select uomOptions = new Select(l1.getWebElement("PDP_UOM_SelectBox", "Shopnav//PDP.properties"));
			List<WebElement> uomOptionsList = uomOptions.getOptions();
			uomOptionsCount = uomOptionsList.size();
			log.info("UOM options count:- "+ uomOptionsCount);
			
			if (uomOptionsCount>1) 
			{
				uomTrue = true;
				String uomSelectedOption = l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("title");
				log.info("uomSelectedOption:-  " + uomSelectedOption);
				
				for (int i = 0; i < uomOptionsCount; i++) 
				{
					log.info("LOOP:- " + i);
					String Options = uomOptionsList.get(i).getText();
					log.info("Options:-  " + Options);

					uomOptions.selectByIndex(i);
					String uomSelectedOptionAfter = l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("title");
					log.info("uomSelectedOptionAfter:-  "+ uomSelectedOptionAfter);
					gVar.assertequalsIgnoreCase(uomSelectedOptionAfter, Options, "Verify the selected UOM options");
					
					log.info("Collect the Product price");
					proPrice.add(l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
					
				}
				log.info("Collected price for Different UOM:- "+proPrice); 
			}
			else 
			{
				sa.assertTrue(false,"Only one option is there in UOM dropdown box, configure UOM with more option");
			}
		}
		else 
		{
			sa.assertTrue(false, "UOM is not displaying in PDP");
		}
		
		
		log.info("Cick on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		Thread.sleep(3000);
		log.info("Navigate to CART page");
		s.navigateToCartPage();
		
		log.info("Verify the UOM in cart page");
		if (uomTrue) 
		{
			sa.assertTrue(gVar.assertVisible("Cart_UOMbox", "Cart//Cart.properties"));
			Select uomInCart = new Select(l1.getWebElement("Cart_UOMbox", "Cart//Cart.properties"));
			List<WebElement> uomOptionsInCart = uomInCart.getOptions();
			for (int i = 0; i < uomOptionsCount; i++) 
			{
				//For each loop webelement is changing. Hence we are creating the SELECT object for each loop
				uomInCart = new Select(l1.getWebElement("Cart_UOMbox", "Cart//Cart.properties"));
				uomOptionsInCart = uomInCart.getOptions();
				
				String uomOption = uomOptionsInCart.get(i).getText();
				log.info("uomOption:- "+uomOption);
				
				uomInCart.selectByIndex(i);
				Thread.sleep(2000);
				String selectedUomInCart = l1.getWebElement("Cart_SelectedUOM", "Cart//Cart.properties").getAttribute("title");
				log.info("selectedUomInCart:-  "+ selectedUomInCart);
				
				log.info("Verify the Selected UOM option in cart page");
				gVar.assertequalsIgnoreCase(selectedUomInCart, uomOption,"Verify the selected UOM options");
				
				log.info("Verify the product price on selecting the UOM options");
				String proPriceInCart = l1.getWebElement("Cart_ItemPrice", "Cart//Cart.properties").getText();
				log.info("proPriceInCart:-  "+ proPriceInCart);
				
				sa.assertEquals(proPriceInCart, proPrice.get(i),"Verify the pro price in cart with PDP for different UOMs-- LOOP:- "+i);
				
			}
		}
		else 
		{
			sa.assertTrue(false, "UOM in cart is not having dropdown values");
		}
		
		sa.assertAll();
	}

// *********** Substitute product should be configured. IF not configured Testcase will fail
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-711")
	public void TC13_verifySubstituteProductInCart() throws Exception
	{
		log.info("Clear the cart item");
		cart.clearCart();
		
		log.info("Add a product to cart for which substitute product should be configured");
		String configuredProduct = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 14, 1);
		s.pdpNavigationThroughURL(configuredProduct);
		
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		log.info("Collect the product name and price");
		configuredProName = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("configuredProName:- " + configuredProName);
		configuredProPrice = l1.getWebElement("PDP_Price", PDPProperties).getText();
		log.info("configuredProPrice:- " + configuredProPrice);
		
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", PDPProperties).click();
		Thread.sleep(3000);
		log.info("Navigate to CArt page");
		s.navigateToCartPage();
		
		log.info("Verify the cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", cartProperties),"Verify the cart page heading");
		
		log.info("Verify the product name in cart page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Cart_Prod_Name", cartProperties), 
				configuredProName,"Verify the product name");
		
		
//		BELOW CODE IS NOT REQUIRED
		//Update the product quantity such that configured similar product should display
//		log.info("Enter the quantity more than the inventry");
//		String qty = "99";
//		l1.getWebElement("Cart_QtyBox", cartProperties).clear();
//		l1.getWebElement("Cart_QtyBox", cartProperties).sendKeys(qty);
//		l1.getWebElement("Cart_QtyBox", cartProperties).sendKeys(Keys.ENTER);
//		Thread.sleep(5000);
		
		//############### COLLECTING THE NUMBER OF SIMILAR ITEMS in heading ###############
		log.info("Verify the Substitute product in cart page");
		sa.assertTrue(gVar.assertVisible("SimilarProductSectionHeading", cartProperties),"Verify the Similar product heading");
		String actualHeading = gVar.assertEqual("SimilarProductSectionHeading", cartProperties);
		log.info("actualHeading:- "+ actualHeading);
		String expectedHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "cart", 3, 0);
		sa.assertTrue(actualHeading.contains(expectedHeading),"Verify the heading text");
		numberOfSimilarItems = actualHeading.replaceAll("[^0-9]", "");
		log.info("numberOfSimilarItems:- "+ numberOfSimilarItems);
		actualHeading = actualHeading.replace(numberOfSimilarItems, "").trim();
		gVar.assertequalsIgnoreCase(actualHeading, expectedHeading,"Verify the similar product heading");
		
		//##################### VERIFY THE UI ##################### 
		log.info("Verify the UI of the SIMILAR ITEMS section");
		sa.assertTrue(gVar.assertVisible("SimilarProductSection", cartProperties),"Similar product section");
		log.info("Verify the BellIcon");
		sa.assertTrue(gVar.assertVisible("SimilarProductSectionBellIcon", cartProperties),"Verify the BellIcon");
		log.info("Verify the product image");
		sa.assertTrue(gVar.assertVisible("SimilarProduct_Image", cartProperties),"Verify the product image in similar product section");
		log.info("Verify the product name");
		if (gVar.mobileView()||gVar.tabletView())
		{
			sa.assertTrue(gVar.assertVisible("SimilarProduct_Name_Mobile", cartProperties),"Verify the product name in similar product section");
		}
		else
		{
			sa.assertTrue(gVar.assertVisible("SimilarProduct_Name", cartProperties),"Verify the product name in similar product section");
		}
		
		log.info("Verify the product ID");
		sa.assertTrue(gVar.assertVisible("SimilarProduct_Id", cartProperties),"Verify the product ID in similar product section");
		String similarProductID = l1.getWebElement("SimilarProduct_Id",cartProperties).getText();
		log.info("similarProductImage:- "+similarProductID);
		
		log.info("Verify the instock text");
		String inStockText = GetData.getDataFromExcel("//data//GenericData_US.xls", "cart", 4, 0);
		sa.assertEquals(gVar.assertEqual("SimilarProduct_InStockText", cartProperties), 
				inStockText,"Verify the instock text");

		log.info("Verify the product Price");
		sa.assertTrue(gVar.assertVisible("SimilarProduct_Price", cartProperties),"Verify the product Price in similar product section");
		
		log.info("Verify the add to cart button");
		String addToCartText = GetData.getDataFromExcel("//data//GenericData_US.xls", "cart", 1, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("SimilarProduct_AddToCartButton", cartProperties), 
				addToCartText,"Verify the add to cart button");
		
		log.info("Verify the See More link");
		String seeMoreText = GetData.getDataFromExcel("//data//GenericData_US.xls", "cart", 2, 1);
		sa.assertEquals(gVar.assertEqual("SimilarProduct_SeeMoreLink", cartProperties), 
				seeMoreText,"Verify the See More link");
		
		//##################### VERIFY EXPAND AND COLLAPSE section ##################### 
		
		log.info("By-default SimilarProduct section should be expanded");
		sa.assertTrue(gVar.assertVisible("SimilarProduct_MinusIcon", cartProperties),"Verify the minus icon");
		sa.assertFalse(gVar.assertVisible("SimilarProduct_CollapsedSection", cartProperties),"section should be expanded");
		log.info("Click on MINUS icon");
		l1.getWebElement("SimilarProduct_MinusIcon", cartProperties).click();
		
		log.info("SimilarProduct section should collpase");
		sa.assertTrue(gVar.assertVisible("SimilarProduct_PlusIcon", cartProperties),"Verify the plus icon");
		sa.assertTrue(gVar.assertVisible("SimilarProduct_CollapsedSection", cartProperties),"section should be collapsed");

		log.info("Click on Plus icon/ Expand the similar product section");
		l1.getWebElement("SimilarProduct_PlusIcon", cartProperties).click();
		sa.assertTrue(gVar.assertVisible("SimilarProduct_MinusIcon", cartProperties),"Verify the minus icon");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-722")
	public void TC14_seeMoreLinkFunctionality() throws Exception
	{
		log.info("Navigate to Cart page");
		s.navigateToCartPage();
		
		log.info("Collect the similar product name and price");
		if (gVar.mobileView()||gVar.tabletView())
		{
			similarProductName = gVar.assertEqual("SimilarProduct_Name_Mobile",cartProperties);
		}
		else
		{
			similarProductName = gVar.assertEqual("SimilarProduct_Name",cartProperties);
		}
		log.info("similarProductName:- "+similarProductName);
		similarProductPrice = l1.getWebElement("SimilarProduct_Price",cartProperties).getText();
		log.info("similarProductPrice:- "+similarProductPrice);
		
		log.info("Click on SEE MORE link in cart page");
		l1.getWebElement("SimilarProduct_SeeMoreLink", cartProperties).click();
		
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", PDPProperties),"Verify the PDP");
		log.info("Verify the product name");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", PDPProperties), 
				configuredProName,"Verify the product name");
//		gVar.assertequalsIgnoreCase(actual, expected);

		log.info("Verify the product name in breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				configuredProName,"Verify the product name in active breadcrumb");
		
		log.info("Verify the substitute product in PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Recommendation_Section", PDPProperties),"Verify the recommendation section");
		
		sa.assertEquals(l1.getWebElements("PDP_Recommendation_Products", PDPProperties).size()+"", 
				numberOfSimilarItems,"Compair the number of products in PDP");

		sa.assertEquals(gVar.assertEqual("PDP_Recommendation_ProductName", PDPProperties), 
				similarProductName,"Verify the similar product name");
		sa.assertEquals(gVar.assertEqual("PDP_Recommendation_ProductPrice", PDPProperties),
				similarProductPrice,"Verify the similar product price");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-723")
	public void TC15_substituteProductAddToCartFunctionality() throws Exception
	{
		log.info("Navigate to Cart page");
		s.navigateToCartPage();
		
		int numberOfLineItems = l1.getWebElements("Cart_Line_Items", cartProperties).size();
		log.info("numberOfLineItems:- "+ numberOfLineItems);
		String cartQuantity;
		if (gVar.mobileView()||gVar.tabletView())
		{
			cartQuantity = "Header_Cart_Qty_Mobile";
		}
		else
		{
			cartQuantity = "Header_Cart_Qty";
		}
		String cartQty = gVar.assertEqual(cartQuantity, "Shopnav//header.properties","value").trim();
		log.info("cartQty:- "+cartQty);
		int cartQtyVal = Integer.parseInt(cartQty);
		
		log.info("Click on ADD TO CART button in SIMILAR product section in cart page");
		l1.getWebElement("SimilarProduct_AddToCartButton", cartProperties).click();
		
		Thread.sleep(3000);
		log.info("Collect the number of line items after adding similar product to cart");
		int numberOfLineItemsAfterAdding = l1.getWebElements("Cart_Line_Items", cartProperties).size();
		log.info("numberOfLineItemsAfterAdding:- "+ numberOfLineItemsAfterAdding);
		
		sa.assertEquals(numberOfLineItemsAfterAdding, numberOfLineItems+1,"Verify the number of line items in cart page");

		String cartQtyAfter = gVar.assertEqual(cartQuantity, "Shopnav//header.properties","value");
		log.info("cartQtyAfter:-"+ cartQtyAfter);
		int cartQtyAfterVal = Integer.parseInt(cartQtyAfter);
		sa.assertEquals(cartQtyAfterVal, cartQtyVal+1,"Verify the cart quantity in header");
		
		log.info("Vereify the Product in cart page");
		String addedSimilarProductName = l1.getWebElements("Cart_Prod_Name", cartProperties).get(1).getText();
		gVar.assertequalsIgnoreCase(addedSimilarProductName, similarProductName,"Verify the product name");
		
		String addedSimilarProductPrice = l1.getWebElements("Cart_ItemPrice", cartProperties).get(1).getText();
		gVar.assertEqual(addedSimilarProductPrice, similarProductPrice,"Verify the product price");
		
		String addedSimilarProductSku = l1.getWebElements("Cart_Prod_ID", cartProperties).get(1).getText();
		gVar.assertEqual(addedSimilarProductSku, 
				gVar.assertEqual("SimilarProduct_Id", cartProperties),"Verify the product ID");
		
		log.info("Verify the quantity of the product");
		gVar.assertEqual(gVar.assertEqual("Cart_QtyBox", cartProperties,"value"), "1","Verify the quantity");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-728")
	public void TC16_removeFunctionalityOfLineItemInCartPage() throws Exception
	{
		log.info("Clear cart item");
		cart.clearCart();
		
		log.info("Verify the empty cart page after removing the line item");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"),"Empty Cart");
		
		sa.assertAll();
	}
	
}
