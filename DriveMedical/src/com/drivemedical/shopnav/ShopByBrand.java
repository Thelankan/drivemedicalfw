package com.drivemedical.shopnav;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ShopByBrand extends BaseTest{

	@Test(groups={"reg"},description="DMED-070 DRM-985")
	public void TC00_shopByBrand() throws Exception
	{
		log.info("Navigate to product for which shop by brand is configured");
		String searchPageUrl = GetData.getDataFromExcel("//data//GenericData_US.xls", "Category", 6, 4);
		driver.get(searchPageUrl);
		log.info("expand brand facet");
		
	}
	
}
