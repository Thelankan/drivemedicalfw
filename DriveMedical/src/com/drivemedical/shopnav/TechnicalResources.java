package com.drivemedical.shopnav;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class TechnicalResources extends BaseTest
{ 
	String pdpProperties = "Shopnav//PDP.properties";
	@Test(groups={"reg"}, description="DMED-157 DRM-1514")
	public void TC00_helpLinkDeskCom() throws Exception
	{
		log.info("TC00_helpLinkDeskCom STARTED");
		log.info("Navigate to PDP");
		String productId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.pdpNavigationThroughURL(productId);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		log.info("Click on Technical Resources Downloads link");
		WebElement technicalResourcesDownloadsLink = l1.getWebElement("PDP_TechnicalResourcesDownloads_Link", pdpProperties);
		if (gVar.mobileView()||gVar.tabletView())
		{
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", technicalResourcesDownloadsLink);
		}
		else
		{
			technicalResourcesDownloadsLink.click();
		}
		
		sa.assertEquals(gVar.assertEqual("PDP_TechnicalResourcesDownloads_Link", pdpProperties,"aria-expanded"), 
				"true", "Verify the Area expanded");
		
		sa.assertTrue(gVar.assertVisible("PDP_TechnicalResourcesDownloads_ArticleLink", pdpProperties),"Verify the Article Links");
		
		log.info("TC00_helpLinkDeskCom ENDED");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-157 DRM-1514")
	public void TC01_verifyArticlePAgeNavigation() throws Exception
	{
		log.info("TC01_verifyArticlePAgeNavigation STARTED");
		
		log.info("Count the number of articles");
		List<WebElement> articleLinks = l1.getWebElements("PDP_TechnicalResourcesDownloads_ArticleLink", pdpProperties);
		log.info("articleLinks size:- "+ articleLinks.size());
		for (int i = 0; i < articleLinks.size(); i++)
		{
			log.info("LOOP:-"+ i);
			articleLinks = l1.getWebElements("PDP_TechnicalResourcesDownloads_ArticleLink", pdpProperties);
			String articleName = articleLinks.get(i).getText().trim().toLowerCase();
			log.info("articleName:- "+ articleName);
			
			log.info("Click on Article link");
			if (gVar.mobileView()||gVar.tabletView())
			{
				log.info("If Condition");
				JavascriptExecutor exe=(JavascriptExecutor)driver;
				exe.executeScript("arguments[0].click();", articleLinks.get(i));
			}
			else
			{
				log.info("ELSE Condition");
				articleLinks.get(i).click();
			}
			Thread.sleep(5000);
			
			log.info("Collect the window identifiers");
			Set<String> windowIds = driver.getWindowHandles();
			Iterator<String> itr = windowIds.iterator();
			String parentWindow = itr.next();
			String childWindow = itr.next();
			
			log.info("Switch to Child window");
			if (windowIds.size()>1)
			{
				driver.switchTo().window(childWindow);
				Thread.sleep(1500);
				log.info("Collect the title");
				String title = driver.getTitle().toLowerCase();
				log.info("title:- "+ title);
				String url = driver.getCurrentUrl();
				sa.assertTrue(url.contains("drivemedical.desk.com"),"Child window URL must contain desk.com");
				sa.assertTrue(title.contains(articleName),"Verify the title-LOOP:-"+i);
				log.info("Close the child window");
				driver.close();
				log.info("Switch the driver controller to parent window");
				driver.switchTo().window(parentWindow);
			}
			else
			{
				sa.assertFalse(true,"No New window is opened");
			}
			
		}
		
		log.info("TC01_verifyArticlePAgeNavigation ENDED");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-157 extraTC-ForGuestUser")
	public void TC02_VerifyForRegisteredUser() throws Exception
	{
		Thread.sleep(5000);
		s.navigateToHomePage();
		p.navigateToLoginPage();
		log.info("Login with valid credential");
		p.logIn(BaseTest.xmlTest);
		
		//Execute above method for registered user
		TC00_helpLinkDeskCom();
		TC01_verifyArticlePAgeNavigation();
		
		sa.assertAll();
	}
	
}
