package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ProductRegistrationForm extends BaseTest
{
	String productRegistrationFormProperties = "Shopnav//ProductRegistrationForm.properties";
	@Test(groups={"reg"}, description="DMED-083 DRM-1500")
	public void TC00_verifyProductRegistrationForm() throws Exception
	{
		log.info("Navigate to Product Registration Form page URL");
		String url = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 1, 0);
		driver.get(url);
		if (gVar.ieBrowser())
		{
			gVar.overRideInIe();
		}
		log.info("Verify the Heading");
		String headingText = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 1, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PRF_Heading", productRegistrationFormProperties), 
				 GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 1, 1),"Heading");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				headingText,"Verify the bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"Home "+headingText,"Verify the full bread crumb");
		
		log.info("Switch the driver controller to iframe");
		WebElement iframeElement = l1.getWebElement("PRF_Iframe", productRegistrationFormProperties);
		driver.switchTo().frame(iframeElement);
		
		log.info("Verify the Form Segment Headings");
		List<WebElement> segmentHeadings = l1.getWebElements("PRF_FormSegmentHeading", productRegistrationFormProperties);
		log.info("segmentHeadings size:- "+ segmentHeadings.size());
		for (int i = 0; i < segmentHeadings.size(); i++)
		{
			log.info("LOOP:-"+ i);
			int cnt = i+2;
			String segmentHeading = segmentHeadings.get(i).getText();
			gVar.assertequalsIgnoreCase(segmentHeading, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", cnt, 1),"verify the segment Headings");
		}
		
		List<WebElement> allLabels = l1.getWebElements("PRF_TopLabels", productRegistrationFormProperties);
		List<WebElement> requiredIndicator = l1.getWebElements("PRF_RequiredIcon", productRegistrationFormProperties);
		log.info("allLabels size:- "+ allLabels.size());
		sa.assertEquals(allLabels.size()-2, requiredIndicator.size(),"Verify the count of requirde indicator");	//NOt required 
		for (int i = 0; i < allLabels.size(); i++)
		{
			log.info("LOOP:-"+i);
			String applicationLabel = allLabels.get(i).getText();
			
			String expectedLabel = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", i+1, 3);
			String address2 = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 5, 3);
			String DateOfBirth = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 10, 3);
			if (applicationLabel.equalsIgnoreCase(address2)||applicationLabel.equalsIgnoreCase(DateOfBirth))
			{
//				allLabels.get(i).findElement(l1.getByReference("PRF_RequiredIcons", productRegistrationFormProperties))
//				sa.assertFalse(gVar.assertVisibleIn(allLabels, i, "PRF_RequiredIcons", productRegistrationFormProperties),"Required Indicator should Not display -LOOP:-"+i);
//				sa.assertFalse(gVar.assertVisible("PRF_RequiredIcon", productRegistrationFormProperties,i),"Required Indicator should Not display -LOOP:-"+i);
			}
			else 
			{
				expectedLabel = expectedLabel+" "+"*";
//				sa.assertTrue(gVar.assertVisible("PRF_RequiredIcon", productRegistrationFormProperties,i),"Required Indicator should display -LOOP:-"+i);
//				sa.assertTrue(gVar.assertVisibleIn(allLabels, i, "PRF_RequiredIcons", productRegistrationFormProperties),"Required Indicator should display -LOOP:-"+i);
			}
			gVar.assertequalsIgnoreCase(applicationLabel, 
					expectedLabel,"Verify the labels");
		}
		
		List<WebElement> leftLabels = l1.getWebElements("PRF_LeftLabels", productRegistrationFormProperties);
		List<WebElement> leftLabelsIndicator = l1.getWebElements("PRF_LeftLabels_Indicator", productRegistrationFormProperties);
		log.info("leftLabels size:- "+ leftLabels.size());
		for (int i = 0; i < leftLabels.size(); i++)
		{
			log.info("LOOP:- "+i);
			String leftLabel = leftLabels.get(i).getText();
			
			String expectedVal = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", i+1, 4);
			String serialNumber= GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 2, 4);
			if (leftLabel.equalsIgnoreCase(serialNumber))
			{
//				sa.assertFalse(gVar.assertVisible("PRF_LeftLabels_Indicator", productRegistrationFormProperties,i),"");
//				sa.assertFalse(gVar.assertVisibleIn(leftLabels, i, "PRF_RequiredIcons", productRegistrationFormProperties),"satr indicator should not display");
				
//				
			}
			else
			{
				expectedVal = expectedVal+" "+"*";
//				sa.assertTrue(gVar.assertVisible("PRF_LeftLabels_Indicator", productRegistrationFormProperties,i),"Verify the satr indicator");
//				sa.assertTrue(gVar.assertVisibleIn(leftLabels, i, "PRF_RequiredIcons", productRegistrationFormProperties),"satr indicator should display");
			}
			
			gVar.assertequalsIgnoreCase(leftLabel, expectedVal,"Verify the labels");
		}
		
		
		List<WebElement> spanText = l1.getWebElements("PRF_SpanText", productRegistrationFormProperties);
		log.info("spanText size:-"+ spanText.size());
		for (int i = 0; i < spanText.size(); i++)
		{
			log.info("LOOP:- "+i);
			String text = spanText.get(i).getText();
			String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", i+1, 5);
			if (i==0||i==1)
			{
				expected = expected+" "+"*";
			}
			
			gVar.assertequalsIgnoreCase(text, expected,"Verify the span text");
		}
		
		log.info("Verify the Checkbox labels");
		List<WebElement> checkboxLabels = l1.getWebElements("PRF_CheckboxLabels", productRegistrationFormProperties);
		log.info("checkboxLabels size:- "+checkboxLabels.size());
		for (int i = 0; i < checkboxLabels.size(); i++)
		{
			log.info("LOOP:- "+ i);
			gVar.assertequalsIgnoreCase(checkboxLabels.get(i).getText(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", i+1, 6),"Verify the checkbox label");
		}
		
		log.info("Verify the Radio button labels");
		List<WebElement> radioButtonLabels = l1.getWebElements("PRF_RadioButtonLabels", productRegistrationFormProperties);
		log.info("checkboxLabels size:- "+radioButtonLabels.size());
		for (int i = 0; i < radioButtonLabels.size(); i++)
		{
			log.info("LOOP:- "+ i);
			gVar.assertequalsIgnoreCase(radioButtonLabels.get(i).getText(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", i+1, 7),"Verify the checkbox label");
		}
		
		log.info("VErify the Textboxs");
		sa.assertTrue(gVar.assertVisible("PRF_FN_TextBox", productRegistrationFormProperties),"VErify PRF_FN_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_LN_TextBox", productRegistrationFormProperties),"VErify PRF_LN_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_Email_TextBox", productRegistrationFormProperties),"VErify PRF_Email_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_Address1_TextBox", productRegistrationFormProperties),"VErify PRF_Address1_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_Address2_TextBox", productRegistrationFormProperties),"VErify PRF_Address2_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_City_TextBox", productRegistrationFormProperties),"VErify PRF_City_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_State_TextBox", productRegistrationFormProperties),"VErify PRF_State_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_ZipCode_TextBox", productRegistrationFormProperties),"VErify PRF_ZipCode_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_PhoneNumber_TextBox", productRegistrationFormProperties),"VErify PRF_PhoneNumber_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_MobileNumber_TextBox", productRegistrationFormProperties),"VErify PRF_MobileNumber_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_SerialNumber_TextBox", productRegistrationFormProperties),"VErify PRF_SerialNumber_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_DateOfPurchase_TextBox", productRegistrationFormProperties),"VErify PRF_DateOfPurchase_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_PurchasePrice_TextBox", productRegistrationFormProperties),"VErify PRF_PurchasePrice_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_PurchaseMethod_TextBox", productRegistrationFormProperties),"VErify PRF_PurchaseMethod_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_StoreName_TextBox", productRegistrationFormProperties),"VErify PRF_StoreName_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_Other_TextBox", productRegistrationFormProperties),"VErify PRF_Other_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_Other1_TextBox", productRegistrationFormProperties),"VErify PRF_Other1_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_DateOfBirth_TextBox", productRegistrationFormProperties),"VErify PRF_DateOfBirth_TextBox");
		sa.assertTrue(gVar.assertVisible("PRF_SubmitButton", productRegistrationFormProperties),"VErify PRF_SubmitButton");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-083 DRM-1802")
	public void TC01_ProductRegistrationSuccessMsg() throws Exception
	{
		log.info("Enter Valid details");
		l1.getWebElement("PRF_FN_TextBox", productRegistrationFormProperties).sendKeys("Test");
		l1.getWebElement("PRF_LN_TextBox", productRegistrationFormProperties).sendKeys("Test");
		l1.getWebElement("PRF_Email_TextBox", productRegistrationFormProperties).sendKeys("test@gmail.com");
		l1.getWebElement("PRF_Checkbox", productRegistrationFormProperties).click();
		l1.getWebElement("PRF_Address1_TextBox", productRegistrationFormProperties).sendKeys("1706 n nagle ave");
		l1.getWebElement("PRF_Address2_TextBox", productRegistrationFormProperties).sendKeys("Address 2");
		l1.getWebElement("PRF_City_TextBox", productRegistrationFormProperties).sendKeys("chicago");
		l1.getWebElement("PRF_State_TextBox", productRegistrationFormProperties).sendKeys("US");
		l1.getWebElement("PRF_ZipCode_TextBox", productRegistrationFormProperties).sendKeys("60707");
		l1.getWebElement("PRF_PhoneNumber_TextBox", productRegistrationFormProperties).sendKeys("3333333333");
		l1.getWebElement("PRF_MobileNumber_TextBox", productRegistrationFormProperties).sendKeys("3333333333");
		l1.getWebElement("PRF_SerialNumber_TextBox", productRegistrationFormProperties).sendKeys("123456");
		l1.getWebElement("PRF_DateOfPurchase_TextBox", productRegistrationFormProperties).sendKeys("05/13/2018");
		l1.getWebElement("PRF_PurchasePrice_TextBox", productRegistrationFormProperties).sendKeys("123");
		WebElement PurchaseMethod = l1.getWebElement("PRF_PurchaseMethod_TextBox", productRegistrationFormProperties);
		new Select(PurchaseMethod).selectByIndex(1);
		l1.getWebElement("PRF_StoreName_TextBox", productRegistrationFormProperties).sendKeys("test");
		Thread.sleep(2000);
		try
		{
			WebElement wb = l1.getWebElement("PRF_PurchaseDescisionCheckbox", productRegistrationFormProperties);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", wb);
			Thread.sleep(1000); 
		}
		catch (Exception e)
		{
			log.info("Catch block executing");
		}
		
		l1.getWebElement("PRF_PurchaseDescisionCheckbox", productRegistrationFormProperties).click();
		l1.getWebElement("PRF_PurchaseDescisionRadioButton", productRegistrationFormProperties).click();
		l1.getWebElement("PRF_RadioButtonMale", productRegistrationFormProperties).click();
		l1.getWebElement("PRF_RadioButtonSingle", productRegistrationFormProperties).click();
		
		l1.getWebElement("PRF_DateOfBirth_TextBox", productRegistrationFormProperties).sendKeys("2000-06-01");
		l1.getWebElement("PRF_SubmitButton", productRegistrationFormProperties).click();
		Thread.sleep(2000);
		log.info("Switch to driver controller to Window");
		driver.switchTo().defaultContent();
		
		String text = GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 1, 1);
		log.info("Verify the page Heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PRF_Heading", productRegistrationFormProperties), 
				text,"Heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				text,"Verify the bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"Home "+text,"Verify the full bread crumb");
		
		log.info("Switch to Iframe");
		WebElement iframeElement = l1.getWebElement("PRF_Iframe", productRegistrationFormProperties);
		driver.switchTo().frame(iframeElement);
		
		log.info("VErify the Success massage");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PRF_SuccessSectionHeading", productRegistrationFormProperties), 
				"Success", "Verify the success");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PRF_SuccessMessage", productRegistrationFormProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "ProductRegistrationForm", 1, 8),"VErify the success message");
		
		log.info("Switch the driver controller to window");
		driver.switchTo().defaultContent();
		
		sa.assertAll();
	}
}
