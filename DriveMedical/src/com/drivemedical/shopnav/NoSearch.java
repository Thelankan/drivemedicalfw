package com.drivemedical.shopnav;

import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class NoSearch extends BaseTest{
	String NoSearchKeyWord;

	@Test(groups={"reg"},description="DMED-049 DRM-663")
	public void TC00_NoSearch_Navigation_Test_1() throws Exception
	{
		log.info("Enter invalid key word in search box and hit enter");
		NoSearchKeyWord = GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 1, 0);
		s.searchProduct(NoSearchKeyWord);
		
		log.info("verify navigation to No Search results page");
		sa.assertTrue(gVar.assertVisible("NoSearch_Content_Heading", "Shopnav\\NoSearch.properties"),"Verify the noSearch page heading");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049-UIDRM-673,674,675")
	public void TC01_NoSearch_UI_Test_2() throws Exception
	{
		Thread.sleep(5000);
		log.info("verify UI of no search results page");
		sa.assertTrue(gVar.assertVisible("NoSearch_Breadcrumb", "Shopnav\\NoSearch.properties"),"verify the breadcrumb");
		String expBreadCrumb=GetData.getDataFromExcel("//data//GenericData_US.xls","NoSearch",1,1)+" "+NoSearchKeyWord;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NoSearch_Breadcrumb", "Shopnav\\NoSearch.properties"), 
				expBreadCrumb,"Verify the breadcrumb text");
		
		log.info("No Search text");
		String expBreadHeading = "Search Results For: "+"\""+NoSearchKeyWord+"\"";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NoSearch_Headline", "Shopnav\\NoSearch.properties"), 
				expBreadHeading,"Heading text");
//		String searchKeyWord = gVar.assertEqual("NoSearch_SearchedKeyword", "Shopnav\\NoSearch.properties");
//		log.info("searchKeyWord:- " + searchKeyWord);
//		gVar.assertequalsIgnoreCase(searchKeyWord, NoSearchKeyWord,"VErify the search keyword");
		
		log.info("Continue Shopping button");
		sa.assertTrue(gVar.assertVisible("NoSearch_ContinueShop_Btn", "Shopnav\\NoSearch.properties"),"Verify CONTINUE SHIPPING method");
		log.info("Sorry we dont have any products heading");
		String expectedContent = "Sorry, your search for"+" "+"\""+NoSearchKeyWord+"\""+" "+"did not return any results. Please try another search.";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NoSearch_Content", "Shopnav\\NoSearch.properties"), 
				expectedContent,"Verify the No Search heading text");
		
		log.info("No Search content");
		sa.assertTrue(gVar.assertVisible("NoSearch_Suggestions", "Shopnav\\NoSearch.properties"),"No Search content");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-Verify no search as registered User")
	public void TC02_noSearch_Reg(XmlTest xmlTest) throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Verify the above testcases in registered user");
		TC00_NoSearch_Navigation_Test_1();
		TC01_NoSearch_UI_Test_2();
		
		sa.assertAll();
	}
}
