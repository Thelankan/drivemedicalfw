package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class SearchResults extends BaseTest{

	static String searchKeyWord;
	String totProds;
	String facetName;
	String searchKey = "air";
	
	String sarchResultProperties = "Shopnav//SearchResultPage.properties";
	
	@Test(groups={"reg"},description="DMED-049 DRM-682")
	public void TC00_searchFunctionality() throws Exception
	{
		searchKeyWord=GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 15, 1);
		log.info("search for any product");
		s.searchProduct(searchKeyWord);
		log.info("verify display of search key word");
		
		String data = GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 1, 0);
		String expected = data+" "+"\""+searchKeyWord+"\"";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Search_Heading", "Shopnav//SearchResultPage.properties"), 
				expected,"Verify the heading of search result page");
		
		log.info("verify search key word in search box in header");
		sa.assertEquals(gVar.assertEqual("Header_Serach_Box", "Shopnav//header.properties"), "","search box should be empty");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-692,694,696,697")
	public void TC01_SearchUI_Guest() throws Exception
	{
		Thread.sleep(5000);
		verifyPlpUI("guest");
		sa.assertAll(); 
	}
	@Test(groups={"reg"}, description="extraTC-Did You mean functionality")
	public void TC02_SearchUI_Guest_DiduMeanFunctionality() throws Exception
	{
		String data = GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 5, 4);
		s.searchProduct(data);
		verifyDidYouMeanFun();
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-683,684")
	public void TC03_prodNameLink() throws Exception
	{
		log.info("navigate back search results page");
		s.searchProduct(searchKeyWord);
		Thread.sleep(6000);
		log.info("fetch product name");
		pName=l1.getWebElements("Search_ProductNames", "Shopnav//SearchResultPage.properties").get(0).getText();
		log.info("pName:- "+ pName);
		log.info("click on product name link");
		l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties").get(0).click();
		
		log.info("verify navigation to PDP");
		s.verifyNavToPDP(pName);
		
		log.info("navigate back to search results page");
		s.searchProduct(searchKeyWord);
		Thread.sleep(6000);
		log.info("click on product image");
		l1.getWebElement("Search_Product_Img", "Shopnav//SearchResultPage.properties").click();
		log.info("verify navigation to PDP");
		s.verifyNavToPDP(pName);
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-686,687,688")
	public void TC04_facets() throws Exception
	{
		log.info("navigate back search results page");
		searchKeyWord=GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 15, 1);
		s.searchProduct(searchKeyWord);
		Thread.sleep(15000);
		log.info("click/Expand the first facet");
		int facet = 1;
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			log.info("Mobile view. Expand the Refinement section");
			l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
			l1.getWebElements("Search_Facets_Mobile", "Shopnav//SearchResultPage.properties").get(facet).click();
		}
		
		else
		{
			l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(facet).click();
		}
		
		log.info("fetch total products in that facet");
		totProds=l1.getWebElement("Search_facetCount", "Shopnav//SearchResultPage.properties").getText().replace("(", "").replace(")", "");
		log.info("totProds:- "+ totProds);
		log.info("fetch facet name");
		facetName=l1.getWebElement("Search_Facets_Links", "Shopnav//SearchResultPage.properties").getText().toLowerCase();
		log.info("facetName:- "+ facetName);
		log.info("click on facet links");
		l1.getWebElement("Search_Facets_Links", "Shopnav//SearchResultPage.properties").click();
		
		Thread.sleep(3000);
		
		log.info("verify total number of products displayed");
		sa.assertEquals(l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties").size()+"", totProds,"Verify the count");
		log.info("remove facet");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			sa.assertTrue(facetName.contains(gVar.assertEqual("Search_Selected_Facet_List_Mobile", "Shopnav//SearchResultPage.properties").toLowerCase()),"verify the name");
			l1.getWebElement("Search_Facet_Remove_Link_Mobile", "Shopnav//SearchResultPage.properties").click();
			
		}
		else
		{
			sa.assertTrue(facetName.contains(gVar.assertEqual("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties").toLowerCase()),"verify the name");
			l1.getWebElement("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties").click();
		}
		
		log.info("verify select facet should be removed");
		sa.assertTrue(gVar.assertNotVisible("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties"),"selected facet");
		sa.assertTrue(gVar.assertNotVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties"),"selected facet remove link");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-689,690,666")
	public void TC05_multipleFacets() throws Exception
	{
		Thread.sleep(10000);
		String facetName = "";
		log.info("select multiple filetrs");
		for(int i=1;i<=2;i++)
		{
			log.info("expand facet");
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
				l1.getWebElements("Search_Facets_Mobile", "Shopnav//SearchResultPage.properties").get(i).click();
			}
			else 
			{
				l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(i).click();
			}
			String cnt = l1.getWebElement("Search_facetCount", "Shopnav//SearchResultPage.properties").getText();
			facetName=facetName+l1.getWebElement("Search_Facets_Links", "Shopnav//SearchResultPage.properties").getText().replace(cnt, "");
			log.info("select facet");
			l1.getWebElement("Search_Facets_Links", "Shopnav//SearchResultPage.properties").click();
		}
		
		log.info("verify both facets are selected or not"+facetName);
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
//			l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
			gVar.assertequalsIgnoreCase(gVar.assertEqual("Search_Selected_Facet_List_Mobile", "Shopnav//SearchResultPage.properties").trim(), facetName.trim(),"Verify the name");
		}
		else 
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties").trim(), facetName.trim(),"Verify the name");
		}
	
		log.info("verify remove links");
		for(int i=0;i<2;i++)
		{
			sa.assertTrue(gVar.assertVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties",i),i+"");
		}
		Thread.sleep(5000);
		log.info("de select selected filters");
		for(int i=1;i<=2;i++)
		{
			log.info("expand facet");
			if (gVar.mobileView()||gVar.tabletView())
			{
				l1.getWebElements("Search_Facets_Mobile", "Shopnav//SearchResultPage.properties").get(i).click();
			}
			else
			{
				l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(i).click();
			}
			
			log.info("de select facet");
			l1.getWebElement("Search_Facets_Links", "Shopnav//SearchResultPage.properties").click();
		}
		
		sa.assertTrue(gVar.assertNotVisible("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties"),"selected facet");
		sa.assertTrue(gVar.assertNotVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties"),"selected facet remove link");

		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-SortBy functionality")
	public void TC06_sortByFunctionality() throws Exception
	{
		Thread.sleep(5000);
		sortByOptionsFunctionality("guest");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-691,693,695")
	public void TC07_SearchUI_Reg(XmlTest xmlTest) throws Exception
	{
		
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Navigate to Search result page");
		s.searchProduct(searchKeyWord);
		Thread.sleep(8000);
		verifyPlpUI("reg");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Did You mean functionality")
	public void TC08_SearchUI_Reg_DiduMeanFunctionality() throws Exception
	{
		String data = GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 5, 4);
		s.searchProduct(data);
		
		verifyDidYouMeanFun();
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-658")
	public void TC09_Search_Fun() throws Exception
	{
		log.info("search for single product");
		s.searchProduct(searchKeyWord);
		log.info("verify search results");
		sa.assertTrue(gVar.assertVisible("Search_Grid_Div","Shopnav//SearchResultPage.properties"),"search results");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-659,662")
	public void TC10_PartialSearch_Fun() throws Exception
	{
		log.info("search with partial word");
		String searchKeyWord=GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 1, 6);
		s.searchProduct(searchKeyWord);
		Thread.sleep(4000);
		
		log.info("verify search results");
		List<WebElement> prodNames=l1.getWebElements("Search_ProductNames", "Shopnav//SearchResultPage.properties");
		log.info("prodNames size:-"+ prodNames.size());
		for(int i=0;i<prodNames.size();i++)
		{
			String proName = prodNames.get(i).getText().toLowerCase();
			log.info("proName- LOOP:- "+i+"- "+ proName);
			searchKeyWord = searchKeyWord.toLowerCase();
			log.info("searchKeyWord:- "+ searchKeyWord);
			sa.assertTrue(proName.contains(searchKeyWord),"product names "+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-660")
	public void TC11_CapitalSearch_Fun() throws Exception
	{
		log.info("search with partial word");
		String searchKeyWord=GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 2, 6);
		s.searchProduct(searchKeyWord);
		log.info("searchKeyWord:-"+ searchKeyWord);
		Thread.sleep(4000);
		log.info("verify search results");
		List<WebElement> prodNames=l1.getWebElements("Search_ProductNames", "Shopnav//SearchResultPage.properties");
		for(int i=0;i<prodNames.size();i++)
		{
//			sa.assertTrue(prodNames.get(i).getText().toLowerCase().contains(searchKeyWord.toLowerCase()),"product names"+i);
			String proName = prodNames.get(i).getText().toLowerCase();
			log.info("proName LOOP:- "+i+"-"+ proName);
			searchKeyWord = searchKeyWord.toLowerCase();
			log.info("searchKeyWord:- "+searchKeyWord);
			sa.assertTrue(proName.contains(searchKeyWord),"product names "+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-662")
	public void TC12_CombinationCapandSmall_Fun() throws Exception
	{
		log.info("search with partial word");
		String searchKeyWord=GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 3, 6);;
		s.searchProduct(searchKeyWord);
		log.info("verify search results");
		Thread.sleep(4000);
		List<WebElement> prodNames=l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		for(int i=0;i<prodNames.size();i++)
		{
//			sa.assertTrue(prodNames.get(i).getText().toLowerCase().contains(searchKeyWord.toLowerCase()),"product names"+i);
			String proName = prodNames.get(i).getText().toLowerCase();
			log.info("proName-LOOP:- "+i+"- "+ proName);
			searchKeyWord = searchKeyWord.toLowerCase();
			log.info("searchKeyWord:- "+ searchKeyWord);
			sa.assertTrue(proName.contains(searchKeyWord.toLowerCase()),"product names "+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-049 DRM-661")
	public void TC13_searchSmallLetters() throws Exception
	{
		log.info("Enter search small letter search keyword");
		searchKeyWord=GetData.getDataFromExcel("//data//GenericData_US.xls", "NoSearch", 1, 6);
		s.searchProduct(searchKeyWord);
		
		Thread.sleep(4000);
		searchKeyword_VerifyItInPlpProductName(searchKeyWord);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-049 DRM-665")
	public void TC14_searchResultSearchModification() throws Exception
	{
		log.info("Select the facets");
		int numOfIteration = 3;	//How many filters need to apply
		selectFilters(numOfIteration);
		
		log.info("Verify whether facets are selected or not");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			//For mobile view, open refinement section and to verify selected refinement section
			l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
		}
		sa.assertTrue(gVar.assertVisible("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties"),"verify the selected facets");
		sa.assertEquals(l1.getWebElements("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties").size(), numOfIteration,"Verifying remove link counts");
		
		log.info("Search the product and verify the searched keyword in product name in PLP");
		s.searchProduct(searchKeyWord);
		Thread.sleep(8000);
		searchKeyword_VerifyItInPlpProductName(searchKeyWord);
		
		log.info("facets should not display");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
		}
		sa.assertFalse(gVar.assertVisible("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties"),"facets should not display");
		sa.assertFalse(gVar.assertVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties"),"facets remove link  should not display");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Verify the sort by functionality")
	public void TC15_verifySortByFunctionality() throws Exception
	{
		sortByOptionsFunctionality("reg");
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-049 DRM-685")
	public void TC16_searchResultPagePagination() throws Exception
	{
//		String str1=l1.getWebElement("Search_ItemPerPage_DD", "Shopnav//SearchResultPage.properties").getText().split(" ")[0];
//		log.info(str1);
//		int itemsDisplayedInPage=new Integer(str1);
//		log.info(itemsDisplayedInPage);
//		log.info("verify pagination");
//		s.paginationAndItemPerPage(itemsDisplayedInPage);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-049 DRM-677")
	public void TC17_searchSuggestionOverlay() throws Exception
	{
		log.info("Navigate to Home page");
		s.navigateToHomePage();
		
		log.info("Enter three letters");
		if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile"))
		{
			System.out.println("Mobile View");
			BaseTest.l1.getWebElement("Header_Mob_Search_Text", "Shopnav//header.properties").click();
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").sendKeys(searchKey);
			
		}
		else if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
		{
			BaseTest.log.info("Tab View");
			BaseTest.l1.getWebElement("Header_Mob_Search_Text", "Shopnav//header.properties").click();
			Thread.sleep(1500);
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").sendKeys(searchKey);
			
		} 
		else
		{
			l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").sendKeys(searchKey);
		}
		Thread.sleep(3000);
		
		log.info("Verify the search suggestion popup");
		String popUpStyle = gVar.assertEqual("Header_SearchSuggestion_Popup1", "Shopnav//header.properties", "style");
		System.out.println("popUpStyle:- "+ popUpStyle);
		sa.assertTrue(popUpStyle.contains("block"),"Verify the popup");
		sa.assertTrue(gVar.assertVisible("Header_SearchSuggestion_Popup", "Shopnav//header.properties"),"Verify suggestion popup");
		sa.assertTrue(gVar.assertVisible("Header_SearchSuggestion_Popup_Heading", "Shopnav//header.properties"),"Verify the popup heading");
		String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 2, 0)+ " '"+ searchKey+"'";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Header_SearchSuggestion_Popup_Heading", "Shopnav//header.properties"),
				expected,"Verify the popup heading text");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-049 DRM-678 DRM-679 DRM-680")
	public void TC18_uiOfSearchSuggestionOverlay() throws Exception
	{
		
		List<WebElement> numberOfSearchSuggestionProductsName = l1.getWebElements("Header_SearchSuggestion_ItemsName", "Shopnav//header.properties");
		System.out.println("numberOfSearchSuggestionProductsName:- " + numberOfSearchSuggestionProductsName.size());
		
		sa.assertEquals(numberOfSearchSuggestionProductsName.size(), 4, "Verify the NUmber of search suggestion product name");
		log.info("verif the searched keyword in products name");
		for (int i = 0; i < numberOfSearchSuggestionProductsName.size(); i++) 
		{
			System.out.println("LOOP:- "+ i);
			String proName = numberOfSearchSuggestionProductsName.get(i).getText().toLowerCase();
			System.out.println("proName:- " + proName);
			
			sa.assertTrue(proName.contains(searchKey.toLowerCase()),"Verify the searched keyword in product name");
		}
		
		log.info("Verify the Close icon");
		sa.assertTrue(gVar.assertVisible("Header_SearchSuggestion_Close_Link", "Shopnav//header.properties"),"Verify the Close icon");
		
		
		
		sa.assertAll();
	}
	
//	##############################################################################
//	##############################################################################
	
	void searchKeyword_VerifyItInPlpProductName(String searchKeyword) throws Exception
	{
		
		log.info("Verify the Search result page");
		sa.assertTrue(gVar.assertVisible("Search_Grid_Div", "Shopnav//SearchResultPage.properties"),"Verify the PLP");
		
		log.info("Verify the search heading");
		String expectedHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 1, 0) +" " + "\""+searchKeyword+"\"";
		System.out.println("expectedHeading:- "+ expectedHeading);
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("SearchResult_PageHeading", "Shopnav//SearchResultPage.properties"), 
				expectedHeading,"Verify the search heading");
		log.info("Verify the search key word in product names");
		List<WebElement> productsInPlp = l1.getWebElements("Search_ProductNames", "Shopnav//SearchResultPage.properties");
		for (int i = 0; i < productsInPlp.size(); i++) 
		{
			String proNameInPlp = productsInPlp.get(i).getText().toLowerCase();
			log.info("proNameInPlp:- "+ proNameInPlp);
			searchKeyword = searchKeyword.toLowerCase();
			log.info("searchKeyword:- "+ searchKeyword);
			sa.assertTrue(proNameInPlp.contains(searchKeyword),"Verify searchKeyword in productnames");
			
		}
	}
	
	void verifyPlpUI(String user) throws Exception
	{
		log.info("verify search heading");
		sa.assertTrue(gVar.assertVisible("Search_Heading", "Shopnav//SearchResultPage.properties"),"Search heading");
		String expectedHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 1, 0)+" "+ "\""+searchKeyWord+"\"";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Search_Heading", "Shopnav//SearchResultPage.properties"), 
				expectedHeading ,"search heading");
		
		log.info("verify bread crumb");
		sa.assertTrue(gVar.assertVisible("Search_BreadCrumb", "Shopnav//SearchResultPage.properties"),"Search bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), searchKeyWord, "Verify the active element in breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), "Home"+ " "+ searchKeyWord, "Verify the full breadcrumb");
		log.info("verify sort by drop down");
		sa.assertTrue(gVar.assertVisible("Search_SortBY_DD", "Shopnav//SearchResultPage.properties"),"sort by dropdown");
		
		log.info("verify items per page drop down");
		sa.assertTrue(gVar.assertVisible("Search_ItemPerPage_DD", "Shopnav//SearchResultPage.properties"),"Items per page");
		
		log.info("products text");
		sa.assertTrue(gVar.assertVisible("Search_Tot_Prods", "Shopnav//SearchResultPage.properties"),"Total Products text");
//		log.info("pagination");
//		sa.assertTrue(gVar.assertVisible("Search_Pagination", "Shopnav//SearchResultPage.properties"),"pagination");
		
		List<WebElement> totProds=l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		for(int i=0;i<totProds.size();i++) 
		{
			log.info("products image");
			sa.assertTrue(gVar.assertVisible("Search_Product_Img", "Shopnav//SearchResultPage.properties",i),"product image");
			log.info("products name");
			sa.assertTrue(gVar.assertVisible("Search_Prod_Names", "Shopnav//SearchResultPage.properties",i),"product name");
			log.info("Verify the Model text");
			sa.assertTrue(gVar.assertVisible("Search_Model_Text", "Shopnav//SearchResultPage.properties",i),"Model text");
			if (user.equalsIgnoreCase("reg"))
			{
				log.info("product price should display for REGISTERED USER");
				sa.assertTrue(gVar.assertVisible("Search_Prices", "Shopnav//SearchResultPage.properties",i),"product price should display for Register user");
			}
			else 
			{
				log.info("product price should not display for guest user");
				sa.assertFalse(gVar.assertVisible("Search_Prices", "Shopnav//SearchResultPage.properties",i),"product price Guest User");
			}
		}
		log.info("product price should not display");
		sa.assertTrue(gVar.assertNotVisible("Search_Price", "Shopnav//SearchResultPage.properties"),"product price");
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
		}
		log.info("verify category section");
		sa.assertTrue(gVar.assertVisible("Search_Leftnav_CatHeading", "Shopnav//SearchResultPage.properties"),"category section heading");
		if (gVar.mobileView()||gVar.tabletView())
		{
			sa.assertEquals(gVar.assertEqual("Search_Leftnav_CatHeading_Mobile", "Shopnav//SearchResultPage.properties"), 
					"CATEGORY", "verify category text");
			sa.assertTrue(gVar.assertVisible("Search_Facets_Mobile", "Shopnav//SearchResultPage.properties"),"Verify refinement section");
		}
		else
		{
			sa.assertEquals(gVar.assertEqual("Search_Leftnav_CatHeading", "Shopnav//SearchResultPage.properties"), 
					"CATEGORY", "verify category text");
			sa.assertTrue(gVar.assertVisible("Search_Facets", "Shopnav//SearchResultPage.properties"),"Verify refinement section");
		}
		
		sa.assertTrue(gVar.assertVisible("Search_LeftnavSection", "Shopnav//SearchResultPage.properties"),"Verify the Left nav section");
	
		log.info("Refine By heading");
		sa.assertTrue(gVar.assertVisible("Search_RefineBY_Heading", "Shopnav//SearchResultPage.properties"),"Refine By");
		log.info("verify display of refine section");
		
	}
	
	void verifyDidYouMeanFun() throws Exception
	{
		log.info("Verify the did you mean section");
		sa.assertTrue(gVar.assertVisible("Search_DiduMeanSection", "Shopnav//SearchResultPage.properties"),"Verify the Did you mean section");
		String didYouMeanLink = l1.getWebElement("Search_DiduMeanSectionLink","Shopnav//SearchResultPage.properties").getText();
		log.info("didYouMeanLink:- "+ didYouMeanLink);
		
		String didYouMeanText = l1.getWebElement("Search_DiduMeanSection", "Shopnav//SearchResultPage.properties").getText();
		didYouMeanText = didYouMeanText.replace(didYouMeanLink, "");
		log.info("didYouMeanText:- "+ didYouMeanText);
		sa.assertEquals(didYouMeanText, GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 1, 3),"Verify the text");
		
		log.info("Click on the link");
		l1.getWebElement("Search_DiduMeanSectionLink", "Shopnav//SearchResultPage.properties").click();
		
		Thread.sleep(2000);
		log.info("Verify the navigated page");
		String data = GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 1, 0);
		String expectedData = data+" "+"\""+didYouMeanLink+"\"";
		sa.assertEquals(gVar.assertEqual("Search_Heading", "Shopnav//SearchResultPage.properties"), 
				expectedData,"Verify the heading");
	}
	
	void selectFilters(int numOfIteration) throws Exception
	{
		Thread.sleep(10000);
		for(int i=1;i<=numOfIteration;i++)
		{
			log.info("LOOP:- "+i);
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				//If the REFINE dropdown box is expanded, then no need to click on dropdownbox
				if (gVar.assertNotVisible("Search_RefineDropdownBox_Active", "Shopnav//SearchResultPage.properties")) 
				{
					log.info("Click on REFINE Dropdown box");
					l1.getWebElement("Search_RefineDropdownBox", "Shopnav//SearchResultPage.properties").click();
				}
				
			}
			log.info("expand facet");
			WebElement facet = l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(i);
			log.info("facet name:- "+ facet.getText());
			facet.click();
//			JavascriptExecutor exe=(JavascriptExecutor)driver;
//			exe.executeScript("arguments[0].click();", facet);
			Thread.sleep(2000);
			String facetName=l1.getWebElement("Search_Facets_Links",  "Shopnav//SearchResultPage.properties").getText();
			log.info("facetName:- "+facetName);
			log.info("select facet");
			l1.getWebElement("Search_Facets_Links", "Shopnav//SearchResultPage.properties").click();
			
			Thread.sleep(5000);
		}
	}
	
	void sortByOptionsFunctionality(String user) throws Exception
	{
		log.info("verify the Sort by selectbox");
		sa.assertTrue(gVar.assertVisible("SearchResult_SortBy_Dropdown", "Shopnav//SearchResultPage.properties"),"Verify the SortBy dropdown box");
		log.info("Verify the number sortBy options");
		WebElement sortBySelectBox = l1.getWebElement("SearchResult_SortByDropdown_Option", "Shopnav//SearchResultPage.properties");
		Select sel = new Select(sortBySelectBox);
		int numberOfOptions = sel.getOptions().size();
		log.info("Number of options:- "+ numberOfOptions);
		int rowNum;
		if (user.equalsIgnoreCase("reg"))
		{
			log.info("Register user");
			sa.assertEquals(numberOfOptions, 7,"Verify the number of options for Registered");
			rowNum=2;
		}
		else
		{
			log.info("Guest user");
			sa.assertEquals(numberOfOptions, 5,"Verify the number of options for Guest user");
			rowNum=1;
		}
		
		for (int i = 1; i < numberOfOptions; i++)
		{
			sortBySelectBox = l1.getWebElement("SearchResult_SortByDropdown_Option", "Shopnav//SearchResultPage.properties");
			sel = new Select(sortBySelectBox);
			log.info("LOOP:- "+ i);
			sel.selectByIndex(i);
			Thread.sleep(3000);
			log.info("Verify the selected option");
			sa.assertEquals(gVar.assertEqual("SearchResult_SortBy_Dropdown", "Shopnav//SearchResultPage.properties","title"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Category", i, rowNum),"VErify the selected options - loop:-"+i);
		}
		
	}
}
