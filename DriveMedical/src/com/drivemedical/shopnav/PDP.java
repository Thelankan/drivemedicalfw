package com.drivemedical.shopnav;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.annotations.TestInstance;
import org.testng.xml.XmlTest;


//DRM-494

public class PDP extends BaseTest
{

	@Test(groups={"reg"}, description="DMED-045 DRM-366 AND DRM-367 AND DRM-368")
	public void TC00_PDP_Product_Sharing_Fun_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		log.info("Verify the Social Sharing labels and icons");
		if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			log.info("Mobile View");
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Lable", "Shopnav//PDP.properties"),"Share this: label");
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Icons_Section", "Shopnav//PDP.properties"));
		
			//Facebook
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_FB_Link", "Shopnav//PDP.properties"),"Facebook");
			sa.assertEquals(l1.getWebElement("PDP_Social_Sharing_FB_Text", "Shopnav//PDP.properties").getText(), "Facebook");
			
			//Twitter
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Twit_Link", "Shopnav//PDP.properties"),"Twiter");
			sa.assertEquals(l1.getWebElement("PDP_Social_Sharing_Twit_Text", "Shopnav//PDP.properties").getText(), "Twitter");
			
			//LinkedIn
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Linked_Link", "Shopnav//PDP.properties"),"LinkedIn");
			sa.assertEquals(l1.getWebElement("PDP_Social_Sharing_Linked_Text", "Shopnav//PDP.properties").getText(), "LinkedIn");
		
		}
		else 
		{
			log.info("Desktop View");
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Lable_Desktop", "Shopnav//PDP.properties"));
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Icons_Section_Desktop", "Shopnav//PDP.properties"));
			//Facebook
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_FB_Link_Desktop", "Shopnav//PDP.properties"),"Facebook");
			sa.assertEquals(l1.getWebElement("PDP_Social_Sharing_FB_Text_Desktop", "Shopnav//PDP.properties").getText(), "Facebook");
			
			//Twitter
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Twit_Link_Desktop", "Shopnav//PDP.properties"),"Twitter");
			sa.assertEquals(l1.getWebElement("PDP_Social_Sharing_Twit_Text_Desktop", "Shopnav//PDP.properties").getText(), "Twitter");
			
			//LinkedIn
			sa.assertTrue(gVar.assertVisible("PDP_Social_Sharing_Linked_Link_Desktop", "Shopnav//PDP.properties"),"LinkedIn");
			sa.assertEquals(l1.getWebElement("PDP_Social_Sharing_Linked_Text_Desktop", "Shopnav//PDP.properties").getText(), "LinkedIn");
			
			log.info("Collect the Parent browser window handler identifier");
			String parent = driver.getWindowHandle();
			log.info("Parent window identifier:- " +parent);
			log.info("Parent Window:- " +parent);

		}
		
		List<WebElement> allLinks = l1.getWebElements("PDP_Social_Sharing_Links", "Shopnav//PDP.properties");
		for (int i = 0; i < allLinks.size(); i++) 
		{
			log.info("Click on each social sharing link. LOOP:- " + i);
			//Click on link
			allLinks.get(i).click();
			Thread.sleep(6000);
			log.info("Collect the Child browser window handler identifier");
			Set<String> s=driver.getWindowHandles();
			Iterator<String> itr=s.iterator();
			String parentWinId=itr.next();
			String child=itr.next();
			log.info("Child browser window identifier:-  " + child);
			log.info("Child window Identifier:-  " + child);
			
			log.info("Switch to Child browser window");
			driver.switchTo().window(child);
			
			log.info("Verify the child browser title");
			String childWindowTitle = driver.getTitle().toLowerCase();
			String childWindowUrl = driver.getCurrentUrl();
			
			int j = i+1;
			String expcetedTitle = GetData.getDataFromExcel("//data//GenericData_US.xls", "Social_icons", j, 1).toLowerCase();
			String actUrl = GetData.getDataFromExcel("//data//GenericData_US.xls", "Social_icons", j, 2);
			log.info("Popwindow title and URL:-"+childWindowTitle + " -->"+ childWindowUrl);
			log.info("expcetedTitle:-" + expcetedTitle);
			log.info("actUrl:-" + actUrl);
			
//			sa.assertTrue(childWindowTitle.contains(expcetedTitle),"Verify the Title:-"+j);
			sa.assertTrue(childWindowUrl.contains(actUrl),"Verify the URL of the popup:-"+j);
			
			log.info("Close the child popup window");
			driver.close();
			
			log.info("Switch to parent browser");
			driver.switchTo().window(parentWinId);
			
		}
		log.info("LOOP ENDED");
		sa.assertAll();
		
	} 	
	
	
	@Test(groups ={"reg","sanity_guest"}, description="OOTB-028 DRM-380 AND DRM-396 AND DRM-397 AND DRM-398")
	public void TC01_productPrice_Unauthenticated_Customer(XmlTest xmlTest) throws Exception
	{
        p.logout(xmlTest);
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		s.pdpNavigationThroughURL(searchData);
		log.info("Product price should not be displayed");
		sa.assertTrue(gVar.assertNotVisible("PDP_Price", "Shopnav//PDP.properties"),"Price should not display");
		sa.assertTrue(gVar.assertNotVisible("PDP_MSRP_Value", "Shopnav//PDP.properties"),"MSRP value should not display");
		sa.assertEquals(gVar.assertEqual("PDP_SignInTo_View_Price", "Shopnav//PDP.properties"), 
				"SIGN IN TO VIEW PRICE","Verify The signin text");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg","sanity_reg"}, description="OOTB-028 DRM-382 AND DRM-400 AND DRM-401")
	public void TC02_login_From_PDP(XmlTest xmlTest) throws Exception
	{
		log.info("log out from application");
		p.logout(xmlTest);
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		log.info("Collect the product name in PDP");
		String productName = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("Product NAme in PDPD:-  "+ productName);
		
		log.info("Click on SIGN IN TO VIEW PRICE button in PDP");
		l1.getWebElement("PDP_SignInTo_View_Price", "Shopnav//PDP.properties").click();
		
		log.info("Page Should navigate to SIGN IN page");
		sa.assertTrue(gVar.assertVisible("Login_Heading", "Profile//login.properties"),"Verify the Signin page heading");
		log.info("Login with valid credentials");
		p.logIn(xmlTest);
		
		log.info("Verify the PDP and Product name");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		String productNameAfterSignin = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("Product NAme After signin:-  "+ productNameAfterSignin);
		sa.assertEquals(productNameAfterSignin, productName,"Verify the Product name after signin");
		
		log.info("Verify the Product price and Add To CArt button");
		sa.assertTrue(gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties"),"Product price ");
		sa.assertTrue(gVar.assertVisible("PDP_MSRP_Value", "Shopnav//PDP.properties"),"Product MSRP value");
		sa.assertTrue(gVar.assertVisible("PDP_AddToCart_button", "Shopnav//PDP.properties"),"ADD TO CART button should display");
		sa.assertTrue(gVar.assertNotVisible("PDP_SignInTo_View_Price", "Shopnav//PDP.properties"),"SIGN IN TO VIEW PRICE should Not displaying");
		
		sa.assertAll();
	}  
	
	@Test(groups={"reg","sanity_reg"}, description="OOTB-028 DRM-399")
	public void TC03_uiOfPDP(XmlTest xmlTest) throws Exception
	{
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"PDP element");
		sa.assertTrue(gVar.assertVisible("PDP_ProductName", "Shopnav//PDP.properties"),"Product name");
		
		log.info("Verify the breadcrumb");
		sa.assertTrue(gVar.assertVisible("PDP_BreadcrumbElement", "Shopnav//PDP.properties"),"Breadcrumb");
		String ProNameInBreadCrumb = l1.getWebElement("PDP_ActiveElement_InBreadcrumb", "Shopnav//PDP.properties").getText();
		log.info("ProNameInBreadCrumb:-  " + ProNameInBreadCrumb);
		log.info("Verify the product name in breadcrumb");
		sa.assertTrue(l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText().equalsIgnoreCase(ProNameInBreadCrumb),"Product name in breadcrumb");
//		sa.assertEquals(l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText(), ProNameInBreadCrumb);
		log.info("Verify the quantity section");
		sa.assertTrue(gVar.assertVisible("PDP_Qty_label", "Shopnav//PDP.properties"),"Quantity label");
		sa.assertTrue(gVar.assertVisible("PDP_Qty_Textbox", "Shopnav//PDP.properties"),"Quantity box");
		
		sa.assertTrue(gVar.assertVisible("PDP_StockStatus", "Shopnav//PDP.properties"),"Verify the Stock status");
		sa.assertTrue(gVar.assertVisible("PDP_StockStatusImage", "Shopnav//PDP.properties"),"Verify the Stock status Image");
		sa.assertTrue(gVar.assertVisible("PDP_SKU", "Shopnav//PDP.properties"),"Verify SKU");
		sa.assertTrue(gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties"),"Verify the product price");
		sa.assertTrue(gVar.assertVisible("PDP_ProductOptions_link", "Shopnav//PDP.properties"),"Verify the product options link");
		
		
		log.info("Verify the tabs in PDP");
		sa.assertTrue(gVar.assertVisible("PDP_tabs_ProductDetails", "Shopnav//PDP.properties"),"Verify the tabs");
		sa.assertEquals(l1.getWebElement("PDP_tabs_ACCESSORIES", "Shopnav//PDP.properties").getText(), "ACCESSORIES","Verify ACCESSORIES");
		sa.assertEquals(gVar.assertEqual("PDP_tabs_PARTS", "Shopnav//PDP.properties"), "PARTS","Verify the parts");
		sa.assertEquals(gVar.assertEqual("PDP_tabs_PARTS", "Shopnav//PDP.properties"), "PARTS","Verify the parts");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_tabs_CustomerReviews", "Shopnav//PDP.properties"),"Reviews","verify Customer Reviews");
		}
		else
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_tabs_CustomerReviews", "Shopnav//PDP.properties"),"Customer Reviews","verify Customer Reviews");
		}
		
		sa.assertTrue(gVar.assertVisible("PDP_Ratings_Section", "Shopnav//PDP.properties"),"Verify the Rating section");
		
		List<WebElement> pdpDrawers = l1.getWebElements("PDP_Drawer_Section", "Shopnav//PDP.properties");
		log.info("PDP drawers count:-  " + pdpDrawers.size());
		for (int i = 0; i < pdpDrawers.size(); i++) 
		{
			log.info("LOOP:- "+ i);
			String actDrawerName = pdpDrawers.get(i).getText().trim();
			log.info("actDrawerName:- " + actDrawerName);
			log.info("Verify the drawer name in pdp");
			gVar.assertequalsIgnoreCase(actDrawerName, GetData.getDataFromExcel("//data//GenericData_US.xls", "PDP", i+1, 0), "PDP Drawer");
		}
		
		log.info("Verify the PDF link in PDP");
		sa.assertTrue(gVar.assertVisible("PDP_pdf_Link", "Shopnav//PDP.properties"),"Verify PDF link");
		log.info("Verify the product image in tabs section");
		sa.assertTrue(gVar.assertVisible("PDP_PdImageInTabs", "Shopnav//PDP.properties"),"Product image in tab section");
		sa.assertAll();
	}
	
		
	@Test(groups={"reg"},description="OOTB-028 DRM-982 DRM-983 DRM-984")
	public void TC03_verifyTruckIconInPDP()
	{
		log.info("Verify the TRUCK ICON");
		sa.assertTrue(gVar.assertVisible("PDP_StockStatusImage", "Shopnav//PDP.properties"),"Verify the Stock status Image");
		
		sa.assertAll();
	}
	
	
	@Test(groups ={"reg"}, description="OOTB-028 DRM-384")
	public void TC04_HCPCS_Code_PDP(XmlTest xmlTest) throws Exception
	{
		
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 8, 1);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		gVar.waitForElement("PDP_HCPCS_Link", "Shopnav//PDP.properties", 6);
		log.info("Verify the HCPCs heading");
		sa.assertTrue(gVar.assertVisible("PDP_HCPCS_Link", "Shopnav//PDP.properties"),"Verify the HCPCS link in PDP");
		
		log.info("By default HCPCS section should be expanded");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		log.info("Verify the HCPCS code in drawer");
		sa.assertNotNull(l1.getWebElement("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties").getText(), "HSPC code in Drawer section");
		
		log.info("Click on HCPCS link");
		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("Expanded section should collapse");
		sa.assertEquals(l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").getAttribute("aria-expanded"), "false");
		
		log.info("Verify the HCPCS code in drawer section");
		//Verify the Code
//		sa.assertTrue(false, "Verify HCPCS code in drawer section manually, Configuration related TC");
		
		sa.assertAll();
	}
	
	@Test(groups ={"reg"}, description="OOTB-028 DRM-388")
	public void TC05_Qty_inPDP(XmlTest xmlTest) throws Exception
	{
		log.info("Enter quantity in PDP");
		String maxQty = "9999";
		l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
		l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(maxQty);
		
		log.info("Verify the quantity after entering");
		String qtyEntered = l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").getAttribute("value");
		String qtyMaxLnth = l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").getAttribute("maxlength");
		log.info("Quantity in quantity box:-  "+ qtyEntered);
		log.info("maximum quantity length:-  "+ qtyMaxLnth);
		
		log.info("Verify the quantity");
		sa.assertEquals(qtyEntered, maxQty,"verif the max Qty");
		sa.assertEquals(qtyMaxLnth, "4","Verify the max length");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-028 DRM-377 AND DRM-392")
	public void TC06_variantsSelection(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 3);
		s.pdpNavigationThroughURL(searchData);
		
		Thread.sleep(3000);
//		List<WebElement> allVariants = l1.getWebElements("PDP_AllVariants_DropdownBox", "Shopnav//PDP.properties");
//		log.info("Number of variants in PDP :-  " + allVariants.size());
//		
//		for (int i = 0; i < allVariants.size(); i++) 
//		{
//			log.info("Number of iterations:-  "+ i);
//			
//			log.info("Click on dropdown box");
//			allVariants.get(i).click();
//			
//			log.info("Verify the Expanded section");
//			sa.assertEquals("true", allVariants.get(i).getAttribute("aria-expanded"), "Verifing whether the dropdown is expanded or not");
//		}
		
			log.info("TRy block started");
			if (gVar.assertVisible("PDP_Variant_DropdownBox", "Shopnav//PDP.properties")) 
			{
				log.info("Click on Dropdown box");
				l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").click();
				sa.assertEquals(l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").getAttribute("aria-expanded"), "true", "Verify the Dropdowndown expanded or not");
				
				List<WebElement> dropdownLinks = l1.getWebElements("PDP_Variant_DropdownBox_Links", "Shopnav//PDP.properties");
				log.info("Number of Links :-  " + dropdownLinks.size());
				List<WebElement> dropdownLinksText = l1.getWebElements("PDP_Variant_DropdownBox_LinksText", "Shopnav//PDP.properties");
				
				for (int i = 0; i < dropdownLinks.size(); i++) 
				{
					log.info("LOOP:- " + i);
					dropdownLinksText = l1.getWebElements("PDP_Variant_DropdownBox_LinksText", "Shopnav//PDP.properties");
					dropdownLinks = l1.getWebElements("PDP_Variant_DropdownBox_Links", "Shopnav//PDP.properties");
					String linkText = dropdownLinksText.get(i).getText();
					log.info("Link text:-  " + linkText);
					
					log.info("selecect link in dropbox");
					dropdownLinks.get(i).click();
					Thread.sleep(7000);
					
					String selectedLink = l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").getAttribute("title");
					log.info("Selected link in dropdown box:-  " + selectedLink);
					log.info("Verify the Selected Link in dropdown box");
					sa.assertEquals(selectedLink, linkText);
					
					log.info("Click on Dropdown box");
					l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").click();
					sa.assertEquals(l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").getAttribute("aria-expanded"), "true", "Verify the Dropdowndown expanded or not");
					Thread.sleep(5000);
				}
				
			}
			else 
			{
				sa.assertTrue(false, "NO VARIANT option is present");
			}
			
		
		sa.assertAll();
	}


//Pre-conditions "VARIANT PRODUCT IS REQUIRED"
@Test(groups ={"reg"}, description="OOTB-028 DRM-378 AND DRM-383")
public void TC07_verify_PDP_ForDifferent_Variants(XmlTest xmlTest) throws Exception
{
	log.info("Navigate to PDP");
	String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 3);
	s.pdpNavigationThroughURL(searchData);
	Thread.sleep(3000);
		
		if (gVar.assertVisible("PDP_Variant_DropdownBox", "Shopnav//PDP.properties")) 
		{
			WebElement selectBoxElement = l1.getWebElement("PDP_Size_Selectbox", "Shopnav//PDP.properties");
			Select sel = new Select(selectBoxElement);
			int totalOptions = sel.getOptions().size();
			log.info("totalOptions:- "+ totalOptions);
			for (int i = 0; i < totalOptions; i++) 
			{
				log.info("LOOP:- " + i);
				
				log.info("Select the Size options");
				sel = new Select(selectBoxElement);
				String selectOption = sel.getOptions().get(i).getText().trim();
				log.info("selectOption:- " + selectOption);

				sel.selectByIndex(i);
				Thread.sleep(5000);
				
				log.info("Verify the selected options");
				String selectedLink = l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").getAttribute("title");
				log.info("Selected link in dropdown box:-  " + selectedLink);
				sa.assertEquals(selectedLink, selectOption,"Verify the selected options");
				
				log.info("Verify the SKU, product price, Add to cart button");
				sa.assertTrue(gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties"),"Verifying product price");
				sa.assertTrue(gVar.assertVisible("PDP_MSRP_Value", "Shopnav//PDP.properties"),"Verify the MSRP value");
				sa.assertTrue(gVar.assertVisible("PDP_SKU", "Shopnav//PDP.properties"),"Verify the SKU");
				
				sa.assertEquals(gVar.assertEqual("PDP_StockStatus", "Shopnav//PDP.properties"), 
						"IN STOCK", "verify IN STOCK");
				sa.assertEquals(gVar.assertEqual("PDP_AddToCart_button", "Shopnav//PDP.properties"), 
						"ADD TO CART", "verify ADD TO CART");
				log.info("Click on Dropdown box");
				l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").click();
				sa.assertEquals(l1.getWebElement("PDP_Variant_DropdownBox", "Shopnav//PDP.properties").getAttribute("aria-expanded"), "true", "Verify the Dropdowndown expanded or not LOOP:- "+i);
				
			}
			
		}
		else 
		{
			sa.assertTrue(false, "NO VARIANT option is present");
		}
		//Verify the ADD TO CART BUTTON for OUT OF STOCK product
//		sa.assertTrue(false,"OUT of stock product is not configured");
	
	sa.assertAll();
}
	
	
	@Test(groups={"reg"},  description="OOTB-028 DRM-394")
	public void TC08_PDP_ForSeleceted_Language(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Home page");
		s.navigateToHomePage();
		
		log.info("Navigate to PDP");
		int qty = 1;
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		log.info("collect the product name");
		String prodName = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		String proSKU = l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText();
		log.info("Product Name in PDP:- "+ prodName);
		log.info("SKU:- "+ proSKU);
		
		log.info("Collect the language from header");
		String locail = l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").getAttribute("title");
		log.info("Selected language:-  "+locail);
		log.info("Chenge language from header");
		WebElement languageDD_Element;
		if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile"))
		{
			log.info("Click on Hamburgar menu");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
			languageDD_Element = l1.getWebElement("Header_Lang_DD_Mobile", "Shopnav//header.properties");
			
		}
		else 
		{
			languageDD_Element = l1.getWebElement("Header_Lang_DD_Mobile", "Shopnav//header.properties");
		}
		
		Select sel = new Select(languageDD_Element);
		log.info("selecting the selected language");
		sel.selectByValue("US-EN");
		Thread.sleep(3000);
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Check whether the hamburgar is opened or not");
			if (BaseTest.gVar.assertVisible("Hamburger_Expanded", "Shopnav\\header.properties"))
			{
				BaseTest.log.info("Click on Close icon in hamburgar menu");
				BaseTest.l1.getWebElement("Hamburger_Close_Button", "Shopnav\\header.properties").click();			
			}
		}
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"),
				prodName,"Verify PDP_ProductName");
		
		log.info("Verify the selected language");
		sa.assertEquals(l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").getAttribute("title"), 
				locail,"Verify Header_Lang_Selector");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-028 DRM-390")
	public void TC09_uomInPDP(XmlTest xmlTest) throws Exception
	{
		s.navigateToHomePage();
		log.info("Navigate to PDP");
		int qty = 1;
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		
		log.info("Verify the UOM(Unit Of Measurement)");
		l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").click();
		
		log.info("Verify the Expanded section on click");
		sa.assertEquals(l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("aria-expanded"), "true", "Verify whether UOM dropdown should opned");
		
		
		Select uomSelect = new Select(l1.getWebElement("PDP_UOM_SelectBox", "Shopnav//PDP.properties"));
		List<WebElement> uomSelectOptions = uomSelect.getOptions();
		log.info("uomSelectOptions:- " + uomSelectOptions);
		
		for (int i = 0; i < uomSelectOptions.size(); i++) 
		{
			log.info("LOOP:-  " + i);
			log.info("Select the options from dropdown box");
			uomSelect = new Select(l1.getWebElement("PDP_UOM_SelectBox", "Shopnav//PDP.properties"));
			uomSelect.selectByIndex(i);
			Thread.sleep(3000);	//wait for some time
			
			log.info("Verify the selected option in UOM dropdown box");
			log.info("UOM actual:-  "+ l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("title"));
			log.info("UOM expected:-  "+ uomSelectOptions.get(i).getText());
			sa.assertEquals(l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("title"), 
					uomSelectOptions.get(i).getText(), "selected UOM Verifying ");
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-028 DRM-493")
	public void TC10_Verify_availability_msg_inCart(XmlTest xmlTest) throws Exception
	{
		log.info("Clear cart item");
		cart.clearCart();
		
		log.info("Navigate to PDP and enter QUANTITY MORE THAN INSTOCK VALUE");
		String qty = "99";
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
		l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(qty);
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		
		log.info("Navigate to CART page");
		s.navigateToCartPage();
		
		log.info("Verify the Cart page heading");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verifying the Cart page heading");
		
		log.info("Verify the product quantity in CART page");
		sa.assertEquals(l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").getAttribute("value"), qty, "Verifying the quantity in cart page");
		
		log.info("Verify the OUT OF STOCK message");
		//Error message is not displaying
		sa.assertTrue(false,"Verify the OOS alert message in cart page");
	
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-028 DRM-393")
	public void TC11_add_ProductTo_Cart(XmlTest xmlTest) throws Exception
	{
		
		log.info("Clear cart items");
		cart.clearCart();
		
		log.info("Navigate to PDP");
		int qty = 1;
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchData);
		s.pdpNavigationThroughURL(searchData);
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		
		log.info("collect the product name");
		String prodName = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		String proSKU = l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText().split("#")[1].trim();
		log.info("Product Name in PDP:- "+ prodName);
		log.info("SKU:- "+ proSKU);
		
		log.info("Navigate to CART page");
		s.navigateToCartPage();
		
		log.info("Verify the Cart page heading");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verifying the Cart page heading");
		log.info("Verify the product quantity in CART page");
		sa.assertEquals(l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").getAttribute("value"), qty+"", "Verifying the quantity in cart page");
		
		String proNameInCart = l1.getWebElement("Cart_Prod_Name", "Cart//Cart.properties").getText();
		sa.assertEquals(proNameInCart, prodName,"Verify the Product name");
		String proNumInCart = l1.getWebElement("Cart_Prod_ID", "Cart//Cart.properties").getText();
		sa.assertEquals(proNumInCart, proSKU,"Verify the SKU");
		
		sa.assertAll();
	}
	

	@Test(groups={"reg"}, description="OOTB-028 DRM-395")
	public void TC12_currancyInPDP(XmlTest xmlTest) throws Exception
	{
//		sa.assertTrue(false, "Change the language in header is navigating SERVER ERROR page");
//		log.info("Navigate to PDP");
//		int qty = 1;
//		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
//		s.pdpNavigationThroughURL(searchData);
//
//		log.info("collect the product name");
//		String prodName = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
//		String proSKU = l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText();
//		log.info("Product Name in PDP:- "+ prodName);
//		log.info("SKU:- "+ proSKU);
//		
//		log.info("Collect the language from header");
//		String locail = l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").getAttribute("title");
//		log.info("Selected language:-  "+locail);
//
//		log.info("Chenge language from header");
//		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
//		log.info("Select the CA language");	 
//		l1.getWebElement("Header_CandaEng_Link", "Shopnav//header.properties").click();
//
//		log.info("Verify the selected language in header");
//		sa.assertEquals(l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").getAttribute("title"), 
//				"CA EN", "Verify the ");
//		
//		log.info("User should navigate to home page");
////		sa.assertTrue(l1.getWebElement("Home_Identifier", "Shopnav//header.properties").isDisplayed());
//		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav//header.properties"), "HOME page should display");
//		
//		//*******************
//		try 
//		{
//			log.info("Navigate to PDP");
//			s.navigateToPDP_Search(searchData);
//			
//			log.info("Verify the Currency display in PDP");
//			sa.assertTrue(false,"Unable to navigate to PDP");
//		} 
//		catch (Exception e) 
//		{
//			log.info("Catch block executing");
//			sa.assertTrue(false,"No Product is configured for CA");
//		}
//		
//		log.info("RESET BACK TO US");
//		log.info("Chenge language from header");
//		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
//		log.info("Select the US language");	 
//		l1.getWebElement("Header_UsEng_Link", "Shopnav//header.properties").click();
		
		sa.assertAll();
		
	}
}