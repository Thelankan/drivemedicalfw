package com.drivemedical.shopnav;

import static org.testng.Assert.assertTrue;

import java.awt.RenderingHints.Key;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Footer extends BaseTest{
	
	@Test(groups={"reg"},description="DMED_039_1 DMED-518 DRM-1522,1523")
	public void TC00_social_Test_1() throws Exception
	{
		log.info("verify social icons in footer");
		sa.assertTrue(gVar.assertVisible("Footer_Social_Share", "Shopnav\\footer.properties"),"Verify social sharing links");

		log.info("verify FaceBook Icon");
		sa.assertTrue(gVar.assertVisible("Footer_FB_Icon", "Shopnav\\footer.properties"),"Verify the FB icon");

		log.info("verify Twitter Icon");
		sa.assertTrue(gVar.assertVisible("Footer_Twitter_Icon", "Shopnav\\footer.properties"),"verify the twiter icon");

		log.info("verify Instagram Icon");
		sa.assertTrue(gVar.assertVisible("Footer_Instagram_Icon", "Shopnav\\footer.properties"),"Verify the Instagram icon");
		
		log.info("verify Linked In Icon");
		sa.assertTrue(gVar.assertVisible("Footer_LinkedIn_Icon", "Shopnav\\footer.properties"),"Verify the Linked icon");

		sa.assertAll();
	}
	
	/*@Test(groups={"reg"}, description="DMED-039 DRM-330")
	public void TC01_verifySocialLinksInFooterSection() throws Exception
	{
		Thread.sleep(5000);
		log.info("Collect the all Social Sharing links");
		List<WebElement> allLinks = l1.getWebElements("Footer_SocialShare_Links", "Shopnav\\footer.properties");
		List<WebElement> socialIcons = l1.getWebElements("Footer_SocialShare_Icons", "Shopnav\\footer.properties");
		
		log.info("allLinks.size():- " + allLinks.size());
		
		log.info("Click on social sharing links and verify the navigation");
		for (int i = 0; i < allLinks.size(); i++)
		{
			log.info("LOOP:- "+ i);
			allLinks = l1.getWebElements("Footer_SocialShare_Links", "Shopnav\\footer.properties");
			socialIcons = l1.getWebElements("Footer_SocialShare_Icons", "Shopnav\\footer.properties");
			String linkName = socialIcons.get(i).getAttribute("title");
			int rowNum = i+1;
			String expectedLink = GetData.getDataFromExcel("//data//GenericData_US.xls", "Social_icons", rowNum, 3).toLowerCase();
			gVar.assertequalsIgnoreCase(linkName, expectedLink,"Verify the link name - LOOP:- "+i);
			
			log.info("Click on The link");
			allLinks.get(i).sendKeys(Keys.ENTER);
			Thread.sleep(4000);
			//On click on Social Sharing link, a new tab will open. Use WindowHandles
			log.info("Change the controller to newley opened tab");
			Set<String> tabs = driver.getWindowHandles();
			Iterator<String> itr = tabs.iterator();
			
			log.info("Collect the window identifiers");
			String parentWindow = itr.next();
			String chieldWindow = itr.next();
			
			log.info("Switch to the child window");
			driver.switchTo().window(chieldWindow);
			Thread.sleep(2000);
			log.info("Verify the title");
			String windowTitle = driver.getTitle().toLowerCase();
			log.info("windowTitle:- "+ windowTitle);
			sa.assertTrue(windowTitle.contains(expectedLink),"Verify the title - Loop:- "+i);
			String drive = "Drive";
			sa.assertTrue(windowTitle.toLowerCase().contains(drive.toLowerCase()),"Verify the site name title - :Loop:- "+ i);
			
			log.info("Verify the url");
			String url = driver.getCurrentUrl().toLowerCase();
			log.info("url:- "+ url);
			sa.assertTrue(url.contains(expectedLink),"Verify the URL");
			log.info("Close the window");
			driver.close();
			
			//Switch the Controller
			driver.switchTo().window(parentWindow);
		}
		
		sa.assertAll();
	}*/
	
	
	@Test(groups={"reg"}, description="extraTc-Verify links in footer")
	public void TC02_verifyFooterLinks_GuestUser() throws Exception
	{
		
		verifyLinksInFooter("guest");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-Verify links in footer")
	public void TC03_verifyFooterLinks_RegisterUser(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		log.info("Login with valid credentials");
		p.logIn(xmlTest);
		
		verifyLinksInFooter("register");
		sa.assertAll();
	}

	void verifyLinksInFooter(String user) throws Exception
	{
		log.info("Verify the links");
		List<WebElement> allLinks = l1.getWebElements("FooterContainer", "Shopnav\\footer.properties");
		List<WebElement> allLinksPlusIcon = l1.getWebElements("FooterContainer_PlusIcon", "Shopnav\\footer.properties");
		
		log.info("allLinks size:- "+ allLinks.size());
		//Not verifying links under last heading becouse there is no links under last heading
		for (int i = 0; i < allLinks.size()-1; i++)
		{
			int rowNum = i+1;
			log.info("LOOP i:- "+i);
			allLinks = l1.getWebElements("FooterContainer", "Shopnav\\footer.properties");
			String heading = allLinks.get(i).findElement(l1.getByReference("FooterContainer_Heading", "Shopnav\\footer.properties")).getText();
			if (gVar.mobileView()||gVar.tabletView())
			{
				heading = heading.replace("+", "").trim();
			}
			log.info("heading:- "+ heading);
			log.info("Verify heading");
			sa.assertEquals(heading, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Footer", rowNum, 0),"Verify the headings");
			List<WebElement> footerLinks;
			if (gVar.mobileView()|| gVar.tabletView())
			{
				log.info("Click on Heading");
				Thread.sleep(7000);
//				allLinks.get(i).click();	
				allLinksPlusIcon.get(i).click();	//Comment this line and Uncomment above line if it fails in Andriod Mobile and Tab view
				
				log.info("Wait for Section to expand");
				gVar.waitForElement("FooterContainer_Expanded_Mobile", "Shopnav\\footer.properties", 5000);
				log.info("Verify the expanded link in mobile view");
				sa.assertEquals(gVar.assertEqual("FooterContainer_Expanded_Mobile", "Shopnav\\footer.properties").replace("-", "").trim(), 
						heading,"Verify the expanded heading");
				
				log.info("Verify the links under each heading");
				footerLinks = l1.getWebElements("FooterContainer_Links_Mobile", "Shopnav\\footer.properties");
			}
			else 
			{
				log.info("Verify the links under each heading");
				footerLinks = allLinks.get(i).findElements(l1.getByReference("FooterContainer_Links", "Shopnav\\footer.properties"));	
			}
			
			log.info("Footer links under "+ heading + " heading:- ");
			log.info("Number of links:- "+ footerLinks.size());
			
			for (int j = 0; j < footerLinks.size(); j++)
			{
				log.info("LOOP J:-"+ j);
				int linksrowNum = j+1;
				//For Register and guest users, links under my account headings are different.
				if (i==0 && user.equalsIgnoreCase("register"))
				{
					rowNum=5;
				}
				String footerLinkName = footerLinks.get(j).getText();
				log.info("footerLinkName:- " + footerLinkName);
				sa.assertEquals(footerLinkName, 
						GetData.getDataFromExcel("//data//GenericData_US.xls", "Footer", linksrowNum, rowNum),"Verify the Links");
			}
			
		}
	}
}
