package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Hcpcs_Pdp extends BaseTest
{
	String pdpProperties = "Shopnav//PDP.properties";
	
	@Test (groups={"reg"}, description="DMED-050 DRM-645")
	public void TC00_verifyHCPCsCodeForBaseProduct() throws Exception
	{
		String productId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 7);
		log.info("Navigate to PDP for which is configured for HCPCs code");
		s.navigateToPDP_Search(productId);
		
		String exptHcpcsCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 8);
		log.info("exptHcpcsCode:- "+exptHcpcsCode);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("HCPCS drawer should be opened bydefault");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		
		log.info("Verify the HCpcs code in HCPCS drawer");
		sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties").replace(" ", ""), 
				exptHcpcsCode.replace(" ", ""),"Verify HCPCs code in PDP");
		Thread.sleep(3000);
		log.info("Click on Product Options link");
		l1.getWebElement("PDP_ProductOptions_link", "Shopnav//PDP.properties").click();
		sa.assertTrue(gVar.assertVisible("PDP_Expanded_ProductOptions_link", "Shopnav//PDP.properties"),"verify the Expanded section");
		
		log.info("Count the HCPCs rows");
		 List<WebElement> hcpcsElements = l1.getWebElements("PDP_ProductOptionTable_HcpcsRows", "Shopnav//PDP.properties");
		 log.info("Number of HCPCs rows in table:- " + hcpcsElements.size());
		 
		 log.info("Verify the HCPCs code for all variants");
		 for (int i = 0; i < hcpcsElements.size(); i++) 
		 {
			log.info("LOOP:- " +i);
			String actHCPCsCode = hcpcsElements.get(i).getText();
			log.info("HCPCS code in product details table:- " + actHCPCsCode);
			gVar.assertequalsIgnoreCase(actHCPCsCode, exptHcpcsCode, "Verify the HCPCS code in product details table");
		 }
		 sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-050 DRM-646")
	public void TC01_hcpcsCodeNotConfiguredForBaseProduct() throws Exception
	{
		log.info("Navigate to PDP for which HCPCs code is configured");
		String productId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 4, 7);
		s.navigateToPDP_Search(productId);
		
		log.info("Expand HCPCs drawer");
		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("verify HCPCS drawer");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
	
		log.info("Hcpcs code should not be displayed in HCPCS drawer");
		sa.assertTrue(gVar.assertNotVisible("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), "HCPCS code should not be displayed drawer");
		
		sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Drawer_Body", "Shopnav//PDP.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 4, 11),"No Results Found");
		sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Body", "Shopnav//PDP.properties","aria-expanded"), 
				"true","Verify the HCPCS tab opened or not");
		
		sa.assertAll();
	}
	@Test(groups={"reg"}, description="DMED-050 DRM-647 DRM-648")
	public void TC02_hcpcsCodeForVariantProduct() throws Exception
	{
		log.info("Navigate to variant product PDP for which HCPCs code is configured");
		String productId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 7);
		s.navigateToPDP_Search(productId);
		
		String exptHcpcsCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 8);
		log.info("exptHcpcsCode:- "+exptHcpcsCode);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		
		log.info("For Base product PDP, HCPCS code should not be displayed");
		sa.assertTrue(gVar.assertNotVisible("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"),"HCPCS code should not be displayed when user in Base product PDP");
		sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Drawer_Body", "Shopnav//PDP.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 4, 11),"No Results Found");
		
		log.info("Navigate to variant product pdp");
		String variantProductId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 9);
		s.pdpNavigationThroughURL(variantProductId);
		Thread.sleep(2000);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("HCPCS drawer should not be opened bydefault when HCPCS code is not configured");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		
		log.info("Verify the HCPCs code in HCPCS drawer when user in variant product PDP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), 
				exptHcpcsCode,"Verify HCPCs code in PDP");
		
		log.info("Click on Product Options link");
		l1.getWebElement("PDP_ProductOptions_link", "Shopnav//PDP.properties").click();
		sa.assertTrue(gVar.assertVisible("PDP_Expanded_ProductOptions_link", "Shopnav//PDP.properties"),"verify the Expanded section");
		
		log.info("Verify the Hcpcs code for varient product");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductOptionTable_HcpcsRows", "Shopnav//PDP.properties"), 
				exptHcpcsCode,"Verify the HCPCs code in product details table for variant");
		
		//******************************
		log.info("Count the HCPCs rows");
		 List<WebElement> hcpcsElements = l1.getWebElements("PDP_ProductOptionTable_HcpcsRows", "Shopnav//PDP.properties");
		 log.info("Number of HCPCs rows in table:- " + hcpcsElements.size());
		 
		 log.info("Verify the HCPCs code for all variants");
		int numberOfHcpcsCode = l1.getWebElements("PDP_ProductOptionTable_HcpcsRows_HcpcsCode", "Shopnav//PDP.properties").size();
		 sa.assertEquals(numberOfHcpcsCode, 1,"Only one hcpcs code should be displayed in table");
		 sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-050 DRM-657")
	public void TC03_verifySearchFunctionalityForHcpcsCode() throws Exception
	{
		log.info("Search the unique HCPCs code which configured for only one product variant.");
		String uniqueHcpcsCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 8);
		log.info("exptHcpcsCode:- "+uniqueHcpcsCode);
		s.searchProduct(uniqueHcpcsCode);
		Thread.sleep(10000);
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		String variantProductName = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 2, 10);
		log.info("variantProductName:-" + variantProductName);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				variantProductName, "Verify the Product name in PDP");
		
		log.info("Verify the HCpcs code in HCPCS drawer");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), 
				uniqueHcpcsCode,"Verify HCPCs code in PDP");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description ="DMED-050 DRM-650")
	public void TC04_searchHcpcsCodeWhichIsAssociatedFordifferentVariantOfSameProduct() throws Exception
	{
		log.info("Search HCPCs code which is configured for different variant of Same product");
		String hcpcsCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 3, 8);
		log.info("exptHcpcsCode:- "+hcpcsCode);
		s.searchProduct(hcpcsCode);
		Thread.sleep(10000);
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		String variantProductName = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 3, 10);
		log.info("variantProductName:-" + variantProductName);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				variantProductName, "Verify the Product name in PDP");
		
		log.info("Verify the HCpcs code in HCPCS drawer");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), 
				hcpcsCode,"Verify HCPCs code in PDP");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-050 DRM-654")
	public void TC05_verifyTheNavigationOfPdpBySearchingHcpcsCode() throws Exception
	{
		log.info("Search a base product which is configured for HCPCs code");
		String searchKey = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 8, 8);
		log.info("exptHcpcsCode:- "+searchKey);
		s.searchProduct(searchKey);
		Thread.sleep(10000);
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		String variantProductName = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 8, 10);
		log.info("variantProductName:-" + variantProductName);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				variantProductName, "Verify the Product name in PDP");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-050 DRM-649 DRM-651 DRM-652 DRM-653 DRM-655")
	public void TC06_verifySearchResultPageForHcpcsCode() throws Exception
	{
		log.info("Search HCPCs code which is assigned for two different product");
		String uniqueHcpcsCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 5, 8);
		log.info("exptHcpcsCode:- "+uniqueHcpcsCode);
		s.searchProduct(uniqueHcpcsCode);
		Thread.sleep(2000);
		log.info("Verify the search result page");
		sa.assertTrue(gVar.assertVisible("Search_Grid_Div", "Shopnav//SearchResultPage.properties"),"Verify the searched products section");

		sa.assertTrue(gVar.assertVisible("Search_ItemPerPage_DD", "Shopnav//SearchResultPage.properties"),"Verify the products per page count section");
		sa.assertTrue(gVar.assertVisible("SearchResult_SortBy_Dropdown", "Shopnav//SearchResultPage.properties"),"Verify Sort by dropdown");
		
//		log.info("Search heading");
//		String exptHeading = "Search Results For: \""+ uniqueHcpcsCode +"\"";
//		gVar.assertequalsIgnoreCase(gVar.assertEqual("SearchResult_PageHeading", "Shopnav//SearchResultPage.properties"), 
//				exptHeading,"Verify the search heading");
		
//		log.info("Verify the total product count");
//		gVar.assertequalsIgnoreCase(gVar.assertEqual("Search_Tot_Prods", "Shopnav//SearchResultPage.properties").replaceAll("[^-?0-9]+", ""), 
//				"3", "Verify the searched product count in search result page");
//		
//		log.info("Verify the number of product in search result page");
//		List<WebElement> numberOfProducts = l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties");
//		log.info("Number of products in search result page:-  "+ numberOfProducts.size());
//		sa.assertEquals(numberOfProducts.size(), 3);
		List<WebElement> allNames = l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		log.info("Click on each product in Search result page and verify the HCPCS code in PDP");
		for (int i = 0; i < 3; i++)
		{
			log.info("LOOP:- "+ i);
			allNames = l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
			allNames.get(i).click();
			Thread.sleep(2000);
			log.info("Verify the HCPCS code in PDP");
			sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties").replace(" ", ""), 
					uniqueHcpcsCode.replace(" ", ""),"Verify HCPCs code in PDP");
			
			log.info("Navigate back to search result page");
			driver.navigate().back();
			Thread.sleep(3000);
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description ="DMED-050 DRM-656")
	public void TC07_SearchResultHCPCSCodeFacet() throws Exception
	{
		sa.assertTrue(false,"Ask Arjun Script");
		
		sa.assertAll();
	}

	
}
