package com.drivemedical.shopnav;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.drivemedical.projectspec.GlobalFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.Locators;

public class ProductQuestinier extends BaseTest 
{
	
	@Test(groups={"reg"}, description="DMED-516 DRM-698,699,700,701")
	public void TC00_verifyNav_UI() throws Exception
	{

		log.info("Click on CONTACT US link in footer");
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Expand Customer Service section");
			l1.getWebElement("Footer_CustomerServiceLink_Mobile", "Shopnav//footer.properties").click();
		}
		l1.getWebElement("Footer_ContactUs", "Shopnav//footer.properties").click();
		log.info("Click on SUBMIT AN IDEA button (navigate to product questonier form)");
		l1.getWebElement("ContactUs_SubmitAnIdeaButton", "Shopnav//ContactUs.properties").click();

		log.info("load product questonier properties file");
		Properties p=new Properties();
		p.load(new FileInputStream(new File(System.getProperty("user.dir")+"/POM/Shopnav/ProductQuestonier.properties")));
		Iterator<Object> i=p.keySet().iterator();

		while (i.hasNext()) 
		{
			log.info("fetch property name");
			String str1=(String)i.next();

			List<WebElement> ele=l1.getWebElements(str1, "/Shopnav/ProductQuestonier.properties");

			if(ele.size()==0) 
			{
				sa.assertTrue(gVar.assertVisible(str1, "/Shopnav/ProductQuestonier.properties"),str1);
				log.info("element sixze is zero" + str1);
			} 
			else 
			{
				for(int a=0;a<ele.size();a++) 
				{
					sa.assertTrue(gVar.assertVisible(str1, "/Shopnav/ProductQuestonier.properties",a),str1);
					log.info("element sixze is not zero" + str1);
				}
			}
		}
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="extraTC-Verify the pre-populated date")
	public void TC01_verifyPrepopulatedDate() throws Exception
	{
		log.info("Fetch the system date");
		String systemDate = gVar.getSystemDate("MMM d, yyyy");
		log.info("systemDate:- "+ systemDate);
		sa.assertEquals(gVar.assertEqual("PQ_Date", "Shopnav//ProductQuestonier.properties","value"), 
				systemDate,"Verify the prepoluted date");
		
		String submissionDate = l1.getWebElement("PQ_SubmissionDate", "Shopnav//ProductQuestonier.properties").getText();
		log.info("submissionDate:- "+ submissionDate);
		submissionDate = submissionDate.split(":")[1].trim();
		log.info("submissionDate:- "+ submissionDate);
		
		sa.assertEquals(submissionDate, systemDate,"Verify the submission Date");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-516 DRM-702,703,704,705,706")
	public void TC02_productSubmit() throws Exception
	{
		log.info("enter name");
		l1.getWebElement("PQ_Name_Filed", "/Shopnav/ProductQuestonier.properties").sendKeys("test");
		log.info("enter title");
		l1.getWebElement("PQ_Title", "/Shopnav/ProductQuestonier.properties").sendKeys("test");
		log.info("enter name below");
		l1.getWebElement("PQ_Name", "/Shopnav/ProductQuestonier.properties").sendKeys("test");
		log.info("tap on submit");
		l1.getWebElement("PQ_Submit", "/Shopnav/ProductQuestonier.properties").click();
		log.info("verify submision");
		String style = gVar.assertEqual("PQ_PopupContent", "Shopnav//ProductQuestonier.properties","style");
		sa.assertTrue(style.contains("display: block;"),"Verify the style of the popup");

		sa.assertTrue(gVar.assertVisible("PQ_popup", "Shopnav//ProductQuestonier.properties"),"Verify the popup");
		sa.assertEquals(gVar.assertEqual("PQ_Popup_Heading", "Shopnav//ProductQuestonier.properties"), 
				"Product Submission","VErify the Heading");
		log.info("Verify the Success icon");
		sa.assertTrue(gVar.assertVisible("PQ_Popup_SuccessfullIcon", "Shopnav//ProductQuestonier.properties"),"Verify the Success icon");
		sa.assertTrue(gVar.assertVisible("PQ_Popup_CloseButton", "Shopnav//ProductQuestonier.properties"),"Verify the Close Button");
		sa.assertTrue(gVar.assertVisible("PQ_Popup_CloseIcon", "Shopnav//ProductQuestonier.properties"),"Verify the Close Icon");

		
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="extraTC-Verify close functionality of popup")
	public void TC03_verifyCloseFunctionality() throws Exception
	{
		log.info("Click on Close button");
		l1.getWebElement("PQ_Popup_CloseButton", "Shopnav//ProductQuestonier.properties").click();
		
		log.info("Popup should be closed");
		sa.assertEquals(gVar.assertEqual("PQ_PopupContent", "Shopnav//ProductQuestonier.properties","style"), 
				"display: none;", "Verify the style of the popup");
		
		log.info("Enter valid details");
		log.info("enter name");
		l1.getWebElement("PQ_Name_Filed", "/Shopnav/ProductQuestonier.properties").sendKeys("test");
		log.info("enter title");
		l1.getWebElement("PQ_Title", "/Shopnav/ProductQuestonier.properties").sendKeys("test");
		log.info("enter name below");
		l1.getWebElement("PQ_Name", "/Shopnav/ProductQuestonier.properties").sendKeys("test");
		log.info("tap on submit");
		l1.getWebElement("PQ_Submit", "/Shopnav/ProductQuestonier.properties").click();
		log.info("verify submision");
		String style = gVar.assertEqual("PQ_PopupContent", "Shopnav//ProductQuestonier.properties","style");
		sa.assertTrue(style.contains("display: block;"),"Verify the style of the popup");
		
		log.info("Click on Close icon");
		l1.getWebElement("PQ_Popup_CloseIcon", "Shopnav//ProductQuestonier.properties").click();
		
		log.info("Popup should be closed");
		sa.assertEquals(gVar.assertEqual("PQ_PopupContent", "Shopnav//ProductQuestonier.properties","style"), 
				"display: none;", "Popup should be closed");
		
		sa.assertAll();
	}
	
}
