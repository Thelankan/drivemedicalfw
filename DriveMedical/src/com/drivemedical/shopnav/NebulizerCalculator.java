package com.drivemedical.shopnav;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class NebulizerCalculator extends BaseTest
{
	@Test(groups={"reg"}, description="DMED-028 DRM-1519")
	public void TC00_verifyNebulizerCalculator() throws Exception
	{
		log.info("Navigate to homecare Providers Page");
		s.homecareProvidersPage();
		verifyTheIframeUI();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-028 DRM-1520")
	public void TC01_verifyNebulizerCalculatorFunctionality() throws Exception
	{
		log.info("Verify the Nebulizer Calculator Functionality");
		checkFunctionality();
		log.info("Switch the control to browser");
		driver.switchTo().defaultContent();
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-028 DRM-1521")
	public void TC02_verifyTheUIandFunctionalityOfNebulizerCalculator() throws Exception
	{
		log.info("NAvigate to Login page");
		p.navigateToLoginPage();
		log.info("LOgin with valid credentials");
		p.logIn(BaseTest.xmlTest);
		
		log.info("Navigate to homecare Providers Page");
		s.homecareProvidersPage();
		log.info("Verify the UI");
		verifyTheIframeUI();
		
		log.info("Verify the Functionality");
		checkFunctionality();
		
		log.info("Switch the control to browser");
		driver.switchTo().defaultContent();
		
		sa.assertAll();
	}
	
	
	void verifyTheIframeUI() throws Exception
	{
		log.info("Verify the Nebulizer Calculator iframe");
		sa.assertTrue(gVar.assertVisible("PLP_NebulizerCalculator", "Shopnav//PLP.properties"),"Verify the Nebulizer Calculator iframe");
		
		log.info("Switch the controller to iframe");
		WebElement calculaterIframeElement = l1.getWebElement("PLP_NebulizerCalculator", "Shopnav//PLP.properties");
		driver.switchTo().frame(calculaterIframeElement);
		
		log.info("Verify the iframe Heading");
		sa.assertEquals(gVar.assertEqual("PLP_NebulizerCalculatorHeading", "Shopnav//PLP.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "PLP", 1, 1),"Verify the calculator iframe heading");
		
		log.info("Verify the Annual Number of Patients inbox");
		sa.assertTrue(gVar.assertVisible("PLP_NebulizerCalculator_AnnualNumberOfPatientsBox", "Shopnav//PLP.properties"),"Verify the Annual Number of Patients inbox");
		
		log.info("Verify the Annual Growth(in percent)");
		sa.assertTrue(gVar.assertVisible("PLP_NebulizerCalculator_AnnualGrowthBox", "Shopnav//PLP.properties"),"Verify the Annual Growth text box");
		
		log.info("Verify the submit button");
		sa.assertTrue(gVar.assertVisible("PLP_NebulizerCalculator_SubmitButton", "Shopnav//PLP.properties"),"Verify the submit button");
	}
	
	
	void checkFunctionality() throws Exception
	{
		Thread.sleep(5000);
		String num1 = "420";
		String num2 = "42";
		log.info("Enter the number in Annual Number of Patients inbox");
		l1.getWebElement("PLP_NebulizerCalculator_AnnualNumberOfPatientsBox","Shopnav//PLP.properties").clear();
		l1.getWebElement("PLP_NebulizerCalculator_AnnualNumberOfPatientsBox","Shopnav//PLP.properties").sendKeys(num1);
		
		log.info("Enter the number in Annual Growth(in percent)");
		l1.getWebElement("PLP_NebulizerCalculator_AnnualGrowthBox", "Shopnav//PLP.properties").clear();
		l1.getWebElement("PLP_NebulizerCalculator_AnnualGrowthBox", "Shopnav//PLP.properties").sendKeys(num2);
		
		log.info("Click on Submit button");
		l1.getWebElement("PLP_NebulizerCalculator_SubmitButton", "Shopnav//PLP.properties").click();
		l1.getWebElement("PLP_NebulizerCalculator_SubmitButton1", "Shopnav//PLP.properties").click();
		
		
		log.info("Verify the Expanded section");
		String styleOfExpandedSection = gVar.assertEqual("PLP_NebulizerCalculator_ExpandedSection", "Shopnav//PLP.properties", "style");
		log.info("styleOfExpandedSection:- "+ styleOfExpandedSection);
		sa.assertTrue(styleOfExpandedSection.contains("display: block"),"Verify the Style");
	}
}
