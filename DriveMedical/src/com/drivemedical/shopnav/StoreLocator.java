package com.drivemedical.shopnav;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;


//not possible to script test cases - DRM-820

//Possible test cases are 

public class StoreLocator extends BaseTest{

	@Test(groups="reg",description="DMED-085 DRM-807,808")
	public void TC00_storeLocNav() throws Exception
	{
		log.info("Click on Store Locator link from header");
		l1.getWebElement("Header_StroeLoc_Link", "//Shopnav//header.properties").click();
		log.info("Tap on locate provider link");
		l1.getWebElement("Header_LocateProvide_Link", "//Shopnav//header.properties").click();
		log.info("Verify navigation to Store Locator Page");
		sa.assertTrue(gVar.assertVisible("StoreFinder_Heading", "//Shopnav//StoreFinder.properties"),"locate provider heading");
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-085 DRM-809,810,811")
	public void TC01_verifyUI() throws Exception
	{
		log.info("Verify locate provider heading");
		sa.assertTrue(gVar.assertVisible("StoreFinder_Heading", "//Shopnav//StoreFinder.properties"),"locate provider heading");
		log.info("search box");
		sa.assertTrue(gVar.assertVisible("StoreFinder_Textbox", "//Shopnav//StoreFinder.properties"),"locate provider heading");
		log.info("text");
		sa.assertTrue(gVar.assertVisible("LP_text", "//Shopnav//StoreFinder.properties"),"locate provider heading");
		log.info("find store button");
		sa.assertTrue(gVar.assertVisible("LP_findstore_Button", "//Shopnav//StoreFinder.properties"),"locate provider heading");
		log.info("locate near me button");
		sa.assertTrue(gVar.assertVisible("LP_locatenearme_button", "//Shopnav//StoreFinder.properties"),"locate provider heading");
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-085 DRM-812,818")
	public void TC02_citysearch() throws Exception
	{
		log.info("enter city name");
		l1.getWebElement("StoreFinder_Textbox", "//Shopnav//StoreFinder.properties").sendKeys("Illinois");
		log.info("click on find store button");
		l1.getWebElement("LP_findstore_Button", "//Shopnav//StoreFinder.properties").click();
		
		sa.assertTrue(false);//issue exist in application
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-085 DRM-816,819")
	public void TC03_zipsearch() throws Exception
	{
		log.info("enter city name");
		l1.getWebElement("StoreFinder_Textbox", "//Shopnav//StoreFinder.properties").sendKeys("60707");
		log.info("click on find store button");
		l1.getWebElement("LP_findstore_Button", "//Shopnav//StoreFinder.properties").click();
		
		sa.assertTrue(false);//issue exist in application
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-085 DRM-817")
	public void TC04_invalidzipsearch() throws Exception
	{
		log.info("enter city name");
		l1.getWebElement("StoreFinder_Textbox", "//Shopnav//StoreFinder.properties").sendKeys("12345");
		log.info("click on find store button");
		l1.getWebElement("LP_findstore_Button", "//Shopnav//StoreFinder.properties").click();
		
		sa.assertTrue(false);//issue exist in application
		sa.assertAll();
	}
	
}
