package com.drivemedical.shopnav;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ContactUs extends BaseTest
{
	String contactUsProperties = "Shopnav//ContactUs.properties";
	static String parentWin;
	static String childWin;
	
	@Test(groups={"reg"},description="DMED-026 DRM-1314 DMED-512 DRM-881")
	public void TC00_navToContactUsFromFooter() throws Exception
	{
		log.info("TC00_navToContactUsFromFooter STARTED");
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Expand Customer Service section in mobile view");
			l1.getWebElement("Footer_CustomerServiceLink_Mobile", "Shopnav//footer.properties").click();
		}

		log.info("click on contact us link from footer");
		l1.getWebElement("Footer_ContactUs", "Shopnav//footer.properties").click();
		Thread.sleep(7000);
		//BEFORE CONTACT US is OPENING IN NEW TAB. BUT NOW ITS OPENING IN SAME WINDOW hence commenting below lines
		/*log.info("fetch new window id");
		Set<String> windows=driver.getWindowHandles();
		log.info("windows:- "+ windows);
		log.info("1");
		Iterator<String> itr=windows.iterator();
		parentWin=itr.next();
		log.info("parentWin:- "+ parentWin);
		childWin=itr.next();
		log.info("childWin:- "+ childWin);
		log.info("swicth to child window");
		driver.switchTo().window(childWin);*/
		
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(4000);
			log.info("Handling SSL certificate in IE browser");
			driver.navigate().to("javascript:document.getElementById('overridelink').click()");
			driver.manage().window().setSize(new Dimension(800, 800));
			driver.manage().window().maximize();
		}
		
		log.info("verify navigation");
		sa.assertTrue(gVar.assertVisible("ContactUs_Heading", "Shopnav//ContactUs.properties"),"contact us heading");
		String headingText = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 1, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ContactUs_Heading", contactUsProperties), 
				headingText,"Verify the page heading text");
		
		log.info("Verify the breadcrumb");
		String contactUsText = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 8, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				contactUsText,"Verify the active breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"Home "+contactUsText,"Verify the full breadcrumb");
		
		log.info("TC00_navToContactUsFromFooter ENDED");
		sa.assertAll();
	}
	@Test(groups={"reg"},description="DMED-026 DRM-1314,DRM-1315,DRM-1316 DMED-512 DRM-882 DRM-883 DRM-884")
	public void TC01_verifyUI() throws Exception
	{
		log.info("TC01_verifyUI STARTED");
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Verify search box");
			sa.assertTrue(gVar.assertVisible("ContactUs_Mob_searchbox", "Shopnav//ContactUs.properties"),"search box");
			log.info("Verify search button");
			sa.assertTrue(gVar.assertVisible("ContactUs_Mob_SearchHelp_Btn", "Shopnav//ContactUs.properties"),"search button");
		}
		else
		{
			log.info("Verify search box");
			sa.assertTrue(gVar.assertVisible("ContactUs_Desktop_Searchbox", "Shopnav//ContactUs.properties"),"search box");
			log.info("Verify search button");
			sa.assertTrue(gVar.assertVisible("ContactUs_SearchHelp_Btn", "Shopnav//ContactUs.properties"),"search button");
		}
		log.info("Verify product support row");
		sa.assertTrue(gVar.assertVisible("ContactUs_ProductSupport_Row", "Shopnav//ContactUs.properties"),"product support row");
		log.info("Verify support heading");
		sa.assertTrue(gVar.assertVisible("ContactUs_ForSupport_Heading", "Shopnav//ContactUs.properties"),"support heading");
		String supportHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 2, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ContactUs_ForSupport_Heading", "Shopnav//ContactUs.properties"), 
				supportHeading,"Contact us for support heading");
		log.info("Verify contact us details");
		sa.assertTrue(gVar.assertVisible("ContactUs_details", "Shopnav//ContactUs.properties"),"contact us details");
		log.info("Verify contact us support div");
		sa.assertTrue(gVar.assertVisible("ContactUs_Support_Div", "Shopnav//ContactUs.properties"),"contact us support div");
		log.info("Verify department info");
		String departmentInfoHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 3, 0);
		sa.assertTrue(gVar.assertVisible("ContactUs_Department_Info", "Shopnav//ContactUs.properties"),"department info");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ContactUs_Department_Info", contactUsProperties), 
				departmentInfoHeading,"department info heading text");
		log.info("Verify department info div");
		sa.assertTrue(gVar.assertVisible("ContactUs_Department_Div", "Shopnav//ContactUs.properties"),"department info div");
		
		if (gVar.desktopView())
		{
			log.info("Verify the subheadings in Contact us for support section");
			List<WebElement> supportSubHeadings = l1.getWebElements("ContactUs_SupportSection_SubHeadings", contactUsProperties);
			log.info("supportSubHeadings size:- "+ supportSubHeadings.size());
			
			int rowNum = 4;
			for (int i = 0; i < supportSubHeadings.size(); i++)
			{
				log.info("LOOP:- "+i);
				String exptdHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", rowNum, 0);
				gVar.assertequalsIgnoreCase(supportSubHeadings.get(i).getText(), exptdHeading,"Verify the subheadings in Support section");
				rowNum++;
			}
		}
		
		log.info("Email US button");
		sa.assertTrue(gVar.assertVisible("ContactUs_EmainUsButton", contactUsProperties),"Verify the Email US button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ContactUs_EmainUsButton", contactUsProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 1, 1),"Verify the Email US button text");
		
		
		log.info("CHAT NOW button");
		sa.assertTrue(gVar.assertVisible("ContactUs_ChatNowButton", contactUsProperties),"Verify the CHAT NOW button");
		
		log.info("SUBMIT AN IDEA button");
		sa.assertTrue(gVar.assertVisible("ContactUs_SubmitAnIdeaButton", contactUsProperties),"Verify the SUBMIT AN IDEA button");
		
		
		log.info("TC01_verifyUI ENDED");		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-512 DRM-885")
	public void TC02_ContactUsBloomFireContentSearch() throws Exception
	{
		log.info("TC02_ContactUsBloomFireContentSearch STARTED");
		log.info("Enter Search keyword in search box");
		String searchKey = "wheelchair";
		if (gVar.mobileView()||gVar.tabletView())
		{
			l1.getWebElement("ContactUs_Mob_searchbox", "Shopnav//ContactUs.properties").sendKeys(searchKey);
			l1.getWebElement("ContactUs_Mob_SearchHelp_Btn", "Shopnav//ContactUs.properties").click();
		}
		else
		{
			l1.getWebElement("ContactUs_Desktop_Searchbox", "Shopnav//ContactUs.properties").sendKeys(searchKey);
			l1.getWebElement("ContactUs_SearchHelp_Btn", "Shopnav//ContactUs.properties").click();
		}
		
		Thread.sleep(5000);
		log.info("Verify the search keyword in search results section");
		List<WebElement> searchResultQuestions = l1.getWebElements("ContactUs_SearchResultSection", "Shopnav//ContactUs.properties");
		log.info("searchResultQuestions.size():- "+ searchResultQuestions.size());
		for (int i = 0; i < searchResultQuestions.size(); i++)
		{
			log.info("LOOP:- "+i);
			String searchResultQuestion = searchResultQuestions.get(i).getText().toLowerCase();
			log.info("searchResultQuestion:- "+ searchResultQuestion);
			
			log.info("Verify the search keword in questions");
			sa.assertTrue(searchResultQuestion.contains(searchKey),"verify the search keyword in questions-LOOP:- "+i);
			
		}
		
		log.info("VErify the search keyword in search box");
		sa.assertEquals(gVar.assertEqual("ContactUs_Desktop_Searchbox", "Shopnav//ContactUs.properties","value"), 
				searchKey,"VErify the search keyword in search box");
		
		log.info("TC02_ContactUsBloomFireContentSearch ENDED");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-026 DRM-1320")
	public void TC03_uiOfContactUsForm() throws Exception
	{
		log.info("TC03_uiOfContactUsForm STARTED");
		
		log.info("Click on Email Us button");
		l1.getWebElement("ContactUs_EmainUsButton", contactUsProperties).click();
		
		log.info("Verify the whether the popwindow is visible or not");
		sa.assertTrue(gVar.assertEqual("ContactUs_Form", contactUsProperties, "style").contains("display: block;"),"Verify the contact us form");
		log.info("Verify the popup heading");
		sa.assertEquals(gVar.assertEqual("ContactUs_Form_Heading", contactUsProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 7, 0),"Verify the popup heading");
		
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_CloseButton", contactUsProperties),"Verify the poup close button");
		
		log.info("Switch the driver controller to contactUs form frame");
		WebElement IframeForm = l1.getWebElement("ContactUs_IframeForm", contactUsProperties);
		driver.switchTo().frame(IframeForm);
		
		log.info("Verify the labels");
		List<WebElement> allLabels = l1.getWebElements("ContactUs_IframeForm_Labels", contactUsProperties);
		for (int i = 0; i < allLabels.size()-1; i++)
		{
			int rowNum = i+1;
			log.info("LOOP:- "+ rowNum);
			gVar.assertequalsIgnoreCase(allLabels.get(i).getText(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", rowNum, 2),"Verify the labels --Loop:- "+i);
		}
		log.info("Verify the depertment select box");
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_DepartmentSelectBox", contactUsProperties),"Verify the department select box");
		
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_FirstNameBox", contactUsProperties),"Verify the First name textbox");
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_LastNameBox", contactUsProperties),"Verify the Last name textbox");
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_EmailBox", contactUsProperties),"Verify the Email textbox");
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_PhoneBox", contactUsProperties),"Verify the Phone number textbox");
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_CommentsBox", contactUsProperties),"Verify the Comments textbox");

		sa.assertTrue(gVar.assertVisible("ContactUs_Form_SubmitButton", contactUsProperties),"Verify the Submit button");
		
		log.info("TC03_uiOfContactUsForm ENDED");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-026 DRM-1318")
	public void TC04_submitFormGuestUser() throws Exception
	{
		log.info("TC04_submitFormGuestUser STARTED");
		
		enterDetailsAndVerifySuccessMsg();
		
		log.info("Change the driver controller from Frame to browser tab");
		driver.switchTo().defaultContent();
		
		log.info("Close the popup");
		l1.getWebElement("ContactUs_Form_CloseButton", contactUsProperties).click();
		log.info("Popup should close");
		sa.assertTrue(gVar.assertEqual("ContactUs_Form", contactUsProperties, "style").contains("display: none;"),"ContactUs form should be closed");
		
//		log.info("Close the child tab");
//		driver.close();
//		log.info("Switch driver controller to parent window");
//		driver.switchTo().window(parentWin);
		
		log.info("TC04_submitFormGuestUser ENDED");
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"}, description="DMED-026 DRM-1319")
	public void TC05_submitFormRegisteredUser() throws Exception
	{
		log.info("TC05_submitFormRegisteredUser STARTED");
		
		log.info("Clear the cookies");
		driver.manage().deleteAllCookies();
		Thread.sleep(3000);
		s.navigateToHomePage();
		
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(BaseTest.xmlTest);
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Expand Customer Service section in mobile view");
			l1.getWebElement("Footer_CustomerServiceLink_Mobile", "Shopnav//footer.properties").click();
		}
		
		log.info("click on contact us link from footer");
		l1.getWebElement("Footer_ContactUs", "Shopnav//footer.properties").click();
		//BEFORE CONTACT US is OPENING IN NEW TAB. BUT NOW ITS OPENING IN SAME WINDOW hence commenting below lines
		/*Thread.sleep(6000);
		log.info("fetch new window id");
		Set<String> windows=driver.getWindowHandles();
		log.info("windows:- "+ windows);
		Iterator<String> i=windows.iterator();
		parentWin=i.next();
		log.info("parentWin:- "+ parentWin);
		childWin=i.next();
		log.info("childWin:- "+ childWin);
		log.info("swicth to child window");
		driver.switchTo().window(childWin);
		
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(4000);
			log.info("Handling SSL certificate in IE browser");
			driver.navigate().to("javascript:document.getElementById('overridelink').click()");
			
			driver.manage().window().setSize(new Dimension(800, 800));
			driver.manage().window().maximize();
		}*/
		
		log.info("verify navigation");
		sa.assertTrue(gVar.assertVisible("ContactUs_Heading", "Shopnav//ContactUs.properties"),"contact us heading");
		String heading = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 1, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ContactUs_Heading", contactUsProperties), 
				heading,"Verify the page heading text");
		
		log.info("Click on Email Us button");
		l1.getWebElement("ContactUs_EmainUsButton", contactUsProperties).click();
		
		log.info("Switch the driver controller to contactUs form frame");
		WebElement IframeForm = l1.getWebElement("ContactUs_IframeForm", contactUsProperties);
		driver.switchTo().frame(IframeForm);
		
		enterDetailsAndVerifySuccessMsg();
		
		log.info("Change the driver controller from Frame to browser tab");
		driver.switchTo().defaultContent();
		log.info("Close the popup");
		l1.getWebElement("ContactUs_Form_CloseButton", contactUsProperties).click();
		log.info("Popup should close");
		sa.assertTrue(gVar.assertEqual("ContactUs_Form", contactUsProperties, "style").contains("display: none;"),"ContactUs form should be closed");

		log.info("Close the child tab");
//		driver.close();
//		log.info("Switch driver controller to parent window");
//		driver.switchTo().window(parentWin);
		
		log.info("TC05_submitFormRegisteredUser ENDED");
		sa.assertAll();
	}
	
	
	
	void enterDetailsAndVerifySuccessMsg() throws Exception
	{
		log.info("Select the Customer Service department");
		WebElement selectBox;
		Thread.sleep(5000);
		if (gVar.assertVisible("ContactUs_Form_DepartmentSelectBox", contactUsProperties))
		{
			log.info("If condition");
			selectBox = l1.getWebElement("ContactUs_Form_DepartmentSelectBox", contactUsProperties);
		}
		else
		{
			log.info("Else condition");
			selectBox = l1.getWebElement("ContactUs_Form_DepartmentDD", contactUsProperties);
		}
		
		Select sel = new Select(selectBox);
		sel.selectByIndex(1);
//		String customerService = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 1, 3);
//		sel.selectByValue(customerService);
		
		clearTheContactUsFormFields();
		
		log.info("Enter the Firstname");
		String fn = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 1, 5);
		l1.getWebElement("ContactUs_Form_FirstNameBox", contactUsProperties).sendKeys(fn);
		String ln = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 2, 5);
		l1.getWebElement("ContactUs_Form_LastNameBox", contactUsProperties).sendKeys(ln);
		String email = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 3, 5);
		l1.getWebElement("ContactUs_Form_EmailBox", contactUsProperties).sendKeys(email);
		String phone = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 4, 5);
		l1.getWebElement("ContactUs_Form_PhoneBox", contactUsProperties).sendKeys(phone);
		String comments = GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 5, 5);
		l1.getWebElement("ContactUs_Form_CommentsBox", contactUsProperties).sendKeys(comments);
		
		log.info("Click on SUBMIT button");
		l1.getWebElement("ContactUs_Form_SubmitButton", contactUsProperties).click();
		
		log.info("Verify the Success message");
		sa.assertTrue(gVar.assertVisible("ContactUs_Form_SuccessMsg", contactUsProperties),"Verify the Success msg");
		sa.assertEquals(gVar.assertEqual("ContactUs_Form_SuccessMsg", contactUsProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "ContactUs", 1, 6), "Verify the Success message");
		
		sa.assertAll();
	}
	
	void clearTheContactUsFormFields() throws Exception
	{
		log.info("Clear the form fields");
		l1.getWebElement("ContactUs_Form_FirstNameBox", contactUsProperties).clear();
		l1.getWebElement("ContactUs_Form_LastNameBox", contactUsProperties).clear();
		l1.getWebElement("ContactUs_Form_EmailBox", contactUsProperties).clear();
		l1.getWebElement("ContactUs_Form_PhoneBox", contactUsProperties).clear();
		l1.getWebElement("ContactUs_Form_CommentsBox", contactUsProperties).clear();
	}
}
