package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PDPRecommendation extends BaseTest
{

//Non-automated:- DRM-1589, DRM-1768, DRM-1597
	
	String pdpProperties = "Shopnav//PDP.properties";
	
	@Test(groups={"reg"}, description="DMED-056 DRM-1591 DRM-1592 DRM-1593")
	public void TC01_PDPRecommendation_UI() throws Exception
	{
		log.info("Navigate to PDP which is configured with Recommendation");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 11, 1);
		s.pdpNavigationThroughURL(searchData);
		
		log.info("Verify the Heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_Recommendation_SectionHeading", pdpProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "PDP", 5, 2),"Verify the Recommendation section heading");
		log.info("Count the number Recommendation products");
		List<WebElement> productsInRecommendation = l1.getWebElements("PDP_Recommendation_Products", pdpProperties);
		log.info("productsInRecommendation size:- "+ productsInRecommendation.size());

		for (int i = 0; i < productsInRecommendation.size(); i++)
		{
			log.info("LOOP:- "+ i);
			sa.assertTrue(gVar.assertVisible("PDP_Recommendation_ProductName", pdpProperties, i),"Verify the Product name");
			sa.assertTrue(gVar.assertVisible("PDP_Recommendation_ProductImage", pdpProperties, i),"Verify the product image");
			String proPrice = gVar.assertEqual("PDP_Recommendation_ProductPrice", pdpProperties, i).trim();
			log.info("proPrice:- "+proPrice);
			if (gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav//header.properties"))//For Registered user
			{
				sa.assertTrue(proPrice.contains("$"),"Verify the price symbol in product price");
				sa.assertTrue(proPrice.length()>1, "Verify the price for Registered user");
			}
			else	//For Guest USer
			{
				sa.assertFalse(proPrice.contains("$"),"Verify the price symbol in product price for Guest user");
				sa.assertTrue(proPrice.length()==0, "Verify the price for Guest user");
			}
			
			sa.assertFalse(true,"Verify the module text");
			sa.assertFalse(true,"Verify the review");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056 DRM-1594")
	public void TC02_pdpRecommendationMaxProducts() throws Exception
	{
		List<WebElement> productsInRecommendation = l1.getWebElements("PDP_Recommendation_Products", pdpProperties);
		sa.assertEquals(productsInRecommendation.size(), 5,"Verify the maximum products in Recommendation section");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056 DRM-1590")
	public void TC03_clickFunctionalityOfProductLinkImage_InRecommendationSection() throws Exception
	{
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 11, 1);
		log.info("Collect the product name in reccomendation section");
		List<WebElement> proNamesInRecSection = l1.getWebElements("PDP_Recommendation_ProductName", pdpProperties);
		List<WebElement> proImagesInRecSection = l1.getWebElements("PDP_Recommendation_ProductImage", pdpProperties);
		
		for (int i = 0; i < proNamesInRecSection.size(); i++)
		{
			log.info("LOOP:- "+i);
			proNamesInRecSection = l1.getWebElements("PDP_Recommendation_ProductName", pdpProperties);
			String proName = proNamesInRecSection.get(i).getText();
			log.info("proName:- "+ proName);
			log.info("Click on Product name in recommendation section");
//			proNamesInRecSection.get(i).click();
			WebElement nameElement = proNamesInRecSection.get(i);
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", nameElement);
			Thread.sleep(4000);
			
			log.info("Verify the PDP");
			sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
			log.info("Verify the Product name in PDP");
			sa.assertEquals(gVar.assertEqual("PDP_ProductName", pdpProperties), proName,"Verify the product name");
			
			log.info("Navigate to same PDP which is configured with Recommendation");
			s.pdpNavigationThroughURL(searchData);
			Thread.sleep(5000);
			log.info("image element");
			proImagesInRecSection = l1.getWebElements("PDP_Recommendation_ProductImage", pdpProperties);
			log.info("Click on Product image");
//			proImagesInRecSection.get(i).click();
			WebElement element = proImagesInRecSection.get(i);
			JavascriptExecutor exe1=(JavascriptExecutor)driver;
			exe1.executeScript("arguments[0].click();", element);
			
			Thread.sleep(4000);
			log.info("Verify the PDP");
			sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
			log.info("Verify the Product name in PDP after clicking on image in Recommendation section");
			sa.assertEquals(gVar.assertEqual("PDP_ProductName", pdpProperties), proName,"Verify the product name");
			
			log.info("NAvigate to same PDP which is configured with Recommendation");
			s.pdpNavigationThroughURL(searchData);
			Thread.sleep(4000);
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056 DRM-1595")
	public void TC04_pdpNoRecommendation() throws Exception
	{
		log.info("Navigate to PDP which is not configured for Recommendations");
		String searchData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 17, 1);
		s.pdpNavigationThroughURL(searchData);
		
		log.info("Recommendations section should not display");
		sa.assertFalse(gVar.assertVisible("PDP_Recommendation_Section", pdpProperties),"Recommendations section should not display");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056")
	public void TC05_PDPRecommendation_UI() throws Exception
	{
		log.info("Navigate to login page");
		p.navigateToLoginPage();
		p.logIn(BaseTest.xmlTest);
		TC01_PDPRecommendation_UI();
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056")
	public void TC06_pdpRecommendationMaxProducts() throws Exception
	{
		TC02_pdpRecommendationMaxProducts();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056")
	public void TC07_clickFunctionalityOfProductLinkImage_InRecommendationSection() throws Exception
	{
		TC03_clickFunctionalityOfProductLinkImage_InRecommendationSection();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056")
	public void TC08_pdpNoRecommendation() throws Exception
	{
		TC04_pdpNoRecommendation();
		
		sa.assertAll();
	}
	
	
}
