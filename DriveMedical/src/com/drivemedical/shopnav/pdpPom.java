package com.drivemedical.shopnav;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class pdpPom extends BaseTest
{
	//907,
	String pdpProperties= "Shopnav//PDP.properties";
	String productName;
	String suggestedProName;
	String suggestedProPrice;
	
	ArrayList<String> productNamesList;
	ArrayList<String> productPriceList;
	
	
	@Test(groups={"reg"}, description="DMED-069 DRM-909")
	public void TC00_pomContentInPDP_ForProductNotAssociatedWithPom_Guest() throws Exception
	{
		pdpNotAssociatedWithPOM();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-908")
	public void TC01_addPOMtoCart(XmlTest xmlTest) throws Exception
	{
		String productID = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 9, 1);
		s.pdpNavigationThroughURL(productID);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
		
		log.info("Click on SIGNIN TO VIEW price button");
		l1.getWebElement("PDP_SignInTo_View_Price", pdpProperties).click();
		
		log.info("Login to the application");
		p.logIn(xmlTest);
		
		log.info("Collect the Product name");
		String proNAmeInPdp = l1.getWebElement("PDP_ProductName", pdpProperties).getText();
		System.out.println("proNAmeInPdp:- "+ proNAmeInPdp);
		log.info("Collect the Qty in pdp");
		String productQty = l1.getWebElement("PDP_Qty_Textbox", pdpProperties).getAttribute("value");
		System.out.println("productQty:- "+ productQty);
		
		if (xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			System.out.println("Executing in IE");
			Thread.sleep(4000);
		}
		
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", pdpProperties).click();
		Thread.sleep(3000);
		log.info("Verify the minicart qty in heading");
		String miniCArtQty = l1.getWebElement("Header_Cart_Qty", "Shopnav//header.properties").getText();
		System.out.println("mini cart qty in header:- "+miniCArtQty);
		
		log.info("Mini cart quantity should be doubled");
		int pdpQtyInInt = Integer.parseInt(productQty);
		int expectedQty = pdpQtyInInt+pdpQtyInInt;
		sa.assertEquals(miniCArtQty,expectedQty+"", "verify the minicart quantity in header");
		
		log.info("NAvigate to CArt page");
		s.navigateToCartPage();
		
		log.info("Verify the cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		
		log.info("verify the BOGO product");
		List<WebElement> productNamesInCart = l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties");
		sa.assertEquals(productNamesInCart.size(), expectedQty,"Verify the number of products in cart");
		sa.assertEquals(gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"), 
			proNAmeInPdp,"Verify the product name in cart page");
		
		sa.assertTrue(gVar.assertVisible("Cart_Promo_Description", "Cart//Cart.properties"),"Verify the promo description");
		String bogoQty = l1.getWebElement("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties").getAttribute("value");
		System.out.println("bogoQty:- "+ bogoQty);
		String bogoQtyStatus = l1.getWebElement("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties").getAttribute("disabled");
		System.out.println("bogoQtyStatus:- "+ bogoQtyStatus);
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-911")
	public void TC02_removePomProductInCartPage() throws Exception
	{
		log.info("Only one remove button should be displayed");
		sa.assertEquals(l1.getWebElements("Cart_DotButtons", "Cart//Cart.properties").size(), 1,"Verify the number of remove icons");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-914")
	public void TC03_updatePomProductQuantity() throws Exception
	{
		log.info("Update the product in CArt page");
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").clear();
		String qty = "4";
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(qty);
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		log.info("Verify the updated quantity");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox", "Cart//Cart.properties", "value"), qty, "verify the updated quantity");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties","value"), qty, "verify the quantity of the BOGO product");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-913")
	public void TC04_removePomProduct() throws Exception
	{
		log.info("Click on Remove link in cart page");
		l1.getWebElement("Cart_DotButtons", "Cart//Cart.properties").click();
		Thread.sleep(7000);
		
		log.info("Product should be deleted, Cart should get empty");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"),"Verify the empty cart");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-069 DRM-910")
	public void TC05_pomContentInPDP_ForProductNotAssociatedWithPom_Reg() throws Exception
	{
		pdpNotAssociatedWithPOM();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 Extra TC")
	public void TC06_updateQuantityOfProductInPDP_AddToCart(XmlTest xmlTest) throws Exception
	{
		productNamesList = new ArrayList<String>();
		productPriceList = new ArrayList<String>();
		log.info("Clear the cart");
		cart.clearCart();
		String productID = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 9, 1);
		s.pdpNavigationThroughURL(productID);
		
		log.info("Collect the product name");
		productName = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("productName:- "+ productName);
		//Store the Product name in Arraylist
		productNamesList.add(productName);
		String proPrice = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		log.info("proPrice:-"+ proPrice);
		productPriceList.add(proPrice);
		
		log.info("Update the quantity in PDP");
		String qtyInPDP ="3";
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).clear();
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).sendKeys(qtyInPDP);
		
		log.info("Click on ADD TO CART button");
		if (xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			System.out.println("Executing in IE");
			Thread.sleep(4000);
		}
		l1.getWebElement("PDP_AddToCart_button", pdpProperties).click();
		
		log.info("Navigate to cart page");
		s.navigateToCartPage();
		
		log.info("verify the product quantity in cart");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox", "Cart//Cart.properties", "value"), 
				qtyInPDP, "Verify the product quantity in cart page");
		
		log.info("Verify the quantity of BOGO product");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties","value"), 
				qtyInPDP, "verify the quantity of the BOGO product");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties","disabled"), 
				"disabled", "BOGO product quantity box should be disabled");
		//sa.assertTrue(l1.getWebElement("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties").isEnabled(),"Verify the Disabled condition of BOGO product quantity box");
		sa.assertEquals(l1.getWebElements("Cart_UOMbox", "Cart//Cart.properties").get(1).getAttribute("disabled"), 
				"disabled", "BOGO product UOM dropdown should be disabled");
		log.info("Collect the bogo Product name in cart page");
		String bogoProductName = l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties").get(1).getText();
		log.info("bogoProductName:- "+ bogoProductName);
		//Collect the bogo product name in cart page and store it in Arraylist
		productNamesList.add(bogoProductName);
		
		//Collect the bogo product price in cart page and store it in Arraylist
		String bogoPrice;
		if (gVar.mobileView()||gVar.tabletView())
		{
			bogoPrice = l1.getWebElements("Cart_ItemTotal_Mobile", "Cart//Cart.properties").get(1).getText();
		}
		else
		{
			bogoPrice = l1.getWebElements("Cart_ItemTotal", "Cart//Cart.properties").get(1).getText();
		}
		log.info("bogoPrice:- "+ bogoPrice);
		productPriceList.add(bogoPrice);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-Verify the POPUP in checkout page")
	public void TC07_verifyThePopUpInCheckoutPage() throws Exception
	{
		log.info("Click Proceed to CheckOut in Cart page"); 
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the Checkout page");
		
		log.info("Click on ADD ADDTESS link");
		l1.getWebElement("ShipTo_AddAddress_Link", "Checkout//ShipTo.properties").click();
		checkout.addAddress(1);
		log.info("Click on CONTINUE button in Address form");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		
		log.info("Verify the POPUP");
		int suggestedProduct=0;
		selecteTheProductAndVerifyIt(suggestedProduct);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc- Verify the EDIT link in the Shipping Method section")
	public void TC08_verifyEditLink() throws Exception
	{
		log.info("Click on EDIT link");
		l1.getWebElement("ShippingMtd_EditLink", "Checkout//ShippingMethod.properties").click();
		
		log.info("Verify the Popup");
		String popUpStyle = l1.getWebElement("ShippingMtd_PremiumDeliveryPopup", "Checkout//ShippingMethod.properties").getAttribute("style");
		sa.assertTrue(popUpStyle.contains("display: block"),"Verify the POPUP");
		
		log.info("VErify the Pre selected value");
		sa.assertEquals(gVar.assertEqual("ShippingMtd_PopUp_SelectredOption", "Checkout//ShippingMethod.properties"), 
				suggestedProName,"Verify the preselected product");
		
		int suggestedProduct=2;
		selecteTheProductAndVerifyIt(suggestedProduct);
		//## "suggestedProName" contains both product name and price. Here only product name is required, so product price is removing   
		suggestedProName = suggestedProName.replace(suggestedProPrice, "").trim();
		log.info("suggestedProName:- "+ suggestedProName);
		
		//Collect suggested product name in popup and store it in Arraylist
		productNamesList.add(suggestedProName);
		productPriceList.add(suggestedProPrice);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-Verify the products in OrderReview section")
	public void TC09_verifyProductsInOrderReviewSection() throws Exception
	{
		log.info("Click on CONTINUE button in Shipping Method section");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Enter PO number");
		String poNum = gVar.generateRandomNum()+"";
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys("poNum");
		
		log.info("Click on Review Order");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Collect the Product name in Order review page");
		List<WebElement> allNames = l1.getWebElements("OrderReview_ProductName", "Checkout//PaymentType.properties");
		log.info("allNames size:- "+ allNames.size());
		ArrayList<String> namesListInOrderReviewPage = new ArrayList<String>();
		for (int i = 0; i < allNames.size(); i++)
		{
			String name = allNames.get(i).getText();
			log.info("name:- "+ name);
			namesListInOrderReviewPage.add(name);
		}
		log.info("namesListInOrderReviewPage:- "+ namesListInOrderReviewPage);
		
		log.info("Collect the product price in order review page");
		ArrayList<String> priceListInOrderReviewPage = new ArrayList<String>();
		List<WebElement> allPrice = l1.getWebElements("OrderReview_ProductPrice", "Checkout//PaymentType.properties");
		log.info("allPrice size:- "+ allPrice.size());
		for (int i = 0; i < allPrice.size(); i++)
		{
			String price = allPrice.get(i).getText();
			log.info("price:- "+ price);
			priceListInOrderReviewPage.add(price);
		}
		log.info("priceListInOrderReviewPage:- "+ priceListInOrderReviewPage);
		
		//Sort the names
		Collections.sort(productNamesList);
		Collections.sort(namesListInOrderReviewPage);
		
		log.info("Compare the product name");
		sa.assertEquals(namesListInOrderReviewPage, productNamesList,"Compare the product names");
		
		log.info("Compare the product price");
		Collections.sort(priceListInOrderReviewPage);
		Collections.sort(productPriceList);
		sa.assertEquals(priceListInOrderReviewPage, productPriceList,"Compare the product price");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc place order")
	public void TC10_placePomProductOrder() throws Exception
	{
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();

		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
		log.info("orderID:- "+ orderID);
		
		sa.assertAll();
	}
	
//####################################################################################
	void pdpNotAssociatedWithPOM() throws Exception
	{
		log.info("Navigate to PDP, product which is not associated to POM");
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 1);
		s.navigateToPDP_Search(searchKeyWord);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
		
		log.info("POM content should not display in PDP");
		sa.assertFalse(gVar.assertVisible("PDP_PomContent1", pdpProperties), "POM content1 should not display");
		sa.assertFalse(gVar.assertVisible("PDP_PomContent2", pdpProperties),"POM content2 should not display");
	}
	
	void selecteTheProductAndVerifyIt(int proNum) throws Exception
	{
		
		log.info("Select the Product in popup");
		l1.getWebElements("ShipToPopUp_ProductsCheckbox", "Checkout//ShipTo.properties").get(proNum).click();
		
		log.info("Collect the product name and price");
		suggestedProName = l1.getWebElements("ShipToPopUp_Products_Name", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("name:- "+ suggestedProName);
		suggestedProPrice = l1.getWebElements("ShipToPopUp_Products_Price", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("price:- " + suggestedProPrice);
		
		log.info("Click on CONTINUE button");
		l1.getWebElement("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties").click();
		
		sa.assertEquals(gVar.assertEqual("ShippingMtd_PremiumDeliveryTitle", "Checkout//ShippingMethod.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 10, 0),"Verify the Premium Delivery Items: title");
		
		log.info("Verify the POM details in SHIPPING METHOD section");
		String actual = gVar.assertEqual("ShippingMtd_POMDetails", "Checkout//ShippingMethod.properties");
		log.info("actual:- " + actual);
		String pomText = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 2);
		String edit = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 2);
		String expected = productName+" "+pomText+" "+suggestedProName+" "+edit;
		sa.assertEquals(actual, expected,"VErify the POM details in Shipping Method section");
		
		log.info("Verify the Edit Link in Shipping Method(POM details) section");
		sa.assertEquals(gVar.assertEqual("ShippingMtd_EditLink", "Checkout//ShippingMethod.properties"), 
				edit,"Verify the Edit link");
	}
}
