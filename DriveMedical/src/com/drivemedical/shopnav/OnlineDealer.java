package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;

public class OnlineDealer extends BaseTest {
	
	String expBreadCrumb="Home";
	String expBreadCrumb1="Online Dealers";

	@Test(groups={"reg"},description="DMED-086 DRM-855")
	public void onlineDealerNav() throws Exception
	{
		log.info("Click on locate a provider drop down from header");
		l1.getWebElement("Header_StroeLoc_Link","Shopnav//header.properties").click();
		log.info("Click on online provider link");
		l1.getWebElement("OnlineDealer_Link","Shopnav//OnlineDealer.properties").click();
		log.info("verify navigation to online dealer page");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_Heading","Shopnav//OnlineDealer.properties"),"Online Dealer Page Heading");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-086 DRM-856,857,858")
	public void onlineDealerUI() throws Exception
	{
		log.info("verify breadcrumb");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_Breadcrumb", "Shopnav//OnlineDealer.properties",0),"Home breadcrumb");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_Breadcrumb", "Shopnav//OnlineDealer.properties",1),"online dealers breadcrumb");
		sa.assertEquals(gVar.assertEqual("OnlineDealer_Breadcrumb", "Shopnav//OnlineDealer.properties",0), expBreadCrumb);
		sa.assertEquals(gVar.assertEqual("OnlineDealer_Breadcrumb", "Shopnav//OnlineDealer.properties",1), expBreadCrumb1);
		log.info("verify Heading");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_Heading","Shopnav//OnlineDealer.properties"),"Online Dealer Page Heading");
		log.info("verify description");
		sa.assertTrue(gVar.assertVisible("OnlineDealers_Description","Shopnav//OnlineDealer.properties"),"Online Dealer Page description");
		log.info("verify products display text");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_prod_text","Shopnav//OnlineDealer.properties"),"Online Dealer Page products text");
		log.info("verify speciality drop down");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_Speciality_DD","Shopnav//OnlineDealer.properties"),"Online Dealer Page speciality drop down");
		log.info("verify products");
		List<WebElement> prodImages=l1.getWebElements("OnlineDealer_Images", "Shopnav//OnlineDealer.properties");
		for(int i=0;i<prodImages.size();i++) {
			log.info("verify images");
			sa.assertTrue(gVar.assertVisible("OnlineDealer_Images", "Shopnav//OnlineDealer.properties",i),"images");
			log.info("verify store details link");
			sa.assertTrue(gVar.assertVisible("OnlineDealer_StoreDet_Link", "Shopnav//OnlineDealer.properties",i),"store details");
			log.info("verify visit site link");
			sa.assertTrue(gVar.assertVisible("OnlineDealer_VisitSite_Link", "Shopnav//OnlineDealer.properties",i),"visit site");
		}
		log.info("verify more button");
		sa.assertTrue(gVar.assertVisible("OnlineDealer_More_Btn", "Shopnav//OnlineDealer.properties",i),"more button");
		sa.assertEquals(gVar.assertEqual("OnlineDealer_More_Btn", "Shopnav//OnlineDealer.properties"), "more");
		sa.assertTrue(false);//few issues present in application
		sa.assertAll();
	}
	
}
