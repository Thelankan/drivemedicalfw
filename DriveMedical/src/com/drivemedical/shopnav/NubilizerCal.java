package com.drivemedical.shopnav;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;

public class NubilizerCal extends BaseTest {

	@Test(groups={"reg"},description="DMED-028 DRM-1519")
	public void verifyUI()
	{
		log.info("Navigate to Nubilizer calculator screen");
		driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/homecareProvidersPage");
		log.info("verify UI");
		sa.assertTrue(gVar.assertVisible("NUBCAL_Heading", "//Shopnav//NubilizerCal.properties"),"Nubliuzer calculator");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-028 DRM-1520")
	public void verifyEleInteraction() throws Exception
	{
		log.info("interact with elements");
		l1.getWebElement("NUBCAL_NumOfPatients_Textbox", "//Shopnav//NubilizerCal.properties").sendKeys("20");
		log.info("interact with elements1");
		l1.getWebElement("NUBCAL_AnnualGrowthInPercentage_Textbox", "//Shopnav//NubilizerCal.properties").sendKeys("20");
		log.info("click submit");
		l1.getWebElement("NUBCAL_Submit", "//Shopnav//NubilizerCal.properties").click();
		log.info("verify display of table");
		sa.assertTrue(gVar.assertVisible("NUBCAL_Tables", "//Shopnav//NubilizerCal.properties"),"Nubliuzer table");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-028 DRM-1521")
	public void verifyEleInteractionLogin() throws Exception
	{
		
		log.info("Log in");
		p.logIn(BaseTest.xmlTest);
		
		log.info("Navigate to Nubilizer calculator screen");
		driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/homecareProvidersPage");
		
		log.info("interact with elements");
		l1.getWebElement("NUBCAL_NumOfPatients_Textbox", "//Shopnav//NubilizerCal.properties").sendKeys("20");
		log.info("interact with elements1");
		l1.getWebElement("NUBCAL_AnnualGrowthInPercentage_Textbox", "//Shopnav//NubilizerCal.properties").sendKeys("20");
		log.info("click submit");
		l1.getWebElement("NUBCAL_Submit", "//Shopnav//NubilizerCal.properties").click();
		log.info("verify display of table");
		sa.assertTrue(gVar.assertVisible("NUBCAL_Tables", "//Shopnav//NubilizerCal.properties"),"Nubliuzer table");
		sa.assertAll();
	}
}
