package com.drivemedical.checkout;

//import org.testng.annotations.AfterClass;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PlaceOrder extends BaseTest
{

/*	@Test(groups={"reg", "sanity_reg"},description="extraTc-Place Order with pom Product")
	public void TC00_placeOrderWithPomProduct(XmlTest xmlTest) throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		ArrayList<String> productNamesList = new ArrayList<String>();
		cart.clearCart();
		log.info("ADD POM product");
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 9, 1);
		int quantity = 1;
		int numberOfProducts = 1;	//Here 1 indicates "number of products added to cart"
		s.navigateToCart(searchKeyWord,numberOfProducts, quantity);	
		Collections.sort(s.prodnames);
		Collections.sort(s.prodPrice);
		Collections.sort(s.materialId);
		log.info("Products Name which are added to Cart:- "+ s.prodnames);
		log.info("Products Price which are added to Cart:- "+s.prodPrice);
		log.info("Products Material ID's which are added to Cart:- "+s.materialId);
		
		List<String> proNameList = s.prodnames;
		ArrayList<String> materialIdsList = s.materialId;
		
		log.info("proNameList:- "+ proNameList);
		log.info("materialIdsList:- "+materialIdsList);
		
		productNamesList.add(proNameList.toString());
		
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		log.info("Collect the Cart ID");
		String cartID = l1.getWebElement("Cart_ID", "Cart//Cart.properties").getText();
		log.info("cartID:- "+ cartID);
		log.info("Collect the bogo Product name in cart page");
		String bogoProductName = l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties").get(1).getText();
		log.info("bogoProductName:- "+ bogoProductName);
		//Collect the bogo product name in cart page and store it in Arraylist
		productNamesList.add(bogoProductName);
		
		//Collect the bogo product price in cart page and store it in Arraylist
		String bogoPrice;
		if (gVar.mobileView()||gVar.tabletView())
		{
			bogoPrice = l1.getWebElements("Cart_ItemTotal_Mobile", "Cart//Cart.properties").get(1).getText();
		}
		else
		{
			bogoPrice = l1.getWebElements("Cart_ItemTotal", "Cart//Cart.properties").get(1).getText();
		}
		
		log.info("bogoPrice:- "+ bogoPrice);
		
		String productTotalPrice = l1.getWebElement("Cart_OrderTotal_Amount", "Cart//Cart.properties").getText();
		log.info("productTotalPrice:- "+ productTotalPrice);
		
		log.info("Click on PROCEED TO CHECKOUT button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		Thread.sleep(2000);
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Select the DropShipp address");
		l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties").get(2).click();
		
		log.info("Click  CONTINUE button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Continue button should be disabled in popup");
		sa.assertEquals(gVar.assertEqual("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties","disabled"), 
				"disabled", "Continue button should be disablid in popup");
		
		log.info("Select the Product in popup");
		int proNum = 1;
		l1.getWebElements("ShipToPopUp_ProductsCheckbox", "Checkout//ShipTo.properties").get(proNum).click();
		
		log.info("Collect the product name and price");
		String suggestedProName = l1.getWebElements("ShipToPopUp_Products_Name", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("name:- "+ suggestedProName);
		String suggestedProPrice = l1.getWebElements("ShipToPopUp_Products_Price", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("price:- " + suggestedProPrice);
		
		//## "suggestedProName" contains both product name and price. Here only product name is required, so product price is removing   
		suggestedProName = suggestedProName.replace(suggestedProPrice, "").trim();
		log.info("suggestedProName:- "+ suggestedProName);
		
		//Collect suggested product name in popup and store it in Arraylist
		productNamesList.add(suggestedProName);
		
		log.info("Click on Continue button in popup");
		l1.getWebElement("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties").click();
		
		log.info("Collect the SHIPPTO Address");
		String shippToAddress = l1.getWebElement("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties").getText();
		log.info("shippToAddress:- "+ shippToAddress);
		
		log.info("Verify the POM details in Shipping Method section");
		String expectedDetail = proNameList.toString()+" "+"For POM "+suggestedProName+" "+ suggestedProPrice+" "+"Edit";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ShippingMtd_POMDetails", "Checkout//ShippingMethod.properties"), 
				expectedDetail,"Verify the Pom Details");
		
//		log.info("Select the shipping method and click on CONTINUE button in SHIPPING METHOD SECTION");
//		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
//		shippingMethodRadioButton.get(1).click();
		l1.getWebElement("ShippingMtd_PomProductContinueButton", "Checkout//ShippingMethod.properties").click();
		
//		log.info("Collect the shipping method");
//		String selectedShippingMethod = l1.getWebElement("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties").getText();
//		log.info("selectedShippingMethod:- " + selectedShippingMethod);
		
		log.info("Enter PO number");
		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		String poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("Collect the Product name in Order review page");
		String proNameInOrderReviewPage = l1.getWebElement("OrderReview_ProductName", "Checkout//PaymentType.properties").getText();
		log.info("proNameInOrderReviewPage:- "+ proNameInOrderReviewPage);
		List<WebElement> allNames = l1.getWebElements("OrderReview_PomProductNames", "Checkout//PaymentType.properties");
		log.info("allNames size:- "+ allNames.size());
		ArrayList<String> namesListInOrderReviewPage = new ArrayList<String>();
		// #* POM product names are not link, its in div. Hence collecting the pom products name(First product name is not collecting because its link, its collecting separately.) 
		for (int i = 1; i < allNames.size(); i++)
		{
			String name = allNames.get(i).getText();
			log.info("name:- "+ name);
			namesListInOrderReviewPage.add(name);
		}
		log.info("namesListInOrderReviewPage:- "+ namesListInOrderReviewPage);
		namesListInOrderReviewPage.add(proNameInOrderReviewPage);
		log.info("namesListInOrderReviewPage:- "+ namesListInOrderReviewPage);
				
		//Sort the names
		Collections.sort(productNamesList);
		Collections.sort(namesListInOrderReviewPage);
		log.info("productNamesList after sort:- "+productNamesList);
		log.info("namesListInOrderReviewPage after Sort:- "+ namesListInOrderReviewPage);
		
				
		log.info("Compare the product name");
		sa.assertEquals(namesListInOrderReviewPage, productNamesList,"Compare the product names");

		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();

		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
		log.info("orderID:- "+ orderID);
		
		sa.assertAll();
	}*/
	
}
