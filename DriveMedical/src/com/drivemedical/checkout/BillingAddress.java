package com.drivemedical.checkout;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class BillingAddress extends BaseTest
{
	String shipToProperties = "Checkout//ShipTo.properties";
	String billingAddressProperties = "Checkout//BillingAddress.properties";
	
	@Test(groups={"reg"}, description="OOTB-013 DRM-852")
	public void UseDeliveryAddressCheckboxStatus() throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(BaseTest.xmlTest);
		
		log.info("Navigate to ShipTo sction of checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToShipToPage(product, 1, 1);
		
		log.info("Click on ADD ADDRESS link");
		l1.getWebElement("ShipTo_AddAddress_Link", shipToProperties).click();
		
		log.info("ENTER address");
		int rowNum = 2;
		checkout.addAddress(rowNum);
		log.info("Enter the Address 2");
		String address2 = "Address 2";
		l1.getWebElement("ShipTo_Addr2_Textbox", shipToProperties).sendKeys(address2);
		String fNmae = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 1);
		String address1 = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 2);
		String city = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 4);
		String state = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 5);
		String zipCode = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 6);
		String phNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum,7);
		
		log.info("Click on CONTINUE button in shipTo section");
		l1.getWebElement("ShipTo_Continue_button", shipToProperties).click();
		
		log.info("Verify the saved ShipTo address");
//		expectedAddress = "Ship To: Test1 LastName   1 main st,  Address 2,  New york,  New York,  10044,  United States,  333-333-3333,  ";
//		String expectedAddress = "Ship To: "+"Test1 LastName"+"   "+"1 main st,"+"  "+"Address 2"+",  "+"New york"+",  "+"New York"+",  "+"10044"+",  "+"United States"+",  "+"333-333-3333"+",  ";
		String expectedAddress = "Ship To: "+fNmae+"   "+address1+",  "+address2+",  "+city+",  "+state+",  "+zipCode+",  "+"United States"+",  "+phNum+",  ";
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ShippingMtd_SavedShipToAddress", "Checkout//ShippingMethod.properties"), 
				expectedAddress,"Verify the saved ShipTo address");
		
		log.info("Click on CONTINUE button in Shipping Method section");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Select Credit card radio button");
		l1.getWebElement("CardPaymentRadioButton", "Checkout//PaymentType.properties").click();
		
		log.info("Verify the Billing address section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddress_FirstName", billingAddressProperties), 
				fNmae,"Verify first name");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddress_Address1Textbox", billingAddressProperties), 
				address1,"Verify Address 1");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddress_Address2Textbox", billingAddressProperties), 
				address2,"Verify Address 2");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddress_CityTextbox", billingAddressProperties), 
				city,"Verify City");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddress_StateSelectbox_SelectedOptions", billingAddressProperties,1), 
				state,"Verify State");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddress_PhoneNumber", billingAddressProperties), 
				phNum,"Verify City");
		
		log.info("All the fields should be disabled");
		sa.assertEquals(gVar.assertEqual("BillingAddress_CountrySelectBox", billingAddressProperties,"disabled"), 
				"","disabled Country");
		sa.assertEquals(gVar.assertEqual("BillingAddress_TitleSelectBox", billingAddressProperties,"disabled"), 
				"","disabled Title");
		sa.assertEquals(gVar.assertEqual("BillingAddress_FirstName", billingAddressProperties,"disabled"), 
				"","disabled Fn");
		sa.assertEquals(gVar.assertEqual("BillingAddress_LastName", billingAddressProperties,"disabled"), 
				"","disabled Ln");
		sa.assertEquals(gVar.assertEqual("BillingAddress_Address1Textbox", billingAddressProperties,"disabled"), 
				"","disabled Address 1");
		sa.assertEquals(gVar.assertEqual("BillingAddress_Address2Textbox", billingAddressProperties,"disabled"), 
				"","disabled Address 2");
		sa.assertEquals(gVar.assertEqual("BillingAddress_CityTextbox", billingAddressProperties,"disabled"), 
				"","disabled City");
		sa.assertEquals(gVar.assertEqual("BillingAddress_StateSelectbox", billingAddressProperties,"disabled"), 
				"","disabled State");
		sa.assertEquals(gVar.assertEqual("BillingAddress_PostcodeBox", billingAddressProperties,"disabled"), 
				"","disabled PostCode");
		sa.assertEquals(gVar.assertEqual("BillingAddress_PhoneNumber", billingAddressProperties,"disabled"), 
				"","disabled Phone number");
		
		sa.assertFalse(l1.getWebElement("BillingAddress_CountrySelectBox", billingAddressProperties).isEnabled(),"Country disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_TitleSelectBox", billingAddressProperties).isEnabled(),"Title disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_FirstName", billingAddressProperties).isEnabled(),"FNdisabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_LastName", billingAddressProperties).isEnabled(),"LN disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_Address1Textbox", billingAddressProperties).isEnabled(),"Address 1disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_Address2Textbox", billingAddressProperties).isEnabled(),"Address 2 disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_CityTextbox", billingAddressProperties).isEnabled(),"City disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_StateSelectbox", billingAddressProperties).isEnabled(),"State disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_PostcodeBox", billingAddressProperties).isEnabled(),"Post code disabled");
		sa.assertFalse(l1.getWebElement("BillingAddress_PhoneNumber", billingAddressProperties).isEnabled(),"Phone number disabled");
		
		
		sa.assertAll();
	}
	
}

