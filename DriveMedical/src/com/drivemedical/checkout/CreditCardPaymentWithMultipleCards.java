package com.drivemedical.checkout;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.CheckoutFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CreditCardPaymentWithMultipleCards extends BaseTest
{
	String paymentTypeProperties = "Checkout//PaymentType.properties";
	String checkoutProperties = "Checkout//Checkout.properties";
	String orderReviewProperties = "Checkout//OrderReview.properties";
	String orderConfirmationProperties = "Checkout//OrderConfirmation.properties";
	String cardType;
	String nameOnCard;
	String cardNumber;
	String month;
	String year;
	int rowNum;
	String poNum;
	int savedCardsCount;
	String expectedBillingAddress;
	ArrayList<String> allCardsList;
	CreditCardPayment cc;
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1604")
	public void TC00_LoginToTheApplication() throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		cc = new CreditCardPayment();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1604")
	public void TC01_CreditCardOrderPlacement() throws Exception
	{
		
		log.info("Clear Cart");
//		cart.clearCart();
		
		log.info("Delete All Saved Cards");
//		deleteSavedCards();
		log.info("Navigate to payment method section in checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		Thread.sleep(5000);
		
		if (gVar.assertVisible("CC_AddCard_Link", paymentTypeProperties))
		{
			log.info("Click on ADD CARD link");
			l1.getWebElement("CC_AddCard_Link", paymentTypeProperties).click();
		}
		
		log.info("Verify the UI of the Credit card form");
		cc.verifyCreditCardFormUi();
		
		log.info("Enter valid credit card details");
		String poNum = gVar.generateRandomNum()+"";
		rowNum = 3;
		cc.enterCreditCardDetails(poNum, rowNum, true);
		
		Thread.sleep(3000);
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		Thread.sleep(2000);
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNum,"Verify the PO number");
		String savedCardNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 6);
		String month = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 3).replace("0", "");
		String year = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 4);
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 0);
		String expectedCardDetails = "Method:"+cardType+" "+savedCardNum+" Exp "+month+"."+year;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		Thread.sleep(5000);
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		Thread.sleep(5000);
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		Thread.sleep(5000);
		log.info("Verify the Order Confirmation page");
		cc.verifyOrderConfirmationPage(poNum, expectedBillingAddress);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1604")
	public void TC01_DiscoverCreditCardOrderPlacement() throws Exception
	{
		s.navigateToHomePage();
		log.info("Clear Cart");
		cart.clearCart();
		
		log.info("Delete All Saved Cards");
//		deleteSavedCards();
		log.info("Navigate to payment method section in checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		Thread.sleep(5000);
		if (gVar.assertVisible("CC_AddCard_Link", paymentTypeProperties))
		{
			log.info("Click on ADD CARD link");
			l1.getWebElement("CC_AddCard_Link", paymentTypeProperties).click();
		}
		log.info("Enter valid credit card details");
		String poNum = gVar.generateRandomNum()+"";
		rowNum = 2;
		cc.enterCreditCardDetails(poNum, rowNum, true);
		
		Thread.sleep(3000);
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		Thread.sleep(2000);
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNum,"Verify the PO number");
		String savedCardNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 6);
		String month = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 3).replace("0", "");
		String year = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 4);
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 0);
		String expectedCardDetails = "Method:"+cardType+" "+savedCardNum+" Exp "+month+"."+year;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		Thread.sleep(5000);
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		Thread.sleep(5000);
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		Thread.sleep(5000);
		log.info("Verify the Order Confirmation page");
		cc.verifyOrderConfirmationPage(poNum, expectedBillingAddress);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1604")
	public void TC02_MasterCreditCardOrderPlacement() throws Exception
	{
		s.navigateToHomePage();
		log.info("Clear Cart");
		cart.clearCart();
		
		log.info("Delete All Saved Cards");
//		deleteSavedCards();
		log.info("Navigate to payment method section in checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		Thread.sleep(5000);
		if (gVar.assertVisible("CC_AddCard_Link", paymentTypeProperties))
		{
			log.info("Click on ADD CARD link");
			l1.getWebElement("CC_AddCard_Link", paymentTypeProperties).click();
		}
		
		log.info("Enter valid credit card details");
		String poNum = gVar.generateRandomNum()+"";
		rowNum = 4;
		cc.enterCreditCardDetails(poNum, rowNum, true);
		
		Thread.sleep(3000);
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		Thread.sleep(2000);
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNum,"Verify the PO number");
		String savedCardNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 6);
		String month = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 3).replace("0", "");
		String year = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 4);
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 0);
		String expectedCardDetails = "Method:"+cardType+" "+savedCardNum+" Exp "+month+"."+year;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		Thread.sleep(5000);
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		Thread.sleep(5000);
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		Thread.sleep(5000);
		log.info("Verify the Order Confirmation page");
		cc.verifyOrderConfirmationPage(poNum, expectedBillingAddress);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1604")
	public void TC03_AmexCreditCardOrderPlacement() throws Exception
	{
		s.navigateToHomePage();
		log.info("Clear Cart");
		cart.clearCart();
		
		log.info("Delete All Saved Cards");
//		deleteSavedCards();
		log.info("Navigate to payment method section in checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		Thread.sleep(5000);
		
		if (gVar.assertVisible("CC_AddCard_Link", paymentTypeProperties))
		{
			log.info("Click on ADD CARD link");
			l1.getWebElement("CC_AddCard_Link", paymentTypeProperties).click();
		}
		
		log.info("Enter valid credit card details");
		String poNum = gVar.generateRandomNum()+"";
		rowNum = 1;
		cc.enterCreditCardDetails(poNum, rowNum, true);
		
		Thread.sleep(3000);
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		Thread.sleep(2000);
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNum,"Verify the PO number");
		String savedCardNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 6);
		String month = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 3).replace("0", "");
		String year = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 4);
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 0);
		String expectedCardDetails = "Method:"+cardType+" "+savedCardNum+" Exp "+month+"."+year;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		Thread.sleep(5000);
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		Thread.sleep(5000);
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		Thread.sleep(5000);
		log.info("Verify the Order Confirmation page");
		cc.verifyOrderConfirmationPage(poNum, expectedBillingAddress);
		
		sa.assertAll();
	}

	

}
