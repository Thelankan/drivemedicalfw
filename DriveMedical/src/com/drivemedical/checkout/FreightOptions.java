package com.drivemedical.checkout;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class FreightOptions extends BaseTest
{
	String shipToproperties = "Checkout//ShipTo.properties";
	String checkoutProperties = "Checkout//Checkout.properties";
	
	String suggestedProName;
	String suggestedProPrice;
	String productName;
	
	//Selecting all three products in PREMIUM DELIVERY OPTION popup and verifying it in Shipping method section
	@Test(groups={"reg"}, description="DMED-554 DRM-886")
	public void TC00_CheckoutUpgradedFreightOptionsForOrganizationAddress() throws Exception
	{
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("Login to the application");
		p.logIn(BaseTest.xmlTest);
		
		log.info("Add a POM product to cart and navigate to ShipTo page");
		String excelDAta = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 9, 1);
		checkout.navigateToShipToPage(excelDAta, 1, 1);	
		
		log.info("Select the DropShipp addresss");
		l1.getWebElements("ShipTo_AllAddress_RadioButtons", shipToproperties).get(1).click();
		
		log.info("Click on CONTINUE button in shippTo section");
		l1.getWebElement("ShipTo_Continue_button", shipToproperties).click();
		
		List<WebElement> checkboxCount = l1.getWebElements("ShipToPopUp_ProductsCheckbox", "Checkout//ShipTo.properties");
		log.info("checkboxCount:- "+ checkboxCount.size());
		sa.assertEquals(checkboxCount.size(), 3,"Verify the number of checkbox in POM popup");
		for (int i = 0; i < checkboxCount.size(); i++)
		{
			log.info("LOOP:- "+ i);
			log.info("Select products in popup and verify the selected one");
			selecteTheProductAndVerifyIt(i);
			log.info("Click on EDIT link in ShipTo section");
			l1.getWebElement("Checkout_SectionsEditLink", checkoutProperties).click();
		}
		
		sa.assertAll();
	}
	
	//Select the Commercial address in Shipto section and verify the pom POPUP
	@Test(groups={"reg"}, description="DMED-554")
	public void TC01_verifyPomPopupForCommercialAddress() throws Exception
	{
		log.info("Navigate ShipTo section");
		l1.getWebElement("Checkout_SectionsEditLink", checkoutProperties).click();
		
		log.info("Click on ADD ADDRESS link");
		l1.getWebElement("ShipTo_AddAddress_Link", shipToproperties).click();
		log.info("Select the Commercial address radio button");
		l1.getWebElement("ShipTo_Commercial_RadioButton", shipToproperties).click();
		log.info("Enter address");
		checkout.addAddress(2);
		
		log.info("Premium Delivery Option popup should not display");
		sa.assertFalse(true,"Verify POpup should not display");
		
		sa.assertTrue(gVar.assertVisible("ShippingMtd_DeliveryMethods", "Checkout//ShippingMethod.properties"),"Verify the shipping methods");
		int shippingMethodsCount = l1.getWebElements("ShippingMtd_DeliveryMethods", "Checkout//ShippingMethod.properties").size();
		sa.assertEquals(shippingMethodsCount, 4,"Verify the shipping methods count");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-554")
	public void TC02_verifyPomPopupForOrganizationAddress() throws Exception
	{
		log.info("Navigate ShipTo section");
		l1.getWebElement("Checkout_SectionsEditLink", checkoutProperties).click();
		log.info("Select the Organization Address");
		l1.getWebElements("ShipTo_AllAddress_RadioButtons", shipToproperties).get(0).click();
		log.info("Premium Delivery Option popup should not display");
		sa.assertFalse(true,"Verify POpup should not display");
		
		sa.assertTrue(gVar.assertVisible("ShippingMtd_DeliveryMethods", "Checkout//ShippingMethod.properties"),"Verify the shipping methods");
		int shippingMethodsCount = l1.getWebElements("ShippingMtd_DeliveryMethods", "Checkout//ShippingMethod.properties").size();
		sa.assertEquals(shippingMethodsCount, 4,"Verify the shipping methods count");
		
		sa.assertAll();
	}
	
	//BELOW TESTCASE COMMENTED. 
//	/*@Test(groups={"reg"}, description="DMED-554 DRM-887 DRM-889 DRM-890 DRM-894")
//	public void TC01_verifyFreightOptionsForCommercial() throws Exception
//	{
//		
//		log.info("Click on EDIT link in ShipTo section / Navigate back to ShippTo section");
//		l1.getWebElement("ShipTo_Edit_Btn", shipToproperties).click();
//		
//		log.info("Click on ADD ADDRESS link");
//		l1.getWebElement("ShipTo_AddAddress_Link", shipToproperties).click();
//		
//		log.info("Enter address");
//		checkout.addAddress(1);
//		
//		log.info("Select the commercial address");
//		l1.getWebElement("ShipTo_Commercial_RadioButton", shipToproperties).click();
//		
//		log.info("Click on CONTINUE button in SHIPTO section");
//		l1.getWebElement("ShipTo_Continue_button", shipToproperties).click();
//		
//		log.info("Verify the Estimated Freight option");
//		sa.assertTrue(gVar.assertVisible("CheckoutRight_EstimatedFreight", checkoutProperties),"Verify the Estimated Freight");
//		String estimatedFreightText = l1.getWebElement("CheckoutRight_EstimatedFreight", checkoutProperties).getText();
//		log.info("estimatedFreightText:- "+estimatedFreightText);
//		String text = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 0);
//		sa.assertTrue(estimatedFreightText.contains(text),"Verify the Estimated Freight text");
//		
//		sa.assertTrue(gVar.assertVisible("CheckoutRight_Tax", checkoutProperties),"Verify the Tax section");
//		sa.assertAll();
//	}
//	
//	@Test(groups={"reg"}, description="DMED-554 DRM-887")
//	public void TC02_CheckoutUpgradedFreightOptionsForCommercialAddress() throws Exception
//	{
//		log.info("NAvigate back to ShipTo Section / Click on edit link in shipTo section");
//		l1.getWebElement("ShipTo_Edit_Btn", shipToproperties).click();
//		
//		log.info("Click on ADD ADDRESS link");
//		l1.getWebElement("ShipTo_AddAddress_Link", shipToproperties).click();
//		
//		log.info("Enter address");
//		checkout.addAddress(2);
//		
//		log.info("Select the Commercial address");
//		l1.getWebElement("ShipTo_Commercial_RadioButton", shipToproperties).click();
//		
//		log.info("Click on CONTINUE button in SHIPTO section");
//		l1.getWebElement("ShipTo_Continue_button", shipToproperties).click();
//		
//		log.info("Estimated Freight option should not display");
//		sa.assertFalse(gVar.assertVisible("CheckoutRight_EstimatedFreight", checkoutProperties),"Estimated Freight option should not display");
//		
//		sa.assertAll();
//	}
//	
//	@Test(groups={"reg"}, description="DMED-554 DRM-891")
//	public void TC03_CheckoutUpgradedFreightOptionsForRandomProduct() throws Exception
//	{
//		log.info("Navigate to home page");
//		s.navigateToHomePage();
//		
//		log.info("Clear the cart items");
//		cart.clearCart();
//		// Product should not be having the upgraded freight options.
//		log.info("Navigate to ShipTo page");
//		String data = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 13, 1);
//		checkout.navigateToShipToPage(data, 1, 1);
//		
//		log.info("Estimated Freight option should not display");
//		sa.assertFalse(gVar.assertVisible("CheckoutRight_EstimatedFreight", checkoutProperties),"Estimated Freight option should not display");
//		
//		sa.assertAll();
//	}*/

	
//	+ -------- + -------- + -------- + -------- + -------- + -------- + 
//	+ -------- + -------- + -------- + -------- + -------- + -------- + 
	
	void selecteTheProductAndVerifyIt(int proNum) throws Exception
	{
		log.info("Select the Product in popup");
		l1.getWebElements("ShipToPopUp_ProductsCheckbox", "Checkout//ShipTo.properties").get(proNum).click();
		
		log.info("Collect the product name and price");
		suggestedProName = l1.getWebElements("ShipToPopUp_Products_Name", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("name:- "+ suggestedProName);
		suggestedProPrice = l1.getWebElements("ShipToPopUp_Products_Price", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("price:- " + suggestedProPrice);
		
		log.info("Click on CONTINUE button");
		l1.getWebElement("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties").click();
		
		sa.assertEquals(gVar.assertEqual("ShippingMtd_PremiumDeliveryTitle", "Checkout//ShippingMethod.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 10, 0),"Verify the Premium Delivery Items: title");
		
		log.info("Verify the POM details in SHIPPING METHOD section");
		String actual = gVar.assertEqual("ShippingMtd_POMDetails", "Checkout//ShippingMethod.properties");
		log.info("actual:- " + actual);
		String pomText = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 2);
		String edit = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 2);
		String expected = productName+" "+pomText+" "+suggestedProName+" "+edit;
		sa.assertEquals(actual, expected,"VErify the POM details in Shipping Method section");
		
		log.info("Verify the Edit Link in Shipping Method(POM details) section");
		sa.assertEquals(gVar.assertEqual("ShippingMtd_EditLink", "Checkout//ShippingMethod.properties"), 
				edit,"Verify the Edit link");
	}
}
