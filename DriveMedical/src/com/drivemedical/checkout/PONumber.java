package com.drivemedical.checkout;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.CheckoutFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

//DMED-090 not possible test cases DRM-1083,1084,1085,1086,1087,1089,1090,1091,1092

public class PONumber extends BaseTest{
	
	String orderNum;
	String productQuantity;
	
	@Test(groups={"reg","sanity_reg"},description="OOTB-012 DRM-804")
	public void TC00_VerifyPONumberInOrderReview(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("navigate to Order Review page");
		checkout.navigateToOrderReviewPage();
		log.info("verify PO number");
		String poNum = checkout.PONumberVal;
		sa.assertTrue(gVar.assertVisible("PONumber", "Checkout//PaymentType.properties"),"PO Number");
		sa.assertEquals(gVar.assertEqual("PONumber", "Checkout//PaymentType.properties"),"PO: "+poNum,"Verify the PO number");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-023 Verify the saved address in Payment section")
	public void TC01_VerifyAddressInPaymentMethodSection() throws Exception
	{
		log.info("Verify the Saved address in Payment method section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PaymentMethod_SavedAddress", "Checkout//PaymentType.properties").trim(), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14),"Verify the saved address in Payment section");
		sa.assertTrue(gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"),"Verify the Checkbox");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-023 DRM-1252")
	public void TC02_VerifyCheckboxInOrderReview()
	{
		log.info("Verify the Checkbox");
		sa.assertTrue(gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"),"Verify the Checkbox");
		sa.assertAll();
	}
	
	@Test(groups={"reg","sanity_reg"},description="OOTB-012 DRM-803,805 OOTB-023 DRM-1251 DMED-090 DRM-1081,1082,1093,1088")
	public void TC03_VerifyPONumberInOrderConfirmation() throws Exception
	{
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("click on place order");
		l1.getWebElements("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").get(0).click();
		log.info("verify Order confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Order Confirmation heading");
		log.info("verify PO Number in Order Confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Text", "Checkout//OrderConfirmation.properties"),"Po Number text");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"),"Po Number value");
		sa.assertEquals(gVar.assertEqual("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"), CheckoutFunctions.PONumberVal);
	
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText();
		log.info("orderID:- "+ orderID);
		gVar.saveOrderId(orderID, "Order placed using PO number");
		log.info("Verify order status");
		sa.assertTrue(gVar.assertVisible("Orderconfirm_Status","Checkout//OrderConfirmation.properties"),"Order Status");
		
		log.info("Collect the Quantity");
		productQuantity = l1.getWebElement("Orderconfirm_ProductQuantity", "Checkout//OrderConfirmation.properties").getText();
		log.info("productQuantity in Order confirmation page:- "+ productQuantity);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="OOTB-012 DRM-806")
	public void TC04_VerifyPONumberInOrderDetails() throws Exception
	{
		log.info("fetch order number");
		orderNum=l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1];
		log.info("orderNum:- "+ orderNum);

		log.info("Navigate to Order History page");
		s.navigateToOrderHistoryPage();
		log.info("click on order number link");
		l1.getWebElement("OrderHistory_AllOrderId", "Profile//OrderHistory.properties").click();
	
		log.info("Verify the Order Details page");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Heading", "Profile//OrderHistory.properties"),"Verify the Order Details page");
		
		log.info("verify PO Number in Order Details page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Text", "Checkout//OrderConfirmation.properties"),"Po Number text");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"),"Po Number value");
		sa.assertEquals(gVar.assertEqual("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"), CheckoutFunctions.PONumberVal);
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-066 DRM-1183")
	public void TC05_verifyReorder(XmlTest xmlTest) throws Exception 
	{
		log.info("verify re order button in order details page");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Reorder_Btn", "Profile//OrderDetails.properties",0),"Re order On top");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Reorder_Btn", "Profile//OrderDetails.properties",1),"Re order On bottom");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-066 DRM-1184,1186")
	public void TC06_reorderFun(XmlTest xmlTest) throws Exception 
	{
		log.info("Click on REORDER button in Order Details page");
		l1.getWebElement("OrderDetails_Reorder1", "Profile//OrderHistory.properties").click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("User Should be in ShipTo section");
		sa.assertTrue(gVar.assertVisible("ShipTo_Continue_button", "Checkout//ShipTo.properties"),"Verify continue button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Select the radio button in SHIPPING METHODS section");
		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		shippingMethodRadioButton.get(0).click();
		sa.assertTrue(gVar.assertVisible("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties"),"Verify the Shipping method");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		String poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Verify the saved address in payment section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PaymentMethod_SavedAddress", "Checkout//PaymentType.properties").trim(), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14),"Verify the saved address in Payment section");
		
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText();
		log.info("orderID:- "+ orderID);
		gVar.saveOrderId(orderID, "Re-Order from Order details page");
		log.info("Verify the product quantity");
		sa.assertEquals(gVar.assertEqual("Orderconfirm_ProductQuantity", "Checkout//OrderConfirmation.properties"), 
				productQuantity,"Verify the product quantity in Order confirmation page");
		
		sa.assertAll();
	}

}
