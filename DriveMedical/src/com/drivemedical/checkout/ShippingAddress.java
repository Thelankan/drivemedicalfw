package com.drivemedical.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

//DRM-850,851,852

//###############  NEED TO DELETE ALL SAVED ADDRESS IN BACKOFFICE   ###############

public class ShippingAddress extends BaseTest
{

	String savedAddr;
	String poNUm;
	String shippingMethodName;
	int addressNum;
	int savedShipToAddressCount;

	@Test(groups={"reg"},description="OOB-013 844,DRM-846,847")
	public void TC00_dropShip_UI(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Clear the cart items");
		cart.clearCart();
		
		log.info("navigate to ship to page");
		checkout.navigateToShipToPage();
		Thread.sleep(5000);
		List<WebElement> allSavedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		savedShipToAddressCount = allSavedAddr.size();
		log.info("savedShipToAddressCount:- "+ savedShipToAddressCount);
		
		log.info("add address");
		for(int i=0;i<2;i++) 
		{
//			log.info("Select the first address");
//			l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
			Thread.sleep(2000);
			log.info("click on add address link");
			l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
			log.info("Verify the default checked radio button");
			sa.assertTrue(l1.getWebElement("ShipTo_Residential_RadioButton", "Checkout//ShipTo.properties").isSelected(),"Residential radio button should be selected by default");
			int k=i+1;
			checkout.addAddress(k);
			Thread.sleep(2000);
			log.info("click on continue");
			l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
			log.info("click on Edit Button to come back to Ship To Page");
			l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(0).click();
		}
		
		//Vetrify the saved address in checkout page
		allSavedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		log.info("allSavedAddr.size() :- "+ allSavedAddr.size());
		int addressRow = 1;
		for(int i=savedShipToAddressCount;i<allSavedAddr.size();i++) //here "i=3" becouse there are three ShipTo address
		{
			log.info("LOOP:-"+i);
			log.info("verify saved dropShipp addresses");
			String actualSavedAddress = allSavedAddr.get(i).getText();
			log.info("actualSavedAddress:- " + actualSavedAddress);
			String expectedSavedAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", addressRow, 9);
			gVar.assertequalsIgnoreCase(actualSavedAddress, expectedSavedAddress,"Verify the Saved address");
			addressRow++;
		}
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="OOB-013 845")
	public void TC01_checkout_verifySavedAddrDisplay() throws Exception
	{
		log.info("In saved address list, First address should be Shipping address, DropShip address should display after shipping address");
		//######## In previous testcase, dropship address is verified(its displaying after saved shipping address)
		//######## Now here are verifying whether the shipping address is in first position or not
		
		log.info("verify saved Shipping addresses");
		List<WebElement> allSavedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		log.info("allSavedAddr.size():- "+ allSavedAddr.size());
		int addressRow = 1;
		for(int i=savedShipToAddressCount-1;i<allSavedAddr.size();i++) //here "i=2" becouse there are three ShipTo address
		{
			log.info("LOOP:-"+i);
			
			log.info("verify saved dropShipp addresses");
			String actualSavedAddress = allSavedAddr.get(i).getText();
			log.info("actualSavedAddress:- " + actualSavedAddress);
			String expectedSavedAddress;
			if (i==2)
			{
				expectedSavedAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 9);
			}
			else
			{
				expectedSavedAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", addressRow, 9);
				addressRow++;
			}
			gVar.assertequalsIgnoreCase(actualSavedAddress, expectedSavedAddress,"Verify the Saved address");
			
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM0-848,849")
	public void TC02_checkout_verifyReqFields() throws Exception
	{
		log.info("Select the first address");
		l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		
		log.info("click on add address link");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		log.info("verify error messages");
		log.info("first name error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Name_Error_Msg", "Checkout//ShipTo.properties"),"First name");
//		sa.assertEquals(gVar.getCssValue("ShipTo_Name_Error_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("Address 1 name error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Addr1_Erro_Msg", "Checkout//ShipTo.properties"),"Address1 name");
//		sa.assertEquals(gVar.getCssValue("ShipTo_Addr1_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("city name error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_City_Erro_Msg", "Checkout//ShipTo.properties"),"city name");
//		sa.assertEquals(gVar.getCssValue("ShipTo_City_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("state error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_State_Erro_Msg", "Checkout//ShipTo.properties"),"state name");
//		sa.assertEquals(gVar.getCssValue("ShipTo_State_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("zip error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Zip_Erro_Msg", "Checkout//ShipTo.properties"),"zip name");
//		sa.assertEquals(gVar.getCssValue("ShipTo_Zip_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("Phone error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Phone_Erro_Msg", "Checkout//ShipTo.properties"),"Phone name");
//		sa.assertEquals(gVar.getCssValue("ShipTo_Phone_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM0-854")
	public void TC03_checkout_verifyCancelLink() throws Exception
	{
		log.info("click on cancel link");
		l1.getWebElement("ShipTo_Cancel_Link","Checkout//ShipTo.properties").click();
		
		String addressListStyle = gVar.assertEqual("ShipTo_AddressList", "Checkout//ShipTo.properties","style");
		String addressFormStyle = gVar.assertEqual("ShipTo_AddressForm", "Checkout//ShipTo.properties","style");
		String styleBlock = "display: block;";
		String styleNone = "display: none;";
		
		log.info("Address form should be closed and Address list should be displayed");
		gVar.assertequalsIgnoreCase(addressListStyle, styleBlock,"Address List should display");
		gVar.assertequalsIgnoreCase(addressFormStyle, styleNone,"Address Form should close");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM-842")
	public void TC04_SaveShippedAddress() throws Exception
	{
		
		log.info("fetch selecting saved address");
		int val = 4;
		savedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties").get(val).getText();
		log.info("select saved address");
		l1.getWebElements("ShipTo_SavedAddr_Radio","Checkout//ShipTo.properties").get(val).click();
		log.info("click on continue button in shipTo section");
		//######### There are two types of continue button.
		//######### "ShipTo_Continue_button":- if Saved SHIPPING ADDRESS is selected
		//"ShipTo_Continue_button_ForDropshippAddress" :- If saved DROPSHIPP ADDRESS is selected
		l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
		
		log.info("Premium Delivery Options Available pop up need to handel");
		if (gVar.assertVisible("ShipToPopUp_ProductsSection", "Checkout//ShipTo.properties"))
		{
			log.info("Check the products checkbox");
			l1.getWebElement("ShipToPopUp_ProductsCheckbox", "Checkout//ShipTo.properties").click();
			
			log.info("Click on CONTINUE button in popup");
			l1.getWebElement("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties").click();
		}
		
		log.info("verify navigation to shipping method section");
		sa.assertTrue(gVar.assertVisible("ShippingMtd_ShipAddress_Heading", "Checkout//ShippingMethod.properties"),"shipping address heading");
		sa.assertEquals(gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"),
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 2, 13),"Verify ShipTo_Shipping_Addr");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="OOB-013 DRM0-853")
	public void TC05_checkout_verifyOrderReviewDetails() throws Exception
	{
		log.info("Collect the shipping method");
//		List<WebElement> allShipMtd=l1.getWebElements("ShippingMtd_DeliveryMtd", "Checkout//ShippingMethod.properties");
		List<WebElement> allShipMtdNames=l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		log.info("allShipMtdNames size:- "+ allShipMtdNames);
		
		int val = 0;
		String selShipMtdVal=allShipMtdNames.get(val).getText();
		log.info("selShipMtdVal:- "+ selShipMtdVal);
		log.info("Select the shipping method");
		allShipMtdNames.get(val).click();
		gVar.waitForElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties", 8000);
		log.info("click on continue button in Shipping Method section");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		log.info("Enter PO number");
		String PONumberVal=gVar.generateRandomNum()+"";
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		BaseTest.log.info("click on Continue button in Payment section");
		BaseTest.l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
		log.info("verify payment type and shipping method value");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"),
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 2, 13),"Verify the ShipTo address");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties"), 
				selShipMtdVal,"Verify the shipping method");
		String actualPOnumber = gVar.assertEqual("PONumber", "Checkout//PaymentType.properties").split(":")[1].trim();
		sa.assertEquals(actualPOnumber, PONumberVal,"Vetrify the PO number");
		
		sa.assertAll();
		
		s.navigateToCartPage();
	}

	
	@Test(groups={"reg"}, description="Extra test case Verifying the UI of Checkout page")
	public void TC06_extraTestCase_VerifyingTheUIofCheckoutPage() throws Exception
	{
		String expectedStepCount = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 6, 0);
		log.info("Navigate to Ship to section");
		log.info("click on Edit Button to come back to Ship To Page");
		l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(0).click();
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(4000);
		}
		log.info("Verify the step count");
		sa.assertEquals(gVar.assertEqual("Checkout_StepCount", "Checkout//Checkout.properties"), 
				expectedStepCount,"Verify the page count in shipTo section");
		log.info("Select the address");
		addressNum = 0;
		l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties").get(addressNum).click();
		
		log.info("Click on Continue button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="Extra test case Verifying Shipping Method section in Checkout page")
	public void TC07_verifyShippingMethodSection() throws Exception
	{
		
		String expectedStepCount = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 7, 0);
		log.info("Verify the step count in Shipping Method section");
		sa.assertEquals(gVar.assertEqual("Checkout_StepCount", "Checkout//Checkout.properties"), 
				expectedStepCount,"Verify the page count in SHIPPING METHODS section");
		
		List<WebElement> allMethods = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		log.info("allMethods size:- "+ allMethods.size());
		for (int i = 0; i < allMethods.size(); i++)
		{
			allMethods = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
			log.info("LOOP:- "+ i);
			shippingMethodName = allMethods.get(i).getText();
			log.info("shippingMethod:- "+ shippingMethodName);
			log.info("Select the shipping methods");
			allMethods.get(i).click();
			Thread.sleep(1000);
			
			log.info("Click on Continue button in SHIPPING METHOD section");
			l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
			
			log.info("Verify the selected shipping method");
			sa.assertEquals(gVar.assertEqual("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties"), 
					shippingMethodName,"Verify the shipping method");
			
			log.info("Click on edit link in SHIPPING METHODS section");
			int temp = l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").size();
			l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(temp-1).click();
			if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
			{
				Thread.sleep(4000);
			}
			
		}
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="Extra test case Verifying the UI of Payment Method section in Checkout page")
	public void TC08_verifyPaymentSection() throws Exception
	{
		
		log.info("Click on Continue button in SHIPPING METHOD section");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Verify the Payment and Billing section");
//		verify the radio buttons and labels in payment method section
		sa.assertTrue(gVar.assertVisible("CardPaymentRadioButton", "Checkout//PaymentType.properties"),"Verify Card Payment Radio Button");
		sa.assertTrue(gVar.assertVisible("CardPaymentLabel", "Checkout//PaymentType.properties"),"Verify Card Payment Label");
		sa.assertEquals(gVar.assertEqual("CardPaymentLabel", "Checkout//PaymentType.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 0),"Card Payment Label text");
		
		sa.assertTrue(gVar.assertVisible("AccountPaymentRadioButton", "Checkout//PaymentType.properties"),"Verify Account Payment Radio Button");
		sa.assertTrue(l1.getWebElement("AccountPaymentRadioButton", "Checkout//PaymentType.properties").isSelected(),"Account Payment radio button should be selected");
		sa.assertTrue(gVar.assertVisible("AccountPaymentLabel", "Checkout//PaymentType.properties"),"Verify Account Payment Label");
		sa.assertEquals(gVar.assertEqual("AccountPaymentLabel", "Checkout//PaymentType.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 3, 0),"Account Payment Label text");
		
		log.info("Verify the ADD COMMENTS link");
		sa.assertEquals(gVar.assertEqual("AddCommentsLink", "Checkout//PaymentType.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 1),"Verify the ADD COMMENTS link");
		
		log.info("Click on ADD COMMENTS link");
		l1.getWebElement("AddCommentsLink", "Checkout//PaymentType.properties").click();
		
		log.info("Verify the Comment box");
		sa.assertTrue(gVar.assertVisible("CommentTextBox", "Checkout//PaymentType.properties"),"Verify the comment box");
		sa.assertTrue(gVar.assertVisible("RemoveCommentLink", "Checkout//PaymentType.properties"),"Verify the Remove link");
		
		log.info("Verify the STEP count in PAyment section");
		String expectedStepCount = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 8, 0);
		sa.assertEquals(gVar.assertEqual("Checkout_StepCount", "Checkout//Checkout.properties"), 
				expectedStepCount,"Verify the page count in PAYMENT section");
		log.info("Enter PO number");
		poNUm = gVar.generateRandomNum()+"";
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNUm);
		
		log.info("Click on CONTINUE BUTTON in payment and billing section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			Thread.sleep(4000);
		}
		
		log.info("Step count should not display");
		sa.assertFalse(gVar.assertVisible("Checkout_StepCount", "Checkout//Checkout.properties"),"Step count should not display");
		sa.assertEquals(gVar.assertEqual("OrderReview_YourCartTitle", "Checkout//OrderReview.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 4, 0),"Your cart title");
		sa.assertEquals(gVar.assertEqual("OrderReview_CartTotalItem", "Checkout//OrderReview.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 5, 0),"cart total");
		sa.assertEquals(gVar.assertEqual("OrderReview_HideDetails", "Checkout//OrderReview.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 1),"Verify the Hide Details link");
		
		log.info("Click on HIDE DETAIL link");
		l1.getWebElement("OrderReview_HideDetails", "Checkout//OrderReview.properties").click();
		
		log.info("Verify the SHOW link");
		sa.assertEquals(gVar.assertEqual("OrderReview_ShowDetails", "Checkout//OrderReview.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 3, 1),"Verify the SHOW Details link");
		sa.assertTrue(gVar.assertEqual("OrderReview_ShowDetails", "Checkout//OrderReview.properties", "style").contains("block"),"Verify the active show details link");
		
		log.info("Click on show details link");
		l1.getWebElement("OrderReview_ShowDetails", "Checkout//OrderReview.properties").click();
		log.info("Vetify the active Hide Detail link");
		String hideDetailsLinkStyle = gVar.assertEqual("OrderReview_HideDetails", "Checkout//OrderReview.properties", "style");
		log.info("hideDetailsLinkStyle:- "+ hideDetailsLinkStyle);
		
		sa.assertTrue(hideDetailsLinkStyle.contains("block"),"Verify the active Hide details link");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="Extra test case Verifying the pre selected values after clicking on Edit link in each section")
	public void TC09_extraTestCase_VerifyingPreSelectedVAlues () throws Exception
	{
		log.info("Click on Edit link in payment section ");
		int numberOfEditLink = l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").size();
		log.info("numberOfEditLink:- "+ numberOfEditLink);
		l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(numberOfEditLink-1).click();
		log.info("verify the pre selected PO number");
		sa.assertEquals(gVar.assertEqual("PT_POBox_Textbox", "Checkout//PaymentType.properties", "value"), 
				"","Verify the pre populated PO number");
		
		log.info("Clic ok Edit link in Shipping Method section");
		numberOfEditLink = l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").size();
		log.info("numberOfEditLink:- "+ numberOfEditLink);
		l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(numberOfEditLink-1).click();
		
		log.info("Verify the pre selected Shipping method");
		sa.assertEquals(gVar.assertEqual("ShippingMtd_Selected", "Checkout//ShippingMethod.properties"), 
				shippingMethodName,"Verify the shipping method");
		
		log.info("Click on Edit button in ShipTo section");
		l1.getWebElement("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").click();
		
		log.info("Verify the pre selected address");
		sa.assertTrue(l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties").get(addressNum).isSelected(),"Verify the pre selected address");
			
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="Extra test case Verifying the contact Us link in checkout page")
	public void TC10_verifyTheContactUsLinkFunctionalityInCheckout() throws Exception
	{
		
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Verify the CONTACT US link in header");
			sa.assertEquals(gVar.assertEqual("Checkout_Header_ContactUsLink_Mobile", "Checkout//Checkout.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 4, 1), "verify the Contact Us Link in checkout page header section");
			log.info("Click on Contact US link");
			l1.getWebElement("Checkout_Header_ContactUsLink_Mobile", "Checkout//Checkout.properties").click();
		}
		else
		{
			log.info("Verify the CONTACT US link in header");
			sa.assertEquals(gVar.assertEqual("Checkout_Header_ContactUsLink", "Checkout//Checkout.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 4, 1), "verify the Contact Us Link in checkout page header section");
			log.info("Click on Contact US link");
			l1.getWebElement("Checkout_Header_ContactUsLink", "Checkout//Checkout.properties").click();
		}
		
		
		log.info("Switch to iframe");
		WebElement contactUsIframe = l1.getWebElement("Checkout_ContactUsIframe", "Checkout//Checkout.properties");
		driver.switchTo().frame(contactUsIframe);
		
		log.info("Verify the popuo header");
		sa.assertEquals(gVar.assertEqual("Checkout_ContactUsPopUp_Heading", "Checkout//Checkout.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 9, 0),"Verify the heading");
		
		log.info("Click on MINI mize icon in popup");
		l1.getWebElement("Checkout_MinimizeIcon", "Checkout//Checkout.properties").click();
		
		driver.switchTo().defaultContent();
		
		sa.assertAll();
	}
	
	
	/*
	@Test(groups={"reg"},description="OOB-013 DRM-843")
	public void TC10_verifySaveShippedAddress_ByOtherUser() throws Exception
	{
		sa.assertFalse(true,"Ask arjun., Testcase:- verify the saved address in profile by other user");
	}
	*/
	
	@AfterClass(groups ={"reg","sanity_guest","sanity_reg"})
	void TC11_navigateToHomePage() throws Exception
	{
		s.navigateToHomePage();
	}
}
