package com.drivemedical.checkout;

//import org.testng.annotations.AfterClass;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;

public class ShipTo extends BaseTest
{
	@Test(groups={"reg"}, description="extraTc-Verify the selected address in ShipTo section when navigated from cart page")
	public void TC00_VerifyTheSelectedAddressWhenNavigatedFromCart() throws Exception
	{
		log.info("LOGIN to the application");
		p.navigateToLoginPage();
		p.logIn(BaseTest.xmlTest);
		log.info("navigate to shipTo page");
		checkout.navigateToShipToPage();
		log.info("One address should be selected bydefault");
		
		sa.assertEquals(l1.getWebElements("ShipTo_SelectedAddress", "Checkout//ShipTo.properties").size(), 1,"One address should be selected in Ship Address section");
		
		log.info("Count the number of address");
		List<WebElement> allRadioButtons = l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties");
		int numberOfButtonms = allRadioButtons.size();
		log.info("numberOfButtonms:- "+ numberOfButtonms);
		List<WebElement> addressNames = l1.getWebElements("ShipTo_SavedAddr_Name", "Checkout//ShipTo.properties");
		log.info("addressNames size:- "+ addressNames.size());
		if (numberOfButtonms>1)
		{
			for (int i = 0; i < numberOfButtonms; i++)
			{
				allRadioButtons = l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties");
				addressNames = l1.getWebElements("ShipTo_SavedAddr_Name", "Checkout//ShipTo.properties");	
				log.info("LOOP:-"+i);
				log.info("Select the radio buttons one by one");
				allRadioButtons.get(i).click();
				log.info("Collect the address name");
				String addressName = addressNames.get(i).getText().toLowerCase();
				
				log.info("Click on CONTINUE button in ShipTo Section");
				l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
				
				log.info("Verify the address");
				String savedShipToAddress = l1.getWebElement("ShippingMtd_SavedShipToAddress", "Checkout//ShippingMethod.properties").getText().toLowerCase();
				sa.assertTrue(savedShipToAddress.contains(addressName),"Verify the Saved address in Saved ShipTo Section");

				log.info("Click on Return to CArt link");
				if (gVar.mobileView()||gVar.tabletView())
				{
					l1.getWebElement("Checkout_ReturnToCart_Link_Mobile", "Checkout//Checkout.properties").click();
				}
				else
				{
					l1.getWebElement("Checkout_ReturnToCart_Link", "Checkout//Checkout.properties").click();
				}
				
				log.info("Click on Checkout button in cart page");
				l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
				
				log.info("Verify the selected address");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("ShipTo_SelectedAddressName", "Checkout//ShipTo.properties"), 
						addressName,"Verify the Selected address name");
			}
		}
		else
		{
			log.info("Only Address is there. Add multiple address and execute it");
			sa.assertFalse(true,"Only Address is there. Add multiple address and execute it");
		}
		
		sa.assertAll();
	}

	@Test(groups={"reg"},description="OOTB-013 DRM-848,849")
	public void TC01_verifyRequiredFields() throws Exception
	{
		log.info("expand add address section");
		l1.getWebElement("ShipTo_AddAddress_Link", "Checkout//ShipTo.properties").click();
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("verify displayed error messages");
		log.info("Name");
		sa.assertTrue(gVar.assertVisible("ShipTo_Name_Error_Msg", "Checkout//ShipTo.properties"),"First Name");
		log.info("Address 1");
		sa.assertTrue(gVar.assertVisible("ShipTo_Addr1_Erro_Msg", "Checkout//ShipTo.properties"),"Address 1");
		log.info("City");
		sa.assertTrue(gVar.assertVisible("ShipTo_City_Erro_Msg", "Checkout//ShipTo.properties"),"City");
		log.info("State");
		sa.assertTrue(gVar.assertVisible("ShipTo_State_Erro_Msg", "Checkout//ShipTo.properties"),"State");
		log.info("Zip");
		sa.assertTrue(gVar.assertVisible("ShipTo_Zip_Erro_Msg", "Checkout//ShipTo.properties"),"Zip");
		log.info("phone");
		sa.assertTrue(gVar.assertVisible("ShipTo_Phone_Erro_Msg", "Checkout//ShipTo.properties"),"Phone");
		
		sa.assertAll();
	}
	
	@AfterClass(groups ={"reg","sanity_guest","sanity_reg"})
	void navigateToHomePage() throws Exception
	{
		log.info("Navigate to Home page");
		s.navigateToHomePage();
	}
}
