package com.drivemedical.checkout;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.CheckoutFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CreditCardPayment extends BaseTest
{
	String paymentTypeProperties = "Checkout//PaymentType.properties";
	String checkoutProperties = "Checkout//Checkout.properties";
	String orderReviewProperties = "Checkout//OrderReview.properties";
	String orderConfirmationProperties = "Checkout//OrderConfirmation.properties";
	String cardType;
	String nameOnCard;
	String cardNumber;
	String month;
	String year;
	int rowNum;
	String poNum;
	int savedCardsCount;
	String expectedBillingAddress;
	ArrayList<String> allCardsList;
	
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1604")
	public void TC00_CreditCardOrderPlacement() throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Clear Cart");
//		cart.clearCart();
		
		log.info("Delete All Saved Cards");
//		deleteSavedCards();
		log.info("Navigate to payment method section in checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		Thread.sleep(1000);
		
		log.info("Verify the UI of the Credit card form");
		verifyCreditCardFormUi();
		
		log.info("Enter valid credit card details");
		String poNum = gVar.generateRandomNum()+"";
		rowNum = 3;
		enterCreditCardDetails(poNum, rowNum, true);
		
		Thread.sleep(3000);
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		Thread.sleep(2000);
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNum,"Verify the PO number");
		String savedCardNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 6);
		String month = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 3).replace("0", "");
		String year = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 4);
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 0);
		String expectedCardDetails = "Method:"+cardType+" "+savedCardNum+" Exp "+month+"."+year;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		Thread.sleep(5000);
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		Thread.sleep(5000);
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		Thread.sleep(5000);
		log.info("Verify the Order Confirmation page");
		verifyOrderConfirmationPage(poNum, expectedBillingAddress);
		
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="OOB-062 DRM-1769 DRM-1770 DRM-1771 DRM-1772")
	public void TC01_AccountPaymentSavedCardDisplay() throws Exception
	{
		log.info("Navigate to Account Payments page");
		s.navigateToAccountPaymentsPage();
		
		log.info("Verify the Account Payment page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AP_Heading", "Profile//AccountPayments.properties"), 
				"Payment Details","Verify the Payment Details heading");
		
		log.info("Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				"Payment Details","Verify the Payment Details");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_LeftNav_ActiveLink", "Profile//MyAccount.properties"), 
				"Payment Details","Verify the Payment Details");
		
		List<WebElement> savedCardSection = l1.getWebElements("AP_SavedCards", "Profile//AccountPayments.properties");
		List<WebElement> deleteLinks = l1.getWebElements("AP_SavedCardsDeleteButton", "Profile//AccountPayments.properties");
		
		log.info("savedCardSection size:- "+ savedCardSection);
		for (int i = 0; i < savedCardSection.size(); i++)
		{
			log.info("LOOP:- "+ i);
			gVar.assertequalsIgnoreCase(gVar.assertEqual(deleteLinks.get(i).getText(), "Profile//AccountPayments.properties",i), 
					" Delete", "Verify the delete link");
		}
		
		log.info("Verify the Default Payment label");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AP_SavedCardAndBillingDetails", "Profile//AccountPayments.properties",0), 
				"Default Payment","Verify the Default Payment label");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AP_MakeDefault_Link", "Profile//AccountPayments.properties"), 
				"Make default","Verify the Make default link");
		sa.assertEquals(l1.getWebElements("AP_MakeDefault_Link", "Profile//AccountPayments.properties").size(), 
				1,"Verify the count of Make default links");
		
		//------------VERIFY THE SAVED CARD -----------------
		List<WebElement> li = l1.getWebElements("AP_SavedCardAndBillingDetails", "Profile//AccountPayments.properties");
		String str1 = li.get(1).getText();
		log.info("str1:- "+ str1);
		
		String savedCardType = str1.split(" ")[0].trim();
		gVar.assertequalsIgnoreCase(savedCardType, cardType,"Verify the Card type");
		String savedCardNum = str1.split(" ")[1].trim();
		String expectedCardNumber = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
		gVar.assertEqual(savedCardNum, "****"+expectedCardNumber,"Verify the saved card");
		
		String expiryMonthYear = li.get(2).getText();
		log.info("expiryMonthYear:- "+ expiryMonthYear);
		String expectedExpiryMonthYear = month+" / "+year;
		gVar.assertEqual(expiryMonthYear, expectedExpiryMonthYear,"Verify the Expairy Year");
		
		log.info("Verify the Billing address in ACCOUNT PAYMENT page");
		String address1 = li.get(3).getText();
		String address2 = li.get(4).getText();
		String address3 = li.get(5).getText();
		String address4 = li.get(6).getText();
		String savedBillingAddress = address1 +" "+ address2 +" "+ address3 +" "+ address4;
		log.info("ALL ADDRESS:-"+savedBillingAddress );
		
		gVar.assertequalsIgnoreCase(savedBillingAddress, expectedBillingAddress,"Verify the saved billing address");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1600")
	public void TC02_PaymentNewCreditCardFormUi() throws Exception
	{
		log.info("Navigate to payment method section in checkout page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		
		log.info("Verify the ADD CARD link");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_AddCard_Link", paymentTypeProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 7, 1),"Verify the ADD CARD link");
		
		log.info("Click on ADD CARD link");
		l1.getWebElement("CC_AddCard_Link", paymentTypeProperties).click();
		
		log.info("Verify the UI of Credit card form");
		verifyCreditCardFormUi();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1670")
	public void TC03_CheckoutCreditCardLogo(XmlTest xmlTest) throws Exception
	{
		
		log.info("Verify the CreditCard Image in the card number box");
		for (int i = 1; i <= 4; i++)
		{
			log.info("LOOP:- "+ i);
			String number = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", i, 7);
			String cardName = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", i, 6);
			log.info("Enter the first four digit number in credit card number");
			l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).clear();
			l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).sendKeys(number);
			l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).sendKeys(Keys.TAB);
			log.info("Verify the Image");
			gVar.assertEqual(gVar.assertEqual("CC_SelectedCreditCardImage", paymentTypeProperties,"alt"), 
					cardName,"Verify the selected credit card image name -LOOP:-"+ i);
			
		}
		
		sa.assertAll();
	}
	
	
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1598")
	public void TC04_PaymentNonUniquePONumber() throws Exception
	{
		log.info("Enter already Used the PO number and Credit card details in CREDIT CARD section");
		String usedPoNumber = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 8);
		enterCreditCardDetails(usedPoNumber, 3, true);
		
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		
		log.info("Verify the PO number exist alert message");
		gVar.assertEqual(gVar.assertEqual("Checkout_PoNumberAlertMessage", checkoutProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 8),"Verify the PO number alert message");
		
		log.info("Verify the close button");
		sa.assertTrue(gVar.assertVisible("Checkout_PoNumberAlertClose_Button", checkoutProperties),"Verify the close button");
		
		log.info("Click on Close button");
		l1.getWebElement("Checkout_PoNumberAlertClose_Button", checkoutProperties).click();
		
		log.info("Alert message should not display");
		sa.assertFalse(gVar.assertVisible("Checkout_PoNumberAlertMessage", checkoutProperties),"Alert message should not display"); 
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1599")
	public void TC05_PaymentNewCreditCard() throws Exception
	{
		log.info("Enter the valid Credit card details");
		poNum = gVar.generateRandomNum()+"";
		rowNum = 3;
		enterCreditCardDetails(poNum, 3, true);
		
		log.info("Click on Order Review button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNum,"Verify the PO number");
		String savedCardNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 6);
		String month = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 3).replace("0", "");
		String year = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 4);
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", rowNum, 0);
		String expectedCardDetails = "Method:"+cardType+" "+savedCardNum+" Exp "+month+"."+year;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1601 DRM-1605")
	public void TC06_PaymentCardSaveFunctionality() throws Exception
	{
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		log.info("Verify the Order Confirmation page");
		verifyOrderConfirmationPage(poNum, expectedBillingAddress);
		
		//Verify the billing address in OCP
		log.info("Navigate to Accounts Payments page");
		s.navigateToAccountPaymentsPage();
		log.info("Verify the page heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AP_Heading", "Profile//AccountPayments.properties"), 
				"Payment Details","Verify the Payment Details heading");
		
		log.info("Count the number of saved cards");
		savedCardsCount = l1.getWebElements("AP_SavedCards", "Profile//AccountPayments.properties").size();
		log.info("savedCardsCount:- "+ savedCardsCount);
		
		log.info("Verify the payment and billing details");
		gVar.assertEqual(gVar.assertEqual("AP_SavedCardAndBillingDetails", "Profile//AccountPayments.properties"), 
				expectedBillingAddress,"Verify the billing and payment details");
		
		
		//Collect the Credit card details (Card number )
		List<WebElement> cardLists = l1.getWebElements("AP_SavedCardList", "Profile//AccountPayments.properties");
		log.info("cardLists size:- "+ cardLists.size());
	//Collect the Cards details
		
		allCardsList = new ArrayList<String>();
		for (int i = 0; i < cardLists.size(); i++)
		{
			log.info("LOOP:-"+i);
//			List<WebElement> cardDetails  = cardLists.get(i).findElements(l1.getByReference("AP_SavedCardAndBillingDetailsWithDot", "Profile//AccountPayments.properties"));

			String details = cardLists.get(i).getText();
			log.info("details:- "+ details);
			details = details.substring(0, details.lastIndexOf("/")+6).trim();
//			for (int j = 0; j < 3; j++)
//			{
//				String card = cardDetails.get(j).getText();
//				al.add(card);
//			}
			allCardsList.add(details);
			
		}
		log.info("allCardsList:- "+ allCardsList);
		log.info("allCardsList size:- "+ allCardsList.size());
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1602")
	public void TC07_PaymentSavedCardSelectBoxFunctionality() throws Exception
	{
		log.info("Navigate to Payments and Billing page");
		String product = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToPaymentTypePage(product, 1, 1);
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		
		log.info("Verify the Saved Cards Dropdown box");
		WebElement savedCardSelectBox = l1.getWebElement("CC_SavedCardsSelectBox", paymentTypeProperties);
		Select sel = new Select(savedCardSelectBox);
		int cnt = sel.getOptions().size();
		sa.assertEquals(cnt, savedCardsCount,"Verify the number of options");
		ArrayList<String> savedCardsInCheckout = new ArrayList<String>();
		for (int i = 0; i < cnt; i++)
		{
			String option = sel.getOptions().get(i).getText();
			log.info("option:- "+option);
			savedCardsInCheckout.add(option);
		}
		log.info("savedCardsInCheckout:- "+ savedCardsInCheckout);
		
		sa.assertEquals(savedCardsInCheckout, allCardsList,"Compare the Card details which same as in Account Payments section");
		
		log.info("Select the saved Cards and verify the selected value");
		for (int i = 0; i < cnt; i++)
		{
			log.info("LOOP:-"+i);
			savedCardSelectBox = l1.getWebElement("CC_SavedCardsSelectBox", paymentTypeProperties);
			 sel = new Select(savedCardSelectBox);
			 String card = sel.getOptions().get(i).getText();
			 log.info("card:- "+ card);
			 
			 log.info("Select the saved card in dropdown option");
			 sel.selectByIndex(i);
			 String expectedVal = "Method : "+card;
			 gVar.assertEqual(gVar.assertEqual("CC_SavedCardsDetailInBillingSection", paymentTypeProperties), 
					 expectedVal,"Verify the Saved card in billing address section");
			 log.info("Verify the Selected option in dropdown box");
			 gVar.assertEqual(gVar.assertEqual("CC_SavedCardsDropDownBox", paymentTypeProperties,"title"), 
					 expectedVal,"Verify the selected option in dropdown box");
			 
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1603 DRM-1605")
	public void TC08_PaymentSavedCardCVNFunctionality() throws Exception
	{
		String cardDetails = l1.getWebElement("CC_SavedCardsDropDownBox", paymentTypeProperties).getAttribute("title");
		log.info("cardDetails:- "+ cardDetails);
		log.info("Verify the CVN field");
		sa.assertTrue(gVar.assertVisible("CC_SavedCardCVV_Textbox", paymentTypeProperties),"CVV");
		gVar.assertEqual(gVar.assertEqual("CC_SavedCardCVV_Textbox", paymentTypeProperties,"value"), "","Verify the CVV field");
		
		log.info("Enter the CVV");
		String cvv = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", 3, 5);
		l1.getWebElement("CC_SavedCardCVV_Textbox", paymentTypeProperties).sendKeys(cvv);
		
		log.info("Enter the PO number");
		String poNumber = gVar.generateRandomNum()+"";
		l1.getWebElement("CC_PoNUmberBox", paymentTypeProperties).sendKeys(poNumber);
		
		log.info("Click on REVIEW ORDER button");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		
		log.info("Verify the Order Review Section");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PONumber", paymentTypeProperties), 
				"PO:"+poNumber,"Verify the PO number");
		
		String expectedCardDetails = "Method:"+cardDetails;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreditCardDetails", paymentTypeProperties), 
				expectedCardDetails,"Verify the saved card details");
		
		log.info("Verify the Billing address");
		String expectedBillingAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 14);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BillingAddressDetails", paymentTypeProperties), 
				expectedBillingAddress,"Verify the saved billing address");
		
		log.info("Check the checkbox");
		l1.getWebElement("Checkout_Freight_Checkbox", checkoutProperties).click();
		
		log.info("Click on PLACE order button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", orderReviewProperties).click();
		
		log.info("Verify the Order confirmationpage");
		log.info("Verify the Order Confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Order Confirmation heading");
		log.info("verify PO Number in Order Confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Text", "Checkout//OrderConfirmation.properties"),"Po Number text");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"),"Po Number value");
		sa.assertEquals(gVar.assertEqual("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"), poNum,"Po Number value");
	
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText();
		log.info("orderID:- "+ orderID);
		
		log.info("Verify order status");
		sa.assertTrue(gVar.assertVisible("Orderconfirm_Status","Checkout//OrderConfirmation.properties"),"Order Status");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Orderconfirm_UserEmailId", orderConfirmationProperties), 
				BaseTest.xmlTest.getParameter("email"),"Verify the User Email in Order Confirmation page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1606")
	public void TC09_PaymentCreditCardValidation() throws Exception
	{
		log.info("Click Review Order Button without entering credit card details");
		l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
		
		log.info("Verify the Error message");
		List<WebElement> errorMessages = l1.getWebElements("CC_ErrorMessgaeForEmptyBox", paymentTypeProperties);
		sa.assertEquals(errorMessages.size(), 7,"Verify the number of Error message");
		for (int i = 0; i < errorMessages.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String em = errorMessages.get(i).getText();
			gVar.assertequalsIgnoreCase(em, GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 11),"Verify the ERROR message - LOOP:- "+i);
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1607")
	public void TC10_PaymentCreditCardValidationEmptyCardNumberField() throws Exception
	{
		for (int i = 0; i < 2; i++)
		{
			log.info("Enter all details except CArd number");
			String poNumber = gVar.generateRandomNum()+"";
			l1.getWebElement("CC_PoNUmberBox", paymentTypeProperties).sendKeys(poNumber);
			WebElement selectBox = l1.getWebElement("CC_CreditCardSelectBox", paymentTypeProperties);
			Select sel = new Select(selectBox);
			log.info("Select the Credit card type");
			sel.selectByIndex(2);
			
			log.info("Enter Name on card");
			l1.getWebElement("CC_CreditCardNameTextbox", paymentTypeProperties).sendKeys("Test");
			if (i==1)
			{
				log.info("Enter the Invalid card number");
				l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).sendKeys("1234567890123456");
			}
			
			log.info("Select the Month");
			WebElement monthSelectBox = l1.getWebElement("CC_ExpairyMonthSelectBox", paymentTypeProperties);
			new Select(monthSelectBox).selectByVisibleText("10");
			
			log.info("Select the Year");
			WebElement yearSelectBox = l1.getWebElement("CC_ExpairyYearSelectBox", paymentTypeProperties);
			new Select(yearSelectBox).selectByVisibleText("2022");
			
			log.info("Enter CVV number");
			l1.getWebElement("CC_CVVTextbox", paymentTypeProperties).sendKeys("123");
			
			log.info("Click on Review Order Button");
			l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
			
			log.info("VErify the ERROR message");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_CreditCardNameTextbox_ErrorMessage", paymentTypeProperties), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 11),"Verify the ERROR message");
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-011 DRM-1607")
	public void TC11_PaymentCreditCardValidationForMonthAndYearField() throws Exception
	{
		for (int i = 0; i < 2; i++)
		{
			log.info("Enter all details except CArd number");
			String poNumber = gVar.generateRandomNum()+"";
			l1.getWebElement("CC_PoNUmberBox", paymentTypeProperties).sendKeys(poNumber);
			WebElement selectBox = l1.getWebElement("CC_CreditCardSelectBox", paymentTypeProperties);
			Select sel = new Select(selectBox);
			log.info("Select the Credit card type");
			sel.selectByIndex(2);
			
			log.info("Enter Name on card");
			l1.getWebElement("CC_CreditCardNameTextbox", paymentTypeProperties).sendKeys("Test");

			log.info("Enter the Invalid card number");
			l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).sendKeys("1234567890123456");
			
			log.info("Select the Month");
			WebElement monthSelectBox = l1.getWebElement("CC_ExpairyMonthSelectBox", paymentTypeProperties);
			log.info("Select the Year");
			WebElement yearSelectBox = l1.getWebElement("CC_ExpairyYearSelectBox", paymentTypeProperties);

			if (i==0)
			{
				new Select(monthSelectBox).selectByIndex(0);
				new Select(yearSelectBox).selectByIndex(0);
			}
			else 
			{
				log.info("Select the Month");
				new Select(monthSelectBox).selectByVisibleText("1");
				log.info("Select the Year");
				new Select(yearSelectBox).selectByVisibleText("2018");
			}
			
			log.info("Enter CVV number");
			l1.getWebElement("CC_CVVTextbox", paymentTypeProperties).sendKeys("123");
			
			log.info("Click on Review Order Button");
			l1.getWebElement("ReviewOrderButton", paymentTypeProperties).click();
			
			log.info("VErify the ERROR message");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_CreditCardNameTextbox_ErrorMessage", paymentTypeProperties), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 11),"Verify the ERROR message");
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOB-062 DRM-1174")
	public void TC12_AccountPaymentMakeDefault() throws Exception
	{
		log.info("Collect the non Default address");
		List<WebElement> lis = l1.getWebElements("AP_CardDetailsOfNonDefaultSection", "Profile//AccountPayments.properties");
		ArrayList<String> nonDefaulrAddress = new ArrayList<String>();
		for (int i = 3; i <= 6; i++)
		{
			nonDefaulrAddress.add(lis.get(i).getText());
		}
		log.info("nonDefaulrAddress:- "+ nonDefaulrAddress);
		
		log.info("Click on Make default link");
		l1.getWebElement("AP_MakeDefault_Link", "Profile//AccountPayments.properties").click();
		Thread.sleep(2000);
		
		log.info("Verify the default address after clicking on Make default link");
		for (int i = 0; i <lis.size(); i++)
		{
			log.info("LOOP:-"+i);
			sa.assertEquals(lis.get(i), 
					l1.getWebElements("AP_SavedCardAndBillingDetails", "Profile//AccountPayments.properties").get(i+3).getText(),"Verify the billing address");
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOB-062 DRM-1773")
	public void TC12_AccountPaymentDeleteCard() throws Exception
	{
		log.info("Delet the saved card");
		deleteSavedCards();
		log.info("Verify the EMPTY SAVED CARDS list in ACCOUNT PAYMENTS page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AP_NoSavedDetailsHeading", "Profile//AccountPayments.properties"), 
				"No Saved Payment Details","Verify the  No Saved Payment Details");
		sa.assertTrue(gVar.assertNotVisible("AP_SavedCards", "Profile//AccountPayments.properties"),"Saved cards section should not display");
		
		sa.assertAll();
	}
	
//	+ -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- +
//	+ -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- +
	
	public void enterCreditCardDetails(String poNum, int rowNum, boolean saveCreditCard) throws Exception
	{
		Thread.sleep(1500);
		log.info("Enter PO number");
		l1.getWebElement("CC_PoNUmberBox", paymentTypeProperties).sendKeys(poNum);
		
		log.info("Enter the card type");
		cardType = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 0);
		new Select(l1.getWebElement("CC_CreditCardSelectBox",paymentTypeProperties)).selectByVisibleText(cardType);
		
		
		log.info("Enter Name on Credit card");
		nameOnCard = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 2);
		l1.getWebElement("CC_CreditCardNameTextbox", paymentTypeProperties).sendKeys(nameOnCard);
		
		log.info("Enter Card number");
		cardNumber = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 1);
		l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).sendKeys(cardNumber);
		l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).sendKeys(Keys.TAB);
		
//		l1.getWebElement("CC_CreditCArdNumberBox", paymentTypeProperties).click();
//		waitForAjax(driver);
//		l1.getWebElement("CC_CVV_Label", paymentTypeProperties).click();
		
//		WebElement valid = l1.getWebElement("CC_ValidNumberTextbox", paymentTypeProperties);
//		WebDriverWait wait=new WebDriverWait(driver, 15);
//		wait.until(ExpectedConditions.visibilityOf(valid));
		
		Thread.sleep(5000);
		
		month = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 3);
		int num = Integer.parseInt(month);
		log.info("num:- "+ num);
		l1.getWebElement("CC_ExpairyMonthDropdownBox", paymentTypeProperties).click();
		l1.getWebElements("CC_Expairy_MonthList", paymentTypeProperties).get(num).click();
//		new Select(l1.getWebElement("CC_ExpairyMonthSelectBox",paymentTypeProperties)).selectByVisibleText(month);
		
		log.info("Select Expairy Year");
		year = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 4);
//		num = Integer.parseInt(year);
//		l1.getWebElement("CC_ExpairyYearDropdownBox", paymentTypeProperties).click();
//		l1.getWebElements("CC_ExpairyYearList", paymentTypeProperties).get(5).click();
		
		
		new Select(l1.getWebElement("CC_ExpairyYearSelectBox",paymentTypeProperties)).selectByVisibleText(year);
		Thread.sleep(1000);
//		log.info("Select the CVV number");
		String cvv = GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 5);
		l1.getWebElement("CC_CVVTextbox", paymentTypeProperties).sendKeys(cvv);
		Thread.sleep(2000);
		if (saveCreditCard)
		{
			BaseTest.gVar.waitForElement("CC_SavePaymentCheckbox_Label", paymentTypeProperties, 6000);
			log.info("Check the Save Credit Card checkbox");
//			l1.getWebElement("CC_SavePaymentCheckbox", paymentTypeProperties).click();
			l1.getWebElement("CC_SavePaymentCheckbox_Label", paymentTypeProperties).click();
		}
		Thread.sleep(1000);
	}
	
	public void verifyCreditCardFormUi() throws Exception
	{
		
		log.info("Select the Credit Card radio button");
		l1.getWebElement("CardPaymentLabel", paymentTypeProperties).click();
		
		log.info("Verify the PO number box");
		sa.assertTrue(gVar.assertVisible("CC_PoNUmberBox", paymentTypeProperties),"Verify the PO number box");
		sa.assertEquals(gVar.assertEqual("CC_PoNUmberBox_Label", paymentTypeProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 3),"Verify the PO number LAbel");
		sa.assertEquals(gVar.assertEqual("CC_PoNUmberBox_Star", paymentTypeProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 3),"Verify the STAR");
		
		log.info("Credit card type");
		sa.assertTrue(gVar.assertVisible("CC_CreditCardSelectBox", paymentTypeProperties),"Card type selectbox");
//		gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_CreditCardSelectBox_Label", paymentTypeProperties), 
//				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 3, 3),"Credit card type label");
		sa.assertEquals(gVar.assertEqual("CC_CreditCardSelectBox_Star", paymentTypeProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 3, 3),"Verify CREDIT CARD TYPE label");
		
		log.info("Credit Card label");
		sa.assertTrue(gVar.assertVisible("CC_CreditCardNameTextbox", paymentTypeProperties),"Credit card name text box");
//		gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_CreditCardNameTextbox_Label", paymentTypeProperties), 
//				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 4, 3),"Credit card type label");
//		sa.assertEquals(gVar.assertEqual("CC_CreditCardNameTextbox_Star", paymentTypeProperties), 
//				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 3),"Verify the STAR in CREDIT CARD NAME label");
		
		log.info("Card Number");
		sa.assertTrue(gVar.assertVisible("CC_CreditCArdNumberBox", paymentTypeProperties),"Card Number box");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_CreditCardNameTextbox_Label", paymentTypeProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 5, 3),"Credit card type label");
//		sa.assertEquals(gVar.assertEqual("CC_CreditCardNameTextbox_Star", paymentTypeProperties), 
//				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 3),"Verify the STAR in CREDIT CARD TYPE label");

		sa.assertTrue(gVar.assertVisible("CC_ExpiryDate_Label", paymentTypeProperties),"Verify the Expiry Date label");
		sa.assertTrue(gVar.assertVisible("CC_CVV_Label", paymentTypeProperties),"Verify the CVV label");
		
		log.info("Month");
		sa.assertTrue(gVar.assertVisible("CC_ExpairyMonthSelectBox", paymentTypeProperties),"Month Select box");
		sa.assertEquals(gVar.assertEqual("CC_ExpairyMonthDropdownBox", paymentTypeProperties,"title"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 1, 4),"Month");
		
		log.info("YEAR");
		sa.assertTrue(gVar.assertVisible("CC_ExpairyYearSelectBox", paymentTypeProperties),"Year select box");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_ExpairyYearDropdownBox", paymentTypeProperties,"title"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 2, 4),"Year");
		
		sa.assertTrue(gVar.assertVisible("CC_SavePaymentCheckbox", paymentTypeProperties),"Verify the Save Payment Checkbox");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CC_SavePaymentCheckbox_Label", paymentTypeProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 6, 3),"Save Payment Info label");
	}
	
	void verifyOrderConfirmationPage(String poNumber, String expectedBillingAddress) throws Exception
	{
		Thread.sleep(4000);
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Order Confirmation heading");
		log.info("verify PO Number in Order Confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Text", "Checkout//OrderConfirmation.properties"),"Po Number text");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"),"Po Number value");
		sa.assertEquals(gVar.assertEqual("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"), poNumber,"Po Number value");
	
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText();
		log.info("orderID:- "+ orderID);
		gVar.saveOrderId(orderID, "Order placed using credit card");
		log.info("Verify order status");
		sa.assertTrue(gVar.assertVisible("Orderconfirm_Status","Checkout//OrderConfirmation.properties"),"Order Status");
		
		log.info("User Email Id");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Orderconfirm_UserEmailId", orderConfirmationProperties), 
				BaseTest.xmlTest.getParameter("email"),"Verify the User Email in Order Confirmation page");
		log.info("Verify the Payment Label");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Orderconfirm_PaymentLabel", orderConfirmationProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "OrderConfirmation", 1, 0),"Payment Label");
		log.info("Verify the Payment details");
		String expectedDetails = cardType+" "+GetData.getDataFromExcel("//data//GenericData_US.xls", "CreditCard", rowNum, 7);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Orderconfirm_PaymentDetail", orderConfirmationProperties), 
				expectedDetails,"Card details in Order confirmation page");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Orderconfirm_BillingLabel", orderConfirmationProperties,1), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "OrderConfirmation", 2, 0),"Billing address label");
		String billingADdress = gVar.assertEqual("Orderconfirm_BillingAddress", orderConfirmationProperties,1);
		log.info("billingADdress:- "+ billingADdress);
		gVar.assertEqual(billingADdress, expectedBillingAddress,"Verify the billing address");
	}
	
	void deleteSavedCards() throws Exception
	{
		log.info("Navigate to ACCOUNT PAYMENTS page");
		s.navigateToAccountPaymentsPage();
		
		List<WebElement> deleteLinks = l1.getWebElements("AP_SavedCardsDeleteButton", "Profile//AccountPayments.properties");
		log.info("deleteLinks size:-"+ deleteLinks.size());
		for (int i = 0; i < deleteLinks.size(); i++)
		{
			log.info("LOOP:-"+i);
			deleteLinks = l1.getWebElements("AP_SavedCardsDeleteButton", "Profile//AccountPayments.properties");
			deleteLinks.get(i).click();
			
			l1.getWebElement("AP_DeletePopUp_DeleteButton", "Profile//AccountPayments.properties").click();
			Thread.sleep(3000);
		}
		
	}
	
	public  void waitForAjax(WebDriver driver)throws Exception
      {
          boolean ajaxIsComplete = false;
          while (!ajaxIsComplete) // Handle timeout somewhere 
          {
              ajaxIsComplete = (boolean)((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
          }

      }
	

}
