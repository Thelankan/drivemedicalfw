package com.drivemedical.checkout;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class OrderSummary extends BaseTest
{
	String proPrice;
	
	@Test(groups={"reg"}, description="OOTB-022 DRM-899")
	public void deliveryCostsInOrderSummary() throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Navigate to PDP which associated with promotion");
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 16, 1);
		s.pdpNavigationThroughURL(searchKeyWord);
		
		log.info("Collect the product price");
		proPrice = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		
		log.info("Click on Add to cart button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		
		log.info("Navigate to CART page");
		s.navigateToCartPage();
		
		log.info("Click on Procead to Checkout button in cart page");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		
		log.info("Select shipping address");
		l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		
		log.info("Click on Continue Button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Select the Shipping method");
		List<WebElement> shippingMethodRadioButton = BaseTest.l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		shippingMethodRadioButton.get(1).click();
		log.info("click on next button");
		l1.getWebElement("ShippingMtd_Next_Btn","Checkout//ShippingMethod.properties").click();
		
		log.info("Verify the delivery costs in order summary.");
		sa.assertFalse(true, "delivery costs in order summary.");

		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-022 DRM-900")
	public void verifyTaxesInOrderSummary()
	{
		log.info("Verify the Tax in Order summary section");
		sa.assertFalse(true,"Ask Arjun");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-022 DRM-901")
	public void verifyDiscountsInOrderSummary()
	{
		 
		log.info("Verify the discounts in Order summary section");
		sa.assertFalse(true,"Ask Arjun");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-022 DRM-902")
	public void verifySubtotalInOrderSummary() throws Exception
	{
		 
		log.info("Verify the subtotal in Order summary section");
		String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 11, 0);
		expected = expected + " "+ proPrice;
		gVar.assertEqual(gVar.assertEqual("OrderSummarySubTotal", "Checkout//Checkout.properties"), 
				expected,"Verify the Subtotal in Order Summary section");
		
		sa.assertAll();
	}
	
	/*@Test(groups={"reg"}, description="OOTB-022 DRM-903")
	public void verifyOrderTotalInOrderSummary() throws Exception
	{
		//Check with arjun whether which and all price will display to calculate Order Total price
		log.info("Verify the Order Total in Order summary section");
		String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "Checkout", 12, 0);
		expected = expected + " "+ total amount;
		gVar.assertEqual(gVar.assertEqual("OrderSummaryOrderTotal", "Checkout//Checkout.properties"), 
				expected,"Verify the Order Total in Order Summary section");
		
		double expectedTotalAmount = total amount;
		sa.assertEquals(gVar.assertEqual("OrderSummaryOrderTotalAmount", "Checkout//Checkout.properties"), 
				expectedTotalAmount,"Verify the total amount in Order Summary section");
		
		sa.assertAll();
	}*/
	
}
