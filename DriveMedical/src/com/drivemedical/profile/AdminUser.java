package com.drivemedical.profile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.xerces.util.XML11Char;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.GlobalFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class AdminUser extends BaseTest{

	boolean verify;
	int count;
	List<WebElement> nameLinks ;
	List<WebElement> editLinks;
	List<WebElement> status;
	List<WebElement> roles;
	String userStatus;
	int numberOfProducts;
	
	String firstName;
	String lastName;
	int rowCnt;
	String emailId;
	
	String updatedFName;
	String updatedLName;
	String updatedEmail;

	
	@Test(groups={"reg"}, description="OOTB-072 DRM-608")
	public void TC00_verifyManageUserNames(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to LOGIN page");
		p.navigateToLoginPage();
		
		log.info("Login to the application");
		p.logIn(xmlTest);
		
		log.info("Navigate to manage user page");
		s.navigateToUsersPage();
		
		log.info("Verify the names");
		List<WebElement> userNames = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		log.info("userNames:- "+ userNames.size());
		
		String results = l1.getWebElement("ManageUser_PaginationText_Top", "Profile//AdminUser.properties").getText();
		results = results.substring(results.indexOf("-")+1, results.indexOf("o")-1).trim();
		log.info("results:-"+ results);
		numberOfProducts = Integer.parseInt(results);
		if (numberOfProducts>10)
		{
			numberOfProducts = numberOfProducts-10;
			log.info("results after parsing:-"+ results);
		}
		sa.assertEquals(userNames.size(), numberOfProducts, "Verify the number of products");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 DRM-609")
	public void TC01_ManageUserRoleDisplay(XmlTest xmlTest) throws Exception
	{
		log.info("Verify the ROLES");
		List<WebElement> rolesList = l1.getWebElements("ManageUser_RolesColumns", "Profile//AdminUser.properties");
		log.info("rolesList:- "+ rolesList.size());
		sa.assertEquals(rolesList.size(), numberOfProducts,"Verify the number of roles");
		for (int i = 0; i < rolesList.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String role = rolesList.get(i).getText();
			log.info("role:- "+ role);
			sa.assertTrue(role.contains("B2B"),"Verify the roles");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 DRM-610")
	public void TC02_ManageUserStatusDisplay() throws Exception
	{
		log.info("Verify the status");
		List<WebElement> userStatusList = l1.getWebElements("ManageUser_StatusColumn","Profile//AdminUser.properties");
		log.info("userStatusList size:- "+ userStatusList.size());
		
		sa.assertEquals(userStatusList.size(), numberOfProducts,"Verify the status");
		for (int i = 0; i < userStatusList.size(); i++)
		{
			log.info("LOOP:- "+ i);
			gVar.assertequalsIgnoreCase(userStatusList.get(i).getText().trim(), "Active","Verify the active text- LOOP:-"+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-072 DRM-606")
	public void TC03_createUser(XmlTest xmlTest) throws Exception
	{
		int num = gVar.randomNum();
		log.info("Random number:- "+ num);
		
		log.info("click on add new user button");
		l1.getWebElement("AdminUser_AddNewUser_Btn", "Profile//AdminUser.properties").click();
		log.info("Enter First Name");
		firstName = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 0)+num;
		log.info("firstName:- "+ firstName);
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").sendKeys(firstName);
		log.info("Enter Last Name");
		lastName =GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 2, 0)+num;
		log.info("lastName:- "+ lastName);
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").sendKeys(lastName);
		log.info("Enter Email Id");
		emailId = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 3, 0)+num;
		emailId = emailId+"@gmail.com";
		log.info("emailId:- "+ emailId);
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").sendKeys(emailId);
		log.info("Select the Admin Role checkbox");
//		l1.getWebElement("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties").click();
		checkCheckbox("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties");
		
		log.info("click on save button");
		l1.getWebElement("NewUser_Save_Btn", "Profile//AdminUser.properties").click();
		Thread.sleep(15000);
		log.info("verify user created or not");
		boolean user = false;
		int whileLoopCnt = 1;
		boolean whileLoopCondition = true;
		while (whileLoopCondition)
		{
			log.info("WHILE LOOP:- "+ whileLoopCnt);
			nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
			log.info("nameLinks:- "+ nameLinks.size());
			for (int i = 0; i < nameLinks.size(); i++)
			{
				log.info("FOR LOOP:- "+ i);
				nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
				String userName = nameLinks.get(i).getText().trim();
				log.info("userName:- "+ userName);
				if (userName.equalsIgnoreCase(firstName+" "+lastName))
				{
					user=true;
					rowCnt = i;
					break;
				}
			}
			if (gVar.assertVisible("ManageUser_NextPage_Link", "Profile//AdminUser.properties"))
			{
				log.info("Click on Next link in pagination");
				l1.getWebElement("ManageUser_NextPage_Link", "Profile//AdminUser.properties").click();
				Thread.sleep(10000);
			}
			else 
			{
				whileLoopCondition = false;
			}
			Thread.sleep(2000);
			whileLoopCnt++;
		}
		
		if (user)
		{
			sa.assertTrue(true,"User created");
		}
		else 
		{
			sa.assertTrue(false,"User NOT created");
		}
	
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-072 DRM-611")
	public void TC04_ManageUserEditUser() throws Exception
	{
		log.info("Click on EDIT link of Newly created USER");
		l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(10000);
		log.info("Verify the first name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_FN", "Profile//AdminUser.properties","value"), 
				firstName,"Verify the firstname");
		
		log.info("Verify the last name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_LN", "Profile//AdminUser.properties","value"), 
				lastName,"Verify the last name");
		
		log.info("Verify the Email in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_Email", "Profile//AdminUser.properties","value"), 
				emailId,"Verify the Email ID");
		
		log.info("Update the details");
		updatedFName = firstName+"updated";
		updatedLName = lastName+"updated";
		updatedEmail = "updated"+emailId;
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").sendKeys(updatedFName);
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").sendKeys(updatedLName);
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").sendKeys(updatedEmail);
		
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		Thread.sleep(10000);
		
		log.info("Verify the Updated name");
		String updatedUserName = gVar.assertEqual("NewUser_UN_Link", "Profile//AdminUser.properties",rowCnt).trim();
		log.info("updatedUserName:- "+ updatedUserName);
		
		gVar.assertEqual(updatedUserName, updatedFName+" "+updatedLName,"Verify the updated name");
		
		log.info("Click on EDIT link of Newly Updated USER");
		l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(10000);
		log.info("Verify the first name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_FN", "Profile//AdminUser.properties","value"), 
				updatedFName,"Verify the firstname");
		
		log.info("Verify the last name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_LN", "Profile//AdminUser.properties","value"), 
				updatedLName,"Verify the last name");
		
		log.info("Verify the Email in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_Email", "Profile//AdminUser.properties","value"), 
				updatedEmail,"Verify the Email ID");
		
		log.info("Close the Popup");
		l1.getWebElement("ManageUser_EditPopUp_CloseIcon", "Profile//AdminUser.properties").click();
		
		sa.assertAll();	
	}
	
	
	@Test(groups={"reg"}, description="OOTB-072 DRM-612")
	public void TC05_ManageUserResetUserPswd() throws Exception
	{
		log.info("Click on the user name");
		l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(10000);
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetailsPageHeading", "Profile//AdminUser.properties"), 
				"User Details", "Verify the User Details page heading");
		log.info("Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				updatedEmail,"Verify the breadcrumb");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"home users "+updatedEmail,"Verify the full breadcrumb");
		
		log.info("Verify the EMAIL ID in User Details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_Value", "Profile//AdminUser.properties",0), 
				updatedEmail,"Verify the EMAIL");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_Value", "Profile//AdminUser.properties",1).trim(), 
				updatedFName+" "+ updatedLName,"Verify the First name and Last name");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_Value", "Profile//AdminUser.properties",3).trim(), 
				"Enable","Verify the User status");
		 
		log.info("Verify the Reset Password link");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_ResetPassword_Link", "Profile//AdminUser.properties"), 
				"Reset Password","Verify Reset Password link");
		
		log.info("Click on Reset Password link");
		l1.getWebElement("UserDetails_ResetPassword_Link", "Profile//AdminUser.properties").click();
		
		Thread.sleep(10000);
		
		log.info("Verify the Update Password page heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_Heading", "Profile//AdminUser.properties"), 
				"Update Password","Verify Update Password page heading");
		
		log.info("Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				"Update Password","Verify the breadcrumb");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"home users "+updatedEmail+" "+"Update Password","Verify the full breadcrumb");
		
		log.info("Verify the password and Update password box");
		sa.assertTrue(gVar.assertVisible("UpdatePasswordPage_PasswordBox", "Profile//AdminUser.properties"),"Password textbox");
		sa.assertTrue(gVar.assertVisible("UpdatePasswordPage_ConfirmPasswordBox", "Profile//AdminUser.properties"),"Password textbox");
		sa.assertTrue(gVar.assertVisible("UpdatePasswordPage_UpdatePasswordButton", "Profile//AdminUser.properties"),"Update Password button");
		
		log.info("Enter password");
		String password = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 12);
		l1.getWebElement("UpdatePasswordPage_PasswordBox", "Profile//AdminUser.properties").sendKeys(password);
		l1.getWebElement("UpdatePasswordPage_ConfirmPasswordBox", "Profile//AdminUser.properties").sendKeys(password);
		
		log.info("Click on Update Password button");
		l1.getWebElement("UpdatePasswordPage_UpdatePasswordButton", "Profile//AdminUser.properties").click();
		Thread.sleep(10000);
		
		log.info("Verify the Success alert message and navigated page");
		String closeButton = gVar.assertEqual("UpdatePasswordPage_Successage_Alert_CloseButton", "Profile//AdminUser.properties");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_Successage_Alert", "Profile//AdminUser.properties").replace(closeButton, "").trim(), 
				"Password of the customer updated successfully","Verify the success message");
		
		log.info("Click on CLOSE button in alert message");
		l1.getWebElement("UpdatePasswordPage_Successage_Alert_CloseButton", "Profile//AdminUser.properties").click();
		log.info("Success message alert should not display");
		sa.assertFalse(gVar.assertVisible("UpdatePasswordPage_Successage_Alert", "Profile//AdminUser.properties"),"Success message alert should not display");
		
		log.info("Verify the page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetailsPageHeading", "Profile//AdminUser.properties"), 
				"User Details", "Verify the User Details page heading");
		log.info("Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				updatedEmail,"Verify the breadcrumb");
		
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="OOTB-072 DRM-613")
	public void TC06_ManagerUserDisableUser() throws Exception
	{
		log.info("Verify the Disable user link");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_DisableUser_Link", "Profile//AdminUser.properties"), 
				"Disable User","Verify  Disable User link");
		log.info("Click on  Disable User link");
		l1.getWebElement("UpdatePasswordPage_DisableUser_Link", "Profile//AdminUser.properties").click();
		
		Thread.sleep(5000);
		
		log.info("Verify the popup");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_DisableUserPopUpTitle", "Profile//AdminUser.properties"), 
				"Disable User","Verify the popup title");
		sa.assertTrue(gVar.assertEqual("UpdatePasswordPage_DisableUserPopUp", "Profile//AdminUser.properties","style").contains("display: table"),"Verify the popup");
		
		log.info("Verify the Disable button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_DisablePopup_DisableButton", "Profile//AdminUser.properties"), 
				"Disable","Verify the  Disable button");
		
		log.info("Click on DISABLE button");
		l1.getWebElement("UpdatePasswordPage_DisablePopup_DisableButton", "Profile//AdminUser.properties").click();
		
		log.info("Verify the success message alert");
		String closeText = gVar.assertEqual("UpdatePasswordPage_Successage_Alert_CloseButton", "Profile//AdminUser.properties");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_Successage_Alert", "Profile//AdminUser.properties").replace(closeText, "").trim(), 
				"The user has been disabled","Verify the alert message");
		
		log.info("Click on close link");
		l1.getWebElement("UpdatePasswordPage_Successage_Alert_CloseButton", "Profile//AdminUser.properties").click();
		sa.assertFalse(gVar.assertVisible("UpdatePasswordPage_Successage_Alert", "Profile//AdminUser.properties"),"Alert message should disappear");
		
		log.info("Verify the disabled status");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_Value", "Profile//AdminUser.properties",3).trim(), 
				"Disable","Verify the User status");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_EnableUser_Link", "Profile//AdminUser.properties"), 
				"Enable User","Verify the  Enable User link");
		
		sa.assertFalse(gVar.assertVisible("UpdatePasswordPage_DisableUser_Link", "Profile//AdminUser.properties"),"Disable link should not display");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Verify login functionality for disabled user")
	public void TC07_loginForDisabledUser(XmlTest xmlTest) throws Exception
	{
		log.info("Logout");
		p.logout(xmlTest);
		
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		log.info("Login with newly created credentials");
		String password = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 12);
		p.logInWithCredentials(updatedEmail, password);
		
		log.info("Verify the Error messaage");
		sa.assertTrue(gVar.assertVisible("Login_ErrorMsg_Text", "Profile//login.properties"),"Verify the Error message");
		
		log.info("User should be in Login page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Login_Heading", "Profile//login.properties"), 
				"Sign In", "Verify the Sign In");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 DRM-614")
	public void TC08_ManagerUserEnableUser(XmlTest xmlTest) throws Exception
	{
		
		log.info("Login to the application");
		p.navigateToLoginPage();
		Thread.sleep(2000);
		p.logIn(xmlTest);
		Thread.sleep(15000);
		log.info("Navigate to Manage User page");
		s.navigateToUsersPage();
		
		log.info("Navigate to last page");
		navigateToLastPage();
		
		log.info("Navigate to User Details page of Recently created user");
		l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(5000);
		
		log.info("Click on ENALBLE user link");
		l1.getWebElement("UpdatePasswordPage_EnableUser_Link", "Profile//AdminUser.properties").click();
		Thread.sleep(3000);
		log.info("Verify the success message alert");
		String closeText = gVar.assertEqual("UpdatePasswordPage_Successage_Alert_CloseButton", "Profile//AdminUser.properties");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UpdatePasswordPage_Successage_Alert", "Profile//AdminUser.properties").replace(closeText, "").trim(), 
				"The user has been enabled","Verify the alert message");
		
		log.info("Click on close link");
		l1.getWebElement("UpdatePasswordPage_Successage_Alert_CloseButton", "Profile//AdminUser.properties").click();
		sa.assertFalse(gVar.assertVisible("UpdatePasswordPage_Successage_Alert", "Profile//AdminUser.properties"),"Alert message should disappear");
		
		sa.assertTrue(gVar.assertVisible("UpdatePasswordPage_DisableUser_Link", "Profile//AdminUser.properties"),"Disable link should display");
		sa.assertFalse(gVar.assertVisible("UpdatePasswordPage_EnableUser_Link", "Profile//AdminUser.properties"),"Enable link should not display");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Edit functionality in User details page")
	public void TC09_editFunctionalityInUserDetailsPage() throws Exception
	{
		log.info("Click on EDIT link in USER DETAILS page");
		l1.getWebElement("UserDetails_EditUserDetailsButton", "Profile//AdminUser.properties").click();
		
		Thread.sleep(3000);
		sa.assertTrue(gVar.assertEqual("UserDetails_PopUp", "Profile//AdminUser.properties","style").contains("display: table"),"Verify the popup");		
		
		log.info("Verify the first name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_FN", "Profile//AdminUser.properties","value"), 
				updatedFName,"Verify the firstname");
		
		log.info("Verify the last name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_LN", "Profile//AdminUser.properties","value"), 
				updatedLName,"Verify the last name");
		
		log.info("Verify the Email in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_Email", "Profile//AdminUser.properties","value"), 
				updatedEmail,"Verify the Email ID");
		
		
		log.info("Enter New details in First name, last name and in Email field");
		updatedFName = updatedFName+"New";
		updatedLName = updatedLName+"New";
		updatedEmail = "new"+updatedEmail;
		
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").sendKeys(updatedFName);
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").sendKeys(updatedLName);
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").sendKeys(updatedEmail);
		
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		Thread.sleep(5000);
		
		log.info("Verify the navigated page heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_Heading", "Profile//AdminUser.properties"), 
				"Manage Users","Verify the Manage Users page");
		
		log.info("Navigate to last page");
		navigateToLastPage();
		
		log.info("Verify the Updated name and Email ID");
		String updatedUserName = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties").get(rowCnt).getText().trim();
		log.info("updatedUserName:- "+ updatedUserName);
		
		gVar.assertEqual(updatedUserName, updatedFName+" "+updatedLName,"Verify the updated name");
		
		log.info("Click on EDIT link of Newly Updated USER");
		l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(10000);
		log.info("Verify the first name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_FN", "Profile//AdminUser.properties","value"), 
				updatedFName,"Verify the firstname");
		
		log.info("Verify the last name in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_LN", "Profile//AdminUser.properties","value"), 
				updatedLName,"Verify the last name");
		
		log.info("Verify the Email in POPUP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("NewUser_Email", "Profile//AdminUser.properties","value"), 
				updatedEmail,"Verify the Email ID");
		
		log.info("Close the Popup");
		l1.getWebElement("ManageUser_EditPopUp_CloseIcon", "Profile//AdminUser.properties").click();
		
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Back To User button functionality")
	public void TC10_backToUserFunctionality() throws Exception
	{
		log.info("Navigate to User Details page of Recently created user");
		l1.getWebElement("NewUser_UN_Link", "Profile//AdminUser.properties").click();
		Thread.sleep(5000);
		log.info("Verify the User details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetailsPageHeading", "Profile//AdminUser.properties"), 
				"User Details", "Verify the User Details page heading");
		
		log.info("Verify Back to User button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_BackToUserButton", "Profile//AdminUser.properties"), 
				"Back to Users","Verify the  Back to Users button");
		
		log.info("Click on  Back to Users button");
		l1.getWebElement("UserDetails_BackToUserButton", "Profile//AdminUser.properties").click();
		
		Thread.sleep(4000);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_Heading", "Profile//AdminUser.properties"), 
				"Manage Users","Verify the Manage Users page heading");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-072 DRM-607")
	public void TC11_sortByOption() throws Exception
	{
		String val = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 11);
		log.info("Verify the default sort by option in TOP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_SortByDD_Top", "Profile//AdminUser.properties","title"), 
				val,"top sortby value");
		
		log.info("Verify the default sort by option in BOTTOM");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_SortByDD_Bottom", "Profile//AdminUser.properties","title"), 
				val,"bottom sortby value");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOB-072 extraTc-login with newly created credentials")
	public void TC12_loginWithNewlyCreatedAccount(XmlTest xmlTest) throws Exception
	{
		log.info("Logout");
		p.logout(xmlTest);
		
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		log.info("Login with newly created credentials");
		String password = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 12);
		p.logInWithCredentials(updatedEmail, password);
		
		Thread.sleep(5000);
		Thread.sleep(15000);
		
		log.info("Navigate to User page");
		s.navigateToUsersPage();
		
		log.info("Verify the page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_Heading", "Profile//AdminUser.properties"), 
				"Manage Users", "Verify the Manage Users page");
		
		log.info("Navigate to Last page");
		navigateToLastPage();
		Thread.sleep(5000);
		
		log.info("Click on EDIT link of Newly created USER");
		l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties").get(rowCnt).click();
		
		Thread.sleep(10000);
		log.info("Verify the popup");
		sa.assertTrue(gVar.assertEqual("UserDetails_PopUp", "Profile//AdminUser.properties","style").contains("display: table"),"Verify the popup");
		
		log.info("Unselect the Admin role checkbox");
//		l1.getWebElement("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties").click();
		unCheckCheckbox("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties");
		Thread.sleep(2000);
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		Thread.sleep(10000);
		
		log.info("Pop should not close");
		sa.assertTrue(gVar.assertEqual("UserDetails_PopUp", "Profile//AdminUser.properties","style").contains("display: table"),"popup should not close");

		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Login to the application with credential having Customer role(Not Admin role)")
	public void TC13_loginWithCredentialHavingCustomerRole(XmlTest xmlTest) throws Exception
	{
		log.info("Logout");
		p.logout(xmlTest);
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("Login with newly created credentials");
		String password = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 12);
		p.logInWithCredentials(updatedEmail, password);
		Thread.sleep(1000);
		Thread.sleep(15000);
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Click on hamburger menu");
			s.openHamburgarMenu();
			s.expandMyAccountSection();
			BaseTest.log.info("Click on User link in Hamburgar menu");
			Thread.sleep(6000);
			sa.assertFalse(gVar.assertVisible("Hamburger_User_Link", "Shopnav//header.properties"),"User link should not display");
			
		}
		else
		{
			WebElement myAccountLink = BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
			log.info("Mouse hover on My Account link in header");
			gVar.mouseHover(myAccountLink);
			Thread.sleep(6000);
			sa.assertFalse(gVar.assertVisible("Header_User_Link", "Shopnav//header.properties"),"User link should not display");
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOB-072 extraTc-Role CheckBox Functionality In Manage User Page")
	public void TC14_roleCheckBoxFunctionalityInManageUserPage(XmlTest xmlTest) throws Exception
	{
		log.info("Logout");
		p.logout(xmlTest);
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		Thread.sleep(15000);
		log.info("Navigate to Manage User page");
		s.navigateToUsersPage();
		
		log.info("Navigate to Last page");
		navigateToLastPage();
		Thread.sleep(5000);
		
		log.info("Click on EDIT link of Newly created USER");
		l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(2000);
		
		log.info("Unselect the both checkbox");
//		l1.getWebElement("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties").click();
		unCheckCheckbox("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties");
		
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		Thread.sleep(5000);
		log.info("Verify the roles");
		String roleText = l1.getWebElements("ManageUser_RolesColumns", "Profile//AdminUser.properties").get(rowCnt).getText().trim();
		log.info("roleText :- "+ roleText);
		sa.assertEquals(roleText, "","Roles should be empty");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Login with newly created account when no role is assigned")
	public void TC15_LoginWithNewlyCreatedAccountWhenNoRoleAssigned(XmlTest xmlTest) throws Exception
	{
		
		log.info("Logout");
		p.logout(xmlTest);
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("Login with newly created credentials");
		String password = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 12);
		p.logInWithCredentials(updatedEmail, password);
		Thread.sleep(1000);
		Thread.sleep(15000);
		log.info("Verify the Error messaage");
		sa.assertTrue(gVar.assertVisible("Login_ErrorMsg_Text", "Profile//login.properties"),"Verify the Error message");
		
		log.info("User should be in Login page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Login_Heading", "Profile//login.properties"), 
				"Sign In", "Verify the Sign In");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOB-072 extraTc-Role CheckBox Functionality In User User Details Page")
	public void TC16_roleCheckBoxFunctionalityInUserDetailsPage(XmlTest xmlTest) throws Exception
	{
		p.logout(xmlTest);
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Navigate to Manage User page");
		s.navigateToUsersPage();
		log.info("Navigate to Last page");
		navigateToLastPage();
		Thread.sleep(5000);
		Thread.sleep(15000);
		log.info("Click on User name link of Newly created USER");
		l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties").get(rowCnt).click();
		Thread.sleep(2000);
		
		log.info("Verify the User details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetailsPageHeading", "Profile//AdminUser.properties"), 
				"User Details", "Verify the User Details page heading");
		log.info("Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				updatedEmail,"Verify the breadcrumb");
		log.info("Verify the Roles in User Details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetails_Value", "Profile//AdminUser.properties",2).trim(), 
				"","Verify the User status");
		
		log.info("Click on EDIT link in USER DETAILS page");
		l1.getWebElement("UserDetails_EditUserDetailsButton", "Profile//AdminUser.properties").click();
		
		Thread.sleep(3000);
		sa.assertTrue(gVar.assertEqual("UserDetails_PopUp", "Profile//AdminUser.properties","style").contains("display: table"),"Verify the popup");
		
		
		log.info("Select both checkbox");
//		l1.getWebElement("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties").click();
//		l1.getWebElement("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties").click();
		checkCheckbox("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties");
		checkCheckbox("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties");
		String roleText = gVar.assertEqual("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties");
		log.info("roleText:- "+ roleText);
		
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		Thread.sleep(10000);
		
		log.info("Navigate to Last page");
		navigateToLastPage();
		
		log.info("Verify the roles");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_RolesColumns", "Profile//AdminUser.properties",rowCnt).trim(), 
				roleText,"Verify the Role text");
	
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Unselecting the admin role of same account")
	public void TC17_unSelectingTheAdminRoleOfSameAccount(XmlTest xmlTest) throws Exception
	{
		log.info("Logout");
		p.logout(xmlTest);
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("Login with newly created credentials");
		String password = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 12);
		p.logInWithCredentials(updatedEmail, password);
		Thread.sleep(1000);
		Thread.sleep(15000);
		log.info("Navigate to Manage Users page");
		s.navigateToUsersPage();
		log.info("Verify the navigated page heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_Heading", "Profile//AdminUser.properties"), 
				"Manage Users","Verify the Manage Users page");
		
		log.info("Navigate to last page");
		navigateToLastPage();
		
		log.info("Click on the user name");
		l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties").get(rowCnt).click();
		gVar.assertequalsIgnoreCase(gVar.assertEqual("UserDetailsPageHeading", "Profile//AdminUser.properties"), 
				"User Details", "Verify the User Details page heading");
		
		log.info("Click on EDIT link in USER DETAILS page");
		l1.getWebElement("UserDetails_EditUserDetailsButton", "Profile//AdminUser.properties").click();
		
		log.info("Unselect the Both the checkbox");
//		l1.getWebElement("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties").click();
//		l1.getWebElement("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties").click();
		unCheckCheckbox("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties");
		unCheckCheckbox("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties");
		
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		log.info("Verify the Error message");
		
		log.info("Verify the alert message ");
		String closeButton = gVar.assertEqual("ManageUser_AlertMessage_CloseIcon", "Profile//AdminUser.properties");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_AlertMessage", "Profile//AdminUser.properties").replace(closeButton, "").trim(), 
				"A B2B Administrator can not downgrade their administrative role.","Verify the Alert message for unselecting the admin role for same account");
		
		log.info("Click on CLOSE button in alert message");
		l1.getWebElement("ManageUser_AlertMessage_CloseIcon", "Profile//AdminUser.properties").click();
		log.info("Success message alert should not display");
		sa.assertFalse(gVar.assertVisible("ManageUser_AlertMessage", "Profile//AdminUser.properties"),"Success message alert should not display");

		log.info("Close the popup");
		l1.getWebElement("ManageUser_EditPopUp_CloseIcon", "Profile//AdminUser.properties").click();
		
		log.info("Verify the breadcrumb links");
		List<WebElement> breadcrumbLinks = l1.getWebElements("BreadcrumbLinks", "Generic//Generic.properties");
		log.info("breadcrumbLinks size:-"+breadcrumbLinks.size());
		sa.assertEquals(breadcrumbLinks.size(), 2,"Verify the number of links in breadcrumb");
		log.info("Click on USER links in breadcrumb");
		breadcrumbLinks.get(1).click();
		Thread.sleep(4000);
		
		log.info("Verify the page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_Heading", "Profile//AdminUser.properties"), 
				"Manage Users","Verify the Manage Users page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description=" in MAnage user page")
	public void TC18_unSelectingTheAdminRoleOfSameAccountInManageUserPage(XmlTest xmlTest) throws Exception
	{

		log.info("Navigate to last page");
		navigateToLastPage();
		log.info("Click on EDIT link of Newly created account");
		l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties").get(rowCnt).click();
		
		log.info("Unselect the Both the checkbox");
//		l1.getWebElement("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties").click();
//		l1.getWebElement("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties").click();
		unCheckCheckbox("ManageUser_Role1CheckboxLabel", "Profile//AdminUser.properties");
		unCheckCheckbox("ManageUser_Role2CheckboxLabel", "Profile//AdminUser.properties");
		
		log.info("Click on SAVE button");
		l1.getWebElement("NewUser_Save_Btn","Profile//AdminUser.properties").click();
		
		log.info("Verify the Error message");
		
		log.info("Verify the alert message ");
		String closeButton = gVar.assertEqual("ManageUser_AlertMessage_CloseIcon", "Profile//AdminUser.properties");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ManageUser_AlertMessage", "Profile//AdminUser.properties").replace(closeButton, "").trim(), 
				"A B2B Administrator can not downgrade their administrative role.","Verify the Alert message for unselecting the admin role for same account");
		
		log.info("Click on CLOSE button in alert message");
		l1.getWebElement("ManageUser_AlertMessage_CloseIcon", "Profile//AdminUser.properties").click();
		log.info("Success message alert should not display");
		sa.assertFalse(gVar.assertVisible("ManageUser_AlertMessage", "Profile//AdminUser.properties"),"Success message alert should not display");

		log.info("Close the popup");
		l1.getWebElement("ManageUser_EditPopUp_CloseIcon", "Profile//AdminUser.properties").click();
		
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-072 extraTc-Sort by name functionality in Manage User page")
	public void TC19_sortByName(XmlTest xmlTest) throws Exception
	{
		log.info("LOGOUT");
		p.logout(xmlTest);
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		Thread.sleep(15000);
		
		log.info("Navigate to Manage User page");
		s.navigateToUsersPage();
		
		log.info("Select the NAME as sortBy Option");
		WebElement selectBox = l1.getWebElement("ManageUser_SortBySelectBox_Top", "Profile//AdminUser.properties");
		new Select(selectBox).selectByIndex(2);
		Thread.sleep(6000);
		
		log.info("Collect the names");
		List<WebElement> allNames = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		log.info("allNames size:-"+ allNames.size());
		ArrayList<String> userNameList = new ArrayList<String>();
		for (int i = 0; i < allNames.size(); i++)
		{
			log.info("LOOP:- "+ i);
			log.info("allNames.get(i).getText():- "+ allNames.get(i).getText());
			userNameList.add(allNames.get(i).getText());
		}
		log.info("userNameList:- "+ userNameList);
		ArrayList<String> userNameListCopy = new ArrayList<String>();
		userNameListCopy = (ArrayList<String>) userNameList.clone();
		Collections.sort(userNameListCopy);
		log.info("All names After sorting:- "+userNameListCopy);
		sa.assertEquals(userNameList, userNameListCopy,"Verify the names");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-072 DRM-608,609,610,615,616,617")
	public void TC20_UI_Test(XmlTest xmlTest) throws Exception
	{
//		p.logout(xmlTest);
//		log.info("Navigate to login page");
//		p.navigateToLoginPage();
//		log.info("Login with valid credentials");
//		String un = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 6);
//		String pwd = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 2, 6);
//		p.logIn(xmlTest);
		
//		log.info("Navigate to users page");
//		s.navigateToUsersPage();

		log.info("verify heading");
		sa.assertTrue(gVar.assertVisible("ManageUser_Heading", "Profile//AdminUser.properties"),"Verify the heading");
		
		System.out.println(l1.getWebElement("ManageUser_Heading", "Profile//AdminUser.properties").getText()+"manage");
		sa.assertEquals(GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 5).toString(), l1.getWebElement("ManageUser_Heading", "Profile//AdminUser.properties").getText().substring(0,12));
		log.info("create new user button");
		sa.assertTrue(gVar.assertVisible("AdminUser_AddNewUser_Btn", "Profile//AdminUser.properties"),"create new user button");
		
		sa.assertEquals(GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 2, 5), l1.getWebElement("AdminUser_AddNewUser_Btn", "Profile//AdminUser.properties").getText());
		log.info("sort by label");
		sa.assertTrue(gVar.assertVisible("ManageUser_SortBy_Label_Top", "Profile//AdminUser.properties"),"Sort By Label");
		
		log.info("sort by drop down");
		sa.assertTrue(gVar.assertVisible("ManageUser_SortDropDown", "Profile//AdminUser.properties"),"Sort By Drop Down");
		
		log.info("pagination text");
		sa.assertTrue(gVar.assertVisible("ManageUser_PaginationText_Top", "Profile//AdminUser.properties"),"Pagination top");
		
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("browser"))
		{
			log.info("name column");
			sa.assertTrue(gVar.assertVisible("ManagerUser_Name_Column", "Profile//AdminUser.properties"),"ManagerUser_Name_Column");
			
			log.info("roles column");
			sa.assertTrue(gVar.assertVisible("ManagerUser_Roles_Column", "Profile//AdminUser.properties"),"Verify ManagerUser_Roles_Column");

			log.info("status column");
			sa.assertTrue(gVar.assertVisible("ManagerUser_Status_Column", "Profile//AdminUser.properties"),"Verify ManagerUser_Status_Column");
		}
		
		log.info("fetch total name links");
		nameLinks=l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		editLinks=l1.getWebElements("NewUser_Edit_Links","Profile//AdminUser.properties");
		status=l1.getWebElements("NewUser_Status_Links","Profile//AdminUser.properties");
		roles=l1.getWebElements("NewUser_Status_Links","Profile//AdminUser.properties");

		for(int i=0;i<nameLinks.size();i++)
		{
			log.info("verify name links");
			sa.assertTrue(nameLinks.get(i).isDisplayed(),"name Links"+i);
			log.info("verify edit links");
			sa.assertTrue(editLinks.get(i).isDisplayed(),"edit Links"+i);
			log.info("verify status");
			sa.assertTrue(status.get(i).isDisplayed(),"Status"+i);
		}

		log.info("sort by label bottm");
		sa.assertTrue(gVar.assertVisible("ManageUser_SortBy_Label_Bottom", "Profile//AdminUser.properties"),"ManageUser_SortBy_Label_Bottom");
		
		log.info("sort by drop down bottom");
		sa.assertTrue(gVar.assertVisible("ManageUser_SortDropDown_Bottom", "Profile//AdminUser.properties"),"ManageUser_SortDropDown_Bottom");
		
		log.info("pagination text bottom");
		sa.assertTrue(gVar.assertVisible("ManageUser_PaginationText_Bottom", "Profile//AdminUser.properties"),"ManageUser_PaginationText_Bottom");
		
		log.info("pagination bottom");
		sa.assertTrue(gVar.assertVisible("ManageUser_Pagination_Bottom", "Profile//AdminUser.properties"),"ManageUser_Pagination_Bottom");
		
		sa.assertAll();
	}
	
//	 + ------- + ------- + ------- + ------- + ------- + -------+ ------- + -------+ ------- +
	public void navigateToLastPage() throws Exception
	{
		boolean user = false;
		int whileLoopCnt = 1;
		while (gVar.assertVisible("ManageUser_NextPage_Link", "Profile//AdminUser.properties"))
		{
			log.info("WHILE LOOP:- "+ whileLoopCnt);
			if (gVar.assertVisible("ManageUser_NextPage_Link", "Profile//AdminUser.properties"))
			{
				log.info("Click on Next link in pagination");
				l1.getWebElement("ManageUser_NextPage_Link", "Profile//AdminUser.properties").click();
				Thread.sleep(10000);
				
			}
			else 
			{
				break;
			}
			Thread.sleep(2000);
			whileLoopCnt++;
		}
		
	}
	
	public void checkCheckbox(String element, String path) throws Exception
	{
		try
		{
			String checkboxStatus = gVar.assertEqual(element, path,"checked");
			log.info("checkboxStatus:-"+ checkboxStatus);
			if (!checkboxStatus.equalsIgnoreCase("checked"))
			{
				l1.getWebElement(element, path).click();
			}
			log.info("Checkbox checked");
		}
		catch (Exception e)
		{
			log.info("Catch block executing");
			l1.getWebElement(element, path).click();
		}
	}
	
	public void unCheckCheckbox(String element, String path) throws Exception
	{
		Thread.sleep(5000);
		try
		{
			String checkboxStatus = gVar.assertEqual(element, path,"checked");
			log.info("checkboxStatus:-"+ checkboxStatus);
			if (checkboxStatus.equalsIgnoreCase("checked"))
			{
				l1.getWebElement(element, path).click();
			}
		}
		catch (Exception e)
		{
			log.info("Catch block executing");
			l1.getWebElement(element, path).click();
		}
		
	}
	
	public int returnCount(String name) throws Exception
	{
		List<WebElement> nameLinksList = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		List<WebElement> editLinksList = l1.getWebElements("ManageUser_EditLink", "Profile//AdminUser.properties");
		for (int i = 0; i < nameLinksList.size(); i++)
		{
			log.info("FOR LOOP:- "+ i);
			String userName = nameLinksList.get(i).getText().trim();
			log.info("userName:- "+ userName);
			if (userName.equalsIgnoreCase(name))
			{
				
				break;
			}
		}
		return i;
	}
}
