package com.drivemedical.profile;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PurchaseHistory extends BaseTest{

	String orderNum = "0006203516";
	String poNumber="785746";
	Float actPrices[];
	int proCnt;
	boolean decide;
	ArrayList<String> proNamesList;
	
	String purchaseHisotryProperties = "Profile//PurchaseHisotry.properties";
	
	@Test(groups="reg",description="DMED-010 DRM-1248")
	public void TC01_DefaultSortingOrder() throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("navigate to frequently ordered items page");
		s.navigateToFrequentlyOrderedItemsPage();
		log.info("verify the heading");
		sa.assertTrue(gVar.assertVisible("PH_Heading", "Profile//PurchaseHisotry.properties"),"Frequently Ordered items heading");
		sa.assertEquals(gVar.assertEqual("PH_Heading", purchaseHisotryProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 1, 1),"Verify the heading text");
		log.info("Default Sort option should be Most Recent");
		String expectedSortOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 5, 0);
		sa.assertEquals(gVar.assertEqual("PH_SortBy_DD", "Profile//PurchaseHisotry.properties", "title"), 
				expectedSortOption,"Verify the selected Drop down option");
		
		sa.assertEquals(gVar.assertEqual("PH_SortBY_DD_Bottom", "Profile//PurchaseHisotry.properties", "title"), 
				expectedSortOption,"Verify the selected Drop down option");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-1612")
	public void TC02_verifySelectFunctionality() throws Exception
	{
		log.info("Count the number of line Items");
		int numberOfLineItems = l1.getWebElements("PH_ProductLineItems", purchaseHisotryProperties).size();
		log.info("numberOfLineItems:- "+ numberOfLineItems);
		
		log.info("Verify the Select box each product line items in Purchase history page");
		List<WebElement> checkbox;
		if (gVar.mobileView()||gVar.tabletView())
		{
			checkbox = l1.getWebElements("PH_Checkbox_Mobile", purchaseHisotryProperties);
		}
		else
		{
			checkbox = l1.getWebElements("PH_Checkbox", purchaseHisotryProperties);
		}
		
		int numberOfCheckbox=checkbox.size();
		log.info("numberOfCheckbox:- "+ numberOfCheckbox);
		sa.assertEquals(numberOfLineItems, numberOfCheckbox,"Verify the checkbox count");
		
		log.info("Select the Checkbox");
		int cnt = 1;
		for (int i = 0; i < numberOfCheckbox; i++)
		{
			log.info("LOOP:- "+ i);
			log.info("Select the Checkbox");
			WebElement element = checkbox.get(i);
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", element);
			Thread.sleep(1500);
			
			log.info("VErify the Selected product count in ADD TO CART section in bottom");
			sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectedProductCount", purchaseHisotryProperties), 
					cnt+"","Verify the Selected product count in ADD TO CART section");	
			cnt++;
		}
		
		String addToCartSectionStyle = gVar.assertEqual("PH_AddToCartSection", purchaseHisotryProperties,"style");
		log.info("addToCartSectionStyle:-"+ addToCartSectionStyle);
		sa.assertFalse(addToCartSectionStyle.contains("display"),"Verify the Add to cart section");
		
	
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-974")
	public void TC03_clearAllLink() throws Exception
	{
		log.info("Verify the clear All Link in ADD TO CART SECTION");
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			String clearAllLink = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 3, 2);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_AddToCartSection_ClearAllLink_Mobile", purchaseHisotryProperties), 
					clearAllLink,"Verify the Select All Link in Add To Cart section");
			log.info("Click on CLEAR link");
			l1.getWebElement("PH_AddToCartSection_ClearAllLink_Mobile", purchaseHisotryProperties).click();
		}
		else
		{
			String clearAllLink = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 2, 2);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_AddToCartSection_ClearAllLink", purchaseHisotryProperties), 
					clearAllLink,"Verify the Select All Link in Add To Cart section");
			log.info("Click on CLEAR link");
			l1.getWebElement("PH_AddToCartSection_ClearAllLink", purchaseHisotryProperties).click();
		}
		
		
	
		log.info("Add  To Cart section should not display");
		String addToCartSectionStyle = gVar.assertEqual("PH_AddToCartSection", purchaseHisotryProperties,"style");
		sa.assertTrue(addToCartSectionStyle.contains("display: none;"),"Verify the ADd to cart section");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 extraTC")
	public void TC04_selectAllLink() throws Exception
	{
		log.info("ADD TO CART section should not display before selecting the checkbox");
		sa.assertFalse(gVar.assertVisible("PH_AddToCartSection_Displayed", purchaseHisotryProperties),"Add to cart section should nod display");
		Thread.sleep(2000);
		log.info("Select one checkbox");
		if (gVar.mobileView()||gVar.tabletView())
		{
//			l1.getWebElement("PH_Checkbox_Mobile", purchaseHisotryProperties).click();
			WebElement element = l1.getWebElement("PH_Checkbox_Mobile", purchaseHisotryProperties);
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", element);
		}
		else
		{
//			l1.getWebElement("PH_Checkbox", purchaseHisotryProperties).click();
			WebElement element = l1.getWebElement("PH_Checkbox", purchaseHisotryProperties);
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", element);
		}
		Thread.sleep(2000);
		log.info("ADD TO CART section should display");
		sa.assertTrue(gVar.assertVisible("PH_AddToCartSection_Displayed", purchaseHisotryProperties),"Verify the Add to cart section");
		
		
		log.info("VErify the Selected product count in ADD TO CART section in bottom");
		sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectedProductCount", purchaseHisotryProperties), 
				"1","Verify the Selected product count in ADD TO CART section");	
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Verify Selected product label");
			sa.assertEquals(gVar.assertEqual("PH_SelectedProductLabel", purchaseHisotryProperties), 
					"","Verify the label");
			
			log.info("Verify the Select All Link in ADD TO CART SECTION");
			String selectAllLink = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 4, 2);
			sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectAllLink_Mobile", purchaseHisotryProperties), 
					selectAllLink,"Verify the Select All Link in Add To Cart section");
			log.info("Click on Select All link");
			l1.getWebElement("PH_AddToCartSection_SelectAllLink_Mobile", purchaseHisotryProperties).click();
		}
		else
		{
			log.info("Verify Selected product label");
			String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 2, 1);
			sa.assertEquals(gVar.assertEqual("PH_SelectedProductLabel", purchaseHisotryProperties), 
					expected,"Verify the label");
			log.info("Verify the Select All Link in ADD TO CART SECTION");
			String selectAllLink = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 1, 2);
			sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectAllLink", purchaseHisotryProperties), 
					selectAllLink,"Verify the Select All Link in Add To Cart section");
			log.info("Click on Select All link");
			l1.getWebElement("PH_AddToCartSection_SelectAllLink", purchaseHisotryProperties).click();
			
			
		}
		
		Thread.sleep(3000);
		log.info("VErify the Selected product count in ADD TO CART section in bottom");
		int numberOfLineItems = l1.getWebElements("PH_ProductLineItems", purchaseHisotryProperties).size();
		sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectedProductCount", purchaseHisotryProperties), 
				numberOfLineItems+"","All products should be selected");	
		
		log.info("Verify Selected product label");
		if (gVar.mobileView()||gVar.tabletView())
		{
			sa.assertEquals(gVar.assertEqual("PH_SelectedProductLabel", purchaseHisotryProperties), 
					"","Verify the label after selecting all products");
		}
		else
		{
			String expectedAfterSelectingAllProducts = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 3, 1);
			sa.assertEquals(gVar.assertEqual("PH_SelectedProductLabel", purchaseHisotryProperties), 
					expectedAfterSelectingAllProducts,"Verify the label after selecting all products");
		}
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 extraTc-Unselecting the selected products by clicking on selected products")
	public void TC05_unSelectingTheselectedByClickingOnSelectedCheckbox() throws Exception
	{
		List<WebElement> checkboxs;
		if (gVar.mobileView()||gVar.tabletView())
		{
			checkboxs = l1.getWebElements("PH_Checkbox_Mobile", purchaseHisotryProperties);
		}
		else
		{
			checkboxs = l1.getWebElements("PH_Checkbox", purchaseHisotryProperties);
		}
		int checkBoxSize = checkboxs.size();
		log.info("checkbox size:- "+checkBoxSize);
		int cnt = checkBoxSize;
		for (int i = 0; i < checkBoxSize; i++)
		{
			log.info("LOOP:- "+ i);
			log.info("Clicking on checkbox");
//			checkboxs.get(i).click();
			WebElement element = checkboxs.get(i);
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", element);
			Thread.sleep(2000);
			log.info("Verify the count in ADD TO CART section");
			sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectedProductCount", purchaseHisotryProperties), 
					--cnt+"","Verify the product count in ADD TO CART section-LOOP:-"+ i);
		}
		
		sa.assertAll();
	}

	@Test(groups="reg",description="DMED-010 DRM-963,964,965")
	public void TC06_verifyUI() throws Exception
	{
		log.info("verify heading");
		sa.assertTrue(gVar.assertVisible("PH_Heading", "Profile//PurchaseHisotry.properties"),"Frequently Ordered items heading");
		sa.assertEquals(gVar.assertEqual("PH_Heading", "Profile//PurchaseHisotry.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 1, 1),"Verify the Heading text");
		log.info("verify filter by date drop down");
		sa.assertTrue(gVar.assertVisible("PH_Date_Filetr", "Profile//PurchaseHisotry.properties"),"Filter by date");
		log.info("verify price filter");
		sa.assertTrue(gVar.assertVisible("PH_Price_Filter", "Profile//PurchaseHisotry.properties"),"price filter");
		log.info("verify Nothing Slected Drop Down");
		sa.assertTrue(gVar.assertVisible("PH_NothingSelected_DD", "Profile//PurchaseHisotry.properties"),"Nothing Selected Drop Down");
		log.info("verify Serach box");
		sa.assertTrue(gVar.assertVisible("PH_SearchBox", "Profile//PurchaseHisotry.properties"),"Search Box");
		log.info("verify Serach button");
		sa.assertTrue(gVar.assertVisible("PH_Search_Btn", "Profile//PurchaseHisotry.properties"),"Search Button");
		log.info("verify Sort By DD Top");
		sa.assertTrue(gVar.assertVisible("PH_SortBy_DD", "Profile//PurchaseHisotry.properties"),"Sort By DD TOP");
		log.info("verify Sort By DD bottom");
		sa.assertTrue(gVar.assertVisible("PH_SortBY_DD_Bottom", "Profile//PurchaseHisotry.properties"),"Sort By DD Bottom");
		
		log.info("Sort by LabelsPagination,Pagination Bar Results");
		for(int i=0;i<2;i++) 
		{
			sa.assertTrue(gVar.assertVisible("PH_SortBy_Label", "Profile//PurchaseHisotry.properties",i),"Sort By Label");
			sa.assertTrue(gVar.assertVisible("PH_Pagination_Bar_Results_Text", "Profile//PurchaseHisotry.properties",i),"Search bar Results");
			sa.assertTrue(gVar.assertVisible("PH_Pagination", "Profile//PurchaseHisotry.properties",i),"Pagination");
		}
		
		List<WebElement> totProds=l1.getWebElements("PH_Tot_Products", "Profile//PurchaseHisotry.properties");
		for(int i=0;i<totProds.size();i++)
		{
			log.info("Item Image");
			sa.assertTrue(gVar.assertVisible("PH_Item_Image", "Profile//PurchaseHisotry.properties",i),"Item Image");
			log.info("Item Name");
			sa.assertTrue(gVar.assertVisible("PH_Item_Name", "Profile//PurchaseHisotry.properties",i),"Item Name");
			log.info("Item Code");
			sa.assertTrue(gVar.assertVisible("PH_Item_Codes", "Profile//PurchaseHisotry.properties",i),"Item Codes");
			log.info("UOM");
			sa.assertTrue(gVar.assertVisible("PH_Item_Unit", "Profile//PurchaseHisotry.properties",i),"UOM");
			log.info("Varients");
			sa.assertTrue(gVar.assertVisible("PH_Variant_Opt", "Profile//PurchaseHisotry.properties",i),"Varients");
			log.info("Prices");
			sa.assertTrue(gVar.assertVisible("PH_Prod_Prices", "Profile//PurchaseHisotry.properties",i),"Prices");
		}
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-955")
	public void TC07_paginationProductsResult() throws Exception
	{
		
		log.info("Verify the product results in top section");
		String text = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 6, 1);
		int numberOfProducts = l1.getWebElements("PH_ProductLineItems", purchaseHisotryProperties).size();
//		Below formate is Wrong
//		gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_Pagination_Bar_Results_TextTop", purchaseHisotryProperties), 
//				numberOfProducts+" "+text,"Top product results");
		String expectedCount = "1 - "+numberOfProducts+"  of  "+numberOfProducts;
		sa.assertEquals(gVar.assertEqual("PH_Pagination_Bar_Results_TextTop", purchaseHisotryProperties).trim(), 
				expectedCount,"Verify the Product count in bottom");
		
		log.info("Verify the bottom products result count once it completely developed");
		sa.assertEquals(gVar.assertEqual("PH_Pagination_Bar_Results_TextBottom", purchaseHisotryProperties).trim(), 
				expectedCount,"Verify the Product count in bottom");

		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-956")
	public void TC08_sortPriceDSC() throws Exception
	{
		ArrayList<Double> priceList = new ArrayList<Double>();
		ArrayList<Double> copyOfpriceList = new ArrayList<Double>();
		
		log.info("Select the Price HIGH TO LOW option");
		WebElement sortByDD = l1.getWebElement("PH_Sort_DD", "Profile//PurchaseHisotry.properties");
		Select sel = new Select(sortByDD);
		String lowToHigh = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 2, 0); 
		sel.selectByVisibleText(lowToHigh);
		Thread.sleep(3000);
		
		List<WebElement> allPrice = l1.getWebElements("PH_Prod_Prices", "Profile//PurchaseHisotry.properties");
		List<WebElement> allHcpcsCode = l1.getWebElements("PH_Prod_HCPCSCode", "Profile//PurchaseHisotry.properties");
		int allPriceSize = allPrice.size();
		log.info("allPriceSize:- "+ allPriceSize);
		String priceText = "";
		for (int i = 0; i < allPriceSize; i++)
		{
			log.info("LOOP:- "+i);
			priceText = allPrice.get(i).getText().trim();
			log.info("priceText:- "+ priceText);
			String hcpcsCode = allHcpcsCode.get(i).getText();
			log.info("hcpcsCode:- "+ hcpcsCode);
			priceText = priceText.replace(hcpcsCode, "").replace("$", "").trim();
			log.info("priceText after triming:- "+ priceText);
			if (priceText.length()>1)	
			{
				double priceValue = Double.parseDouble(priceText);
				//Collect the prices in an arraylist
				priceList.add(priceValue);
			}
			else
			{
				sa.assertFalse(true,"Product price is not displaying- LOOP:- "+ i);
			}
		
		}
		
		log.info("All Prices:- "+ priceList);
		copyOfpriceList = (ArrayList<Double>) priceList.clone();
		//Sort the "copyOfpriceList" In High to low order
		Collections.sort(copyOfpriceList);
		Collections.reverse(copyOfpriceList);
		log.info("copyOfpriceList:- "+ copyOfpriceList);
		
		log.info("Compair the Prices");
		sa.assertEquals(priceList, copyOfpriceList,"Price should be sorted in High to Low option");
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-955")
	public void TC09_sortPriceASC() throws Exception
	{
		ArrayList<Double> priceList = new ArrayList<Double>();
		ArrayList<Double> copyOfpriceList = new ArrayList<Double>();
		
		log.info("Select the Price Low To High option");
		WebElement sortByDD = l1.getWebElement("PH_Sort_DD", "Profile//PurchaseHisotry.properties");
		Select sel = new Select(sortByDD);
		String lowToHigh = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 1, 0); 
		sel.selectByVisibleText(lowToHigh);
		Thread.sleep(3000);
		
		List<WebElement> allPrice = l1.getWebElements("PH_Prod_Prices", "Profile//PurchaseHisotry.properties");
		List<WebElement> allHcpcsCode = l1.getWebElements("PH_Prod_HCPCSCode", "Profile//PurchaseHisotry.properties");
		int allPriceSize = allPrice.size();
		log.info("allPriceSize:- "+ allPriceSize);
		String priceText = "";
		for (int i = 0; i < allPriceSize; i++)
		{
			log.info("LOOP:- "+i);
			priceText = allPrice.get(i).getText().trim();
			log.info("priceText:- "+ priceText);
			String hcpcsCode = allHcpcsCode.get(i).getText();
			log.info("hcpcsCode:- "+ hcpcsCode);
			priceText = priceText.replace(hcpcsCode, "").replace("$", "").trim();
			log.info("priceText after triming:-"+ priceText);
			if (priceText.length()>1)
			{
				double priceValue = Double.parseDouble(priceText);
				//Collect the prices in an arraylist
				priceList.add(priceValue);
			}
			else
			{
				sa.assertFalse(true,"Product price is not displaying- LOOP:- "+ i);
			}
		}
		log.info("All Prices:- "+ priceList);
		copyOfpriceList = (ArrayList<Double>) priceList.clone();
		//Sort the "copyOfpriceList" Arraylist
		Collections.sort(copyOfpriceList);
		log.info("copyOfpriceList:- "+ copyOfpriceList);
		
		log.info("Compair the Prices");
		sa.assertEquals(priceList, copyOfpriceList,"Compare the prices");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-961 DRM-962")
	public void TC10_productNameAscendingAndDescendingOrder() throws Exception
	{
		log.info("navigate to frequently ordered items page");
		s.navigateToFrequentlyOrderedItemsPage();
		
		//For both Ascending and Descending order
		for (int k = 3; k <= 4; k++) 
		{
			log.info("LOOP K:-"+ k);
			ArrayList<String> proNamesList = new ArrayList<String>();

			log.info("Select the sort by NAme options");
			WebElement sortByDD = l1.getWebElement("PH_Sort_DD", "Profile//PurchaseHisotry.properties");
			Select sel = new Select(sortByDD);
			String sortByOptionText = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", k, 0);
			log.info("Select the Sort Option:- "+ sortByOptionText);
			sel.selectByVisibleText(sortByOptionText);
			Thread.sleep(3000);
			
			log.info("Collect the Product names");
			List<WebElement> proNames = l1.getWebElements("PH_ProductNames", "Profile//PurchaseHisotry.properties");
			log.info("Total number of Products :- " + proNames.size());
			for (int i = 0; i < proNames.size(); i++) 
			{
				log.info("LOOP i:-"+ i);
				String proName = proNames.get(i).getText();
				proNamesList.add(proName);
			}
			
			log.info("proNamesList:- " + proNamesList);
			
			log.info("Create a copy of product names list");
			ArrayList<String> copyOfproNamesList = new ArrayList<String>();
			copyOfproNamesList = (ArrayList<String>) proNamesList.clone();
			
			log.info("Sort the copyOfproNamesList");
			//For Ascending order
			Collections.sort(copyOfproNamesList);
			if (k==4) 
			{
				//For Descending order
				Collections.reverse(copyOfproNamesList);
			}
			
			log.info("proNamesList after Sort:- " + proNamesList);
			
			log.info("Compair the product names");
			sa.assertEquals(proNamesList, copyOfproNamesList, "Compair product names -LOOP:-"+k);
		}
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-010 DRM-957")
	public void TC11_purchaseHistoryCategoryFunctionality() throws Exception
	{
		
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Click on Filters button");
			l1.getWebElement("PH_FilterButton_Mobile", purchaseHisotryProperties).click();
		}
		log.info("Verify the CAtegory dropdownbox");
		sa.assertTrue(gVar.assertVisible("PH_CategoryDD", purchaseHisotryProperties),"Verify the CAtegory dropdown box");
		
		WebElement categorySelectBox = l1.getWebElement("PH_CategorySelectbox",purchaseHisotryProperties);
		Select sel = new Select(categorySelectBox);
		int optionsCount = sel.getOptions().size()-1;
		log.info("optionsCount:- "+ optionsCount);
		
		for (int i = 1; i < optionsCount; i++)
		{
			categorySelectBox = l1.getWebElement("PH_CategorySelectbox",purchaseHisotryProperties);
			sel = new Select(categorySelectBox);
			log.info("LOOP I:- "+ i);
			log.info("Select the options");
			String option = sel.getOptions().get(i).getText().trim();
			log.info("option:- "+ option);
			sel.selectByIndex(i);
			Thread.sleep(2000);
			if (gVar.mobileView()||gVar.tabletView())
			{
				log.info("Click on Apply button");
				l1.getWebElement("PH_FilterApplyButton_Mobile", purchaseHisotryProperties).click();
				Thread.sleep(3000);
				
				log.info("Click on FILTER button");
				l1.getWebElement("PH_FilterButton_Mobile", purchaseHisotryProperties).click();
				log.info("Verify the Filter Overlay");
				String filterStyle = gVar.assertEqual("PH_FilterOverlay_Mobile", purchaseHisotryProperties,"style");
				sa.assertTrue(filterStyle.contains("display: block"),"Verify the Filter overlay");
			}
			
			log.info("Verify the selected option in dropdown box");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_CategoryDD", purchaseHisotryProperties,"title"),
					option,"VErify the selected option");
			
			if (gVar.mobileView()||gVar.tabletView())
			{
				log.info("Close the overlay");
				l1.getWebElement("PH_FilterOverlay_CloseIcon_Mobile", purchaseHisotryProperties).click();
				
				String filterStyle = gVar.assertEqual("PH_FilterOverlay_Mobile", purchaseHisotryProperties,"style");
				sa.assertFalse(filterStyle.contains("display: block"),"Filter overlay should not display");
			}
			log.info("Verify the selected category in products line item section");
			List<WebElement> variantOptions = l1.getWebElements("PH_Variant_Opt", purchaseHisotryProperties);
			log.info("variantOptions size:-"+ variantOptions.size());
			//To verify the selected category in all the product line items
			for (int j = 0; j < variantOptions.size(); j++)
			{
				log.info("LOOP J:-"+ j);
				String variantText = variantOptions.get(j).getText().toLowerCase();
				log.info("variantText:- "+variantText);
				sa.assertTrue(variantText.contains(option.toLowerCase()),"Verify the selected category in products lin item -LOOP i:-"+i+"-LOOP j:-"+j);
			}
			
			log.info("VErify the Clear Link in selected category displaying section");
			String clearAllLink = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 3, 2);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_CategoryClearAllLink", purchaseHisotryProperties), 
					clearAllLink,"VErify the Clear All link");
			
			log.info("VErify the Selected option in selected category displaying section");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_SelectedCategory", purchaseHisotryProperties), 
					option+" x", "Verify the selected option");
			log.info("Verify the close icon");
			sa.assertTrue(gVar.assertVisible("PH_CategoryCloseIcon", purchaseHisotryProperties),"Verify the Close icon");
			
			log.info("Verify the label");
			String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 4, 1);
			sa.assertEquals(gVar.assertEqual("PH_CategoryLabel", purchaseHisotryProperties), 
					expected,"Verify the label");
			
			log.info("Click on Clear link");
			l1.getWebElement("PH_CategoryClearAllLink",purchaseHisotryProperties).click();
			Thread.sleep(4000);
			
			if (gVar.mobileView()||gVar.tabletView())
			{
				log.info("Click on Filters button");
				l1.getWebElement("PH_FilterButton_Mobile", purchaseHisotryProperties).click();
			}
			
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_CategoryDD", purchaseHisotryProperties,"title"),
					GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 5, 2),"VErify the selected option");
		}
		
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="DMED-010 DRM-958")
	public void TC12_selectingMultipleCategoryOptions() throws Exception
	{
		String filterStyle = gVar.assertEqual("PH_FilterOverlay_Mobile", purchaseHisotryProperties,"style");
		log.info("filterStyle:- "+ filterStyle);
		System.out.println(!filterStyle.contains("display: block"));
//		if (gVar.mobileView()||gVar.tabletView() && !filterStyle.contains("display: block"))
//		{
//			log.info("Click on Filters button");
//			l1.getWebElement("PH_FilterButton_Mobile", purchaseHisotryProperties).click();
//		}
		
		WebElement categorySelectBox = l1.getWebElement("PH_CategorySelectbox",purchaseHisotryProperties);
		Select sel = new Select(categorySelectBox);
		int optionsCount = sel.getOptions().size()-1;
		log.info("optionsCount:- "+ optionsCount);
		ArrayList<String> al = new ArrayList<String>();
		int numberOfSelectedOptions=0;
		//To Select multiple category option
		for (int i = 1; i < optionsCount; i++)
		{
			log.info("LOOP:-"+i);
			categorySelectBox = l1.getWebElement("PH_CategorySelectbox",purchaseHisotryProperties);
			log.info("Select the Filter");
			sel = new Select(categorySelectBox);
			int cnt = sel.getOptions().size();
			log.info("cnt:- "+cnt);
			
			//Verify whether the the options are available or not, then select the category option
			if (i<= cnt-2)
			{
				String selectedOptions = sel.getOptions().get(i).getText().trim();
				log.info("Collect the selected Category option in an arraylist");
				al.add(selectedOptions);
				sel.selectByIndex(i);
				Thread.sleep(3000);
				
				if (gVar.mobileView()||gVar.tabletView())
				{
					log.info("Click on Apply button");
					l1.getWebElement("PH_FilterApplyButton_Mobile", purchaseHisotryProperties).click();
					Thread.sleep(3000);
				}
				
				log.info("Verify the selected option");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_SelectedCategory", purchaseHisotryProperties), 
						selectedOptions+" x", "Verify the selected option");
				String selectedCategory = l1.getWebElements("PH_SelectedCategory", purchaseHisotryProperties).get(i-1).getText().trim();
				log.info("selectedCategory:- "+ selectedCategory);
				gVar.assertequalsIgnoreCase(selectedOptions, selectedCategory,"Verify the selected category");
				
				if (gVar.mobileView()||gVar.tabletView())
				{
					log.info("Click on Filters button");
					l1.getWebElement("PH_FilterButton_Mobile", purchaseHisotryProperties).click();
				}
				
				numberOfSelectedOptions++;
			}
			else
			{
				log.info("All available options are selected hence exit the loop");
				break;
			}
		}
		
		log.info("al:- "+ al);
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Close the overlay");
			l1.getWebElement("PH_FilterOverlay_CloseIcon_Mobile", purchaseHisotryProperties).click();
		}
		
		log.info("Collect the category options which are selected and store it in array");
		ArrayList<String> optionsList = new ArrayList<String>();
		List<WebElement> allOptions = l1.getWebElements("PH_SelectedCategory", purchaseHisotryProperties);
		for (int i = 0; i < allOptions.size(); i++)
		{
			log.info("LOOP:- "+ i);
			optionsList.add(allOptions.get(i).getText().trim());
		}
		log.info("Compare the Array list");
		sa.assertEquals(al, optionsList,"Verify the arraylist");
		
		log.info("Verify the CAtegory selected value in category dropdown box");
		String expected = numberOfSelectedOptions +" "+GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 5, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_CategoryDD", purchaseHisotryProperties,"title"), 
				expected,"Verify the selected value in category drop down box");
		
		//Verify the multiple selected category options in product line items
		for (int k = 0; k < al.size(); k++)
		{
			log.info("Verify the selected category in products line item section");
			List<WebElement> variantOptions = l1.getWebElements("PH_Variant_Opt", purchaseHisotryProperties);
			log.info("variantOptions size:-"+ variantOptions.size());
			for (int j = 0; j < variantOptions.size(); j++)
			{
				log.info("LOOP J:-"+ j);
				String variantText = variantOptions.get(j).getText().toLowerCase().trim();
				log.info("variantText:- "+variantText);
				String catOption = al.get(k).toLowerCase();
				log.info("catOption:- "+ catOption);
				sa.assertTrue(variantText.contains(catOption),"Verify the selected category in products lin item -LOOP i:-"+i+"-LOOP j:-"+j);
			}
		}
		
		
		//############################################################
		log.info("Verify the close functionality of selected category options");
		int numberOfCloseIcons = l1.getWebElements("PH_CategoryCloseIcon", purchaseHisotryProperties).size();
		sa.assertEquals(numberOfCloseIcons, numberOfSelectedOptions,"Verify the number of close icons");

		for (int i = 0; i < numberOfCloseIcons; i++)
		{
			log.info("LOOP:- "+i);
			log.info("Click on CLOSE link in selected Category options");
			l1.getWebElement("PH_CategoryCloseIcon", purchaseHisotryProperties).click();
			log.info("verify the option");
			sa.assertEquals(gVar.assertEqual("PH_SelectedCategory", purchaseHisotryProperties),
					optionsList.get(i+1),"Verify the option");
		}
		sa.assertAll();
	}
	
	
	@Test(groups="reg")
	public void TC13_plcaeOrder() throws Exception
	{
		
		log.info("navigate to Order Review page");
		checkout.navigateToOrderReviewPage();
		poNumber = checkout.PONumberVal;
		log.info("poNumber:- "+ poNumber);
		log.info("Check the Freight Checkbox if its visible");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("click on place order");
		l1.getWebElements("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").get(0).click();
		log.info("fetch order number");
		orderNum=l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1];
		log.info("orderNum:- "+orderNum);
		gVar.saveOrderId(orderNum, "TC13_plcaeOrder");
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-1045")
	public void TC14_searchByOrderNum() throws Exception
	{
		s.navigateToFrequentlyOrderedItemsPage();
		log.info("Enter order number in search box");
		l1.getWebElement("PH_SearchBox","Profile//PurchaseHisotry.properties").sendKeys(orderNum);
		l1.getWebElement("PH_Search_Btn","Profile//PurchaseHisotry.properties").click();
		log.info("verify results");
		
		log.info("Only one product row should display");
		sa.assertEquals(l1.getWebElements("PH_ProductLineItems", purchaseHisotryProperties).size(), 1,"Verify the number of product line item");
		String text = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 6, 1);
		String resultText = GetData.getDataFromExcel("//data//GenericData_US.xls", "PurchaseHistory", 7, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_Pagination_Bar_Results_TextTop", purchaseHisotryProperties), 
				1+" "+text,"Top product results");
		sa.assertEquals(gVar.assertEqual("PH_Pagination_Bar_Results_TextBottom", purchaseHisotryProperties), 
				resultText,"Verify the result in bottom");
		
		log.info("Enter PO number in search box");
		l1.getWebElement("PH_SearchBox","Profile//PurchaseHisotry.properties").sendKeys(poNumber);
		l1.getWebElement("PH_Search_Btn","Profile//PurchaseHisotry.properties").click();
		log.info("verify results");
		
		log.info("Only one product row should display");
		sa.assertEquals(l1.getWebElements("PH_ProductLineItems", purchaseHisotryProperties).size(), 1,"Verify the number of product line item");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_Pagination_Bar_Results_TextTop", purchaseHisotryProperties), 
				1+" "+text,"Top product results");
		sa.assertEquals(gVar.assertEqual("PH_Pagination_Bar_Results_TextBottom", purchaseHisotryProperties), 
				resultText,"Verify the result in bottom");
		
		log.info("Enter product ID in search box");
		String productID = "10220-1";
		l1.getWebElement("PH_SearchBox","Profile//PurchaseHisotry.properties").sendKeys(productID);
		l1.getWebElement("PH_Search_Btn","Profile//PurchaseHisotry.properties").click();
		log.info("verify results");
		
		log.info("Only one product row should display");
		sa.assertEquals(l1.getWebElements("PH_ProductLineItems", purchaseHisotryProperties).size(), 1,"Verify the number of product line item");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PH_Pagination_Bar_Results_TextTop", purchaseHisotryProperties), 
				1+" "+text,"Top product results");
		sa.assertEquals(gVar.assertEqual("PH_Pagination_Bar_Results_TextBottom", purchaseHisotryProperties), 
				resultText,"Verify the result in bottom");
		
		sa.assertEquals(gVar.assertEqual("PH_Item_Codes", purchaseHisotryProperties), 
				productID,"Verify the product ID");
		
		sa.assertAll();
	}

	
	@Test(groups={"reg"}, description="DMED-010 DRM-968 DRM-970")
	public void TC16_addMultipleProductsToCart() throws Exception
	{
		cart.clearCart();
		
		log.info("Navigate to Purchase history page");
		s.navigateToFrequentlyOrderedItemsPage();
		proNamesList = new ArrayList<String>();
		ArrayList<String> priceList = new ArrayList<String>();
		List<WebElement> proNAmeIdentifiers = l1.getWebElements("PH_ProductNames", purchaseHisotryProperties);
		List<WebElement> priceNamesIdentifier = l1.getWebElements("PH_Prod_Prices",purchaseHisotryProperties);
		List<WebElement> allHcpcsCode = l1.getWebElements("PH_Prod_HCPCSCode", "Profile//PurchaseHisotry.properties");
		List<WebElement> checkboxes;
		if (gVar.mobileView()||gVar.tabletView())
		{
			checkboxes = l1.getWebElements("PH_Checkbox_Mobile", purchaseHisotryProperties);
		}
		else
		{
			checkboxes = l1.getWebElements("PH_Checkbox", purchaseHisotryProperties);
		}
		
		log.info("Select the multiple products");
		proCnt = checkboxes.size();//Select all products in Purchase history page
		for (int i = 0; i < proCnt; i++)
		{
			log.info("LOOP:- "+ i);
			log.info("Collect the product name and price");
			String proName = proNAmeIdentifiers.get(i).getText();
			log.info("proName:- "+ proName);
			String hcpcsCode = allHcpcsCode.get(i).getText();
			String price = priceNamesIdentifier.get(i).getText().replace(hcpcsCode, "").trim();
			log.info("price:-"+price);
			
			proNamesList.add(proName);
			priceList.add(price);
			log.info("Select the checkbox");
			WebElement element = checkboxes.get(i);
			JavascriptExecutor exe=(JavascriptExecutor)driver;
			exe.executeScript("arguments[0].click();", element);
			
			Thread.sleep(1000);
		}
		
		log.info("Names List:- "+ proNamesList);
		log.info("Price list:- "+ priceList);
		
//		log.info("Verify the ADD TO CART section");
//		String addToCartSectionStyle = gVar.assertEqual("PH_AddToCartSection", purchaseHisotryProperties,"style");
//		sa.assertTrue(addToCartSectionStyle.contains("display: block;"),"Verify the ADd to cart section");
		
		log.info("VErify the Selected product count in ADD TO CART section in bottom");
		sa.assertEquals(gVar.assertEqual("PH_AddToCartSection_SelectedProductCount", purchaseHisotryProperties), 
				proCnt+"","Verify the Selected product count in ADD TO CART section");
		
		
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PH_AddToCartButton", purchaseHisotryProperties).click();
		
		log.info("Navigate to cart page");
		s.navigateToCartPage();
		log.info("Verify the mini cart quantity");
		sa.assertEquals(gVar.assertEqual("Header_Cart_Qty", "Shopnav//header.properties"), 
				proCnt+"","Verify the cart quantity in header");
		
		log.info("Verify the Product name");
		List<WebElement> proNAmesInCart = l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties");
		List<WebElement> priceListInCart = l1.getWebElements("Cart_ItemTotal", "Cart//Cart.properties");
		
		for (int i = 0; i < proCnt; i++)
		{
			log.info("LOOP:- "+ i);
			sa.assertEquals(proNAmesInCart.get(i).getText(), 
					proNamesList.get(i),"Verify the product name");
			sa.assertEquals(priceListInCart.get(i).getText(), 
					priceList.get(i),"Verify the price list");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-970")
	public void TC17_navigationToPaymentPage() throws Exception
	{
		log.info("Click on PROCEED TO CHECKOUT button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		Thread.sleep(2000);
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Selectaddress");
		l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties").get(0).click();
		
		log.info("Click  CONTINUE button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Collect the SHIPPTO Address");
		String shippToAddress = l1.getWebElement("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties").getText();
		log.info("shippToAddress:- "+ shippToAddress);
		
		
		log.info("Select the shipping method and click on CONTINUE button in SHIPPING METHOD SECTION");
//		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
//		shippingMethodRadioButton.get(0).click();
		l1.getWebElement("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties").click();
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Collect the shipping method");
		String selectedShippingMethod = l1.getWebElement("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties").getText();
		log.info("selectedShippingMethod:- " + selectedShippingMethod);
		
		
		log.info("Enter PO number");
		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		String poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		
		log.info("Collect the Product names in Order review page");
		List<WebElement> allNames = l1.getWebElements("OrderReview_ProductName", "Checkout//PaymentType.properties");
		log.info("allNames size:- "+ allNames.size());
		ArrayList<String> namesListInOrderReviewPage = new ArrayList<String>();
		for (int i = 1; i < allNames.size(); i++)
		{
			String name = allNames.get(i).getText();
			log.info("name:- "+ name);
			namesListInOrderReviewPage.add(name);
		}
		log.info("namesListInOrderReviewPage:- "+ namesListInOrderReviewPage);
		log.info("VErify the product names");
		sa.assertEquals(namesListInOrderReviewPage, proNamesList,"Verify the products name");
		//Sort the names
//		Collections.sort(namesListInOrderReviewPage);
//		log.info("namesListInOrderReviewPage after Sort:- "+ namesListInOrderReviewPage);
		

		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();

		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
		log.info("orderID:- "+ orderID);
		gVar.saveOrderId(orderID, "Product ADDED TO CART PAGE form Frequently Ordered Items page and placed order");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-973")
	public void TC18_PurchaseHistoryReorderQuantity()
	{
		log.info("VErify the Quantity in Order confirmation page");
			log.info("Verify the product quantity");
		sa.assertEquals(gVar.assertEqual("Orderconfirm_ProductQuantity", "Checkout//OrderConfirmation.properties"), 
				proCnt+"","Verify the product quantity in Order confirmation page");
		
		sa.assertAll();
	}
}
