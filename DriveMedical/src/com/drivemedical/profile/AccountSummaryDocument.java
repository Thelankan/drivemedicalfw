package com.drivemedical.profile;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class AccountSummaryDocument extends BaseTest
{
	String AccountSummaryDocumentProperties = "Profile//AccountSummaryDocument.properties";

	@Test(groups={"reg"}, description="DMED-007 DRM-1619 DRM-1620 DRM-1621")
	public void verifyAccountSummaryUI()
	{
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-007 DRM-1622")
	public void verifyDefaultSortOption() throws Exception
	{
		log.info("Verify the default sort option");
		String oldToNew = GetData.getDataFromExcel("//data//GenericData_US.xls", "AccountSummaryDocument", 1, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountSummary_SortByDD_Top", AccountSummaryDocumentProperties,"title"), 
				oldToNew,"Verify the selected sortby option in top");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountSummary_SortByDD_Bottom", AccountSummaryDocumentProperties,"title"), 
				oldToNew,"Verify the selected sortby option in top");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-007 DRM-1624")
	public void sortFunctionality() throws Exception
	{
		log.info("Select sort option Newest to Oldest");
		for (int i = 0; i < 2; i++)
		{
			WebElement sortByTop = l1.getWebElement("AccountSummary_SelectBox_Top", AccountSummaryDocumentProperties);
			Select sel = new Select(sortByTop);
			sel.selectByIndex(i);
			Thread.sleep(2000);
			String sortOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "AccountSummaryDocument", i, 1);
			
			log.info("VErify the selected value");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountSummary_SortByDD_Top", AccountSummaryDocumentProperties,"title"), 
					sortOption,"Verify the selected value top");
			
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountSummary_SortByDD_Bottom", AccountSummaryDocumentProperties,"title"), 
					sortOption,"Verify the selected value bottom");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="1629")
	public void AccountSummaryInvoicingForNonAdmin() throws Exception
	{
		log.info("Logout from the application");
		p.logout(BaseTest.xmlTest);
		
		log.info("Login with non-admin user");
		p.navigateToLoginPage();
		String un = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 9);
		String pwd = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 2, 9);
		p.logInWithCredentials(un, pwd);
		log.info("Account Summary Invoicing should not display");
		sa.assertFalse(true,"script");
		
		
		sa.assertAll();
	}
}
