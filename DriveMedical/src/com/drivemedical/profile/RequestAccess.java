package com.drivemedical.profile;

import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.Data;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.TestData;

public class RequestAccess extends BaseTest
{
	public String requestAccessText;
	public String requestAccessStepOne;
	public String requestAccessAccNum;
	public String requestAccessStepTwo;
	int j = 1;
	
	@Test(groups="reg")
	public void TC00_dataDeclaration() throws Exception 
	{
		requestAccessText = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties", "RequestAccessText");
		System.out.println("requestAccessText:-"+requestAccessText);
		requestAccessStepOne = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties", "RequestAccess_StepOne");
		System.out.println("requestAccessStepOne:-"+requestAccessStepOne);
		requestAccessStepTwo = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties", "RequestAccess_StepTwo");
		System.out.println("requestAccessStepTwo:- "+ requestAccessStepTwo);
		requestAccessAccNum = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties","RequestAccess_AccNum");
		System.out.println("requestAccessAccNum:-"+requestAccessAccNum);
		
	}
	
	@Test(groups = {"reg"}, description = "DMED-517 DRM-505 AND DRM-508")
	public void TC01_accessRequestFunctionality(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Signin page");
		p.navigateToLoginPage();
		
		log.info("Verify the REQUEST ACCESS link in sign-in page");
		sa.assertTrue(gVar.assertVisible("Login_RequestAccess_Login", "Profile//login.properties"),"Verify the Login_RequestAccess_Login");
		
		log.info("Click on REQUEST ACCESS link");
		l1.getWebElement("Login_RequestAccess_Login", "Profile//login.properties").click();
		
		log.info("Verify the navigation/Requrst Access page");
		sa.assertTrue(gVar.assertVisible("RequestAccessHeading", "Profile//RequestAccess.properties"),"Verify the Request Access Heading");
		gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessHeading", "Profile//RequestAccess.properties").getText(), 
				requestAccessText, "Verify the Request Access Heading");
		log.info("Verify the Breadcrumb links");
		gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessBreadcrumbHome", "Profile//RequestAccess.properties").getText(), 
				"Home", "Verify the HOME link in breadcrumb");
		gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessBreadcrumbActLink", "Profile//RequestAccess.properties").getText(), 
				requestAccessText, "Verify the REQUEST ACCESS link in breadcrumb");
		
		log.info("Step Number, Account number label and Account number textbox");
		gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessStpeNum", "Profile//RequestAccess.properties").getText(), 
				requestAccessStepOne,"Verify the reques access stpe");
		sa.assertTrue(gVar.assertVisible("RequestAccessAccNumLabel", "Profile//RequestAccess.properties"),"Verify the RequestAccessAccNumLabel");
		sa.assertTrue(gVar.assertVisible("RequestAccessAccountNumBox", "Profile//RequestAccess.properties"),"Verify the RequestAccessAccountNumBox");
		
		log.info("Verify the Question mark symbol in Account number textbox");
		sa.assertTrue(gVar.assertVisible("RequestAccessQstSymb", "Profile//RequestAccess.properties"),"Verify the Question mark symbol");
		log.info("Clikc on Question mark symbol");
		l1.getWebElement("RequestAccessQstSymb", "Profile//RequestAccess.properties").click();
		log.info("Verify the popup");
		sa.assertTrue(gVar.assertVisible("RequestAccessPopup", "Profile//RequestAccess.properties"),"Verify the popup");
		sa.assertTrue(gVar.assertVisible("RequestAccessPopupCloseLink", "Profile//RequestAccess.properties"),"Verify the CLOSE link in popup");
		for (int i = 0; i < 2; i++) 
		{
			log.info("LOOP:- "+ i);
			if (i==0) 
			{
				log.info("Click on Question mark symbol");
				l1.getWebElement("RequestAccessQstSymb", "Profile//RequestAccess.properties").click();
			}
			else
			{
				log.info("Click on CLOSE icon in popup");
				l1.getWebElement("RequestAccessPopupCloseLink", "Profile//RequestAccess.properties").click();
			}
			
			try 
			{
				sa.assertFalse(gVar.assertVisible("RequestAccessPopup", "Profile//RequestAccess.properties"),"popup should disappear");
				log.info("Popup is Displaying");
			} 
			catch (Exception e) 
			{
				sa.assertTrue(true, "Popup is not displaying on click on Question Mark button");
				
				log.info("Open popup");
				l1.getWebElement("RequestAccessQstSymb", "Profile//RequestAccess.properties").click();
			}
			
		}
		
		log.info("Enter Valid account number");
		l1.getWebElement("RequestAccessAccountNumBox", "Profile//RequestAccess.properties").sendKeys(requestAccessAccNum);
		log.info("Click on CONTINUE button");
		l1.getWebElement("RequestAccessContinueButton", "Profile//RequestAccess.properties").click();
		
		// ################################
		log.info("Verify the navigated page");
		gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessHeading", "Profile//RequestAccess.properties").getText(), 
				requestAccessText,"Verify the Request Access Heading");
		gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessStpeNum", "Profile//RequestAccess.properties").getText(), 
				requestAccessStepTwo, "Verify steps heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description = "DMED-517 DRM-516")
	public void TC02_accessRequestSuccessPage(XmlTest xmlTest) throws Exception
	{
		log.info("Enter Valid Details");
		l1.getWebElement("RequestAccessFName", "Profile//RequestAccess.properties").sendKeys("Tony");
		l1.getWebElement("RequestAccessLName", "Profile//RequestAccess.properties").sendKeys("Stark");
		l1.getWebElement("RequestAccessEmail", "Profile//RequestAccess.properties").sendKeys("tony@gmail.com");
		l1.getWebElement("RequestAccessCompany", "Profile//RequestAccess.properties").sendKeys("stark industries");
		log.info("Click on continue button");
		l1.getWebElement("RequestAccess1ContinueButton", "Profile//RequestAccess.properties").click();
		
		log.info("Verify the Thank You page");
		sa.assertEquals(gVar.assertEqual("RequestAccessThankuPage", "Profile//RequestAccess.properties"), 
				"Thank you!","Verify the thank you heading");
		sa.assertTrue(gVar.assertVisible("RequestAccessSuccessMark", "Profile//RequestAccess.properties"),"Verify the Success mark");
		sa.assertEquals(gVar.assertEqual("RequestAccessSuccessMsg", "Profile//RequestAccess.properties"), 
				"Your Account Administrator has been notified.", "Verify the success message");
		sa.assertEquals(gVar.assertEqual("RequestAccessBackToSignin", "Profile//RequestAccess.properties"), 
				"BACK TO SIGN IN", "Verify the BACK TO SIGN IN button");
		
		log.info("Click on BACK TO SIGN IN button");
		l1.getWebElement("RequestAccessBackToSignin", "Profile//RequestAccess.properties").click();
		
		log.info("It should navigate to Signin page");
		gVar.assertequalsIgnoreCase(l1.getWebElement("SignIn_Heading", "Profile//login.properties").getText(), 
				"Sign In", "Verify the SIGN IN heading");
		
		sa.assertAll();
	}
	
	
	@Test(groups = {"reg"}, description="DMED-517 DRM-506")
	public void TC03_accessRequestFunctionalityCA(XmlTest xmlTest) throws Exception
	{
		sa.assertTrue(false,"SERVER ERROR is displaying on switching the site ");
// ******** SERVER ERROR is displaying on switching the site HENCE BELOW CODE is commented ******** 
//		log.info("Navigate to home page");
//		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
//		log.info("Chenge the locail from header");
//		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
//		log.info("Select Canada-English language");
//		l1.getWebElement("Header_CandaEng_Link", "Shopnav//header.properties").click();
//		
//		log.info("Run above method \"accessRequestFunctionality\" in CA_EN locail");
//		TC01_accessRequestFunctionality(xmlTest);
//		TC02_accessRequestSuccessPage(xmlTest);
//		
//		log.info("Resest back to US locail");
//		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
//		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
//		l1.getWebElement("Header_UsEng_Link", "Shopnav//header.properties").click();
//		
		log.info("Navigate to signin page");
		p.navigateToLoginPage();
		
		log.info("Click on Access Request link in signin page");
		l1.getWebElement("Login_RequestAccess_Login", "Profile//login.properties").click();
	}
	
	
	@Test(groups={"reg"}, dataProvider="accessRequestNumVal",dataProviderClass = Data.class, description="DMED-517 DRM-509 AND DRM-510 AND DRM-511 AND DRM-512")
	public void TC04_accessRequestAccountNumberBoxValidation(TestData t) throws Exception
	{

		String condition = t.get(0);
		System.out.println("condition:-  "+ condition);
		
		log.info("Enter data in Account number fields");
		l1.getWebElement("RequestAccessAccountNumBox", "Profile//RequestAccess.properties").clear();
		l1.getWebElement("RequestAccessAccountNumBox", "Profile//RequestAccess.properties").sendKeys(t.get(1));
		
		log.info("Click on CONTINUE button");
		l1.getWebElement("RequestAccessContinueButton", "Profile//RequestAccess.properties").click();
		Thread.sleep(5000);
		log.info("Verify the message for the iteration  i :- " +i);
		
		if (condition=="0") 
		{
			gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessAccNumErrMsg", "Profile//RequestAccess.properties").getText(),
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Validation", j, 1),"Verify the Account number Error messgae");
			log.info("Verify the ERROR message color");
//			sa.assertEquals(l1.getWebElement("RequestAccessAccNumErrMsg", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "AccNumErrMsg color");
			
		} 
		else if (condition=="7") 
		{
			log.info("Verify the navigated page for valid data");
			sa.assertEquals(l1.getWebElement("RequestAccessHeading", "Profile//RequestAccess.properties").getText().toLowerCase(), 
					requestAccessText.toLowerCase(), "Verify the Request Access Heading");
			sa.assertEquals(l1.getWebElement("RequestAccessStpeNum", "Profile//RequestAccess.properties").getText().toLowerCase(), 
					requestAccessStepTwo.toLowerCase(), "Verify steps heading");
		}
		else 
		{
			String xlData = GetData.getDataFromExcel("//data//GenericData_US.xls", "Validation", j, 1);
			System.out.println("xlData:- "+ xlData);
			System.out.println("application Data:- "+l1.getWebElement("RequestAccessAccNumServerErrMsg", "Profile//RequestAccess.properties").getText());
		
			gVar.assertequalsIgnoreCase(l1.getWebElement("RequestAccessAccNumServerErrMsg", "Profile//RequestAccess.properties").getText(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Validation", j, 1),"Verify RequestAccessAccNumServerErrMsg");
			
//			sa.assertEquals(l1.getWebElement("RequestAccessAccNumServerErrMsg", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "AccNumServerErrMsg color");

		}
		
		
		j++;
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, dataProvider="accessRequestFormVal", dataProviderClass=Data.class ,description="DMED-517 DRM-513")
	public void TC05_accessRequestStepTwoFormValidation(TestData t, XmlTest xmlTest) throws Exception
	{
		log.info("User should be in second step");
		sa.assertEquals(gVar.assertEqual("RequestAccessStpeNum", "Profile//RequestAccess.properties").toLowerCase(), 
				requestAccessStepTwo.toLowerCase(), "Verify the second page");
		
		String fnErrorMessage = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties", "RequestAccessFName_ErrorMessage");
		String lnErrorMessage = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties", "RequestAccessLName_ErrorMessage");
		String emailErrorMessage = GetData.getDataFromProperties("//POM//Profile//RequestAccess.properties", "RequestAccessEmail_ErrorMessage");
		
		String condition = t.get(0);
		System.out.println("condition/Loop:- " + condition);
		log.info("Clear fields");
		l1.getWebElement("RequestAccessFName", "Profile//RequestAccess.properties").clear();
		l1.getWebElement("RequestAccessLName", "Profile//RequestAccess.properties").clear();
		l1.getWebElement("RequestAccessEmail", "Profile//RequestAccess.properties").clear();
		l1.getWebElement("RequestAccessCompany", "Profile//RequestAccess.properties").clear();
		
		
		log.info("Enter validation details");
		l1.getWebElement("RequestAccessFName", "Profile//RequestAccess.properties").sendKeys(t.get(1));
		l1.getWebElement("RequestAccessLName", "Profile//RequestAccess.properties").sendKeys(t.get(2));
		l1.getWebElement("RequestAccessEmail", "Profile//RequestAccess.properties").sendKeys(t.get(3));
		l1.getWebElement("RequestAccessCompany", "Profile//RequestAccess.properties").sendKeys(t.get(4));
		
		log.info("Click on CONTINUE button");
		l1.getWebElement("RequestAccess1ContinueButton", "Profile//RequestAccess.properties").click();
		
		if (condition=="0")	//For Blank
		{

			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessFName_Error", "Profile//RequestAccess.properties"), 
					fnErrorMessage, "Verify the First name error message");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessLName_Error", "Profile//RequestAccess.properties"), 
					lnErrorMessage, "Verify the Last name error message");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessEmail_Error", "Profile//RequestAccess.properties"), 
					emailErrorMessage, "Verify the Email ID error message");
			
//			log.info("Verify the Error message color");
//			sa.assertEquals(l1.getWebElement("RequestAccessFName_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "Fnmae Error color");
//			sa.assertEquals(l1.getWebElement("RequestAccessLName_Error", "Profile//RequestAccess.properties").getCssValue("color"),
//					gVar.emTextColor, "Lname error color");
//			sa.assertEquals(l1.getWebElement("RequestAccessEmail_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor);
			
		} 
		else if (condition=="1") 	//For alpha numeric
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessEmail_Error", "Profile//RequestAccess.properties"), 
					emailErrorMessage, "Verify the Email ID error message");
//			sa.assertEquals(l1.getWebElement("RequestAccessEmail_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "email Error color");
		} 
		else if (condition=="2") //Last name is blank
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessLName_Error", "Profile//RequestAccess.properties"), 
					lnErrorMessage, "Verify the Last name error message");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessEmail_Error", "Profile//RequestAccess.properties"), 
					emailErrorMessage, "Verify the Email ID error message");
//			sa.assertEquals(l1.getWebElement("RequestAccessLName_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "LName_Error color");
//			sa.assertEquals(l1.getWebElement("RequestAccessEmail_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "Email_Error");
			
		} 
		else if (condition=="3") //First name is blank
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessFName_Error", "Profile//RequestAccess.properties"), 
					fnErrorMessage, "Verify the First name error message");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessEmail_Error", "Profile//RequestAccess.properties"), 
					emailErrorMessage, "Verify the Email ID error message");
			
//			sa.assertEquals(l1.getWebElement("RequestAccessFName_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "FName_Error");
//			sa.assertEquals(l1.getWebElement("RequestAccessEmail_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "Email_Error");
		} 
		else if (condition=="4" ||condition=="5"||condition=="6"||condition=="7")	//For invalid EMAIL ID 
		{
			gVar.assertequalsIgnoreCase(gVar.assertEqual("RequestAccessEmail_Error", "Profile//RequestAccess.properties"), 
					emailErrorMessage, "Verify the email error message");
//			sa.assertEquals(l1.getWebElement("RequestAccessEmail_Error", "Profile//RequestAccess.properties").getCssValue("color"), 
//					gVar.emTextColor, "email error message color");
		} 
		else 	//For Valid First name, last name and Email id but empty company name
		{
			log.info("It should navigate to Thank you page");
			sa.assertEquals(l1.getWebElement("RequestAccessThankuPage", "Profile//RequestAccess.properties").getText().toLowerCase(), "Thank you!".toLowerCase(), "Verify the thank you heading");
			sa.assertTrue(gVar.assertVisible("RequestAccessSuccessMark", "Profile//RequestAccess.properties"),"Verify the Success mark");
			sa.assertEquals(l1.getWebElement("RequestAccessSuccessMsg", "Profile//RequestAccess.properties").getText().toLowerCase(), "Your Account Administrator has been notified.".toLowerCase(), "Verify the success message");
			sa.assertEquals(l1.getWebElement("RequestAccessBackToSignin", "Profile//RequestAccess.properties").getText().toLowerCase(), "BACK TO SIGN IN".toLowerCase(), "Verify the BACK TO SIGN IN button");
		}
		
		sa.assertAll();
	}
	
}
