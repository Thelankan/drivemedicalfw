package com.drivemedical.profile;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ApplyForAnAccount extends BaseTest
{
	@Test(groups={"reg"}, description="ApplyForAnAccount")
	public void TC00_ApplyForAnAccount() throws Exception
	{
		log.info("Navifate to Sign in page");
		p.navigateToLoginPage();
		
		log.info("Verify the Apply FOR an account button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Login_ApplyForAnAccount", "Profile//login.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 1, 8),"Verify the APPLY FOR AN ACCOUNT button");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Login_NewCustomersHeading", "Profile//login.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 1, 9),"Verify the small title");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Login_CreateAccountSectionHeading", "Profile//login.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 3, 9),"Verify the Create account section heading");
		log.info("Click on APPLY FOR AN ACCOUNT button");
		l1.getWebElement("Login_ApplyForAnAccount", "Profile//login.properties").click();
		
		log.info("Verify the Navigated page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ApplyForAccount_PageHeading", "Profile//login.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 2, 9),"VErify the heading");
		log.info("Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 2, 9),"Verify the breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"Home "+GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 2, 9),"Verify the breadcrumb");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="verifyUiOfApplyForAccountPage")
	public void TC01_verifyUiOfApplyForAccountPage() throws Exception
	{
		log.info("Switch the driver controller to iframe");
		WebElement iframe = l1.getWebElement("ApplyForAccount_Iframe", "Profile//login.properties");
		driver.switchTo().frame(iframe);
		Thread.sleep(2000);
		log.info("Verify the labels");
		List<WebElement> allLabels = l1.getWebElements("ApplyForAccount_Labels", "Profile//login.properties");
		log.info("allLabels count:- "+ allLabels.size());
		for (int i = 0; i < allLabels.size(); i++)
		{
			log.info("LOOP:-"+i);
			String label = allLabels.get(i).getText();
			log.info("label:- "+ label);
			gVar.assertequalsIgnoreCase(label, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", i+1, 10),"Verify the label");
		}
		
		log.info("Verify the submit button");
		gVar.assertEqual(gVar.assertEqual("ApplyForAccount_SubmitButton", "Profile//login.properties","value"), 
				"Submit","Verify the submit button");
		
		log.info("Switch the driver controller to the window");
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		sa.assertAll();
	}
}
