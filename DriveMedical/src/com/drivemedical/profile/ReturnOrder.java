package com.drivemedical.profile;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterSuite;

//UN : lsteinberg@pfsweb.com pwd: passw0rd$

//order ID -0005940132

//fright product - 10220-1


import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;


//DRM-1453
//DRM-1463
//DRM-1466
//DRM-1467
//DRM-1468
//DRM-1469
//DRM-1470
//DRM-1471
//DRM-1472
//DRM-1473
//DRM-1474

public class ReturnOrder extends BaseTest {
	
	String UN="lsteinberg@pfsweb.com";
	String pwd="passw0rd$";
	String oredrNum="0005940132";
	Select selectReason;

	@Test(groups={"reg"},description="DMED-095 DRM-1450,1451 DMED-209 DRM-")
	public void returnOrderFun(XmlTest xmlTest) throws Exception
	{
		log.info("log in to application");
		p.navigateToLoginPage();
		//p.logIn(xmlTest);
		p.logInWithCredentials(UN, pwd);
		log.info("navigate to order history page");
		s.navigateToOrderHistoryPage();
		log.info("enter order number");
		l1.getWebElement("OrderHistory_SearchBox", "/Profile/OrderHistory.properties").sendKeys(oredrNum);
		log.info("click on submit");
		l1.getWebElement("OrderHistory_Search_Button", "/Profile/OrderHistory.properties").click();
		log.info("click on order id number");
		l1.getWebElements("OrderHistory_AllOrderId", "/Profile/OrderHistory.properties").get(0).click();
		log.info("click on return order");
		l1.getWebElement("OrderDetails_ReturnOrder_Btn", "/Profile/OrderDetails.properties").click();
		log.info("verify navigation");
		sa.assertTrue(gVar.assertVisible("RO_Heading", "/Profile/ReturnOrder.properties"),"Return order heading");
		sa.assertTrue(gVar.assertVisible("RO_ReturnOrder_Text", "/Profile/ReturnOrder.properties"),"selects items to return");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1454")
	public void defaultReturnOption() throws Exception
	{
		log.info("check item check box");
		l1.getWebElement("RO_return_Checkbox", "/Profile/ReturnOrder.properties").click();
		log.info("verify credit option should be selected");
		sa.assertTrue(gVar.assertVisible("RO_Credit_Selected", "/Profile/ReturnOrder.properties"),"credit option should be selected");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1457")
	public void reasonRequestDropDownDisplay() throws Exception
	{
		log.info("un check item check box");
		l1.getWebElement("RO_return_Checkbox", "/Profile/ReturnOrder.properties").click();
		sa.assertFalse(gVar.assertVisible("RO_Replacement_Sec", "/Profile/ReturnOrder.properties"),"replacement section");
		log.info("check item check box again");
		l1.getWebElement("RO_return_Checkbox", "/Profile/ReturnOrder.properties").click();
		sa.assertTrue(gVar.assertVisible("RO_Replacement_Sec", "/Profile/ReturnOrder.properties"),"replacement section");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1455")
	public void selectReturnOutCome() throws Exception
	{
		log.info("click on replacement");
		l1.getWebElement("RO_Replacement", "/Profile/ReturnOrder.properties").click();
		log.info("verify replacement option should be selected");
		sa.assertTrue(gVar.assertVisible("RO_Replacement_Selected", "/Profile/ReturnOrder.properties"),"replacement option should be selected");
		sa.assertTrue(gVar.assertVisible("RO_Replacement_Sec", "/Profile/ReturnOrder.properties"),"replacement section");
		log.info("click on credit");
		l1.getWebElement("RO_Credit", "/Profile/ReturnOrder.properties").click();
		log.info("verify credit option should be selected");
		sa.assertTrue(gVar.assertVisible("RO_Credit_Selected", "/Profile/ReturnOrder.properties"),"replacement option should be selected");
		sa.assertTrue(gVar.assertVisible("RO_Replacement_Sec", "/Profile/ReturnOrder.properties"),"replacement section");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1456,1460")
	public void requestReturnBtn() throws Exception
	{
		log.info("verify request return button state");
		sa.assertEquals(gVar.assertEqual("RO_RequestReturn_Btn", "/Profile/ReturnOrder.properties","disabled"),"disabled","replacement option should be selected");
		log.info("select any reason");
		selectReason=new Select(l1.getWebElement("RO_Reason_DD", "/Profile/ReturnOrder.properties"));
		selectReason.selectByValue("ChangedMind");
		log.info("verify ttach link should not display");
		sa.assertFalse(gVar.assertVisible("RO_Attach_Link", "/Profile/ReturnOrder.properties"),"attach link");
		log.info("verify request return button state");
		sa.assertEquals(gVar.assertEqual("RO_RequestReturn_Btn", "/Profile/ReturnOrder.properties","disabled"),"disabled","replacement option should be selected");
		log.info("enter quantity");
		l1.getWebElement("RO_Qty_Box","/Profile/ReturnOrder.properties").clear();
		l1.getWebElement("RO_Qty_Box","/Profile/ReturnOrder.properties").sendKeys("1");
		sa.assertEquals(gVar.assertEqual("RO_RequestReturn_Btn", "/Profile/ReturnOrder.properties","disabled"),null,"replacement option should be selected");
		sa.assertAll();
	}

	@Test(groups={"reg"},description="DMED-095 DRM-1452,1458")
	public void requestReturnSubmit() throws Exception
	{
		log.info("click on request return");
		l1.getWebElement("RO_RequestReturn_Btn", "/Profile/ReturnOrder.properties").click();
		log.info("verify request submitted");
		String expHeading="Return Order # "+oredrNum+" Confirmation";
		sa.assertEquals(gVar.assertEqual("RO_Heading", "/Profile/ReturnOrder.properties"), expHeading,"Confirmation heading");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1462")
	public void orderAfterReturnSubmission() throws Exception
	{
		log.info("navigate to order history page");
		s.navigateToOrderHistoryPage();
		log.info("enter order number");
		l1.getWebElement("OrderHistory_SearchBox", "/Profile/OrderHistory.properties").sendKeys(oredrNum);
		log.info("click on submit");
		l1.getWebElement("OrderHistory_Search_Button", "/Profile/OrderHistory.properties").click();
		log.info("verify ");
		sa.assertEquals(gVar.assertEqual("OrderHistory_All_", "/Profile/OrderHistory.properties"),"In progress"," check");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1452,1459")
	public void attachment() throws Exception
	{
		log.info("navigate to order history page");
		s.navigateToOrderHistoryPage();
		log.info("enter order number");
		l1.getWebElement("OrderHistory_SearchBox", "/Profile/OrderHistory.properties").sendKeys(oredrNum);
		log.info("click on submit");
		l1.getWebElement("OrderHistory_Search_Button", "/Profile/OrderHistory.properties").click();
		log.info("click on order id number");
		l1.getWebElements("OrderHistory_AllOrderId", "/Profile/OrderHistory.properties").get(0).click();
		log.info("click on return order");
		l1.getWebElement("OrderDetails_ReturnOrder_Btn", "/Profile/OrderDetails.properties").click();
		log.info("check item check box again");
		l1.getWebElement("RO_return_Checkbox", "/Profile/ReturnOrder.properties").click();
		log.info("click on credit");
		l1.getWebElement("RO_Credit", "/Profile/ReturnOrder.properties").click();
		log.info("select damage option");
		selectReason.selectByValue("DamagedInTransit");
		sa.assertTrue(gVar.assertVisible("RO_Attach_Link", "/Profile/ReturnOrder.properties"),"attach link");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1461")
	public void reOrderNumandDateDisplay() throws Exception
	{
		log.info("click on yes radio button");
		l1.getWebElement("RO_Yes_Radio", "/Profile/ReturnOrder.properties").click();
		log.info("verify display of Re Order Details");
		sa.assertTrue(gVar.assertVisible("RO_ReOrder_Textbox", "/Profile/ReturnOrder.properties"),"RO_ReOrder_Textbox");
		sa.assertTrue(gVar.assertVisible("RO_DatePicker_Form", "/Profile/ReturnOrder.properties"),"RO_ReOrder_Textbox");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-095 DRM-1464,1465")
	public void addComment() throws Exception
	{
		log.info("click on add comment link");
		l1.getWebElement("RO_Comment_Link", "/Profile/ReturnOrder.properties").click();
		log.info("verify display of Re Order Details");
		sa.assertTrue(gVar.assertVisible("RO_AddComment_Textbox", "/Profile/ReturnOrder.properties"),"RO_AddComment_Textbox");
		String textEnter="I didnt lie it";
		l1.getWebElement("RO_AddComment_Textbox", "/Profile/ReturnOrder.properties").sendKeys(textEnter);
		sa.assertEquals(gVar.assertEqual("RO_AddComment_Textbox", "/Profile/ReturnOrder.properties"), textEnter);
		log.info("click on add comment link to close it");
		l1.getWebElement("RO_Comment_Link", "/Profile/ReturnOrder.properties").click();
		log.info("verify display of Re Order Details");
		sa.assertFalse(gVar.assertVisible("RO_AddComment_Textbox", "/Profile/ReturnOrder.properties"),"RO_AddComment_Textbox");
		log.info("click on add comment link to re open it");
		l1.getWebElement("RO_Comment_Link", "/Profile/ReturnOrder.properties").click();
		sa.assertEquals(gVar.assertEqual("RO_AddComment_Textbox", "/Profile/ReturnOrder.properties"), textEnter);
		sa.assertAll();
	}
}