package com.drivemedical.profile;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;

public class Info extends BaseTest {

	
	@Test(groups={"reg"},description="DMED-508 DRM-540,541")
	public void companyInfo() throws Exception
	{
		log.info("Log in to application");
		p.logIn(BaseTest.xmlTest);
		log.info("click on my account link");
		l1.getWebElement("Header_Reg_MyAct_Link", "//Shopnav//header.properties").click();
		log.info("Verify Company name");
		Properties p=new Properties();
		p.load(new FileInputStream(new File(System.getProperty("user.dir")+"//POM//Profile//AccountOverview.properties")));
		Set s=p.keySet();
		for(Object o:s) {
			String str=(String)o;
		sa.assertTrue(gVar.assertVisible(str, "//Profile//AccountOverview.properties"),str);
		}
		sa.assertAll();
	}

}


