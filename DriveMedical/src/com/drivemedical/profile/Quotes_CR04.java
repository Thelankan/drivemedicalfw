package com.drivemedical.profile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Quotes_CR04 extends BaseTest
{
	String quoteID;
	String reason;
	List<String> proName;
	List<String> proPrice;
	ArrayList<String> proMaterialId;
	String resultCount;
	int quoteCount;
	
	String quotesProperties = "Profile//Quotes.properties";
	
//	QuoteSubmissionOneProduct
	@Test(groups={"reg"}, description="CR04 DRM-1794")
	public void TC00_QuoteSubmissionOneProduct(XmlTest xmlTest) throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		Thread.sleep(3000);
		log.info("NAvigate to Payment and Billing page");
		String products = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToOrderReviewPage(products, 1, 1);
		log.info("Product Names:- "+ s.prodnames);
		log.info("Place Quote and verify the Navigation");
		placeQuoteAndVerifyTheNavigation();
		
		sa.assertAll();
	}
	
//	QuoteListingQuoteStatus
//	QuoetListingQuoteIDNavigation
	@Test(groups={"reg"}, description="CR04 DRM-1799 DRM-1800")
	public void TC01_QuoteListingQuoteStatus() throws Exception
	{
		String status = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 1);
		log.info("Verify the Status in Quote listing page");
		gVar.assertEqual(gVar.assertEqual("Quote_Status", quotesProperties), 
				status, "Verify the status");
		
		log.info("Click on Quote name and navigate to Quote details page");
		l1.getWebElement("Quote_Links", "Profile//Quotes.properties").click();
		log.info("Verify the Quote details page heading");
		String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 9)+" "+quoteID;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Heading", quotesProperties), 
				expected);
		log.info("verify quote status in Details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Status", quotesProperties), 
				status,"Verify the Status in Quote details page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="CR04 DRM-1798")
	public void TC02_QuoteListingPrint() throws Exception
	{
		log.info("Click on Back to Quotes in Quote details page");
		l1.getWebElement("QuoteDetails_Back_Link", quotesProperties).click();
		
		log.info("Verify the Quote listing page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quotes_Heading", quotesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 9),"Verify the heading");
		for (int i = 1; i < 4; i++)
		{
			log.info("LOOP:- "+i);
			WebElement documentSelectBox = l1.getWebElement("Quote_DocumentTypeSelectbox", quotesProperties);
			Select sel = new Select(documentSelectBox);
			sel.selectByIndex(i);
			
			log.info("Verify the New tab");
			Set<String> allWins = driver.getWindowHandles();
			if (allWins.size()>1)
			{
				Iterator<String> itr = allWins.iterator();
				String parentWin = itr.next();
				String childWin = itr.next();
				log.info("Switch the driver controller to child window");
				driver.switchTo().window(childWin);
				Thread.sleep(3000);
				if(gVar.ieBrowser()) 
				{
					gVar.overRideInIe();					
				} 
				
				String data = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", i, 6).toLowerCase();
				String url = driver.getCurrentUrl().toLowerCase();
				log.info("url:- "+ url);
				log.info("data:-"+ data);
				sa.assertTrue(url.contains(data),"Verify the Child window URL-LOOP:-"+i);
				
				log.info("Close the Child window");
				driver.close();
				
				log.info("Switch to parent window");
				driver.switchTo().window(parentWin);
			}
			else
			{
				sa.assertFalse(true,"New tab is not opening on selecting download document");
			}
		}
		
		sa.assertAll();
	}
	
//	QuoteSubmissionMultipleProducts
//	QuoteSubmissionNavigation
	@Test(groups={"reg"}, description="CR04 DRM-1797 DRM-1795")
	public void TC03_QuoteSubmissionMultipleProducts() throws Exception
	{
		log.info("Add a product to cart page");
		log.info("NAvigate to Payment and Billing page");
		String products = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 1, 3);
		checkout.navigateToOrderReviewPage(products, 2, 1);
		proName = s.prodnames;
		proPrice = s.prodPrice;
		proMaterialId = s.materialId;
		log.info("Product Names:- "+ proName);
		log.info("prod Price:- "+ proPrice);
		log.info("Product ID"+ proMaterialId);
		if (gVar.ieBrowser())
		{
			Thread.sleep(7000);
		}
		log.info("Place Quote and verify the Navigation");
		placeQuoteAndVerifyTheNavigation();
		
		log.info("Click on Quote name");
		l1.getWebElement("Quote_Links", "Profile//Quotes.properties").click();
		log.info("verify quote status in Details page");
		sa.assertEquals(gVar.assertEqual("QuoteDetails_Status", "Profile//Quotes.properties"),
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 1),"Quote status in details page");
		
		log.info("Verify the Quote code in Breadcrumb");
		String actualBreadCrumb = gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties");
		log.info("actualBreadCrumb:- "+ actualBreadCrumb);
		sa.assertTrue(actualBreadCrumb.contains(quoteID),"Verify the Quote number in breadcrumb");
		
		log.info("Verify the Product names Quote details page");
		List<WebElement> productNames = l1.getWebElements("QuoteDetails_ProductName", quotesProperties);
		List<WebElement> prices = l1.getWebElements("QuoteDetails_ProductPrice", quotesProperties);
		List<WebElement> productSkus = l1.getWebElements("QuoteDetails_Item_SKU", quotesProperties);
		ArrayList<String> productNamesList = new ArrayList<String>();
		ArrayList<String> productPriceList = new ArrayList<String>();
		ArrayList<String> productSkuList = new ArrayList<String>();
		for (int i = 0; i < productNames.size(); i++)
		{
			String proName = productNames.get(i).getText();
			productNamesList.add(proName);
			String price = prices.get(i).getText();
			price = price.substring(price.indexOf("$"),price.length());
			productPriceList.add(price);
			String sku = productSkus.get(i).getText().replace("#", "").trim();
			productSkuList.add(sku);
		}
		
		log.info("productNamesList in Quote details page:- "+ productNamesList);
		log.info("productPriceList Quote details page:-"+ productPriceList);
		log.info("productSkuList Quote details page:- "+productSkuList);
		
		log.info("Verify the Names");
		sa.assertEquals(proName, productNamesList,"Verify the name");
		sa.assertEquals(proPrice, productPriceList,"Verify the Price");
		sa.assertEquals(proMaterialId, productSkuList,"Verify the products SKU");
		
		log.info("Verify the Discription");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_DescriptionValue", quotesProperties), 
				reason,"VErify the reason");
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="CR04 DRM-1801 DMED-541 DRM-1176 OOTB-064 DRM-1147 DRM-1148")
	public void TC04_QuoteIntoOrderTransform() throws Exception
	{
		SimpleDateFormat sf = new SimpleDateFormat("MMM d, yyyy");
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 90);
        Date date = cal.getTime();
        String expExpireDate = sf.format(date);
        log.info("Expected ExpireDate:- "+ expExpireDate);
        
		log.info("Verify the Accept and Checkout button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_AcceptCheckout_Button", quotesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 10),"Verify the checkout button");
		
		log.info("Click on Accept and Checkout button");
		l1.getWebElement("QuoteDetails_AcceptCheckout_Button", quotesProperties).click();
		
		log.info("Verify the POPUP");
		sa.assertTrue(gVar.assertVisible("QuoteDetails_CheckoutPopup", quotesProperties),"Verify the popup");
		
		String heading = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 9);
		heading = heading+" "+quoteID+"?";
		sa.assertEquals(gVar.assertEqual("QuoteDetails_CheckoutPopup_Heading", quotesProperties), 
				heading,"Verify the popup heading");
		
		log.info("Verify the Quote ID");
		sa.assertEquals(l1.getWebElements("QuoteDetails_CheckoutPopupQuoteAndDiscription", quotesProperties).get(0).getText(), 
				"Quote "+quoteID,"Verify the Quote ID");
		
		sa.assertEquals(l1.getWebElements("QuoteDetails_CheckoutPopupQuoteAndDiscription", quotesProperties).get(1).getText(), 
				reason,"Verify Description");
		
		String expairyDetails = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 4, 2)+" "+expExpireDate+" "+"12:00 AM";
		sa.assertEquals(gVar.assertEqual("QuoteDetails_CheckoutPopupExpiryDetails", quotesProperties), 
				expairyDetails,"Verify expairyDetails");
		
		sa.assertTrue(gVar.assertVisible("QuoteDetails_CheckoutPopupNote", quotesProperties),"Verify the Note");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_CheckoutPopupYesButton", quotesProperties), "Yes","Yes button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_CheckoutPopupNoButton", quotesProperties), "No","No button");
		
		log.info("Click on YES button");
		l1.getWebElement("QuoteDetails_CheckoutPopupYesButton", quotesProperties).click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the checkout heading");
		
		log.info("Verify the PO number box");
		sa.assertEquals(gVar.assertEqual("PT_POBox_Textbox", "Checkout//PaymentType.properties","value"), "","PO number box should be empty");
		
		log.info("Enter PO number");
		String poNum = gVar.generateRandomNum()+"";
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNum);
		
		log.info("Click on Review Order number");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Check the Freight Checkbox");
		Thread.sleep(5000);
		gVar.waitForElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties", 5000);
		l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("Verify the Order Confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Thank You page");
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
		log.info("orderID:- "+ orderID);
		
		gVar.saveOrderId(orderID, "Products are added to cart from Quick order page and placed order");
		
		List<WebElement> proNameElements = l1.getWebElements("Orderconfirm_ProductNames", "Checkout//OrderConfirmation.properties");
		List<WebElement> proPriceElements;
		if (gVar.mobileView()||gVar.tabletView())
		{
			proPriceElements = l1.getWebElements("Orderconfirm_ProductPrice_Mobile", "Checkout//OrderConfirmation.properties");
		}
		else
		{
			proPriceElements = l1.getWebElements("Orderconfirm_ProductPrice", "Checkout//OrderConfirmation.properties");
		}
		log.info("proPriceElements size:- "+ proPriceElements.size());
		log.info("proNameElements.size():- "+ proNameElements.size());
		ArrayList<String> proNameListInOC = new ArrayList<String>();
		ArrayList<String> proPriceListInOC = new ArrayList<String>();
		for (int i = 0; i < proNameElements.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String proNameInOrderConfirmation = proNameElements.get(i).getText();
			proNameListInOC.add(proNameInOrderConfirmation);
			proPriceListInOC.add(proPriceElements.get(i).getText());
		}
		
		log.info("proNameListInOC:- "+ proNameListInOC);
		log.info("proPriceListInOC:- "+ proPriceListInOC);
		
		sa.assertEquals(proNameListInOC, proName,"Verify the Product names");
		sa.assertEquals(proPriceListInOC, proPrice,"Verify the Product Price");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="CR04-extraTc-Verify the status of the ordered quote")
	public void TC05_statusOfOrderedQuote() throws Exception
	{
		log.info("Navigate to Quotes page");
		if (gVar.ieBrowser())
		{
			Thread.sleep(5000);
		}
		s.navigateToQuotesPage();
		
		log.info("Verify the quote number");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"), 
				quoteID, "Verify the Quote ID");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Links", quotesProperties), "Quote "+quoteID,"Verify the Quote name");
		
		String status = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 4, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Status", quotesProperties), 
				status, "Verify the Quote status");
		
		log.info("Collect the count of the quotes");
		String result = l1.getWebElement("Quote_Pagination_Results_Top", quotesProperties).getText();
		resultCount = result.substring(result.lastIndexOf(" "), result.length()).trim();
		log.info("resultCount:- "+ resultCount);
		quoteCount = Integer.parseInt(resultCount)+1;
	
		log.info("Click on Quote name");
		l1.getWebElement("Quote_Links", quotesProperties).click();
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Status", quotesProperties), status,"Verify the status in Quote details page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-064 DRM-1141")
	public void TC06_verifyReQuoteFunctionality() throws Exception
	{
		log.info("Collect the price");
		String totalPriceInQuoteDetailsPage = l1.getWebElement("QuoteDetails_Order_Tot", quotesProperties).getText();
		log.info("totalPriceInQuoteDetailsPage:- "+ totalPriceInQuoteDetailsPage);
		List<WebElement> allProductNmesInDetailsPage = l1.getWebElements("QuoteDetails_ProductName", quotesProperties);
		log.info("allProductNmesInDetailsPage size:- "+ allProductNmesInDetailsPage.size());
		ArrayList<String> proNameListInQuoteDetailsPage = new ArrayList<String>();
		for (int i = 0; i < allProductNmesInDetailsPage.size(); i++)
		{
			log.info("LOOP:-"+ i);
			proNameListInQuoteDetailsPage.add(allProductNmesInDetailsPage.get(i).getText());
		}
		log.info("proNameListInQuoteDetailsPage:- "+ proNameListInQuoteDetailsPage);
		
		log.info("Verify the Re-quote button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_ReQuoterButton", quotesProperties).trim(), 
				"Requote","VErify the  Requote button");
		
		log.info("Click on Re-quote button");
		l1.getWebElement("QuoteDetails_ReQuoterButton", quotesProperties).click();
		Thread.sleep(2000);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_Continue_Cart_Button", quotesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 10),"verify Continue to Cart");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_CancelButton", quotesProperties), 
				"CANCEL","Verify CANCEL button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_SubmitButton", quotesProperties), 
				"SUBMIT","Verify SUBMIT button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_NewCart_Link", quotesProperties).trim(), 
				"New Cart","Verify New Cart link");
		String expectedLink =  "Quotes ("+quoteCount+")";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_Quotes_Link", quotesProperties).trim(), 
				expectedLink,"Verify Quote count");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_SectionTitle", quotesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 6, 2),"This cart is being quoted");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_Labels", quotesProperties,0).trim(), 
				"Status:","Verify the Status label");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_Labels", quotesProperties,1).trim(), 
				"Quote ID:","Verify the  Quote ID: label");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_Status", quotesProperties), 
				"draft","Verify the status");
		Thread.sleep(1000);
		String quoteId = l1.getWebElement("Requote_QuoteId", quotesProperties).getText();
		log.info("quoteId:- "+ quoteId);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_QuoteName_Textbox", quotesProperties,"value"), 
				"Quote "+quoteId,"Verify the quote name in textbox");
		String description="Testing Description field";
		l1.getWebElement("Requote_QuoteDescription_Textbox", quotesProperties).sendKeys(description);
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_EstimateLabel", quotesProperties).trim(), 
				"Previous Estimated Total","Verify Previous Estimated Total");
		sa.assertTrue(gVar.assertVisible("Requote_CartComments_ArrowDown", quotesProperties),"Verify the Cart Comments Arrow Down");

		log.info("Click on Cart Comments Arrow Down");
		l1.getWebElement("Requote_CartComments_ArrowDown", quotesProperties).click();
		sa.assertFalse(gVar.assertVisible("Requote_CartComments_ArrowDown", quotesProperties),"Cart Comments Arrow Down should not display");
		sa.assertTrue(gVar.assertVisible("Requote_CartComments_ArrowUp", quotesProperties),"Verify the Cart Comments Arrow UP");
		
		log.info("Click on ARROW UP");
		l1.getWebElement("Requote_CartComments_ArrowUp", quotesProperties).click();
		sa.assertTrue(gVar.assertVisible("Requote_CartComments_ArrowDown", quotesProperties),"Cart Comments Arrow Down should display");
		sa.assertFalse(gVar.assertVisible("Requote_CartComments_ArrowUp", quotesProperties),"Cart Comments Arrow UP should not display");
		
		log.info("Click on Cart Comments Arrow Down");
		l1.getWebElement("Requote_CartComments_ArrowDown", quotesProperties).click();
		Thread.sleep(1000);
		log.info("Verify the cart comment textbox");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_CartComments_Textbox", quotesProperties,"placeholder"), 
				"Add a comment and press enter","Verify the placeholder text");
		
		String message = "Testing message box";
		l1.getWebElement("Requote_CartComments_Textbox", quotesProperties).sendKeys(message);
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_ExportCSVLink", quotesProperties,0).trim(), 
				"Export CSV","Verify Export CSV");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_ExportCSVLink", quotesProperties,1).trim(), 
				"Export CSV","Verify Export CSV");
		
		//Fetch from quotes page
		sa.assertEquals(gVar.assertEqual("Requote_CartTopTotalAmount", quotesProperties), 
				totalPriceInQuoteDetailsPage,"Verify the total amount");
		
		log.info("Verify the Cart count");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_CartTopTotal", quotesProperties).trim(), 
				proNameListInQuoteDetailsPage.size()+" items"+" "+ totalPriceInQuoteDetailsPage,"Verify the cart count and amount");
		
		log.info("Verify the cart Total amount");
		sa.assertEquals(gVar.assertEqual("Requote_CartTotalAmount", quotesProperties), 
				totalPriceInQuoteDetailsPage,"Verify the total amount");
		
		List<WebElement> proNamesIdentifiers = l1.getWebElements("Requote_ProductNames", quotesProperties);
		log.info("proNames size:-"+ proNamesIdentifiers.size());
		ArrayList<String> productList = new ArrayList<String>();
		for (int i = 0; i < proNamesIdentifiers.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String name = proNamesIdentifiers.get(i).getText();
			productList.add(name);
		}

		log.info("Product names in Requote page:-"+ productList);
		log.info("Verify the product names");
		sa.assertEquals(productList, proNameListInQuoteDetailsPage,"Compare the product names");//Compare the product names in Requote page with the product name in quote details page
		
		log.info("Click on Submit button");
		l1.getWebElement("Requote_SubmitButton", quotesProperties).click();
		Thread.sleep(6000);
		log.info("Verify the quote ID and description");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_QuoteName", quotesProperties), 
				"Quote "+quoteId, "Verify the Quote name");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_QuoteId_InPopup", quotesProperties), 
				quoteId,"Verify the Quote ID");
		String expectedTitle = "Confirm Requested Quote "+quoteId+"?";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_SubmitModalTitle", quotesProperties), 
				expectedTitle,"Verify the Submit Module Title");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Requote_Description_Label", quotesProperties), 
				description,"Verify the description in Quote submition popup");
		
		log.info("Click on YES button");
		l1.getWebElement("Requote_YesButton", quotesProperties).click();
		
		log.info("Verify the Quote listing page");
		gVar.assertEqual(gVar.assertEqual("Quotes_Heading", quotesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 9),"Verify the page Heading");
		log.info("Verify the success message");
		String closeButton = l1.getWebElement("Quote_SuccessMessage_CloseButton", "Profile//Quotes.properties").getText();
		String successMessage = gVar.assertEqual("Quote_SuccessMessage", "Profile//Quotes.properties").replace(closeButton, "").trim();
		gVar.assertequalsIgnoreCase(successMessage, 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 5, 2),"verify the success message");
		
		String status = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 1);
		log.info("Verify the Status in Quote listing page");
		gVar.assertEqual(gVar.assertEqual("Quote_Status", quotesProperties), 
				status, "Verify the status");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Links", "Profile//Quotes.properties"), 
				"Quote "+quoteID,"Verify the Quote name");
		log.info("Click on Quote name and navigate to Quote details page");
		l1.getWebElement("Quote_Links", "Profile//Quotes.properties").click();
		log.info("Verify the Quote details page heading");
		String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 9)+" "+quoteID;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Heading", quotesProperties), 
				expected);
		log.info("verify quote status in Details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Status", quotesProperties), 
				status,"Verify the Status in Quote details page");
		
		log.info("Verify the Discription");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_DescriptionValue", quotesProperties), 
				description,"VErify the reason");
		
		sa.assertAll();
	}
	
	
//	+ ---------- + ---------- + ---------- + ---------- + ---------- + ---------- +
//	+ ---------- + ---------- + ---------- + ---------- + ---------- + ---------- +
	
	void placeQuoteAndVerifyTheNavigation() throws Exception
	{
		log.info("click on quote a request button");
		if (gVar.ieBrowser())
		{
			Thread.sleep(7000);
		}
		l1.getWebElement("OrderReview_RequestQuote_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		
		log.info("VErify the Status of the Quote");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("CreateQuotePopup_QuoteStatus", "Checkout//OrderReview.properties"), "Draft", "Verify the status");
		
		log.info("Collect the prepopulated QuoteId");
		String quoteNameInCheckout = l1.getWebElement("CreateQuotePopup_QuoteId", "Checkout//OrderReview.properties").getAttribute("value");
		String quoteName= quoteNameInCheckout.split(" ")[1].trim();
		log.info("quoteName:- "+ quoteName);
		
		log.info("enter reason");
		reason = "Reason Entered";
		l1.getWebElement("Quote_Reason_Textbox", "Checkout//OrderReview.properties").sendKeys(reason);
		log.info("click on submit");
		if (gVar.ieBrowser())
		{
			Thread.sleep(5000);
		}
		l1.getWebElement("Quote_Submit_Btn", "Checkout//OrderReview.properties").click();
		log.info("fetch quote code");
		quoteID = l1.getWebElement("Quote_Id", "Checkout//OrderReview.properties").getText().replace("#", "").trim();
		log.info("quoteID:- "+ quoteID);
		
		sa.assertEquals(quoteName, quoteID, "Verify the Quote id value in both the popups");
		log.info("click yes on quote confirmation pop up");
		if (gVar.ieBrowser())
		{
			Thread.sleep(5000);
		}
		l1.getWebElement("Quote_Yes_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify navigation to quotes page");
		sa.assertTrue(gVar.assertVisible("Quotes_Heading", "Profile//Quotes.properties"),"Quotes heading");
		
		log.info("Verify recently submitted quotes code");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"), 
				quoteID, "Verify the Quote ID");
		
		log.info("Verify recently submitted quotes status");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Status", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 1), "Verify the Quote status");
	}
}
