package com.drivemedical.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;









//import com.drivemedical.projectspec.GenericFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.Data;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.TestData;

public class PersonalDetails extends BaseTest{
	
	
	private static final String String = null;
	String ErrorMessageColor = "rgb(208, 2, 27)";
	String ErrorTextboxBackgroundColor = "#fec3c3";
	String ErrorTextboxBorderColor = "#fd7b7b";
	static int cnt;
	
	String fNameUpdt;
	String lNameUpdt;
	String email;
	String phNum;
	String updatePassword;
	String phNumUpdated;
	String emailIdOriginal;
	String pwdOriginal;

	@Test(groups={"reg"},description="OOTB-059 DRM-632 DRM-636")
	public void TC00_PD_Update_Test_1(XmlTest xmlTest) throws Exception
	{
		log.info("navigate to Sign In page");
		p.navigateToLoginPage();
		log.info("log in to the application");
		p.logIn(xmlTest);
		
		log.info("Navigate to Personal details page");
		s.navigateToPersonalDetailsPAge();
		log.info("Verify the prepoluted data of First name and Email ID");
		String fnExpected = l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").getAttribute("value");
		System.out.println("fnExpected:-  "+ fnExpected);
		String emilExpected = l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value");
		System.out.println("emilExpected:-  "+ emilExpected);
		sa.assertEquals(fnExpected,"Mohan","Verify prepopulated FIRST NAME");	//Verify 
		sa.assertEquals(emilExpected,xmlTest.getParameter("email"), "Verify prepopulated EMAIL ID");
		
		log.info("Clear the details");
		clearPD_Details();
		
		fNameUpdt = GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 1, 1);
		lNameUpdt = GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 2, 1);
		email = GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 3, 1);
		phNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 4, 1);
		phNumUpdated = GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 4, 2);
		log.info("Enter User details and Click on Update button");
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(fNameUpdt);
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(lNameUpdt);
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys(email);
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys(phNum);
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
		Thread.sleep(2000);
		
		log.info("Pgae should reload and update the details");
		sa.assertTrue(gVar.assertVisible("PD_Heading", "Profile//PersonalDetails.properties"),"Verify the heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PD_Heading", "Profile//PersonalDetails.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 1, 3),"Verify the heading text");
		log.info("Verify the pre-poluted data");
		sa.assertEquals(fNameUpdt, l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify First name");
		sa.assertEquals(lNameUpdt, l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify Last name");
		sa.assertEquals(email, l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify Email Id");
		sa.assertEquals(phNumUpdated, l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify the Phone number");
		
		if (gVar.mobileView()|| gVar.tabletView())
		{
//			sa.assertFalse(gVar.assertVisible("PD_ShopperName", "Profile//PersonalDetails.properties"),"Username heading should not display in mobile view");
		}
		else
		{
			sa.assertEquals(l1.getWebElement("PD_ShopperName", "Profile//PersonalDetails.properties").getText(), 
					"Hi,"+fNameUpdt+" "+lNameUpdt,"Verify the heading");
		}
		
//		############### Change Password ############### 
		
		log.info("Clikc on Change Password link");
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
		
		log.info("Verify the password expanded section");
		sa.assertTrue(gVar.assertVisible("PD_KeepPwd_Link", "Profile//PersonalDetails.properties"),"Verify PD_KeepPwd_Link");
		sa.assertTrue(gVar.assertVisible("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties"),"Verify PD_CurrentPwd_Textbox");
		sa.assertTrue(gVar.assertVisible("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties"),"Verify PD_NewPwd_Textbox");
		sa.assertTrue(gVar.assertVisible("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties"),"Verify PD_ConfirmPwd_Textbox");
		log.info("Update Password");
		pwdOriginal = xmlTest.getParameter("password");
		updatePassword = GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 6, 1);
		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);
		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(updatePassword);
		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(updatePassword);
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();	//Click on Update button
		
		log.info("After Updating, password section should collapse and user should be in same page");
		sa.assertTrue(gVar.assertVisible("PD_ChangePWD_Link", "Profile//PersonalDetails.properties"),"Verify PD_ChangePWD_Link");
		sa.assertTrue(gVar.assertVisible("PD_Heading", "Profile//PersonalDetails.properties"),"Verify heading");
		log.info("Logout from the application");
		p.logout(xmlTest);
	
		log.info("Navigate to login page");
		p.navigateToLoginPage();
		log.info("Login with old credentials");
		p.logIn(xmlTest);
		
		log.info("Verify the Error message for Old credentials");
		sa.assertTrue(gVar.assertVisible("Login_ErrorMsg_Text", "Profile//login.properties"),"Verify the Login_ErrorMsg_Text");
		
		log.info("Login with updated password");
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").clear();
		emailIdOriginal= xmlTest.getParameter("email");
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys(emailIdOriginal);
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys(updatePassword);
		System.out.println("Clicked on LOGIN button");
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		

		log.info("Navigate to contact details");
		s.navigateToPersonalDetailsPAge();
		
		log.info("Verify the pre-poluted data");
		sa.assertEquals(fNameUpdt, l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify fNameUpdt");
		sa.assertEquals(lNameUpdt, l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify lNameUpdt");
		sa.assertEquals(email, l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify email");
		sa.assertEquals(phNumUpdated, l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"),"Verify phNum");
		
		log.info("Reset the details");
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
		clearPD_Details();
		fNameUpdt = "Mohan";
		lNameUpdt = "Pattar";
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(fNameUpdt);
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(lNameUpdt);
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys(emailIdOriginal);
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys("9999999999");
		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(updatePassword);	
		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);
		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
		
		log.info("Verify the updated Name in Contact Ditails page");
		if (gVar.mobileView()|| gVar.tabletView())
		{
//			sa.assertFalse(gVar.assertVisible("PD_ShopperName", "Profile//PersonalDetails.properties"),"Username heading should not display in mobile view");
		}
		else
		{
			sa.assertEquals(l1.getWebElement("PD_ShopperName", "Profile//PersonalDetails.properties").getText(), 
					"Hi,"+fNameUpdt+" "+lNameUpdt,"Verify the heading");
		}
		sa.assertAll();
	}

	
	@Test(groups={"reg"},description="OOTB-059 DRM-634 AND DRM-635")
	public void TC01_PD_Update_Test_2(XmlTest xmlTest) throws Exception
	{
		sa.assertFalse(true,"Discuss Testcase with Arjun...Test manually.");
//		log.info("CLear the details");
//		clearPD_Details();
//		
//		log.info("Update the User Details");
//		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(fNameUpdt);
//		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(lNameUpdt);
//		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys(email);
//		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys(phNum);
//		
//		log.info("Clikc on Change Password link");
//		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
//		
//		log.info("Update the password");
//		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);	
//		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(updatePassword);
//		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(updatePassword);
//		log.info("Click on Update button");
//		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
//		
//		log.info("Logout from the application");
//		p.logout(xmlTest);
//		
//		log.info("Change the locail to CA");
//		WebElement languageDD_Element;
//		if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile"))
//		{
//			log.info("Click on Hamburgar menu");
//			BaseTest.l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
//			languageDD_Element = l1.getWebElement("Header_Lang_DD_Mobile", "Shopnav//header.properties");
//			
//		}
//		else 
//		{
//			languageDD_Element = l1.getWebElement("Header_Lang_DD_Mobile", "Shopnav//header.properties");
//		}
//		Select sel = new Select(languageDD_Element);
//		log.info("selecting the selected language");
//		sel.selectByValue("CA-EN");
//		Thread.sleep(3000);
//		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
//		{
//			BaseTest.log.info("Check whether the hamburgar is opened or not");
//			if (BaseTest.gVar.assertVisible("Hamburger_Expanded", "Shopnav\\header.properties"))
//			{
//				BaseTest.log.info("Click on Close icon in hamburgar menu");
//				BaseTest.l1.getWebElement("Hamburger_Close_Button", "Shopnav\\header.properties").click();			
//			}
//		}
//		
//		log.info("Login with updated credentials");
//		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
//		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys(emailIdOriginal);
//		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys(updatePassword);
//		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
//		
//		log.info("User should not be logged-in with updated credentials");
//		sa.assertTrue(gVar.assertVisible("Login_ErrorMsg_Text", "Profile//login.properties"),"Verify Login_ErrorMsg_Text");
//		
//		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").clear();
//		log.info("Login to the application");
//		p.logIn(xmlTest);
//		
//		log.info("Navigate to Personal Detail page");
//		s.navigateToPersonalDetailsPAge();
//		
//		log.info("User Should be in Personal data page and details should not be updated");
//		sa.assertTrue(l1.getWebElement("PD_Heading", "Profile//PersonalDetails.properties").isDisplayed());	
//		sa.assertEquals("Aditya", l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
//		sa.assertEquals("Reddy", l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
//		sa.assertEquals("9999999999", l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
//		
//		log.info("Logout from the application");
//		p.logout(xmlTest);
//		
//		log.info("Change Country to US");
//		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
//		l1.getWebElement("Header_UsEng_Link", "Shopnav//header.properties").click();
//		
//		log.info("Login to the US application and reset the datails");
//		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
//		p.logIn(xmlTest);
//		
//		log.info("Navigate to Personal details page");
//		s.navigateToPersonalDetailsPAge();
//		
//		log.info("Reset the details");
//		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
//		clearPD_Details();
//		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys("Mohan");
//		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys("Pattar");
//		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("mohan@gmail.com");
//		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys("9999999999");
//		
//		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");	
//		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
//		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
//		
//		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();

		sa.assertAll();
	}

	
	
@Test(groups={"reg"},description="OOTB-059 DRM-638")
	public void TC02_PD_Update_Test_4(XmlTest xmlTest) throws Exception
	{
//		//Pre Condition:- Two account should be created with SAME EMAIL ID
//		p.logout(xmlTest);
//		log.info("Login with email id Which is assosiated with multiple account");
//		p.navigateToLoginPage();
//		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys("mohan@gmail.com");
//		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys("Mohan@1234");
//		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
//		
//		
//		log.info("Navigate to Personal detail page");
//		s.navigateToPersonalDetailsPAge();
//		
//		log.info("Update the Email ID");
//		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("mohan@gmail.com");
//		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
//		
//		log.info("Logout from the application");
//		p.logout(xmlTest);
//		
//		log.info("Login to anathor account which is having same email ID");
//		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
//		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys("mohan@gmail.com");
//		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys("Aditya@2017");
//		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
//		
//		log.info("Navigate to Personal detail page");
//		s.navigateToPersonalDetailsPAge();
//		
//		log.info("Verify the Email Id");
//		sa.assertEquals("mohan@gmail.com", l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
//		
//		log.info("Logout from the application");
//		p.logout(xmlTest);
		
		sa.assertAll();
	}
	

	
@Test(groups="reg",dataProvider="personalDetailsVal",dataProviderClass=Data.class, description="OOTB-059 DRM-633 DRM-637")
public void TC03_PD_val(TestData t) throws Exception
{

	log.info("Clear the details");
	clearPD_Details();
	
	log.info("enter validations values");
	l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(t.get(0));
	l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(1));
	l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(2));
	l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(3));
	log.info("Click on Update Button");
	l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
	Thread.sleep(5000);
	log.info("Verify the message for the iteration  cnt :- " + cnt);
	if(cnt==0)	//For Blank fields
	{
		sa.assertTrue(gVar.assertVisible("PD_FN_Error_message", "Profile//PersonalDetails.properties"),"Verify PD_FN_Error_message");
		sa.assertTrue(gVar.assertVisible("PD_LN_Error_message", "Profile//PersonalDetails.properties"),"Verify PD_LN_Error_message");
		sa.assertTrue(gVar.assertVisible("PD_Email_Error_message", "Profile//PersonalDetails.properties"),"Verify PD_Email_Error_message");
		sa.assertTrue(gVar.assertVisible("PD_Phone_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Phone_Error_message");
		
	}
	else if(cnt==1)	// invalid password AND invalid Phone number AND blank LName
	{
		sa.assertTrue(gVar.assertVisible("PD_LN_Error_message", "Profile//PersonalDetails.properties"),"Verify PD_LN_Error_message");
		sa.assertTrue(gVar.assertVisible("PD_Email_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Email_Error_message");
		sa.assertTrue(gVar.assertVisible("PD_Phone_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Phone_Error_message");
	}
	
	else if(cnt==2)	// invalid Email Id AND blank FName
	{
		sa.assertTrue(gVar.assertVisible("PD_FN_Error_message", "Profile//PersonalDetails.properties"),"Verify PD_FN_Error_message");
		
		sa.assertTrue(gVar.assertVisible("PD_Email_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Email_Error_message");
		
		sa.assertTrue(gVar.assertVisible("PD_Phone_Error_message", "Profile//PersonalDetails.properties"),"");
	}
	
	else if(cnt==3)	// For invalid Email  AND invalid Phone number
	{
		sa.assertTrue(gVar.assertVisible("PD_Email_Error_message", "Profile//PersonalDetails.properties"),"Verify PD_Email_Error_message");
		sa.assertTrue(gVar.assertVisible("PD_Phone_less_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Phone_less_Error_message");
	}
	else if(cnt==4)	// For Alphanumeric phone number
	{
		sa.assertTrue(gVar.assertVisible("PD_Phone_dig_Error_message", "Profile//PersonalDetails.properties"),"");
	}
	else if (cnt==5 && cnt==6) //For Existing email id and Valid data, error message should not display
	{
		try
		{
			sa.assertTrue(gVar.assertVisible("PD_FN_Error_message", "Profile//PersonalDetails.properties"),"verify PD_FN_Error_message");
			sa.assertTrue(gVar.assertVisible("PD_LN_Error_message", "Profile//PersonalDetails.properties"),"verify PD_LN_Error_message");
			sa.assertTrue(gVar.assertVisible("PD_Email_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Email_Error_message");
			sa.assertTrue(gVar.assertVisible("PD_Phone_Error_message", "Profile//PersonalDetails.properties"),"verify PD_Phone_Error_message");
		} 
		catch (Exception e)
		{
			sa.assertTrue(true);
		}
	}
	
	cnt++;
	sa.assertAll();
}

int cnt1 = 0;
@Test(groups="reg",dataProvider="personalDetailsPasswordVal",dataProviderClass=Data.class, description="OOTB-059 DRM-633 DRM-637")
public void TC04_PD_val_Password(TestData t) throws Exception
{
	try 
	{
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
	} 
	catch (Exception e) 
	{
		sa.assertTrue(true);
	}

	log.info("Enter passwords in the fields");
	
	l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").clear();
	l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").clear();
	l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").clear();
	log.info("t.get(0):- "+ t.get(0));
	log.info("t.get(1):- "+ t.get(1));
	log.info("t.get(2):- "+ t.get(2));
	l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(0));
	l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(1));
	l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(2));
	
	log.info("Click on Update Button");
	l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();

	Thread.sleep(3000);
	log.info("Verify the message for the iteration  cnt1 :- " +cnt1);
	if(cnt1==0)	//For Blank fields
	{
		sa.assertTrue(gVar.assertVisible("PD_Current_PassEM", "Profile//PersonalDetails.properties"),"verify PD_Current_PassEM");
		sa.assertTrue(gVar.assertVisible("PD_New_PassEM", "Profile//PersonalDetails.properties"),"verify PD_New_PassEM");
		sa.assertTrue(gVar.assertVisible("PD_Confm_PassEM", "Profile//PersonalDetails.properties"),"verify PD_Confm_PassEM");
		
	}
	else if(cnt1==1 && cnt1==2 && cnt1==3)	// invalid passwords 
	{
		sa.assertTrue(gVar.assertVisible("PD_Invalid_PassEM", "Profile//PersonalDetails.properties"),"Verify PD_Invalid_PassEM");
		sa.assertTrue(gVar.assertVisible("PD_Invalid_PassEM", "Profile//PersonalDetails.properties"),"Verify PD_Invalid_PassEM");
		sa.assertTrue(gVar.assertVisible("PD_Invalid_PassEM", "Profile//PersonalDetails.properties"),"Verify PD_Invalid_PassEM");
	}
	
	else if(cnt1==4 && cnt1==5)	// For Less chars
	{
		sa.assertTrue(gVar.assertVisible("PD_Invalid_Current_PassEM", "Profile//PersonalDetails.properties"),"Verify PD_Invalid_Current_PassEM");
		
	}
	
	else if(cnt1==6)	// For invalid password
	{
		sa.assertTrue(gVar.assertVisible("PD_Invalid_ConfPsdEM", "Profile//PersonalDetails.properties"),"Verify PD_Invalid_ConfPsdEM");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PD_Invalid_ConfPsdEM", "Profile//PersonalDetails.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "PersonalDetails", 1, 5),"Verify the Error message");
		
	}

	cnt1++;
	
	sa.assertAll();
}

@Test(groups={"reg"}, description="OOTB-059 DRM-630")
public void TC05_resetCurrentPasswordAsNewPassword() throws Exception
{
	log.info("Navigate to Personal details page");
	s.navigateToPersonalDetailsPAge();
	
	log.info("Clikc on Change Password link");
	l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
	
	log.info("Verify the password expanded section");
	sa.assertTrue(gVar.assertVisible("PD_KeepPwd_Link", "Profile//PersonalDetails.properties"),"Verify PD_KeepPwd_Link");
	sa.assertTrue(gVar.assertVisible("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties"),"Verify PD_CurrentPwd_Textbox");
	sa.assertTrue(gVar.assertVisible("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties"),"Verify PD_NewPwd_Textbox");
	sa.assertTrue(gVar.assertVisible("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties"),"Verify PD_ConfirmPwd_Textbox");
	log.info("Update Password");
	pwdOriginal = xmlTest.getParameter("password");
	l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);
	l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);
	l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(pwdOriginal);
	log.info("Click on Update button");
	l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
	
	log.info("After Updating, password section should collapse and user should be in same page");
	sa.assertTrue(gVar.assertVisible("PD_ChangePWD_Link", "Profile//PersonalDetails.properties"),"Verify PD_ChangePWD_Link");
	sa.assertTrue(gVar.assertVisible("PD_Heading", "Profile//PersonalDetails.properties"),"Verify heading");
	
	sa.assertEquals(gVar.assertEqual("PD_AlertMessage", "Profile//PersonalDetails.properties"), 
			"Your profile has been updated", "Verify the alert message");
	sa.assertTrue(gVar.assertVisible("PD_AlertMessage_CloseButton", "Profile//PersonalDetails.properties"),"Verify the close button");
	
	sa.assertAll();
	
}

	public void clearPD_Details() throws Exception
	{
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").clear();
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").clear();;
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").clear();
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").clear();
	}
}
