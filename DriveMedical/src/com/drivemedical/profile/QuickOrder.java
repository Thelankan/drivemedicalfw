package com.drivemedical.profile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;



//DRM-838,841 - can not be automated.

public class QuickOrder extends BaseTest{

	@Test(groups={"reg"},description="DMED-521 DRM-829")
	public void TC00_meterialNumFun(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Clear the cart item");
		cart.clearCart();
		
		log.info("Navigate to Quick Order page");
		if (gVar.mobileView()|| gVar.tabletView())
		{
			log.info("MObile view");
			String homePageUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
			String quickOrderUrl = homePageUrl+"quickOrder";
			driver.get(quickOrderUrl);
		}
		else
		{
			log.info("click on quick order link from header");
			l1.getWebElement("Header_Reg_QuickOrder_Link","Shopnav//header.properties").click();
		}
		List<WebElement> meterialNum;
		for(int i=0;i<25;i++) 
		{
			log.info("get meterial number text boxes");
			meterialNum=l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
			log.info("Enter meterial number");
			meterialNum.get(i).sendKeys(GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", i+1, 3));
			Thread.sleep(2000);
		}
		log.info("fetch total meterial boxes");
		meterialNum=l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
		sa.assertEquals(meterialNum.size(),25 ,"Verify the total material number box");
		log.info("click on clear form");
		l1.getWebElement("QuickOrder_ClearForm_Top", "Profile//QuickOrder.properties").click();
		Thread.sleep(3000);
		
		log.info("After clearing the form, Material Number count should be three");
		meterialNum=l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
		sa.assertEquals(meterialNum.size(), 3,"Verify the material textbox count after clearing the form");
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="DMED-521 DRM-830,836")
	public void TC01_validAndInvalidMeterialNumbers() throws Exception
	{
		log.info("Enter valid and invalid meterial numbers");
		for(int i=0;i<5;i++) 
		{
			log.info("LOOP:- "+ i);
			log.info("Enter meterial numbers");
			int j=i+1;
			l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(i).sendKeys(GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", j, 0));
			l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(Keys.TAB);
			Thread.sleep(5000);
			if(i==0) 
			{
			sa.assertTrue(gVar.assertVisible("QuickOrder_StockDetails", "Profile//QuickOrder.properties"),"Displayed product details");
			log.info("verify price");
			sa.assertTrue(gVar.assertVisible("QuickOrder_ProductPrice", "Profile//QuickOrder.properties",i),"verify price");
			
			} 
			else if(i==1) 
			{
				sa.assertTrue(gVar.assertVisible("QuickOrder_StockDetails", "Profile//QuickOrder.properties",i),"Displayed product details");
				sa.assertEquals(j, l1.getWebElements("QuickOrder_StockDetails", "Profile//QuickOrder.properties").size(),"total displayed products");
				log.info("verify price");
				sa.assertTrue(gVar.assertVisible("QuickOrder_ProductPrice", "Profile//QuickOrder.properties",i),"verify price");
			} 
			else 
			{
				log.info("Verify Error message");
				sa.assertTrue(gVar.assertVisible("QuickOrder_Error_Field", "Profile//QuickOrder.properties",i),"Error messages");
				String errorMsg = gVar.assertEqual("QuickOrder_Error_Field", "Profile//QuickOrder.properties",i);
				log.info("errorMsg:- "+ errorMsg);
				gVar.assertequalsIgnoreCase(errorMsg, GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", j, 1),"verify the error message");
			}
			sa.assertAll();
		}
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-837")
	public void TC02_qytUpdate() throws Exception
	{
		log.info("verify quantity update");
		List<WebElement> priceList = l1.getWebElements("QuickOrder_ProductPrice", "Profile//QuickOrder.properties");
		List<WebElement> quantityBox = l1.getWebElements("QuickOrder_Qty","Profile//QuickOrder.properties");
		 List<WebElement> totalAmount = l1.getWebElements("QuickOrder_TotalItemPrice", "Profile//QuickOrder.properties");
		 log.info("Verify for two products");
		for(int i=0;i<2;i++) 
		{
			 log.info("LOOP:- " +i);
			 String actPrice=priceList.get(i).getText().replace("$", "");
			 log.info("actPrice:- "+actPrice);
			
			//Update the quantity
			quantityBox.get(i).clear();
			quantityBox.get(i).sendKeys("2");
			quantityBox.get(i).sendKeys(Keys.TAB);
			Thread.sleep(1000);
			
			//Conver the String value to double
			double actpriceVal = Double.parseDouble(actPrice);
			log.info("actpriceVal:- "+ actpriceVal);
			DecimalFormat df = new DecimalFormat("##.00");
			 
			actpriceVal=actpriceVal*2;
			log.info("actpriceVal:- "+ actpriceVal);
			String expTotPrice=totalAmount.get(i).getText().replace("$", "").replace(",", "");
			log.info("verify price after quantity update");
			sa.assertEquals(expTotPrice,df.format(actpriceVal),"Verify the updated Total price");
			
		}
		sa.assertAll();
	}
	@Test(groups={"reg"},description="DMED-521 DRM-831,832")
	public void TC03_UOMCheck() throws Exception
	{
		log.info("click on clear form");
		l1.getWebElement("QuickOrder_ClearForm_LinkBottom", "Profile//QuickOrder.properties").click();
		Thread.sleep(3000);
		List<WebElement> meterialNum = l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
		log.info("After clearing the form, Material Number count should be three");
		meterialNum=l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
		sa.assertEquals(meterialNum.size(), 3,"Verify the material textbox count after clearing the form");
		
		
		log.info("Enter the product id in material number which is configured with contract price");
		String ProductId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 4, 1);
		l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(ProductId);
		l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(Keys.TAB);
		Thread.sleep(2000);
		sa.assertTrue(gVar.assertVisible("QuickOrder_Unit_Select", "Profile//QuickOrder.properties"),"UOM drop down");
		log.info("verify default UOM measurement");
		String UOM=new Select(l1.getWebElement("QuickOrder_Unit_DD", "Profile//QuickOrder.properties")).getFirstSelectedOption().getText();
		sa.assertEquals(UOM,GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", 2, 2),"Verify the UOM");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-833")
	public void TC04_removeFun() throws Exception
	{
		for(int i=0;i<2;i++) 
		{
			log.info("fetch total displayed products");
			int totProds=l1.getWebElements("QuickOrder_StockDetails", "Profile//QuickOrder.properties").size();
			log.info("click on remove link");
			l1.getWebElements("QuickOrder_Remove", "Profile//QuickOrder.properties").get(i).click();
			if(i==0) 
			{
			log.info("verify total products");
			sa.assertEquals(totProds-1,l1.getWebElements("QuickOrder_StockDetails", "Profile//QuickOrder.properties").size());
			} else if(i==1) {
				log.info("verify products should not visible");
				sa.assertTrue(gVar.assertNotVisible("QuickOrder_StockDetails", "Profile//QuickOrder.properties"),"Quick order products");
				break;
			}
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-839,840")
	public void TC05_addToCart() throws Exception
	{
		log.info("click on clear form");
		l1.getWebElement("QuickOrder_ClearForm_LinkBottom", "Profile//QuickOrder.properties").click();
		
		log.info("Enter the product id in material number which can be added to cart");
		String ProductId = GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", 2, 0);
		l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(ProductId);
		l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(Keys.TAB);
		Thread.sleep(3000);
		log.info("Collect the product name, id and price");
		String proNAmeInQuickOrder = gVar.assertEqual("QuickOrder_PeoductsName", "Profile//QuickOrder.properties");
		String quantity = l1.getWebElement("QuickOrder_Qty","Profile//QuickOrder.properties").getAttribute("value");
		String totalAmount = l1.getWebElement("QuickOrder_TotalItemPrice", "Profile//QuickOrder.properties").getText();
		log.info("proNAmeInQuickOrder:- "+ proNAmeInQuickOrder);
		log.info("quantityBox:- "+ quantity);
		log.info("totalAmount:-"+totalAmount);
		log.info("Click on ADD TO CART button");
		l1.getWebElement("QuickOrder_AddToCart_Top","Profile//QuickOrder.properties").click();
		
		log.info("User should navigate to cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		log.info("Verify the product name");
		sa.assertEquals(gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"), 
				proNAmeInQuickOrder,"Verify the product name");
		
		sa.assertEquals(gVar.assertEqual("Cart_Prod_ID", "Cart//Cart.properties"), ProductId,"Verify the product ID");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox", "Cart//Cart.properties","value"), quantity,"Verify the quantity");
		sa.assertEquals(gVar.assertEqual("Cart_ItemTotal", "Cart//Cart.properties"), totalAmount,"Verify the total amount");
		
		
//		PLACE ORDER scenario covered in LAST testcase
		
//		log.info("Click on PROCEED TO CHECKOUT button IN Cart page");
//		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
//		Thread.sleep(2000);
//		log.info("Verify the Checkout page");
//		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
//		
//		log.info("Click on radio button");
//		l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
//		
//		log.info("Click on SELECT pickup address CONTINUE button");
//		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
//		
//		log.info("Select the shipping method and click on CONTINUE button in SHIPPING METHOD SECTION");
//		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
//		shippingMethodRadioButton.get(0).click();
//		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
//		
//		
//		log.info("Enter PO number");
//		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
//		
//		log.info("Enter PO number");
//		int randomNumber = gVar.generateRandomNum();
//		String poNumber = String.valueOf(randomNumber);
//		log.info("poNumber:- "+ poNumber);
//		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
//		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
//		
//		log.info("Click on CONTINUE button in payment section");
//		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
//		
//		log.info("Check the Freight Checkbox");
//		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
//		{
//			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
//		}
//		log.info("Click on PLACE ORDER button");
//		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
//
//		log.info("Verify the THANK YOU page");
//		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
//		
//		log.info("Collect the Order ID");
//		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
//		log.info("orderID:- "+ orderID);
		
		log.info("Clear the cart");
		cart.clearCart();
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-834,835")
	public void TC06_validate() throws Exception
	{
		
		ArrayList<String> productPriceList = new ArrayList<String>();
		
		log.info("Navigate to PDP where multiple UOM is configured for a product");
		String proId = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 8, 1);
		s.pdpNavigationThroughURL(proId);
		log.info("Collect the Product name");
		String proNAmeInPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		log.info("Collect the Product price for different UOM options");
		WebElement selectBoxElement = l1.getWebElement("PDP_UOM_SelectBox", "Shopnav//PDP.properties");
		Select sel = new Select(selectBoxElement);
		int totalOptions = sel.getOptions().size();
		if (totalOptions<2)
		{
			sa.assertFalse(true,"There are no Multiple UOMs for the product. Change the product which is having more Multiple UOMs");
		}
		for (int i = 0; i < totalOptions; i++)
		{
			log.info("LOOP:- " + i);
			sel = new Select(selectBoxElement);
			log.info("Select the option");
			sel.selectByIndex(i);
			Thread.sleep(3000);
			
			log.info("Collect the price for respective UOM");
			String proPrize = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
			productPriceList.add(proPrize);
			
		}
		log.info("productPriceList:- "+ productPriceList);
		log.info("Navigate to Quick Order page");
		if (gVar.mobileView()|| gVar.tabletView())
		{
			log.info("MObile view");
			String homePageUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
			String quickOrderUrl = homePageUrl+"quickOrder";
			driver.get(quickOrderUrl);
		}
		else
		{
			log.info("click on quick order link from header");
			l1.getWebElement("Header_Reg_QuickOrder_Link","Shopnav//header.properties").click();
		}
		log.info("Enter SKU value");
		l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(0).sendKeys(proId);
		l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(0).sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		
		log.info("Verify the product name");
		sa.assertEquals(gVar.assertEqual("QuickOrder_PeoductsName", "Profile//QuickOrder.properties"), 
				proNAmeInPDP,"Verify the product name");
		log.info("Verify the UOM select box");
		sa.assertTrue(gVar.assertVisible("QuickOrder_Unit_DD", "Profile//QuickOrder.properties"),"Verify the select box");
		WebElement uomSelectBox = l1.getWebElement("QuickOrder_Unit_DD", "Profile//QuickOrder.properties");
		Select uomDD = new Select(uomSelectBox);
		int uomOptions = uomDD.getOptions().size();
		for (int i = 0; i < uomOptions; i++)
		{
			log.info("LOOP:- "+i);
			uomDD = new Select(uomSelectBox);
			uomDD.selectByIndex(i);
			Thread.sleep(3000);
			
			log.info("Verify the price for different UOM");
			String proPriceInQuickOrder = l1.getWebElement("QuickOrder_ProductPrice", "Profile//QuickOrder.properties").getText();
			proPriceInQuickOrder = proPriceInQuickOrder;
			log.info("proPriceInQuickOrder:-"+proPriceInQuickOrder);
			
			sa.assertEquals(proPriceInQuickOrder, productPriceList.get(i),"compare the price for different UOM options");
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="Extra TestCAse. Adding more products to cart")
	public void TC07_addMultipleProductsToCart() throws Exception
	{
		log.info("click on clear form");
		l1.getWebElement("QuickOrder_ClearForm_LinkBottom", "Profile//QuickOrder.properties").click();
		
		log.info("Enter the product id in material number which can be added to cart");
		
		
		for (int i = 0; i < 2; i++)
		{
			log.info("LOOP:- "+ i);
			log.info("Enter meterial numbers");
			l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(i).sendKeys(GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", i+1, 0));
			l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(Keys.TAB);
			Thread.sleep(1500);
		}
		
		
//		String ProductId = GetData.getDataFromExcel("//data//GenericData_US.xls", "QuickOrder", 2, 0);
//		l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(ProductId);
//		l1.getWebElement("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		ArrayList<String> alProName = new ArrayList<String>();
		ArrayList<String> alProQty = new ArrayList<String>();
		ArrayList<String> alProPrice = new ArrayList<String>();
		
		log.info("Collect the product name, id and price");
		List<WebElement> proNameInQuickOrder = l1.getWebElements("QuickOrder_PeoductsName", "Profile//QuickOrder.properties");
		List<WebElement> quantity = l1.getWebElements("QuickOrder_Qty","Profile//QuickOrder.properties");
		List<WebElement> totalAmount = l1.getWebElements("QuickOrder_TotalItemPrice", "Profile//QuickOrder.properties");
		for (int i = 0; i < proNameInQuickOrder.size(); i++)
		{
			log.info("LOOP:-"+i);
			alProName.add(proNameInQuickOrder.get(i).getText());
			alProQty.add(quantity.get(i).getAttribute("value"));
			alProPrice.add(totalAmount.get(i).getText());
		}
		log.info("alProName:- "+ alProName);
		log.info("alProQty:- "+ alProQty);
		log.info("alProPrice:- "+ alProPrice);
		
		log.info("Click on ADD TO CART button");
		l1.getWebElement("QuickOrder_AddToCart_Top","Profile//QuickOrder.properties").click();
		
		Thread.sleep(2000);
		
		log.info("User should navigate to cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		log.info("Verify the product name");
		List<WebElement> proNameInCart = l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties");
		List<WebElement> proQtyInCart = l1.getWebElements("Cart_QtyBox", "Cart//Cart.properties");
		List<WebElement> proPriceInCart = l1.getWebElements("Cart_ItemTotal", "Cart//Cart.properties");
		log.info("proNameInCart.size() :-"+ proNameInCart.size());
		for (int i = 0; i < proNameInCart.size(); i++)
		{
			log.info("LOOP:-"+ i);
			log.info("Verify the Pro name in CART page");
			gVar.assertequalsIgnoreCase(alProName.get(i), proNameInCart.get(i).getText(),"Verify the Pro name in CART page");
			log.info("Verify the Pro name in CART page");
			gVar.assertequalsIgnoreCase(alProQty.get(i), proQtyInCart.get(i).getAttribute("value"),"Verify the Pro name in CART page");
			log.info("Verify the Pro name in CART page");
			gVar.assertequalsIgnoreCase(alProPrice.get(i), proPriceInCart.get(i).getText(),"Verify the Pro name in CART page");
		}

		log.info("Click on PROCEED TO CHECKOUT button IN Cart page");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		Thread.sleep(2000);
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Click on radio button");
		l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		
		log.info("Click on SELECT pickup address CONTINUE button");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Select the shipping method and click on CONTINUE button in SHIPPING METHOD SECTION");
		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		shippingMethodRadioButton.get(0).click();
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Enter PO number");
		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		String poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();

		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
		log.info("orderID:- "+ orderID);
		
		gVar.saveOrderId(orderID, "Products are added to cart from Quick order page and placed order");
		
		sa.assertAll();
	}
}
