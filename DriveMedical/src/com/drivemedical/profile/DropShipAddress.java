package com.drivemedical.profile;

import java.util.List;

//

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class DropShipAddress extends BaseTest {
	
	String savedAddr;

	@Test(groups={"reg"},description="OOB-060 DRM-823,824,825,826,827,828|| OOB-013 DRM-846,847|| DMED-535 DRM-1102")
	public void TC00_dropShip_UI(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("navigate to ship to page");
		checkout.navigateToShipToPage();
		log.info("add address");
		for(int i=0;i<2;i++)
		{
			log.info("Select the first address");
			l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
			
			log.info("click on add address link");
			l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
			int k=i+1;
			checkout.addAddress(k);
			
			log.info("Click on CONTINUE button in ShipTo section");
			l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
			Thread.sleep(2000);
			
			//click on Continue button in pop if it is visible 
			if (gVar.assertVisible("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties"))
			{
				l1.getWebElement("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties").click();
			}
			
			log.info("Verify the Address in ShipTo section");
			sa.assertEquals(gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", k, 13),"Verify the ShipAddress -LOOP:- "+i);
			
			log.info("Click on Edit link in ShipTo section to navigate back to ship to page");
			l1.getWebElement("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").click();
			
		}
		log.info("Count the Saved address in ShipTo address section");
		List<WebElement> savedAddressList = l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		sa.assertEquals(savedAddressList.size(), 3,"Verify the saved address count");
		// #** NOTE:- ADDRESS SHOULD be deleted before execution in BACK OFFICE. if address not deleted "NullPointerException" will come
		//Verify the saved address in shipTo section in checkout page
		for (int i = 1; i < savedAddressList.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String actualSavedAddress = savedAddressList.get(i).getText().toLowerCase();
			log.info("actualSavedAddress:- "+ actualSavedAddress);
			String expectedAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", i, 9).toLowerCase();
			log.info("Compare the addresses");
			sa.assertEquals(actualSavedAddress, expectedAddress,"Compare the saved address in ShipTo address section");
		}
		
		sa.assertAll();
	}

	@Test(groups={"reg"},description="DMED-535 DRM-1103")
	public void TC01_savedAddrUnCheck() throws Exception 
	{
		
		log.info("Select the first address");
		l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		
		log.info("click on add address link");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		int addressRow = 3;
		checkout.addAddress(addressRow);
		String name = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", addressRow, 1);
		
		log.info("un check save this address check box");
		try
		{
			l1.getWebElement("ShipTo_SaveThisAddr_CheckBox", "Checkout//ShipTo.properties").click();
			sa.assertTrue(gVar.assertVisible("ShipTo_SaveThisAddr_CheckBox_Unchacked", "Checkout//ShipTo.properties"),"Verify the unchecked checkbox");
			sa.assertFalse(gVar.assertVisible("ShipTo_SaveThisAddr_CheckBoxChecked", "Checkout//ShipTo.properties"),"Checkbox should not be checked");
		}
		catch (Exception e)
		{
			log.info("Unable to Uncheck the checkbox");
		}
		
		log.info("click on continue in shipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Verify the Address in ShipTo section");
		sa.assertEquals(gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", addressRow, 13),"Verify the ShipAddress");
		
		log.info("Click on Edit link in ShipTo section");
		l1.getWebElement("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").click();
		
		log.info("Count the Saved address in ShipTo address section");
		List<WebElement> savedAddressList = l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		sa.assertEquals(savedAddressList.size(), 3,"Verify the saved address count");
		
		// ######## To verify that, Entered address should not be saved in ShipTo section in checkout page  ######## 
		for (int i = 0; i < savedAddressList.size(); i++)
		{
			log.info("LOOP:- " + i);
			String actualAddress = gVar.assertEqual("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties",i).toLowerCase();
			String expectedAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", addressRow, 9).toLowerCase();
			sa.assertNotEquals(actualAddress, expectedAddress,"Address should not be Saved in SipTo Address list");
			String actualName = gVar.assertEqual("ShipTo_SavedAddr_Name", "Checkout//ShipTo.properties",i).toLowerCase();
			sa.assertNotEquals(actualName, name.toLowerCase(),"NAme should not be same");
		}
		
		log.info("Navigate to Home page");
		s.navigateToHomePage();
		log.info("Navigate To DropShipp address in profile");
		s.navigateToDropShipAddressPage();
		
		log.info("verify UI of the DropShip Addresses page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("DropShip_Heading", "Profile//DropShip.properties"), 
				"Dropship Addresses", "Verify the heading text");
		log.info("Verify the bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				"Address Book","Verify the breadcrumb active");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"Home Address Book","Verify the full breadcrumb");
		
		log.info("Verify the saved address in profile page");
		List<WebElement> totAddresses=l1.getWebElements("DropShip_Address_Containers", "Profile//DropShip.properties");
		int totalAddress = totAddresses.size();
		//##### Only TWO address entered in ShipTo section in checkout page. Hence Only TWO address should be saved in profile ##### 
		sa.assertEquals(totalAddress, 2,"Verify the total saved address");
		//To verify the saved address (when user check the Save Address checkbox in ShipTo section of checkout page)
		int j=1;
		for(int i=0;i<totAddresses.size();i++) 
		{
			log.info("LOOP:- "+ i);
			log.info("verify saved address addresses");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("DropShip_Addresses", "Profile//DropShip.properties", i).toString(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", j, 8),"Address Verification - LOOP:- "+i);
			j++;
		}
		
		//######## Address entered in ShipTo section in Checkout page should not be saved ########  
		// ########  when user UNCHECK the SAVE ADDRESS CHECKBOX ######## 
		j=1;
		for(int i=0;i<totalAddress;i++) 
		{
			log.info("LOOP:- "+ i);
			log.info("Address entered in ShipTo section should not be saved when user uncheck the SAVE ADDRESS CHECKBOX");
			String actualAddress = gVar.assertEqual("DropShip_Addresses", "Profile//DropShip.properties", i).toLowerCase();
			log.info("actualAddress:- "+ actualAddress);
			
			String expectedAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", addressRow, 8).toLowerCase();
			sa.assertNotEquals(actualAddress, expectedAddress,"address should not be saved -LOOP:- "+i);
			sa.assertFalse(actualAddress.equals(expectedAddress),"address should not be SAME- LOOP:- "+i);
			
			String actualName = gVar.assertEqual("DropShip_Address_Name", "Profile//DropShip.properties",i).toLowerCase();
			log.info("actualName:- "+ actualName);
			sa.assertNotEquals(actualName, name.toLowerCase(),"NAme should not be same -LOOP:- "+ i);
			sa.assertFalse(actualName.equals(name),"Address should not saved - LOOP:- "+ i);
			j++;
		}
		
		sa.assertAll();
	}
	
}
