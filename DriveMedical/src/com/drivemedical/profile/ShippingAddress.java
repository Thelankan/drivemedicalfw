package com.drivemedical.profile;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ShippingAddress extends BaseTest{

	
	@Test(groups={"reg"},description="DMED-555 DRM-737,738,739,740")
	public void verifyShippingAddr_UI(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application with Admin credentials");
		p.navigateToLoginPage();
		String un = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 6);
		String pwd = GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 2, 6);
		p.logInWithCredentials(un, pwd);
		log.info("Navigate to LOGIN page");
		s.navigateToShippingAddressPage();
		
		log.info("Verify Heading");
		sa.assertTrue(gVar.assertVisible("ShippingAddr_Heading", "Profile//ShippingAddr.properties"),"Shipping Address Heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ShippingAddr_Heading", "Profile//ShippingAddr.properties"), 
				"Shipping Addresses","Verify the Heading text");
	
		log.info("Verify the bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				"Shipping Addresses","Verify the breadcrumb active");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
				"Home Shipping Addresses","Verify the full breadcrumb");
		
		log.info("Verify saved address");
		String expectedADdress = GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", 10, 12);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ShippingAddr_SavedAddr", "Profile//ShippingAddr.properties"), 
				expectedADdress,"Verify the saved address");
//		List<WebElement> savedShippingAddr=l1.getWebElements("ShippingAddr_SavedAddr", "Profile//ShippingAddr.properties");
//		int rowNum = 1;
//		for(int i=0;i<savedShippingAddr.size();i++) 
//		{
//			log.info("Verify saved address");
//			sa.assertEquals(GetData.getDataFromExcel("//data//GenericData_US.xls", "Address", rowNum, 10), savedShippingAddr.get(i).getText());
//			rowNum++;
//		}
		
		sa.assertAll();
	}
	
}
