package com.drivemedical.profile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Quotes extends BaseTest
{

	/* Can not be automated Test Cases are DRM-1100*/
	
	
	String quoteID;
	static String reason;
	String quoteNameInCheckout;
	String proNameInPDP;
	String productPriceInPdp;
	String proNAmeInQuoteDetails;
	
	@Test(groups={"reg"},description="DMED-535 DRM-1095,1101")
	public void TC00_quote(XmlTest xmlTest) throws Exception
	{
		log.info("log out from application");
		p.logout(xmlTest);
		
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		cart.clearCart();
		log.info("navigate to Order Review page");
		checkout.navigateToShipToPage();
		proNameInPDP = s.prodnames.get(0);
		productPriceInPdp = s.prodPrice.get(0);
		log.info("proNameInPDP:- "+ proNameInPDP);
		log.info("productPriceInPdp:- "+ productPriceInPdp);
		
		log.info("Request quote button should be disabled");
		sa.assertEquals(gVar.assertEqual("ShipTo_RequestQuote_Disabled", "Checkout//ShipTo.properties","disabled"), "true", "Verify the Request quote button");
		
		log.info("enter new address");
//########## DONT DELETE BELOW LINE ##############
//		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
//		checkout.addAddress(1);
		log.info("Select the RADIO button in ShippTo section");
		l1.getWebElement("ShipTo_SavedAddr_Radio", "Checkout//ShipTo.properties").click();
		log.info("Click on CONTINUE button in SHIPTO section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		Thread.sleep(3000);
		log.info("Select the radio button in SHIPPING METHODS section");
		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		shippingMethodRadioButton.get(1).click();
		log.info("Click on Continue button SHIPPING METHOD section");
		l1.getWebElement("ShippingMtd_Next_Btn","Checkout//ShippingMethod.properties").click();
		log.info("enter PO number");
		int randomNum = gVar.generateRandomNum();
		String PONumberVal=randomNum+"";
		log.info("PONumberVal:-"+PONumberVal);
		l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		log.info("click on next button");
		l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
		Thread.sleep(3000);
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("click on quote a request");
		l1.getWebElement("OrderReview_RequestQuote_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1096,1099,1104,1107 DMED-545 DRM-1182")
	public void TC01_quoteReason() throws Exception 
	{
		log.info("verify max length of reason text box");
		sa.assertEquals(gVar.assertEqual("Quote_Reason_Textbox", "Checkout//OrderReview.properties", "maxlength"), "255");
		log.info("Collect the prepopulated QuoteId");
		quoteNameInCheckout = l1.getWebElement("CreateQuotePopup_QuoteId", "Checkout//OrderReview.properties").getAttribute("value");
		String quoteName= quoteNameInCheckout.split(" ")[1].trim();
		log.info("quoteName:- "+ quoteName);
		
		log.info("enter reason");
		reason = "Reason Entered";
		l1.getWebElement("Quote_Reason_Textbox", "Checkout//OrderReview.properties").sendKeys(reason);
		log.info("click on submit");
		l1.getWebElement("Quote_Submit_Btn", "Checkout//OrderReview.properties").click();
		log.info("fetch quote code");
		quoteID=l1.getWebElement("Quote_Id", "Checkout//OrderReview.properties").getText().replace("#", "").trim();
		log.info("quoteID:- "+ quoteID);
		
		sa.assertEquals(quoteName, quoteID, "Verify the Quote id value in both the popups");
		log.info("click yes on quote confirmation pop up");
		l1.getWebElement("Quote_Yes_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify navigation to quotes page");
		sa.assertTrue(gVar.assertVisible("Quotes_Heading", "Profile//Quotes.properties"),"Quotes heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quotes_Heading", "Profile//Quotes.properties"), 
				"quotes", "Verify the heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1098,1105 DMED-064 DRM-1142")
	public void TC02_quoteStatus() throws Exception 
	{
		log.info("Verify the Success message");
		String closeButton = l1.getWebElement("Quote_SuccessMessage_CloseButton", "Profile//Quotes.properties").getText();
		String successMessage = gVar.assertEqual("Quote_SuccessMessage", "Profile//Quotes.properties").replace(closeButton, "").trim();
		gVar.assertequalsIgnoreCase(successMessage, 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 5, 2),"verify the success message");
		log.info("Click on CLOSE button in success message section");
		l1.getWebElement("Quote_SuccessMessage_CloseButton", "Profile//Quotes.properties").click();
		Thread.sleep(1000);
		log.info("Message should not display");
		sa.assertFalse(gVar.assertVisible("Quote_SuccessMessage", "Profile//Quotes.properties"),"Success message should not display");
		
		log.info("Verify recently submitted quotes code");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"), 
				quoteID, "Verify the Quote ID");
		
		log.info("Verify recently submitted quotes status");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Status", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 8), "Verify the Quote status");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="DMED-537 DRM-1163,1164,1165 OOTB-064 DRM-1155 DRM-1156")
	public void TC03_quotesListingUI(XmlTest xmlTest) throws Exception
	{
		
		log.info("verify Quote Listing page UI");
		log.info("bread crumb");
		sa.assertTrue(gVar.assertVisible("Quote_Home_BreadCrumb_link", "Profile//Quotes.properties"),"Home bread crumb link");
		log.info("quotes bread crumb text");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 2),"quotes bread crumb text");
		
		log.info("quotes Heading");
		sa.assertTrue(gVar.assertVisible("Quote_Heading", "Profile//Quotes.properties"),"quotes heading");
		log.info("sort by label");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_Top_Label", "Profile//Quotes.properties"),"sort by label top");
		log.info("sort by drop down top");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_DropDown_Top", "Profile//Quotes.properties"),"sort by drop down top");
		log.info("pagination results top");
		sa.assertTrue(gVar.assertVisible("Quote_Pagination_Results_Top", "Profile//Quotes.properties"),"pagination results top");
		log.info("quote listing tabel headings");
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
		{
			log.info("MOBILE VIEW");
			List<WebElement> quotesRows = l1.getWebElements("QuoteDetails_Rows_Mob", "Profile//Quotes.properties");
			
			for (int i = 0; i < quotesRows.size(); i++) 
			{
				log.info("Loop i:- "+ i);
				
				By byRefLabel = l1.getByReference("QuoteDetails_Labels_Mob", "Profile//Quotes.properties");
				By byRefQuoteId = l1.getByReference("QuoteDetails_QuoteLink", "Profile//Quotes.properties");
				
				WebElement rowElement = quotesRows.get(i);
				List<WebElement> labels = rowElement.findElements(byRefLabel);
				log.info("labels count:- "+ labels.size());
				int jrowNum =1;
				for (int j = 0; j < labels.size(); j++) 
				{
					log.info("Loop j:- "+ j);
					String labelElement = labels.get(j).getText();
					log.info("Labels:- "+labelElement);
					String expectedLAbel = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", jrowNum,0);
					sa.assertEquals(labelElement, expectedLAbel,"Verify the labels");
					jrowNum++;
				}
			}
		
		} 
		else 
		{
			List<WebElement> quoteHeadings=l1.getWebElements("Quote_Listing_Table", "Profile//Quotes.properties");
			int i=1;
			for(WebElement ele:quoteHeadings) 
			{
				sa.assertTrue(ele.isDisplayed(),"quotes table headings");
				String expectedLAbel = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", i,0);
				gVar.assertequalsIgnoreCase(ele.getText(),expectedLAbel,"verify with EXCEL data");
				i++;
			}
		}
		
		List<WebElement> nameLinks=l1.getWebElements("Quote_Name_Links", "Profile//Quotes.properties");
		i=0;
		for(WebElement ele:nameLinks) 
		{
			log.info("name links");
			sa.assertTrue(gVar.assertVisible("Quote_Name_Links", "Profile//Quotes.properties",i),"name link");
			log.info("quote Id");
			sa.assertTrue(gVar.assertVisible("Quote_Code", "Profile//Quotes.properties",i),"Quote Code");
			log.info("Date Updated");
			sa.assertTrue(gVar.assertVisible("Quote_Date_Updated", "Profile//Quotes.properties",i),"Date Updated");
			log.info("Status");
			sa.assertTrue(gVar.assertVisible("Quote_Status", "Profile//Quotes.properties",i),"Quote status");
			log.info("Documents");
			sa.assertTrue(gVar.assertVisible("Quote_Documnets", "Profile//Quotes.properties",i),"Documents");
		}
		
		log.info("sort by label");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_bottom_Label", "Profile//Quotes.properties"),"sort by label bottom");
		log.info("sort by drop down top");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_DropDown_bottom", "Profile//Quotes.properties"),"sort by drop down bottom");
		log.info("pagination results top");
		sa.assertTrue(gVar.assertVisible("Quote_Pagination_Results_bottom", "Profile//Quotes.properties"),"pagination results bottom");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-537 DRM-1161")
	public void TC04_quotesStatus() throws Exception
	{
		log.info("verify quotes status");
		List<WebElement> status=l1.getWebElements("Quote_Status", "Profile//Quotes.properties");
		String approved = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 8);
		String vendorQuote = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 8);
		String draft = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 8);
		String cancelled = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 4, 8);
		String Ordered = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 5, 8);
		
		for (int i = 0; i < status.size(); i++) 
		{
			log.info("Loop:- " +i);
			String ele = status.get(i).getText();
			//Status of the Quotes updated
			if(ele.equalsIgnoreCase(vendorQuote) || ele.equalsIgnoreCase(draft) || ele.equalsIgnoreCase(Ordered)) 
			{
				sa.assertTrue(true);
			} 
			else 
			{
				log.info("ele:- "+ ele);
				sa.assertTrue(false,"Status is not mentianed in Testcase -LOOP:-"+i);
			}
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-538 DRM-1167 DRM-1168 DRM-1169 DRM-1170 OOTB-064 DRM-1158 DRM-1159 DRM-1160")
	public void TC05_quoteDetailsUI() throws Exception 
	{
		log.info("click on Quote link");
		l1.getWebElement("Quote_Links", "Profile//Quotes.properties").click();
		log.info("verify quote status in Details page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Status", "Profile//Quotes.properties"),
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 8),"Quote status in details page");
		
		log.info("Verify the Quote code in Breadcrumb");
		String actualBreadCrumb = gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties");
		log.info("actualBreadCrumb:- "+ actualBreadCrumb);
		sa.assertTrue(actualBreadCrumb.contains(quoteID),"Verify the Quote number in breadcrumb");
		
		log.info("Verify the Quote name");
		gVar.assertequalsIgnoreCase(l1.getWebElement("QuoteDetails_QuoteName", "Profile//Quotes.properties").getText(), quoteNameInCheckout,"Verify the quote name");
		
		log.info("Verify the Description label and value");
		gVar.assertequalsIgnoreCase(l1.getWebElement("QuoteDetails_DescriptionLabel", "Profile//Quotes.properties").getText()
				, GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 2),"Verifyt Description label");
		gVar.assertequalsIgnoreCase(l1.getWebElement("QuoteDetails_DescriptionValue", "Profile//Quotes.properties").getText(), reason, "Verify the reason");
		
		log.info("Verify the updated date and date placed");
		String actualUpdatedDate = l1.getWebElement("QuoteDetails_UpdatedDate", "Profile//Quotes.properties").getText();
		log.info("actualUpdatedDate:- "+ actualUpdatedDate);
		String systemDate = gVar.getSystemDate("MMM d, yyyy");
		log.info("systemDate:- "+ systemDate);
		
		sa.assertTrue(actualUpdatedDate.contains(systemDate), "Verify the updated date");
		String actualDAte = actualUpdatedDate.substring(0,systemDate.length());
		sa.assertEquals(actualDAte, systemDate,"Verify the updated date..");
		String actualDatePlaced = l1.getWebElement("QuoteDetails_PlacedDate", "Profile//Quotes.properties").getText();
		log.info("actualDatePlaced:- "+ actualDatePlaced);
		sa.assertTrue(actualDatePlaced.contains(systemDate),"Verify the Date placed");
		sa.assertEquals(actualDAte, systemDate,"Verify the Date placed..");
		
		log.info("Verify the product name");
		proNAmeInQuoteDetails = gVar.assertEqual("QuoteDetails_ProductName", "Profile//Quotes.properties");
//		sa.assertTrue(proNameInPDP.contains(proNAmeInQuoteDetails), "Verify the Product name");
//		sa.assertEquals(proNAmeInQuoteDetails, proNameInPDP.substring(0, proNAmeInQuoteDetails.length()), "Verify the Product name..");
		sa.assertEquals(proNAmeInQuoteDetails, proNameInPDP,"Verify the Product name");
		if (gVar.desktopView())
		{
			log.info("Verify the item table heading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_ItemHeading", "Profile//Quotes.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 3),"QuoteDetails_ItemHeading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_PriceHeading", "Profile//Quotes.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 3),"QuoteDetails_PriceHeading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_QuantityHeading", "Profile//Quotes.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 3),"QuoteDetails_QuantityHeading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_UnitHeading", "Profile//Quotes.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 4, 3),"QuoteDetails_UnitHeading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_DeliveryHeading", "Profile//Quotes.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 5, 3),"QuoteDetails_DeliveryHeading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_TotalHeading", "Profile//Quotes.properties"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 6, 3),"QuoteDetails_TotalHeading");
		}
		
		String proPrice = l1.getWebElement("QuoteDetails_ProductPrice", "Profile//Quotes.properties").getText();
		String proPriceInQuoteDetailsPage  = proPrice.substring(proPrice.indexOf("$"), proPrice.length());
		log.info("proPriceInQuoteDetailsPage:- "+ proPriceInQuoteDetailsPage);
		sa.assertEquals(proPriceInQuoteDetailsPage, productPriceInPdp, "Verify the price");
		
		log.info("Collect the Quantity");
		String qtyValText;
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			qtyValText = l1.getWebElement("QuoteDetails_QuantityValue_Mobile", "Profile//Quotes.properties").getText();
		}
		else
		{
			qtyValText = l1.getWebElement("QuoteDetails_QuantityValue", "Profile//Quotes.properties").getText();
		}
		
		log.info("qtyValText:- " + qtyValText);
		qtyValText = qtyValText.replaceAll("[^0-9]", "").replace(",", "");
		log.info("qtyValText:- " + qtyValText);
		int quantityVal = Integer.parseInt(qtyValText);
		double productPriceInPdpVal = Double.parseDouble(productPriceInPdp.replace("$", ""));
		log.info("productPriceInPdpVal:- " + productPriceInPdpVal);
		double expectedTotalPrice = productPriceInPdpVal*quantityVal;
		log.info("expectedTotalPrice:- "+ expectedTotalPrice);
		String actualTotalPrice ;
		if (gVar.mobileView()||gVar.tabletView())
		{
			
			actualTotalPrice = l1.getWebElement("QuoteDetails_ProductTotalPrice_Mobile", "Profile//Quotes.properties").getText();
		}
		else
		{
			actualTotalPrice = l1.getWebElement("QuoteDetails_ProductTotalPrice", "Profile//Quotes.properties").getText();
		}
		
		log.info("actualTotalPrice:- "+actualTotalPrice);
		actualTotalPrice = actualTotalPrice.replace("$", "").replace(",", "");
		log.info("actualTotalPrice:- "+actualTotalPrice);
		double actualTotalPriceVal = Double.parseDouble(actualTotalPrice);
		sa.assertEquals(actualTotalPriceVal, expectedTotalPrice,"Verify the total price");
		
		sa.assertFalse(true, "negotiation details, comments and quote properties need to check manually(these things can't automated)");

		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="DMED-535 DRM-1106 DMED-537 DRM-1162 OOTB-064 DRM-1153")
	public void TC06_quoteExpireDate() throws Exception 
	{
		SimpleDateFormat sf = new SimpleDateFormat("MMM d, yyyy");
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 90);
        Date date = cal.getTime();
        String expExpireDate = sf.format(date);
        log.info("Expected ExpireDate:- "+ expExpireDate);
		
		log.info("verify expiration date");
		String actualExpaireDate = gVar.assertEqual("Quote_Expire_Date", "Profile//Quotes.properties").trim();
		log.info("actualExpaireDate:- "+ actualExpaireDate);
		
		sa.assertTrue(actualExpaireDate.contains(expExpireDate),"Verify the expiry date");
	
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-064 DRM-1152")
	public void TC07_quoteExpiryDate() throws Exception
	{
		log.info("Verify the Quote is displayed below the Quotes name in Quotes details page");
		sa.assertTrue(gVar.assertVisible("Quote_Expire_BelowName", "Profile//Quotes.properties"),"Verify the Quote is displayed below the Quotes name in Quotes details page");
		
		log.info("VErify the Expire label");
		sa.assertEquals(gVar.assertEqual("Quote_Expire_Label", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 2), "Verify the LAbel");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-064 DRM-1146")
	public void TC08_pdpNavigationOnClickONProductNameInQuotesDetailPage() throws Exception
	{
		log.info("Click on Product name in Quote details page");
		l1.getWebElement("QuoteDetails_ProductNameLink", "Profile//Quotes.properties").click();

		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		log.info("Verify the product name");
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				proNAmeInQuoteDetails,"Compare the product name");
		
		//##########################################
		log.info("Navigate to Quote details page");
		s.navigateToQuotesPage();
		
		log.info("click on Quote link/Navigate to Quote Detail page");
		l1.getWebElement("Quote_Links", "Profile//Quotes.properties").click();
		
		log.info("Click on Product image");
		l1.getWebElement("QuoteDetails_ProductImageLink", "Profile//Quotes.properties").click();
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		log.info("Verify the product name");
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				proNAmeInQuoteDetails,"Compare the product name");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1097")
	public void TC09_quoteWithoutReason() throws Exception 
	{
		log.info("Navigate to Order Review page");
		checkout.navigateToOrderReviewPage();
		Thread.sleep(3000);
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}
		log.info("click on quote a request");
		l1.getWebElement("OrderReview_RequestQuote_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		
		Thread.sleep(3000);
		log.info("click on submit button without entering Reason");
		l1.getWebElement("Quote_Submit_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("fetch quote code");
		quoteID=l1.getWebElement("Quote_Id", "Checkout//OrderReview.properties").getText().replace("#", "");
		log.info("quoteID:- "+ quoteID);
		
		log.info("click yes on quote confirmation pop up");
		l1.getWebElement("Quote_Yes_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("verify navigation to quotes page");
		sa.assertTrue(gVar.assertVisible("Quotes_Heading", "Profile//Quotes.properties"),"Quotes heading");
		
		log.info("Verify recently submitted quotes code");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"),quoteID,"Quote number");
		
		log.info("Click on Quotes name");
		l1.getWebElement("Quote_Name_Links", "Profile//Quotes.properties").click();
		
		log.info("Verify the Quote Details page");
		String expectedHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 9)+" "+quoteID;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_Heading", "Profile//Quotes.properties"), 
				expectedHeading,"Verify the Quote details page");
		log.info("Verify the satatus");
		sa.assertEquals(gVar.assertEqual("QuoteDetails_DescriptionValue", "Profile//Quotes.properties").trim(), 
				"N/A","Verify the status");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-535 DRM-1098")
	public void TC10_verifyTheQuoteStatus() throws Exception
	{
		log.info("Navigate back  to Quotes listning page");
		l1.getWebElement("QuoteDetails_Back_Link", "Profile//Quotes.properties").click();
		
		log.info("Verify the status Quote");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Status", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 2, 8), "Verify the status");

		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-064  DRM-1144")
	public void TC11_myAccountQuotesDefaultOrderDispay(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Quotes listning page");
		s.navigateToQuotesPage();
		
		log.info("Collect all the Dates");
		List<WebElement> allDates = l1.getWebElements("Quote_Date_Updated", "Profile//Quotes.properties");
		log.info("allDates size:- "+allDates.size());
		ArrayList<String> collectedDates = new ArrayList<String>();
		ArrayList<String> sortedDatesList = new ArrayList<String>();
		
		for (int i = 0; i < allDates.size(); i++)
		{
			log.info("LOOP:-"+i);
			String date = allDates.get(i).getText();
			log.info("date:- "+date);
			collectedDates.add(date);
		}
		
		log.info("collectedDates:- "+collectedDates);
		//Copy collectedDates to sortedDatesList
		sortedDatesList = (ArrayList<String>) collectedDates.clone();
		Collections.sort(sortedDatesList);	//This will return low to high
		Collections.reverse(sortedDatesList);	//We need High low to low
		
		log.info("After Sort collectedDates:- "+ sortedDatesList);
		
		log.info("Compare the dates");
		sa.assertEquals(sortedDatesList,collectedDates, "Compare the dates");

		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-SortBy Quote name")
	public void TC12_sortByQuoteName() throws Exception
	{
		log.info("Select the sort by Quote name");
		String sortOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 3, 4);
		log.info("sortOption:- "+ sortOption);
		
		log.info("Select the Sort By Date placed option");
		WebElement sortBySelectbox = l1.getWebElement("Quote_SortBy_SelectBox_Top", "Profile//Quotes.properties");
		Select sel = new Select(sortBySelectbox);
		sel.selectByVisibleText(sortOption);
		Thread.sleep(5000);
		log.info("Verify the Selected option in dropdown box");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_Top", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option TOP");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_bottom", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option BOTTOM");
		
		log.info("Collect the all the names");
		List<WebElement> allQuoteNams = l1.getWebElements("Quote_Links", "Profile//Quotes.properties");
		log.info("allQuoteNams size:- "+ allQuoteNams.size());
		ArrayList<String> quoteNamesList = new ArrayList<String>();
		ArrayList<String> quoteNamesListCopy = new ArrayList<String>();
		for (int i = 0; i < allQuoteNams.size(); i++)
		{
			log.info("LOOP:- "+i);
			String quoteName = allQuoteNams.get(i).getText();
			log.info("quoteName:- "+ quoteName);
			quoteNamesList.add(quoteName);
		}
		log.info("quoteNamesList:- "+ quoteNamesList);
		
		quoteNamesListCopy = (ArrayList<String>) quoteNamesList.clone();
		//Sort
		Collections.sort(quoteNamesListCopy);
		log.info("quoteNamesListCopy:- "+ quoteNamesListCopy);
		log.info("Compare the Quote names");
		sa.assertEquals(quoteNamesListCopy, quoteNamesList,"Compare the names");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-Sort by Status")
	public void TC13_sortByStatus() throws Exception
	{
		String sortOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 4, 4);
		log.info("sortOption:- "+ sortOption);
		
		log.info("Select the Sort By Status option");
		WebElement sortBySelectbox = l1.getWebElement("Quote_SortBy_SelectBox_Top", "Profile//Quotes.properties");
		Select sel = new Select(sortBySelectbox);
		sel.selectByVisibleText(sortOption);
		Thread.sleep(5000);
		log.info("Verify the Selected option in dropdown box");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_Top", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option TOP");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_bottom", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option BOTTOM");
		
		log.info("Collect all the status");
		List<WebElement> allStatuse = l1.getWebElements("Quote_Status", "Profile//Quotes.properties");
		log.info("allStatuse size:- "+ allStatuse.size());
		ArrayList<String> allStatuseList = new ArrayList<String>();
		ArrayList<String> allStatuseListCopy = new ArrayList<String>();
		
		for (int i = 0; i < allStatuse.size(); i++)
		{
			log.info("LOOP:-"+ i);
			String status = allStatuse.get(i).getText();
			log.info("status:- "+ status);
			allStatuseList.add(status);
			
		}
		log.info("allStatuseList :- "+ allStatuseList);
		allStatuseListCopy = (ArrayList<String>) allStatuseList.clone();
		
		Collections.sort(allStatuseListCopy);
		log.info("allStatuseListCopy:- "+ allStatuseListCopy);
		
		log.info("Compare the Status");
		sa.assertEquals(allStatuseListCopy, allStatuseList,"Verify the sort option of status ");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-064 DRM-1143")
	public void TC14_sortByMOdifiedDate() throws Exception
	{
		String sortOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 4);
		log.info("sortOption:- "+ sortOption);
		log.info("Select the Sort By Date placed option");
		WebElement sortBySelectbox = l1.getWebElement("Quote_SortBy_SelectBox_Top", "Profile//Quotes.properties");
		Select sel = new Select(sortBySelectbox);
		sel.selectByVisibleText(sortOption);
		Thread.sleep(5000);
		log.info("Verify the Selected option in dropdown box");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_Top", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option TOP");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_bottom", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option BOTTOM");
		
		log.info("Collect all the Dates");
		List<WebElement> allDates = l1.getWebElements("Quote_Date_Updated", "Profile//Quotes.properties");
		log.info("allDates size:- "+allDates.size());
		ArrayList<String> collectedDates = new ArrayList<String>();
		ArrayList<String> sortedDatesList = new ArrayList<String>();
		
		for (int i = 0; i < allDates.size(); i++)
		{
			log.info("LOOP:-"+i);
			String date = allDates.get(i).getText();
			log.info("date:- "+date);
			collectedDates.add(date);
		}
		
		log.info("collectedDates:- "+collectedDates);
		//Copy collectedDates to sortedDatesList
		sortedDatesList = (ArrayList<String>) collectedDates.clone();
		Collections.sort(sortedDatesList);	//This will return low to high
		Collections.reverse(sortedDatesList);	//We need High low to low
		
		log.info("After Sort collectedDates:- "+ sortedDatesList);
		
		log.info("Compare the dates");
		sa.assertEquals(sortedDatesList,collectedDates, "Compare the dates");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-064 DRM-1143")
	public void TC15_sortByDatePlaced() throws Exception
	{
		log.info("Navigate to Quotes listning page");
		s.navigateToQuotesPage();
		
		String sortOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 5, 4);
		log.info("sortOption:- "+ sortOption);
		log.info("Select the Sort By Date placed option");
		WebElement sortBySelectbox = l1.getWebElement("Quote_SortBy_SelectBox_Top", "Profile//Quotes.properties");
		Select sel = new Select(sortBySelectbox);
		sel.selectByVisibleText(sortOption);
		Thread.sleep(5000);
		log.info("Verify the Selected option in dropdown box");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_Top", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option TOP");
		sa.assertEquals(gVar.assertEqual("Quote_SortBy_DropDown_bottom", "Profile//Quotes.properties","title"), 
				sortOption,"VErify the selected option BOTTOM");
		
		log.info("Collect all the Dates");
		List<WebElement> allDates = l1.getWebElements("Quote_Date_Updated", "Profile//Quotes.properties");
		log.info("allDates size:- "+allDates.size());
		ArrayList<String> collectedDates = new ArrayList<String>();
		ArrayList<String> sortedDatesList = new ArrayList<String>();
		
		log.info("Collect the Date placed");//Collect the Placed date in Quote details page
		List<WebElement> allQuotesLink = l1.getWebElements("Quote_Links", "Profile//Quotes.properties");
		log.info("allQuotesLink size:- "+ allQuotesLink.size());
		for (int i = 0; i < allDates.size(); i++)
		{
			log.info("LOOP:-"+i);
			allQuotesLink = l1.getWebElements("Quote_Links", "Profile//Quotes.properties");
			log.info("Click on Quotes link");//First navigate to Quote details page
			allQuotesLink.get(i).click();
			log.info("Collect the Date placed in Quotes details page");
			String placedDate = l1.getWebElement("QuoteDetails_PlacedDate", "Profile//Quotes.properties").getText();
			log.info("placedDate:- "+ placedDate);
			//Extracting Only date (removing time from date)
			placedDate = placedDate.substring(0, placedDate.lastIndexOf(":"));
			log.info("placedDate:- "+ placedDate);
			placedDate = placedDate.substring(0, placedDate.lastIndexOf(" ")).trim();
			log.info("placedDate:- "+ placedDate);	//Contains Only date(No time)
			if (placedDate.length()==10)	//To make date in mm,dd,yyyy(i.e., May 7, 2018 to May 07, 2018 )
			{
				StringBuffer sb = new StringBuffer(placedDate);
				placedDate = sb.insert(4, "0").toString();
				log.info("placedDate:- "+ placedDate);
			}
			
			//Collect placed date in Arraylist
			collectedDates.add(placedDate);
			
			log.info("Navigate back  to Quotes listning page");
			l1.getWebElement("QuoteDetails_Back_Link", "Profile//Quotes.properties").click();
			
			log.info("Select the Sort by Date placed option");
			sortBySelectbox = l1.getWebElement("Quote_SortBy_SelectBox_Top", "Profile//Quotes.properties");
			sel = new Select(sortBySelectbox);
			sel.selectByVisibleText(sortOption);
			Thread.sleep(5000);
		}
		
		log.info("collectedDates:- "+collectedDates);
		//Copy collectedDates to sortedDatesList
		sortedDatesList = (ArrayList<String>) collectedDates.clone();
		Collections.sort(sortedDatesList);	//This will return low to high
		log.info("After Sort collectedDates:- "+ sortedDatesList);
		Collections.reverse(sortedDatesList);	//We need High high to low
		log.info("After Sort collectedDates:- "+ sortedDatesList);
		
		log.info("Compare the dates");
		sa.assertEquals(sortedDatesList,collectedDates, "Compare the dates");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-064 DRM-1145 DMED-537 DRM-1166")
	public void TC16_selectQuoteByStatusPDF() throws Exception
	{
		log.info("Navigate to Quootes page");
		s.navigateToQuotesPage();
		
		for (int i = 1; i <=3; i++)
		{
			log.info("LOOP:- "+ i);
			WebElement documentType = l1.getWebElement("Quote_DocumentTypeSelectbox", "Profile//Quotes.properties");
			Select sel = new Select(documentType);
			
			log.info("Select the Document type options");
			sel.selectByIndex(i);
			Thread.sleep(4000);
			
			log.info("On selecting the option, a new tab will open");
			Set<String> tabs = driver.getWindowHandles();
			Iterator<String> itr = tabs.iterator();
			String parentTab = itr.next();
			String childTab = itr.next();
			
			log.info("Switch to Child tab");
			driver.switchTo().window(childTab);
			Thread.sleep(1000);
			log.info("Verify the window title Title");
			String title = driver.getTitle();
			log.info("title:- "+ title);
			
			String url = driver.getCurrentUrl();
			log.info("url:- "+ url);
			
			String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", i, 6);
			String expectedUrl = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", 1, 7);
			sa.assertTrue(title.contains(expected),"Verify the title - LOOP:-"+i);
			sa.assertTrue(url.contains(expectedUrl),"Verify the URL-LOOP:- "+ i);
			
			log.info("Close the window and switch the controller");
			driver.close();
			driver.switchTo().window(parentTab);
			
			log.info("Verify the Selected options");
			String expectedOption = GetData.getDataFromExcel("//data//GenericData_US.xls", "Quotes", i, 5);
			sa.assertEquals(gVar.assertEqual("Quote_DocumentTypeDropdownbox", "Profile//Quotes.properties","title"), 
					expectedOption,"Verify the Expected option");
		}
		
	}
	
	
	@Test(groups={"reg","sanity_reg"}, description="extraTC-PlaceQuote for POM products")
	public void TC17_placeQuoteForPOM() throws Exception
	{
		cart.clearCart();
		log.info("ADD POM product");
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData_US.xls", "Products", 9, 1);
		int quantity = 1;
		int numberOfProducts = 1;	//Here 1 indicates "number of products added to cart"
		s.navigateToCart(searchKeyWord,numberOfProducts, quantity);	
		Collections.sort(s.prodnames);
		Collections.sort(s.prodPrice);
		Collections.sort(s.materialId);
		log.info("Products Name which are added to Cart:- "+ s.prodnames);
		log.info("Products Price which are added to Cart:- "+s.prodPrice);
		log.info("Products Material ID's which are added to Cart:- "+s.materialId);
		
		List<String> proNameList = s.prodnames;
		ArrayList<String> materialIdsList = s.materialId;
		
		log.info("proNameList:- "+ proNameList);
		log.info("materialIdsList:- "+materialIdsList);
		
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		log.info("Collect the Cart ID");
		String cartID = l1.getWebElement("Cart_ID", "Cart//Cart.properties").getText();
		log.info("cartID:- "+ cartID);
		
		String productTotalPrice = l1.getWebElement("Cart_OrderTotal_Amount", "Cart//Cart.properties").getText();
		log.info("productTotalPrice:- "+ productTotalPrice);
		
		log.info("Click on PROCEED TO CHECKOUT button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		Thread.sleep(2000);
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Select the DropShipp address");
		l1.getWebElements("ShipTo_AllAddress_RadioButtons", "Checkout//ShipTo.properties").get(1).click();
		
		log.info("Click  CONTINUE button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Select the Product in popup");
		int proNum = 1;
		l1.getWebElements("ShipToPopUp_ProductsCheckbox", "Checkout//ShipTo.properties").get(proNum).click();
		
		log.info("Collect the product name and price");
		String suggestedProName = l1.getWebElements("ShipToPopUp_Products_Name", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("name:- "+ suggestedProName);
		String suggestedProPrice = l1.getWebElements("ShipToPopUp_Products_Price", "Checkout//ShipTo.properties").get(proNum).getText();
		log.info("price:- " + suggestedProPrice);
		
		//## "suggestedProName" contains both product name and price. Here only product name is required, so product price is removing   
		suggestedProName = suggestedProName.replace(suggestedProPrice, "").trim();
		log.info("suggestedProName:- "+ suggestedProName);
		
		log.info("Click on Continue button in popup");
		l1.getWebElement("ShipToPopUp_ContinueButton", "Checkout//ShipTo.properties").click();
		
		log.info("Collect the SHIPPTO Address");
		String shippToAddress = l1.getWebElement("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties").getText();
		log.info("shippToAddress:- "+ shippToAddress);
		
		log.info("Select the shipping method and click on CONTINUE button in SHIPPING METHOD SECTION");
		List<WebElement> shippingMethodRadioButton = l1.getWebElements("ShippingMtd_RadioButtonsLabels", "Checkout//ShippingMethod.properties");
		shippingMethodRadioButton.get(1).click();
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Collect the shipping method");
		String selectedShippingMethod = l1.getWebElement("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties").getText();
		log.info("selectedShippingMethod:- " + selectedShippingMethod);
		
		log.info("Enter PO number");
		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		String poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Check the Freight Checkbox");
		if (gVar.assertVisible("Checkout_Freight_Checkbox", "Checkout//Checkout.properties"))
		{
			l1.getWebElement("Checkout_Freight_Checkbox", "Checkout//Checkout.properties").click();
		}

		log.info("click on quote a request");
		l1.getWebElement("OrderReview_RequestQuote_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		quoteNameInCheckout = l1.getWebElement("CreateQuotePopup_QuoteId", "Checkout//OrderReview.properties").getAttribute("value");
		String quoteName= quoteNameInCheckout.split(" ")[1].trim();
		log.info("quoteName:- "+ quoteName);
		
		log.info("enter reason");
		reason = "Reason Entered";
		l1.getWebElement("Quote_Reason_Textbox", "Checkout//OrderReview.properties").sendKeys(reason);
		
		Thread.sleep(3000);
		log.info("click on submit button");
		l1.getWebElement("Quote_Submit_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("fetch quote code");
		quoteID=l1.getWebElement("Quote_Id", "Checkout//OrderReview.properties").getText().replace("#", "");
		log.info("quoteID:- "+ quoteID);
		sa.assertEquals(quoteName, quoteID, "Verify the Quote id value in both the popups");
		
		log.info("click yes on quote confirmation pop up");
		l1.getWebElement("Quote_Yes_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("verify navigation to quotes page");
		sa.assertTrue(gVar.assertVisible("Quotes_Heading", "Profile//Quotes.properties"),"Quotes heading");
		
		log.info("Verify recently submitted quotes code");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"),quoteID,"Quote number");
		
		sa.assertAll();
		
	}
	
}
