package com.drivemedical.profile;

import io.appium.java_client.functions.ExpectedCondition;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class MyAccountHome extends BaseTest
{
	String accountOverviewProperties = "Profile\\AccountOverview.properties";

	@Test(groups={"reg"},description="DMED-009 DRM-1502,1503,1504")
	public void TC01_contactInfo(XmlTest xmlTest) throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(BaseTest.xmlTest);

		log.info("navigate to account overview page");
		s.navigateToAccountOverviewPage();

		for(int i=0;i<2;i++) 
		{
			int cnt = i;
			if (gVar.mobileView()||gVar.tabletView())
			{
				cnt=cnt+2;
			}
			log.info("LOOP:- "+i);
			log.info("Verify name");
			sa.assertTrue(gVar.assertVisible("AO_SalesRep_Name", "Profile\\AccountOverview.properties",cnt),"Name");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_SalesRep_Name", "Profile\\AccountOverview.properties",cnt), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 2, i+11),"Verify Name");

			log.info("Verify email label");
			sa.assertTrue(gVar.assertVisible("AO_Email_Label", "Profile\\AccountOverview.properties",cnt),"Email label");

			log.info("Verify email");
			sa.assertTrue(gVar.assertVisible("AO_Email", "Profile\\AccountOverview.properties",cnt),"Email");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_Email", "Profile\\AccountOverview.properties",cnt), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 3, i+11),"Verify EMAIL");

			log.info("Verify Phone Number label");
			sa.assertTrue(gVar.assertVisible("AO_PhoneNumberLabel", "Profile\\AccountOverview.properties",cnt),"Phone Number label");
			log.info("Verify Phone Number");
			sa.assertTrue(gVar.assertVisible("AO_PhoneNumber", "Profile\\AccountOverview.properties",cnt),"Phone Number");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_PhoneNumber", "Profile\\AccountOverview.properties",cnt), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 4, i+11),"Verify Phone number");

			log.info("Verify Phone Number ext");
			sa.assertTrue(gVar.assertVisible("AO_PhoneNumberExt", "Profile\\AccountOverview.properties",cnt),"Phone Number");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_PhoneNumberExt", "Profile\\AccountOverview.properties",cnt), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 5, i+11),"Verify Phone number Extention");

			log.info("Verify Heading");
			sa.assertTrue(gVar.assertVisible("AO_SalesRep", "Profile\\AccountOverview.properties",cnt),"Content Name");

			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_SalesRep", "Profile\\AccountOverview.properties", cnt), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 1, i+11),"Verify the Content Name");
			log.info("Verify Headin 1");

			sa.assertTrue(gVar.assertVisible("AO_email_Link", "Profile\\AccountOverview.properties",cnt),"Verify the Email button");
		}



		sa.assertAll();
	}

	@Test(groups={"reg"},description="DMED-009 DRM-1505")
	public void TC02_accountclearkLink() throws Exception
	{

		int cnt = 0;
		if (gVar.mobileView()||gVar.tabletView())
		{
			cnt =2;
		}
		log.info("click on account clerical email link");
		l1.getWebElements("AO_email_Link", "Profile\\AccountOverview.properties").get(cnt).click();
		log.info("Verify the popup");
		sa.assertTrue(gVar.assertVisible("AO_ContactPopup", accountOverviewProperties),"Verify the popup");
		log.info("Verify Contact Heading");
		sa.assertTrue(gVar.assertVisible("AO_Contact_Heading", "Profile\\AccountOverview.properties"),"contact heading");
		log.info("Click on Close icon in popup window");
		l1.getWebElement("AO_ContactPopupCloseIcon", accountOverviewProperties).click();

		sa.assertAll();
	}

	@Test(groups={"reg"},description="DMED-009 DRM-1506")
	public void TC03_salesrepLink() throws Exception
	{
		int cnt = 1;
		if (gVar.mobileView()||gVar.tabletView())
		{
			cnt =3;
		}
		log.info("click on account clerical email link");
		l1.getWebElements("AO_email_Link", "Profile\\AccountOverview.properties").get(cnt).click();
		log.info("Verify the popup");
		sa.assertTrue(gVar.assertVisible("AO_ContactPopup", accountOverviewProperties),"Verify the popup");
		log.info("Verify Contact Heading");
		sa.assertTrue(gVar.assertVisible("AO_Contact_Heading", "Profile\\AccountOverview.properties"),"contact heading"); 
		log.info("Click on Close icon in popup window");
		l1.getWebElement("AO_ContactPopupCloseIcon", accountOverviewProperties).click();

		sa.assertAll();
	}

	@Test(groups={"reg"},description="DMED-009 DRM-1507,1508,1509,1510")
	public void TC04_contactUsUI() throws Exception
	{
		for (int j = 0; j < 2; j++)
		{
			int cnt = j;
			if (gVar.mobileView()||gVar.tabletView())
			{
				cnt =cnt+2;
			}
			log.info("click on mail icon");
			l1.getWebElements("AO_email_Link", "Profile\\AccountOverview.properties").get(cnt).click();

			log.info("Verify the POPUP");
			sa.assertTrue(gVar.assertVisible("AO_ContactPopup", accountOverviewProperties),"Verify the Contact Popup");
			log.info("Verifyu the POPUP heading");
			sa.assertEquals(gVar.assertEqual("AO_ContactPopup_Heading", accountOverviewProperties), 
					"Contact", "Popup heading");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_ContactPopup_StaticText", accountOverviewProperties), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 1, j+11),"verify Your Sales Representative");

			log.info("Verify the sales rep Name");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_ContactPopup_Name", accountOverviewProperties), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 2, j+11),"verify Rep Name");

			log.info("Verify the prepopulated Name");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_ContactPopupNameTextbox", accountOverviewProperties,"value"), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "AdminUser", 1, 7),"verify prepoluted Name");

			log.info("Verify the prepoluted email id");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("AO_ContactPopupEmailTextbox", accountOverviewProperties,"value"), 
					BaseTest.xmlTest.getParameter("email"),"verify prepoluted  Email");

			sa.assertTrue(gVar.assertVisible("AO_ContactPopup_Textbox", accountOverviewProperties),"Verify the text area box");
			sa.assertFalse(true,"place holder text is not displaying in AO_ContactPopup_Textbox");
			sa.assertEquals(gVar.assertEqual("AO_ContactPopup_CancelButton", accountOverviewProperties), 
					"CANCEL","Verify the CANCEL button");
			sa.assertEquals(gVar.assertEqual("AO_ContactPopup_SubmitButton", accountOverviewProperties), 
					"SUBMIT","Verify the Submit button");
			List<WebElement> allLabels = l1.getWebElements("AO_ContactPopup_Labels", accountOverviewProperties);
			log.info("allLabels:- "+ allLabels.size());
			for (int i = 0; i < allLabels.size(); i++)
			{
				log.info("LOOP:- "+ i);
				sa.assertEquals(allLabels.get(i).getText().trim(), 
						GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 13),"Verify the labels");
			}

			log.info("Click on CANCEL button");
			l1.getWebElement("AO_ContactPopup_CancelButton",accountOverviewProperties).click();

			log.info("Contact Us popup should not display");
			sa.assertFalse(gVar.assertVisible("AO_ContactPopup", accountOverviewProperties),"Verify the popup");

		}

		sa.assertAll();
	}

	@Test(groups={"reg"}, description="DMED-009 DRM-1513")
	public void TC05_navigationOfSuccessfullSubmitionOfContactUsForm() throws Exception
	{
		for (int i = 0; i < 2; i++)
		{
			int cnt = i;
			if (gVar.mobileView()||gVar.tabletView())
			{
				cnt =cnt+2;
			}
			log.info("LOOP:- "+ i);
			log.info("click on mail icon");
			l1.getWebElements("AO_email_Link", "Profile\\AccountOverview.properties").get(cnt).click();

			log.info("Verify the POPUP");
			sa.assertTrue(gVar.assertVisible("AO_ContactPopup", accountOverviewProperties),"Verify the Contact Popup");

			log.info("Enter MESSAGE");
			l1.getWebElement("AO_ContactPopup_Textbox", accountOverviewProperties).sendKeys("Testing Contact Form");

			log.info("Click on SUBMIT button");
			l1.getWebElement("AO_ContactPopup_SubmitButton", accountOverviewProperties).click();
			if (gVar.ieBrowser())
			{
				Thread.sleep(10000);
			}
			else
			{
				Thread.sleep(2000);
			}

			log.info("Contact Popup should not display");
			sa.assertFalse(gVar.assertVisible("AO_ContactPopup", accountOverviewProperties),"Contact Popup should not display");

		}
		sa.assertAll();
	}

	@Test(groups={"reg", "sanity_reg"}, description="extraTC- NAvigate To My Account links from header")
	public void TC06_verifyNavigateToMyAccountLinks() throws Exception
	{

		if (gVar.desktopView())
		{
			log.info("Mousehover on My Account Link in Header");
			WebElement myAccountLink = l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
			gVar.mouseHover(myAccountLink);
			Thread.sleep(500);
			log.info("Collect all My Account Links in header");
			List<WebElement> allMyAccountLinks = l1.getWebElements("Header_AllMyAccountLinks", "Shopnav//header.properties");
			log.info("allMyAccountLinks size:- "+ allMyAccountLinks.size());

			for (int i = 0; i < allMyAccountLinks.size(); i++)
			{
				log.info("LOOP:- "+ i);
				log.info("Verify the Link");
				String linkText = allMyAccountLinks.get(i).getText();
				log.info("linkText:- "+ linkText);
				String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 1);
				gVar.assertequalsIgnoreCase(linkText, expected, "Verify the link");
				if (!linkText.equalsIgnoreCase("Sign Out"))
				{
					log.info("Click on Link");
					allMyAccountLinks.get(i).click();

					Thread.sleep(2000);
					log.info("Verify the navigated page heading");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccountPageHeading", "Profile//MyAccount.properties"), 
							expected,"Verify the Page Heading");

					log.info("Verify the breadcrumb");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
							expected,"Verify the active bread crumb");
					log.info("Verify the Full breadcrumb");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
							"Home"+" "+expected,"Verify the full bread crumb");

					log.info("Verify the Selected link in Left Nav section");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_LeftNav_ActiveLink", "Profile//MyAccount.properties"), 
							linkText,"Verify the selected link in leftnav section");

					Thread.sleep(3000);
					myAccountLink = l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties");
					gVar.mouseHover(myAccountLink);
					Thread.sleep(500);

					log.info("Collect all My Account Links in header");
					allMyAccountLinks = l1.getWebElements("Header_AllMyAccountLinks", "Shopnav//header.properties");
				}
			}
		}

		sa.assertAll();
	}


	@Test(groups={"reg"}, description="extraTC-Click on My Account Links IN Hamburgar menu in")
	public void TC07_varifyMyAccountLinksNavigationInMobileView() throws Exception
	{
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Click on Hamburgar icon");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();

			s.expandMyAccountSection();
			List<WebElement> allLinksInHamburgarMenu = l1.getWebElements("MyAccount_Links_HamburgarMenu", "Profile//MyAccount.properties");
			log.info("allLinksInHamburgarMenu size:- "+ allLinksInHamburgarMenu.size());

			for (int i = 0; i < allLinksInHamburgarMenu.size(); i++)
			{
				log.info("LOOP:- "+ i);
				String linkText = allLinksInHamburgarMenu.get(i).getText();
				log.info("linkText:- "+ linkText);

				if (!linkText.equalsIgnoreCase("Sign Out"))
				{
					allLinksInHamburgarMenu.get(i).click();
					Thread.sleep(2000);

					log.info("Verify the NAvigation");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccountPageHeading", "Profile//MyAccount.properties"), 
							linkText,"Verify the Page Heading");
					log.info("Verify the Bread crumb");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
							linkText,"Verify the active bread crumb");
					log.info("Verify the Full breadcrumb");
					gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), 
							"Home"+" "+linkText,"Verify the full bread crumb");

					log.info("Click on Hamburgar icon");
					l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
					s.expandMyAccountSection();
					allLinksInHamburgarMenu = l1.getWebElements("MyAccount_Links_HamburgarMenu", "Profile//MyAccount.properties");
				}

			}
		}

		sa.assertAll();
	}


	@Test(groups={"reg"}, description="extraTC-Click on My Account Links in LeftNAv section")
	public void TC08_verifyMyAccountLinksInLeftNavSection() throws Exception
	{
		if (gVar.desktopView())
		{
			log.info("Navigate to My Account page");
			l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties").click();
			Thread.sleep(3000);
			log.info("Collect the links in Left Nav section");
			List<WebElement> allLinks = l1.getWebElements("MyAccount_LeftNAvLinks", "Profile//MyAccount.properties");
			log.info("allLinks size:- "+ allLinks.size());
			for (int i = 0; i < allLinks.size(); i++)
			{
				log.info("LOOP:- "+i);
				allLinks = l1.getWebElements("MyAccount_LeftNAvLinks", "Profile//MyAccount.properties");
				String linkText = allLinks.get(i).getText();
				log.info("linkText:- "+ linkText);

				log.info("Click on link");
				allLinks.get(i).click();
				Thread.sleep(2000);

				log.info("Verify the Navigated page heading");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccountPageHeading", "Profile//MyAccount.properties"), 
						linkText,"Verify the Page Heading");
				String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 1);
				gVar.assertequalsIgnoreCase(linkText, expected, "Verify the link");
				sa.assertEquals(linkText, expected,"Verify the heading with Excel data");
				log.info("Verify the breadcrumb");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
						linkText,"Verify the active bread crumb");
				log.info("Verify the Full breadcrumb");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
						"Home"+" "+linkText,"Verify the full bread crumb");
			}
		}

		sa.assertAll();
	}

}
