package com.drivemedical.profile;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.Data;
import com.drivemedical.utilgeneric.GetData;

public class SignIn extends BaseTest
{
	
	String emailId;
	String myAccountProperties = "Profile//MyAccount.properties";
	@Test(groups={"reg"})
	public void TC00_Signin(XmlTest xmlTest) throws Exception
	{
		log.info("Click on Sign In Link");
		p.navigateToLoginPage();
		//verify UI
		log.info("Verify heading");
		sa.assertTrue(gVar.assertVisible("Login_Heading", "Profile\\login.properties"),"Verify Login_Heading");
		sa.assertEquals(gVar.assertEqual("Login_Heading", "Profile\\login.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 1, 2),"Verify the Login heading");
		log.info("Verify required text");
		sa.assertTrue(gVar.assertVisible("Login_ReqField_Text", "Profile\\login.properties"),"Verify Login_ReqField_Text");
		log.info("Verify username label");
		sa.assertTrue(gVar.assertVisible("Login_UserName_Text", "Profile\\login.properties"),"Verify Login_UserName_Text");
		log.info("Verify username textbox");
		sa.assertTrue(gVar.assertVisible("Login_Email_TextBox", "Profile\\login.properties"),"Verify Login_Email_TextBox");
		log.info("Verify password label");
		sa.assertTrue(gVar.assertVisible("Login_Password_Text", "Profile\\login.properties"),"Verify Login_Password_Text");
		log.info("Verify password textbox");
		sa.assertTrue(gVar.assertVisible("Login_Password_Box", "Profile\\login.properties"),"Verify Login_Password_Box");
		log.info("Verify sign in button");
		sa.assertTrue(gVar.assertVisible("Login_Login_Btn", "Profile\\login.properties"),"Verify Login_Login_Btn");
		log.info("Verify forgot password link");
		sa.assertTrue(gVar.assertVisible("Login_ForgotPwd_Link", "Profile\\login.properties"),"Verify Login_ForgotPwd_Link");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},dataProvider="loginVal",dataProviderClass=Data.class,description="OOTB-055 DRM-593,594 OOTB-059 DRM-627 DRM-628 DRM-631")
	public void TC01_SignInValidation(String un,String pwd) throws Exception
	{
		log.info("Verify validations");
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").clear();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys(un);
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").clear();
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys(pwd);
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		Thread.sleep(5000);
		if(i==0 || i==1 || i==2 || i==3 || i==4)
		{
			log.info("Verify error message");
			sa.assertTrue(gVar.assertVisible("Login_ErrorMsg_Text", "Profile\\login.properties"),"Verify Login_ErrorMsg_Text");
			String expText=GetData.getDataFromExcel("//data//GenericData_US.xls", "Validation", 1, 3);
			sa.assertTrue(l1.getWebElement("Login_ErrorMsg_Text", "Profile\\login.properties").getText().contains(expText));
		} 
		else if(i==5) 
		{
			log.info("User should be logged in");
			if (gVar.mobileView()||gVar.tabletView())
			{
				sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link_Mobile", "Shopnav//header.properties"),"Verify My Account link in header");
			}
			else
			{
				sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav//header.properties"),"Verify My Account link in header");
			}
		}
		
		++i;
		System.out.println(i);
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-054 DRM-545 DRM-546 DRM-547")
	public void TC02_ForgotPWD_UI(XmlTest xmlTest) throws Exception 
	{
		log.info("Log out from application");
		p.logout(BaseTest.xmlTest);
		
		log.info("Click on Sign In Link");
		p.navigateToLoginPage();
		log.info("click on forgot password link");
		l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").click();
		log.info("verify overlay is opened or not and UI");
		log.info("Reset password overlay heading");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_ResetPwd_Heading", "Profile\\login.properties"),"Verify ForgotPwd_ResetPwd_Heading");
		log.info("Reset password text");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Text", "Profile\\login.properties"),"Verify ForgotPwd_Text");
		log.info("Reset password email label text");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_EmailLbl_Text", "Profile\\login.properties"),"Verify ForgotPwd_EmailLbl_Text");
		log.info("Reset password email textbox");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Email_Textbox", "Profile\\login.properties"),"Verify ForgotPwd_Email_Textbox");
		log.info("Reset password submit button");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_ResetPwd_Btn", "Profile\\login.properties"),"Verify ForgotPwd_ResetPwd_Btn");
		log.info("Reset password close link");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Close_icon", "Profile\\login.properties"),"Verify ForgotPwd_Close_icon");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-054")
	public void TC03_forgot_Pwd_Close() throws Exception 
	{
		log.info("click on close link");
		Thread.sleep(3000);
		l1.getWebElement("ForgotPwd_Close_icon", "Profile\\login.properties").click();
		log.info("Verify forgot password overay is closed");
		Thread.sleep(3000);
		
		String style = gVar.assertEqual("ForgotPwd_Content", "Profile\\login.properties","style");
		log.info("style:- "+style);
		sa.assertTrue(style.contains("display: none"),"POPUP should close");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-054 DRM-549")
	public void TC04_Forgot_Pwd_Success_UI(XmlTest xmlTest) throws Exception
	{
		log.info("open fogot password overlay");
		l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").click();
		log.info("enter valid email id");
		emailId=GetData.getDataFromExcel("//data//GenericData_US.xls", "Login",1,0);
		l1.getWebElement("ForgotPwd_Email_Textbox", "Profile\\login.properties").sendKeys(emailId);
		log.info("click on reset password button");
		l1.getWebElement("ForgotPwd_ResetPwd_Btn","Profile\\login.properties").click();
		log.info("verisy success pop up and UI");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_ResetPwd_Heading", "Profile\\login.properties"),"Verify ForgotPwd_ResetPwd_Heading");
		log.info("Success Text");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Success_Text", "Profile\\login.properties"),"Verify ForgotPwd_Success_Text");
		log.info("Success Text1");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Success1_Text", "Profile\\login.properties"),"Verify ForgotPwd_Success1_Text");
		log.info("close button");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Close_btn", "Profile\\login.properties"),"Verify ForgotPwd_Close_btn");
		log.info("close icon");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Close_icon", "Profile\\login.properties"),"Verify ForgotPwd_Close_icon");
		sa.assertAll();
	}
	
	@Test(groups={"reg"})
	public void TC05_Forgot_Success_Close(XmlTest xmlTest) throws Exception
	{
		log.info("click on close button");
		l1.getWebElement("ForgotPwd_Close_btn", "Profile\\login.properties").click();
		
		log.info("Reset Password Over should close");
		String style = gVar.assertEqual("ForgotPwd_Content", "Profile\\login.properties","style");
		log.info("style:- "+style);
		sa.assertTrue(style.contains("display: none"),"forgot password Overlay should be closed");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-054 DRM-548 551")
	public void TC06_Forgot_Pwd_InvalidEmail() throws Exception
	{
		log.info("Open forgot password overlay");
		l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").click();
		log.info("Enter Invalid Email");
		emailId=GetData.getDataFromExcel("//data//GenericData_US.xls", "Login",2,0);
		l1.getWebElement("ForgotPwd_Email_Textbox", "Profile\\login.properties").sendKeys(emailId);
		log.info("click on reset password button");
		l1.getWebElement("ForgotPwd_ResetPwd_Btn","Profile\\login.properties").click();
		log.info("verify error message");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Error_Msg", "Profile\\login.properties"),"forgot pwd error message");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("ForgotPwd_Error_Msg", "Profile\\login.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "Login", 1, 6),"Verify the Error messgae");
		
		log.info("Close the forgotpassword popup popup");
		if (gVar.assertVisible("ForgotPwd_Close_icon", "Profile\\login.properties"))
		{
			l1.getWebElement("ForgotPwd_Close_icon", "Profile\\login.properties").click();
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-058 DRM-542")
	public void TC07_signInToTheApplication(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("Login with valid credentials");
		p.logIn(xmlTest);
		Thread.sleep(3000);
		if (gVar.mobileView()||gVar.tabletView())
		{
			sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link_Mobile", "Shopnav//header.properties"),"Verify the My Account heading in header");
		}
		else
		{
			sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav//header.properties"),"Verify the My Account heading in header");
		}
		
		
		sa.assertAll();
	}

	// **** IF it fails, Execute it in DEV 
	@Test(groups={"reg"}, description="OOTB-058 DRM-1043 DRM-1044 DRM-544")
	public void TC08_UiOFMyaccountDetails() throws Exception
	{
		log.info("Navigate to Account Overview page");
		s.navigateToAccountOverviewPage();
		
		log.info("Verify the Account Overview page");
		String accountOverviewHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "AccountOverview", 1, 0);
		
		String heading = gVar.assertEqual("AccountOverview_heading", "Profile//AccountOverview.properties");
		heading = heading.substring(0, accountOverviewHeading.length()).trim();
		sa.assertEquals(heading,
				accountOverviewHeading,"Verify the Heading");
		
//		+ ------- + ------- + ------- + Account Summary & Invoicing SECTION + ------- + ------- + ------- + 
		sa.assertEquals(gVar.assertEqual("MyAccount_AccountOverview_Heading", "Profile//MyAccount.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 1, 3),"Verify the Account OverView section");
		sa.assertEquals(gVar.assertEqual("MyAccount_AccountOverview_Subheading", "Profile//MyAccount.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 1, 4),"Account overview sub heading");
		List<WebElement> allHeadings = l1.getWebElements("MyAccount_AccountOverview_TableHeading", "Profile//MyAccount.properties");
		int tableHeadingSize = allHeadings.size()-1;
		log.info("allHeadings:-"+ tableHeadingSize);
		for (int i = 0; i < tableHeadingSize; i++)
		{
			log.info("LOOP:- "+i);
			String tableHeading = allHeadings.get(i).getText();
			String expectedTableHeading = GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 5);
			gVar.assertequalsIgnoreCase(tableHeading, 
					expectedTableHeading,"Verify the Table Heading");
		}
		log.info("Count the number of rows");
		List<WebElement> allRows = l1.getWebElements("MyAccount_AccountOverview_TableRows", "Profile//MyAccount.properties");
		log.info("allRows size:- "+ allRows.size());
		List<WebElement> viewPdfLink;
		if (gVar.mobileView()||gVar.tabletView())
		{
			viewPdfLink = l1.getWebElements("MyAccount_AccountOverview_ViewPDFLink_MObile", "Profile//MyAccount.properties");
		}
		else
		{
			viewPdfLink = l1.getWebElements("MyAccount_AccountOverview_ViewPDFLink_Desktop", "Profile//MyAccount.properties");
		}
		
		sa.assertEquals(allRows.size(), viewPdfLink.size(),"Verify the count of rows and view PDF link");
		String viewPdfLinkText = GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 2, 6);
		for (int i = 0; i < allRows.size(); i++)
		{
			log.info("LOOP:-"+ i);
			String linkText = viewPdfLink.get(i).getText();
			gVar.assertequalsIgnoreCase(linkText, viewPdfLinkText,"Verify the View PDF link text");
		}
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_AccountOverview_ViewAccountSummary", "Profile//MyAccount.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 1, 6),"Verify the View Account Summary link");
		sa.assertTrue(gVar.assertVisible("AccountOverview_DownloadPriceSheet_Link", "Profile//AccountOverview.properties"), "Verify the Donload link");
		
		
		String downloadLinkText = GetData.getDataFromExcel("//data//GenericData_US.xls", "AccountOverview", 1, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountOverview_DownloadPriceSheet_Link", "Profile//AccountOverview.properties"), 
				downloadLinkText, "Download Price Sheet link text");
		
//		 + ------- + ------- + ------- +  Order History SECTION + ------- + ------- + ------- + 
		String OrderHistoryText = GetData.getDataFromExcel("//data//GenericData_US.xls", "AccountOverview", 2, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountOverview_OrderHistory_Heading", "Profile//AccountOverview.properties"), 
				OrderHistoryText,"Verify the Order History heading");
		
		List<WebElement> orderHistoryTableHeadings = l1.getWebElements("MyAccount_OrderHistory_TableHeadings", "Profile//MyAccount.properties");
		log.info("orderHistoryTableHeadings size:- "+ orderHistoryTableHeadings.size());
		for (int i = 0; i < orderHistoryTableHeadings.size(); i++)
		{
			String orderHistoryTableHeading = orderHistoryTableHeadings.get(i).getText();
			gVar.assertequalsIgnoreCase(orderHistoryTableHeading, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 7),"Verify the table heading");
		}
		
		String viewOrdersLinkText = GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 8, 6);
		sa.assertEquals(gVar.assertEqual("AccountOverview_ViewOrders_Link", "Profile//AccountOverview.properties"), 
				viewOrdersLinkText,"Verify the View orders link");
		
		sa.assertEquals(l1.getWebElements("AccountOverview_OrderItemsRow", "Profile//AccountOverview.properties").size(), 3,"Verify the number of order items row");
		
//		+ ------- + ------- + ------- + Manage Users Section + ------- + ------- + ------- + 
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_ManageUsersHeading", "Profile//MyAccount.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 2, 3),"Manage Users heading");
		
		List<WebElement> manageUsersTableHeadings = l1.getWebElements("MyAccount_ManageUsers_TableHeadings", "Profile//MyAccount.properties");
		log.info("manageUsersTableHeadings size:- "+ manageUsersTableHeadings.size());
		for (int i = 0; i < manageUsersTableHeadings.size()-1; i++)
		{
			log.info("LOOP:-"+i);
			String manageUsersTableHeading = manageUsersTableHeadings.get(i).getText();
			gVar.assertequalsIgnoreCase(manageUsersTableHeading, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 8),"ManageUsers Table Heading");
		}
		
		List<WebElement> manageUsersTableRows = l1.getWebElements("MyAccount_ManageUsers_TableRows", "Profile//MyAccount.properties");
		log.info("manageUsersTableRows size:- "+ manageUsersTableRows.size());
		List<WebElement> manageUsersEditLinks = l1.getWebElements("MyAccount_ManageUsers_EditLinks", "Profile//MyAccount.properties");
		log.info("manageUsersEditLinks size:- "+ manageUsersEditLinks.size());
		sa.assertEquals(manageUsersTableRows.size(), manageUsersEditLinks.size(),"Verify the number of links");
		for (int i = 0; i < manageUsersEditLinks.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String manageUsersEditLink = manageUsersEditLinks.get(i).getText();
			gVar.assertequalsIgnoreCase(manageUsersEditLink, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 3, 6),"Verify the EDIT link");
		}
		log.info("Verify the number of rows in Manage User section");
		sa.assertEquals(l1.getWebElements("MyAccount_ManageUsers_TableRows", myAccountProperties).size(), 3, "Verify the number of rows in MAnage User section");
		
		
//		+ ------- + ------- + ------- + QUOTES Section + ------- + ------- + ------- + 
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_QuoteHeading", "Profile//MyAccount.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 5, 3),"Verify the Quote section heading");
		log.info("Verify the Quote section table heading");
		List<WebElement> quoteTableHeadings = l1.getWebElements("MyAccount_QuoteTableHeadings", "Profile//MyAccount.properties");
		log.info("quoteTableHeadings size:- "+ quoteTableHeadings);
		for (int i = 0; i < quoteTableHeadings.size(); i++)
		{
			log.info("LOOP:- "+ i);
			String th = quoteTableHeadings.get(i).getText();
			gVar.assertequalsIgnoreCase(th, 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", i+1, 9),"Verify the quote section table headings");
		}
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_ViewAllQuotesLink", "Profile//MyAccount.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 4, 6),"View All Quotes link");
		
		sa.assertEquals(l1.getWebElements("MyAccount_QuoteLinks", "Profile//MyAccount.properties").size(), 
				3,"Verify the number of quote links");
		
		sa.assertAll();
	}
	
//	###############################################################################################
//	+ ------- + ------- + ------- + EXTRA TESTCASES + ------- + ------- + ------- + 
//	###############################################################################################
	@Test(groups={"reg"}, description="extraTC-Click on VIEW PDF links in Account Overview Section")
	public void TC09_viewPDFlinkFunctionalityInAccountOverviewSection() throws Exception
	{
		log.info("Collect the Document number");
		String docNum = l1.getWebElement("MyAccount_AccountOverview_DocumentNumber","Profile//MyAccount.properties").getText();
		log.info("docNum:-"+ docNum);
		log.info("Collect the parent window identifier");
		String parent = driver.getWindowHandle();
		
		log.info("Click on View PDF link in Account Overview Section");
		if (gVar.mobileView()||gVar.tabletView())
		{
			log.info("Click on Details link");
			l1.getWebElement("MyAccount_AccountOverview_DetailsLink", "Profile//MyAccount.properties").click();
			l1.getWebElement("MyAccount_AccountOverview_ViewPDFLink_MObile", "Profile//MyAccount.properties").click();
		}
		else
		{
			l1.getWebElement("MyAccount_AccountOverview_ViewPDFLink_Desktop", "Profile//MyAccount.properties").click();
		}
		Thread.sleep(3000);
		log.info("Collect the windows");
		Set<String> allWindows = driver.getWindowHandles();
		log.info("allWindows size:- "+ allWindows.size());
		
		if (allWindows.size()>1)
		{
			Iterator<String> itr = allWindows.iterator();
			String parentWindow = itr.next();
			String childWindow = itr.next();
			log.info("Switch to Child window");
			driver.switchTo().window(childWindow);
			Thread.sleep(4000);
			String childUrl = driver.getCurrentUrl();
			log.info("childUrl:- "+ childUrl);
			
			log.info("Verify the URL");
			String homePageUrl = GetData.getDataFromProperties("//data//config.properties", "URL");
			sa.assertTrue(childUrl.contains(homePageUrl),"Verify the url");
			sa.assertTrue(childUrl.contains(docNum),"Verify the doc number in url");
			
			log.info("Colose the window");
			driver.close();
			
			log.info("Switch to parent window");
			driver.switchTo().window(parent);
		}
		else
		{
			sa.assertFalse(true,"New window is not opened on click on VIEW PDF link in AccountOverview section in My Account page");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-Click on Edit link in Manage Users section")
	public void TC10_editLinkFunctionalityInManageUserSection() throws Exception
	{
		log.info("Click on EDIT link in Manage Users section");
		l1.getWebElement("MyAccount_ManageUsers_EditLinks", "Profile//MyAccount.properties").click();
		Thread.sleep(2000);
		log.info("Verify the Edit popup");
		sa.assertTrue(gVar.assertVisible("MyAccount_ManageUsers_EditPopup", myAccountProperties),"Verify the popup");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_ManageUsers_EditPopup_Heading", myAccountProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 3, 3), "Verify the Popup heading");
		
		sa.assertTrue(gVar.assertVisible("MyAccount_ManageUsers_EditPopup_CloseIcon", myAccountProperties),"Verify the Close link");
		
		log.info("Click on CLose Icon");
		l1.getWebElement("MyAccount_ManageUsers_EditPopup_CloseIcon", myAccountProperties).click();
		
		log.info("Popup should not display");
		sa.assertTrue(gVar.assertNotVisible("MyAccount_ManageUsers_EditPopup", myAccountProperties),"Popup should not display");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTc-PrePopulated Data In Edit Popup in Manage Users section")
	public void TC11_prePopulatedDataInEditPopupInManageUserSection() throws Exception
	{
		List<WebElement> allEditLinks = l1.getWebElements("MyAccount_ManageUsers_EditLinks", myAccountProperties);
		List<WebElement> allNames = l1.getWebElements("MyAccount_ManageUsers_Name", myAccountProperties);
		List<WebElement> allRoles = l1.getWebElements("MyAccount_ManageUsers_Roles", myAccountProperties);
		
		for (int k = 0; k < allEditLinks.size(); k++)
		{
			log.info("LOOP k:-"+ k);
			
			log.info("Collect the name");
//			String name = l1.getWebElement("MyAccount_ManageUsers_Name", myAccountProperties).getText();	
			String name = allNames.get(k).getText();
			log.info("name:- "+ name);
			String[] nameArray = name.trim().split(" ");
			
//			String roles = l1.getWebElement("MyAccount_ManageUsers_Roles", myAccountProperties).getText();
			String roles = allRoles.get(k).getText();
//			log.info("roles:- "+ roles);
			String[] rolesArray = roles.split(",");
			
			log.info("rolesArray:- "+ Arrays.toString(rolesArray));
			//Store the Array values in Arraylist
			ArrayList<String> role = new ArrayList<String>();
			for (int j = 0; j < rolesArray.length; j++)
			{
				role.add(rolesArray[j].trim());
			}
			log.info("role:- "+ role);
			
			log.info("Click on EDIT link in Manage Users section");
//			l1.getWebElement("MyAccount_ManageUsers_EditLinks", myAccountProperties).click();
			allEditLinks.get(k).click();
			
			Thread.sleep(2000);
			log.info("Verify the Edit popup");
			sa.assertTrue(gVar.assertVisible("MyAccount_ManageUsers_EditPopup", myAccountProperties),"Verify the popup");
			
			log.info("Verify the pre populated name in popup");
			sa.assertEquals(gVar.assertEqual("MyAccount_ManageUsers_EditPopup_FNbox", myAccountProperties,"value"), 
					nameArray[0].trim(),"Verify the pre-populated name");
			
			List<WebElement> checkedLabels = l1.getWebElements("MyAccount_ManageUsers_EditPopup_CheckedBox", myAccountProperties);
			int numberOfCheckedBox = checkedLabels.size();
			log.info("numberOfCheckedBox:- "+ numberOfCheckedBox);
			ArrayList<String> allLabels = new ArrayList<String>();
			for (int i = 0; i < numberOfCheckedBox; i++)
			{
				log.info("LOOP:- "+ i);
				String label = checkedLabels.get(i).getText();
				log.info("label:- "+ label);
				
				allLabels.add(label);
			}
			log.info("allLabels:- "+ allLabels);
			sa.assertEquals(role.size(), allLabels.size(),"Verify the labels count");
			log.info("Sort the array");
			Collections.sort(role);
			log.info("rolesArray after sort:- "+ role);
			log.info("Sort the Array list");
			Collections.sort(allLabels);
			log.info("allLabels After sort:- "+ allLabels);
			sa.assertEquals(role, allLabels,"Verify the labels");
			
			log.info("Click on Cancel button");
			l1.getWebElement("MyAccount_ManageUsers_EditPopup_CancelBUtton", myAccountProperties).click();
			log.info("POPup should not display");
			sa.assertTrue(gVar.assertNotVisible("MyAccount_ManageUsers_EditPopup", myAccountProperties),"Popup should not display");
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Verify view all link in Account Overview")
	public void TC12_viewAllLinkFunctionalityInAccountOverview() throws Exception
	{
		log.info("Click view all link in Account Overview");
		l1.getWebElement("MyAccount_AccountOverview_ViewAccountSummary", myAccountProperties).click();
		log.info("Verify the navigated page");
		gVar.waitForElement("MyAccount_AccountSummarySection_Heading", myAccountProperties, 6);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_AccountSummarySection_Heading", myAccountProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 1, 3),"Verify the page heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Verify view all link in Order History")
	public void TC13_viewAllLinkFunctionalityInOrderHistory() throws Exception
	{
		log.info("Navigate to My Account page");
		s.navigateToMyAccountPage();
		
		log.info("Click view all link in Order History");
		l1.getWebElement("AccountOverview_ViewOrders_Link", "Profile//AccountOverview.properties").click();
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_Section_Header", "Profile//OrderHistory.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 4, 3),"Verify the page heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Verify view all link in Manage Users")
	public void TC14_viewAllLinkFunctionalityInManageUsers() throws Exception
	{
		log.info("Navigate to My Account page");
		s.navigateToMyAccountPage();
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_ManageUsers_ViewAllLink", myAccountProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 6, 6),"Verify the View All Users link");
		log.info("Click view all link in Manage Users");
		l1.getWebElement("MyAccount_ManageUsers_ViewAllLink", myAccountProperties).click();
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_ManageUsers_Heading", myAccountProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 2, 3),"Verify the page heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Verify view all link in Quotes section")
	public void TC15_viewAllLinkFunctionalityInQuotes() throws Exception
	{
		log.info("Navigate to My Account page");
		s.navigateToMyAccountPage();
		
		log.info("Click view all link in Quotes section");
		l1.getWebElement("MyAccount_ViewAllQuotesLink", myAccountProperties).click();
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quotes_Heading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 5, 3),"Verify the page heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Navigation on click on quote link")
	public void TC16_quoteLinkNavigation() throws Exception
	{
		log.info("Navigate to My Account page");
		s.navigateToMyAccountPage();
		
		String quoteName = l1.getWebElement("MyAccount_QuoteLinks", myAccountProperties).getText();
		log.info("quoteName:- "+ quoteName);
		
		log.info("Click on Quotes link");
		l1.getWebElement("MyAccount_QuoteLinks", myAccountProperties).click();
		
		log.info("Verify the Quote details page");
		gVar.assertEqual(gVar.assertEqual("QuoteDetails_Heading", "Profile//Quotes.properties"), 
				quoteName,"Verify the page heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Navigation on click on orders link")
	public void TC17_OrderLinkNavigation() throws Exception
	{
		log.info("Navigate to My Account page");
		s.navigateToMyAccountPage();
		
		String orderId = l1.getWebElement("MyAccount_OrderLink",myAccountProperties).getText();
		log.info("Click on Order link");
		l1.getWebElement("MyAccount_OrderLink",myAccountProperties).click();

		log.info("Verify the Order details page heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Heading", "Profile//OrderHistory.properties"), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "OrderHistory", 2, 0),"Verify the Order details heading");
		
		log.info("Verify the bread crumb");
		sa.assertEquals(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties").replaceAll("[^0-9]", ""), 
				orderId,"Verify the order Id in breadcrumb");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="extraTC-Download Price Sheet Link functionality")
	public void TC18_DownloadPriceSheetLinkFunctionality() throws Exception 
	{
		log.info("Navigate to My Account page");
		s.navigateToMyAccountPage();
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MyAccount_DownloadPriceSheetLink", myAccountProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "My Account", 7, 6),"Verify the Download Price Sheet link");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-055-DRM-596")
	public void TC19_leftNavSignOut(XmlTest xmlTest) throws Exception
	{
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			log.info("This Testcase is not for mobile view");
		}
		else
		{
			log.info("Navigate to My account page");
			s.navigateToMyAccountPage();
			sa.assertTrue(gVar.assertVisible("Header_SignIn_LINK", "Shopnav\\header.properties"),"Verify the Signin link");
			log.info("sign out from left nav");
			l1.getWebElement("Leftnav_SignOut_Link", "Profile\\leftnav.properties").click();
			log.info("Verify navigation back to home page");
			sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav\\header.properties"),"Verify the Home page");

		}

		sa.assertAll();
	}
	
}
