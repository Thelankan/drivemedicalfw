package com.drivemedical.profile;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CommunicationPreferences extends BaseTest
{
	String CommunicationPreferencesProperties = "Profile//CommunicationPreferences.properties";
	@Test(groups={"reg"}, description="DMED-047 DRM-1499")
	public void TC00_varifyTheUiOfTheForm(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("LOgin with valid credentials");
		p.logIn(xmlTest);
		
		log.info("Navigate to Communication Preferences page");
		s.navigateToCommunicationPreferencesPage();
		
		log.info("Verify the page heading");
		String expected = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 1, 0);
		sa.assertEquals(gVar.assertEqual("CommunicationPreferences_Heading", "Profile//CommunicationPreferences.properties"), 
				expected, "Verify the heading");
		
		log.info("Verify the page breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				expected,"Verify the breadcrumb");
		
		log.info("Switch the driver controller from window to iframe");
		WebElement iframe = l1.getWebElement("CommPreferences_Iframe", CommunicationPreferencesProperties);
		driver.switchTo().frame(iframe);
		
		log.info("Verify the labels");
		int rowNum=1;
		List<WebElement> allLAbels = l1.getWebElements("CommPreferences_FormLabels", CommunicationPreferencesProperties);
		for (int i = 0; i < 8; i++)
		{
			log.info("LOOP:- "+ i);
			sa.assertEquals(allLAbels.get(i).getText(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", rowNum, 1),"Verify the label");
			rowNum++;
		}
		
		sa.assertTrue(gVar.assertVisible("CommPreferences_AccountNumberBox", CommunicationPreferencesProperties),"Verify CommPreferences_AccountNumberBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_companyBox", CommunicationPreferencesProperties),"Verify CommPreferences_companyBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_FirstNameBox", CommunicationPreferencesProperties),"Verify CommPreferences_FirstNameBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_LastNameBox", CommunicationPreferencesProperties),"Verify CommPreferences_LastNameBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_phoneBox", CommunicationPreferencesProperties),"Verify CommPreferences_phoneBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_emailBox", CommunicationPreferencesProperties),"Verify CommPreferences_emailBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_SubscribeAnnouncementBox", CommunicationPreferencesProperties),"Verify CommPreferences_SubscribeAnnouncementBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_SubscribePromotionsBox", CommunicationPreferencesProperties),"Verify CommPreferences_SubscribePromotionsBox");
		sa.assertTrue(gVar.assertVisible("CommPreferences_SubmitButton", CommunicationPreferencesProperties),"Verify the submit button");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-047 extraTC- VAlidation")
	public void TC01_verifyValidateTheForm() throws Exception
	{
		log.info("Click on SUBMIT button without entering any values");
		l1.getWebElement("CommPreferences_SubmitButton", CommunicationPreferencesProperties).click();
		
		Thread.sleep(4000);
		log.info("Verify the Error message");
		sa.assertEquals(gVar.assertEqual("CommPreferences_ErrorMsg", CommunicationPreferencesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 1, 2),"Verify the Error message");
		
		List<WebElement> errorMessages = l1.getWebElements("CommPreferences_ErrorLabel", CommunicationPreferencesProperties);
		for (int i = 0; i < 7; i++)
		{
			sa.assertEquals(errorMessages.get(i).getText(), 
					GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 2, 2),"Verify the Error message- loop:-"+i);
		}
		
		sa.assertAll();
	}
	@Test(groups={"reg"}, description="DMED-047 extraTC- Verify Success message for valid data")
	public void TC02_verifySuccessMsg() throws Exception
	{
		log.info("Enter Valid data in all fields");
		String DriveAccount = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 1, 3);
		String company = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 2, 3);
		String fName = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 3, 3);
		String lName = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 4, 3);
		String phoneNum = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 5, 3);
		String emailAddress = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 6, 3);
		String subAnnouncement = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 7, 3);
		String subscribePromotions = GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 8, 3);
		
		log.info("Enter the valid data in all the fields");
		l1.getWebElement("CommPreferences_AccountNumberBox", CommunicationPreferencesProperties).sendKeys(DriveAccount);
		l1.getWebElement("CommPreferences_companyBox", CommunicationPreferencesProperties).sendKeys(company);
		l1.getWebElement("CommPreferences_FirstNameBox", CommunicationPreferencesProperties).sendKeys(fName);
		l1.getWebElement("CommPreferences_LastNameBox", CommunicationPreferencesProperties).sendKeys(lName);
		l1.getWebElement("CommPreferences_phoneBox", CommunicationPreferencesProperties).sendKeys(phoneNum);
		l1.getWebElement("CommPreferences_emailBox", CommunicationPreferencesProperties).sendKeys(emailAddress);
		l1.getWebElement("CommPreferences_SubscribeAnnouncementBox", CommunicationPreferencesProperties).sendKeys(subAnnouncement);
		l1.getWebElement("CommPreferences_SubscribePromotionsBox", CommunicationPreferencesProperties).sendKeys(subscribePromotions);
		
		log.info("Click on SUBMIT button");
		l1.getWebElement("CommPreferences_SubmitButton", CommunicationPreferencesProperties).click();;
		
		Thread.sleep(3000);
		log.info("Verify the Success message");
		sa.assertEquals(gVar.assertEqual("CommPreferences_SuccessMsg", CommunicationPreferencesProperties), 
				GetData.getDataFromExcel("//data//GenericData_US.xls", "CommunicationPreferences", 1, 4),"Verify the success message");
		
		log.info("Switch the controller from framre to window");
		driver.switchTo().defaultContent();
		
		
		sa.assertAll();
	}
}
