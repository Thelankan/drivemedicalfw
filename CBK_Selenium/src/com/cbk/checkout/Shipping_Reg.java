package com.cbk.checkout;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.CheckoutIlpPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.PaymentPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.projectspec.ProfileFunctions;
import com.cbk.projectspec.ShopnavFunctions;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.TestData;

public class Shipping_Reg extends BaseTest {
	
	String ProductNameinOrderSummarySection;
	String ProductNameinPDPage;
	String OS_Subtotal;
	String OS_ShippingValue;
	String OS_SalesTax;
	
	ShippingPOM shippingObjects;

	@Test(groups="{Regression}",description="124470,289178")
	public void TC00_Shippingpage() throws Exception
	{
		p.loginToAppliction(l1);
		checkout.NavigateToShippingPage();
		Thread.sleep(2000);
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_FN_TextBox),"Shipping First Name");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_LN_TextBox),"Shipping Last Name");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address1_TextBox),"Shipping Address1 Name");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address2_TextBox),"Shipping Address2 Name");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_City_TextBox),"City Name");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_ZipCode_TextBox),"Zipcode Textbox");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Country_Dropdown),"Country dropdown");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_State_Dropdown),"State dropdown");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Phone_TextBox),"phone textbox");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_SameasBilling_Checkbox),"checkbox shipping same as billing");
	    sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_FN_Label),"First Name Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_LN_Label),"Last Name Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address1_Label),"Address1 Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address2_Label),"Address2 Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_City_Label),"City Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_ZipCode_Label),"Zipcode Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Country_Label),"Country Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_State_Label),"State Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Phone_Label),"State Label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_SelectMethod_Heading),"SelectMethod_Heading");
		sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_SelectMethod_List),"SelectMethod_List");
		sa.assertAll();
			
	}	
		
	@Test(groups="{Regression}",description="148951,148950,148900,289178")
	public void TC01_ShippingpageBillingPrePopulate() throws Exception
	{
		
		log.info("Order summary Section UI");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingpaymentAccordion),"shippingpaymentAccordion");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderReviewAccordion),"shippingOrderReviewAccordion");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingSigninAccordionGuest),"shippingSigninAccordionGuest");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummaryQuantity),"order summary quantity");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummarysubtotalLabel),"order summary subtotal label");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummaryEditLink),"order summary Edit link");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummarysalestax),"order summary sales tax");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummaryOrderTotal),"order summary total price");
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummarOrderValue),"order summary order value");
		
		log.info("Clear Phone");
		l1.getWebElement(shippingObjects.ShippingPage_Phone_TextBox).clear();
		log.info("Enter shipping address");
		checkout.ShippingAddress("//data//Checkout.xls","ShippingAddress",1);
		
		log.info("Entering shipping address");
		gVar.SelectCheckbox(shippingObjects.ShippingPage_SameasBilling_Checkbox);
		
		
		log.info("Verifying the billing heading");
		l1.getWebElement(billingObjects.billingPage_Heading).isDisplayed();
		log.info("Verifying shipping and billing values");
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_FN_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,1));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_LN_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,2));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Address1_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,3));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Address2_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,4));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_City_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,5));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_ZipCode_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,6));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Country_Dropdown),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,7));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_State_Dropdown),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,8));
		sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Phone_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,9));
		log.info("Enter billing email address");
		l1.getWebElement(billingObjects.billingPage_Email_Textbox).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","BillingAddress", 1,10));
		l1.getWebElement(billingObjects.billingPage_continue_button).click();
		sa.assertAll();
		
	}


	@Test(groups="{Regression}",description="148951,148950,148900,289178")
	public void TC02_EnterpaymentdetailsAndPlaceOrder() throws Exception
	{

		log.info("entering Payment details");
		checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
		log.info("Click on continue to order details page");
		l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
		
	}
}
