package com.cbk.checkout;

import static org.testng.Assert.assertEquals;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.CheckoutIlpPOM;
import com.cbk.pom.OrderConfirmationPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.PaymentPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.projectspec.ProfileFunctions;
import com.cbk.projectspec.ShopnavFunctions;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.Data;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.TestData;

public class Shipping_Guest extends BaseTest {
	
	String ProductNameinOrderSummarySection;
	String ProductNameinCartPage;
	String OS_Subtotal;
	String OS_ShippingValue;
	String OS_SalesTax;
	String ShippingBillingbutton;
	
@Test(groups={"Regression"},description="124470,289178")
public void TC00_Shippingpage() throws Exception
{

log.info("entered in to test case");	
driver.findElement(By.xpath("//span[contains(text(),'Login')]")).click();	
	
checkout.NavigateToShippingPage();
Thread.sleep(2000);
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_FN_TextBox),"Shipping First Name");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_LN_TextBox),"Shipping Last Name");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address1_TextBox),"Shipping Address1 Name");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address2_TextBox),"Shipping Address2 Name");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_City_TextBox),"City Name");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_ZipCode_TextBox),"Zipcode Textbox");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Country_Dropdown),"Country dropdown");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_State_Dropdown),"State dropdown");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Phone_TextBox),"phone textbox");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_SameasBilling_Checkbox),"checkbox shipping same as billing");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_FN_Label),"First Name Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_LN_Label),"Last Name Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address1_Label),"Address1 Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Address2_Label),"Address2 Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_City_Label),"City Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_ZipCode_Label),"Zipcode Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Country_Label),"Country Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_State_Label),"State Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Phone_Label),"State Label");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_SelectMethod_Heading),"SelectMethod_Heading");
sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_SelectMethod_List),"SelectMethod_List");
sa.assertAll();
		
}	
	
/*@Test(groups={"Regression"},description="148951,148950,148900,289178")
public void TC01_ShippingpageBillingPrePopulate() throws Exception
{
	
log.info("Order summary Section UI");
sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummaryQuantity),"order summary quantity");
sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummarysubtotalLabel),"order summary subtotal label");
sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummaryEditLink),"order summary Edit link");
sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummarysalestax),"order summary sales tax");
sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummaryOrderTotal),"order summary total price");
sa.assertTrue(gVar.assertVisible(shippingObjects.shippingOrderSummarOrderValue),"order summary order value");
log.info("Enter shipping address");
checkout.ShippingAddress("//data//Checkout.xls","ShippingAddress",1);
Thread.sleep(2000);
log.info("Entering shipping address");
//l1.getWebElement(shippingObjects.ShippingPage_SameasBilling_Checkbox).click();
gVar.UnSelectCheckbox(shippingObjects.ShippingPage_SameasBilling_Checkbox);
log.info("Verifying the billing heading");
l1.getWebElement(billingObjects.billingPage_Heading).isDisplayed();
log.info("Entering Billing Address");
checkout.BillingAddress("//data//Checkout.xls","BillingAddress",1);
log.info("Enter billing email address");
l1.getWebElement(billingObjects.billingPage_Email_Textbox).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","BillingAddress", 1,10));
l1.getWebElement(billingObjects.billingPage_continue_button).click();
sa.assertAll();
	
}

@Test(groups={"sanity"},description="148951,148950,148900,289178")
public void TC02_EnterpaymentdetailsAndPlaceOrder() throws Exception
{
	
log.info("entering Payment details");
checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
log.info("Click on continue to order details page");
l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
log.info("order placement button in order review page");
l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).click();
log.info("order confirmation page");
l1.getWebElement(OrderConfirmationPOM.orderconfirmation_Heading);
	
}*/


}
