package com.cbk.checkout;

import org.testng.annotations.Test;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.OrderConfirmationPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.PaymentPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.GetData;

public class OCP_Reg extends BaseTest{
	
@Test(groups="{Regression}",description="125884,148937")
public void TC00_navigateToOrderConfirmationPOMPage() throws Exception
{

log.info("Login");
p.loginToAppliction(l1);
log.info("Clear cart items");
cart.ClearCartItems();
checkout.NavigateToOrderConfirmationPage();
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_date_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_number_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_date));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_number));


}
/*
@Test(groups="{Regression}",description="125907")
public void TC01_uiOfShipment1Section() throws Exception
{
	
log.info("Navigate to order confirmation page");
checkout.NavigateToOrderConfirmationPage();
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipment1_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_product_link));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_item_number_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_sku));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_sku_div));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_product_details_div));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_line_item_quantity));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_line_item_price));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_product_details_div));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_address));

//verifying address
sa.assertEquals(GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,12), l1.getWebElement(OrderConfirmationPOM.order_confirmation_shipping_address).getText());

sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_return_to_shopping_link));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_status_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_status));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_method_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_method));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_div));


}

@Test(groups="{Regression}",description="148897")
public void TC02_detailsDisplayedInPaymentMethod() throws Exception
{
		
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_payment_div));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_payment_type));
sa.assertEquals("Credit Card",l1.getWebElement(OrderConfirmationPOM.order_confirmation_payment_type).getText());
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_payment_div));

}

@Test(groups="{Regression}",description="148897")
public void TC03_DisplayOfPaymentTotalbutton() throws Exception
{
	
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_payment_total_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_subtotal_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_charges_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_sales_tax));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_total));
sa.assertEquals(l1.getWebElement(OrderConfirmationPOM.order_confirmation_subtotalvalue).getText().replace("$",""),l1.getWebElement(ShippingPOM.shippingOrderSummarysubtotalValue).getText().replace("$",""),"Comparing price");
sa.assertEquals(l1.getWebElement(OrderConfirmationPOM.order_confirmation_sales_taxValue).getText().replace("$",""),l1.getWebElement(ShippingPOM.shippingOrderSummarySalestaxValue).getText().replace("$",""),"Comparing price");
sa.assertEquals(l1.getWebElement(OrderConfirmationPOM.order_confirmation_ordertotal_value).getText().replace("$",""),l1.getWebElement(ShippingPOM.shippingOrderSummarOrderValue).getText().replace("$",""),"Comparing price");

}


@Test(groups="{Regression}",description="148895")
public void TC04_ShippingAddressComparison() throws Exception
{
		
log.info("Comparing the shipping address");
sa.assertEquals(GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,12), l1.getWebElement(OrderConfirmationPOM.order_confirmation_shipping_addressMain).getText());

}

@Test(groups="{Regression}",description="148896")
public void TC05_BillingAddressComparison() throws Exception
{
		
log.info("Comparing the shipping address");
sa.assertEquals(GetData.getDataFromExcel("//data//Checkout.xls","BillingAddress",1,12), l1.getWebElement(OrderConfirmationPOM.order_confirmation_billing_address).getText());

}

@Test(groups="{Regression}",description="148894")
public void TC06_ProductLineitemDetails() throws Exception
{

sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_product_link));
sa.assertEquals(p.PDP_Pname, l1.getWebElement(OrderConfirmationPOM.order_confirmation_product_link).getText());
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_sku));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_shipping_charges_heading));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_line_item_quantity));
sa.assertEquals(checkout.productQuantityinCart, l1.getWebElement(OrderConfirmationPOM.order_confirmation_line_item_quantity).getText());
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_line_item_price));
sa.assertEquals(cart.CartProductPrice, l1.getWebElement(OrderConfirmationPOM.order_confirmation_line_item_price).getText().replace("$",""));
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_total));
sa.assertEquals(checkout.orderTotalInBillingPage, l1.getWebElement(OrderConfirmationPOM.order_confirmation_order_total).getText().replace("$",""));

}

@Test(groups="{Regression}",description="125783")
public void TC06_ProductNameLinkinOCPPage() throws Exception
{

log.info("Product link in ocp page");
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_product_link));
log.info("get text of Product link in ocp page");
String ProductName=l1.getWebElement(OrderConfirmationPOM.order_confirmation_product_link).getText();
log.info("click on Product link in ocp page");
l1.getWebElement(OrderConfirmationPOM.order_confirmation_product_link).click();
log.info("PDP Product link in ocp page");
sa.assertTrue(gVar.assertVisible("PDP_Product_Name", "ShopNav\\PDP.properties"));
log.info("get Text of Product link in ocp page");
String ProdNameInPDP=l1.getWebElement("PDP_Product_Name", "ShopNav\\PDP.properties").getText();
sa.assertEquals(ProductName, ProdNameInPDP);

}

@Test(groups="{Regression}",description="125905,148899")
public void TC06_Returntoshoppingbutton() throws Exception
{

log.info("Navigate to order confirmation page");
checkout.NavigateToOrderConfirmationPage();
log.info("Fetch order confirmation number");
sa.assertTrue(gVar.assertVisible(OrderConfirmationPOM.order_confirmation_order_number));  
String orderNumber=l1.getWebElement(OrderConfirmationPOM.order_confirmation_order_number).getText();
log.info("Fetch order confirmation number"+orderNumber);
l1.getWebElement(OrderConfirmationPOM.order_confirmation_return_to_shopping_link).click();
log.info("Navigate to home page");
sa.assertTrue(gVar.assertVisible("HomePage_Logo","ShopNav\\HomePage.properties"),"Home page");

}*/





}
