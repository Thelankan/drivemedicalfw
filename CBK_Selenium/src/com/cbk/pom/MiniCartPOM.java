package com.cbk.pom;

import org.openqa.selenium.By;

public class MiniCartPOM {

	public static By MiniCartGoStraightToCheckoutButton=By.xpath("//a[contains(@class,'button mini-cart-link-checkout')]");
	public static By MiniCartLink=By.xpath("//a[@class='mini-cart-link']");
	public static By MiniCartEmptyLink=By.xpath("//a[@class='mini-cart-link mini-cart-empty']/span[@class='minicart-quantity']");

	
}
