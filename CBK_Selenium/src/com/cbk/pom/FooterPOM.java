package com.cbk.pom;

import org.openqa.selenium.By;

public class FooterPOM {
	
	public static By footerLinksCustomerService=By.xpath("//div[@class='footer-item']//ul[@class='menu-footer menu pipe']//li");
	public static By footerLinksManageyourcreditcardlinks=By.xpath("//div[@class='footer-item footer-second-item']//ul[@class='menu-footer']/li");
	public static By footerLinksManageyourFriendship=By.xpath("//div[@class='footer-item footer-third-item']//ul[@class='menu-footer']/li");
	public static By footerLinksManageservice=By.xpath("//div[@class='footer-item footer-forth-item second-class']//ul[@class='menu-footer menu pipe']//li");

}
