package com.cbk.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.cbk.utilgeneric.BaseTest;

public class LoginPom
{
	
//	public LoginPom(WebDriver driver) {
//		this.driver=driver;
//	}

	public By login_link=By.xpath("//span[contains(text(),'Login')]");
	public By login_textbox=By.xpath("//div[@class='login-box login-account']//input[(contains(@id,'dwfrm_login_username'))]");
	public By Login_pwd_textbox=By.xpath("//div[@class='login-box login-account']//input[(contains(@id,'dwfrm_login_password'))]");
	public By Login_button=By.xpath("//button[@class='spc-login-btn']");
	public By Login_CreateaccountButton=By.xpath("//button[@type='submit'][contains(text(),'Create Account')]");
	public By Login_rewardsHeading=By.xpath("//div[@class='Fr-title ']");
	//public By Login_CreateaccountFNtextbox=By.id("dwfrm_profile_customer_firstname");
	
}
