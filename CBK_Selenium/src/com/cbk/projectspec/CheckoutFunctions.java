package com.cbk.projectspec;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.CheckoutIlpPOM;
import com.cbk.pom.OrderConfirmationPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.PaymentPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.GetData;

public class CheckoutFunctions extends BaseTest {

	public String productQuantityinCart;
	public String orderTotalInBillingPage;
	public BaseTest BaseTest;
	
	
//	JavascriptExecutor js = (JavascriptExecutor) driver;
//	myExecutor.executeScript.("document.getElementsByName('q')[0].value='Kirtesh'");
		
public void NavigateToILPPage() throws Exception
{
	
	BaseTest.cart.navigateToCart();
	Thread.sleep(2000);
	productQuantityinCart=BaseTest.l1.getWebElement(CartPOM.cartQuantity).getAttribute("value");
	BaseTest.log.info("Click on checkout button top");
	BaseTest.l1.getWebElements(CartPOM.cartCheckoutbuttons).get(0).click();
	
}
	
public void NavigateToShippingPage() throws Exception
{
	
	BaseTest.cart.navigateToCart();
	Thread.sleep(2000);
	BaseTest.log.info("Click on checkout button top");
	BaseTest.l1.getWebElements(CartPOM.cartCheckoutbuttons).get(1).click();
	BaseTest.log.info("Verifying Shipping Page Heading");
	Thread.sleep(2000);
	try {
		
		BaseTest.log.info("Clicking on checkout as a guest button");
		l1.getWebElement(CheckoutIlpPOM.Ilp_CheckoutasGuestButton).click();
		Thread.sleep(4000);
		BaseTest.log.info("Shipping page Heading1");
		//sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Heading),"Shipping page Heading");
		
	} catch (Exception e) {
		
		BaseTest.log.info("Shipping page Heading2");
		//sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Heading));
		
	}
	
}

public void NavigateToBillingPage() throws Exception
{
	BaseTest.cart.navigateToCart();
	Thread.sleep(3000);
	BaseTest.log.info("Click on checkout button top");
	BaseTest.l1.getWebElements(CartPOM.cartCheckoutbuttons).get(1).click();
	BaseTest.log.info("Verifying Shipping Page Heading");
	try {
		
		BaseTest.log.info("Clicking on checkout as a guest button");
		l1.getWebElement(CheckoutIlpPOM.Ilp_CheckoutasGuestButton).click();
		Thread.sleep(4000);
		BaseTest.log.info("Shipping page Heading1");
		//sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Heading),"Shipping page Heading");
		
		
	} catch (Exception e) {
		
		BaseTest.log.info("Shipping page Heading2");
		//sa.assertTrue(gVar.assertVisible(shippingObjects.ShippingPage_Heading));
		
	}
	
	Thread.sleep(8000);
	log.info("Enter shipping address");
	checkout.ShippingAddress("//data//Checkout.xls","ShippingAddress",1);
	
	log.info("un select the billing checkbox");
	gVar.UnSelectCheckbox(shippingObjects.ShippingPage_SameasBilling_Checkbox);
	
	orderTotalInBillingPage=BaseTest.l1.getWebElement(billingObjects.BillingPage_OrderTotal).getText().replace("$","");
	
	
}

public void NavigateToOrderConfirmationPage() throws Exception
{
	
	BaseTest.checkout.NavigateToBillingPage();
	log.info("Enter Billing address");
	checkout.BillingAddress("//data//Checkout.xls","BillingAddress",1);
	log.info("Enter email ID in billing page");
	l1.getWebElement(billingObjects.billingPage_Email_Textbox).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","BillingAddress",1,10));
	Thread.sleep(3000);
	log.info("billing page continue button");
	
	while (l1.getWebElement(billingObjects.billingPage_continue_button).isDisplayed()){
		l1.getWebElement(billingObjects.billingPage_continue_button).click();
	}
	
	log.info("Entering payment details");
	checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
	log.info("click on continue button ib billing page");
	l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
	
	try {
		
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingaddressValidationDialogue));
		l1.getWebElement(shippingObjects.shippingaddressValidationContinue).click();
		l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).isDisplayed();
		
	} catch (Exception e) {
		
		log.info("Navigated to billing page");
		l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).isDisplayed();
		
	}
	
	log.info("click on place order button");
	l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).click();
	
	Thread.sleep(2000);
	log.info("Order confirmation heading");
	l1.getWebElement(OrderConfirmationPOM.order_confirmation_heading);
	
}

	
public void ShippingAddress(String Excel,String SheetName,int Row) throws Exception
{

	BaseTest.log.info("Enter shipping address First Name");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_FN_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,1));
	BaseTest.log.info("Enter shipping address Last Name");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_LN_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,2));
	BaseTest.log.info("Enter shipping address Address1 Name");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_Address1_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,3));
	BaseTest.log.info("Enter shipping address Address2 Name");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_Address2_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,4));
	BaseTest.log.info("Enter shipping address City Name");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_City_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,5));
	BaseTest.log.info("Enter shipping address ZipCode Name");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_ZipCode_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,6));
	BaseTest.log.info("Enter shipping address Country Name");
	BaseTest.gVar.handlingDropdown(shippingObjects.ShippingPage_Country_Dropdown,GetData.getDataFromExcel(Excel,SheetName,Row,7));
	BaseTest.log.info("Enter shipping address State Name");
	BaseTest.gVar.handlingDropdown(shippingObjects.ShippingPage_State_Dropdown,GetData.getDataFromExcel(Excel,SheetName,Row,8));
	BaseTest.log.info("Enter shipping address Phone");
	BaseTest.l1.getWebElement(shippingObjects.ShippingPage_Phone_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,9));

}


public void BillingAddress(String Excel,String SheetName,int Row) throws Exception
{
	
	BaseTest.log.info("Enter billing address First Name");
	BaseTest.l1.getWebElement(billingObjects.billingPage_FN_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,1));
	BaseTest.log.info("Enter billing address Last Name");
	BaseTest.l1.getWebElement(billingObjects.billingPage_LN_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,2));
	BaseTest.log.info("Enter billing address Address1 Name");
	BaseTest.l1.getWebElement(billingObjects.billingPage_Address1_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,3));
	BaseTest.log.info("Enter billing address Address2 Name");
	BaseTest.l1.getWebElement(billingObjects.billingPage_Address2_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,4));
	BaseTest.log.info("Enter billing address City Name");
	BaseTest.l1.getWebElement(billingObjects.billingPage_City_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,5));
	BaseTest.log.info("Enter billing address ZipCode Name");
	BaseTest.l1.getWebElement(billingObjects.billingPage_ZipCode_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,6));
	BaseTest.log.info("Enter billing address Country Name");
	BaseTest.gVar.handlingDropdown(billingObjects.billingPage_Country_Dropdown,GetData.getDataFromExcel(Excel,SheetName,Row,7));
	BaseTest.log.info("Enter billing address State Name");
	BaseTest.gVar.handlingDropdown(billingObjects.billingPage_State_Dropdown,GetData.getDataFromExcel(Excel,SheetName,Row,8));
	BaseTest.log.info("Enter billing address Phone");
	BaseTest.l1.getWebElement(billingObjects.billingPage_Phone_TextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,9));

}

public void PaymentDetails(String Excel,String SheetName,int Row) throws Exception
{
	
	BaseTest.gVar.handlingDropdown(billingObjects.billingPage_paymentCardtypeDropdown,GetData.getDataFromExcel(Excel,SheetName,Row,2));
	BaseTest.l1.getWebElement(billingObjects.billingPage_paymentOwnerNameTextbox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,1));
	BaseTest.l1.getWebElement(billingObjects.billingPage_paymentCreditCardTextBox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,3));
	BaseTest.gVar.handlingDropdown(billingObjects.billingPage_PaymentExpireMonthtextbox,GetData.getDataFromExcel(Excel,SheetName,Row,4));
	BaseTest.gVar.handlingDropdown(billingObjects.billingPage_paymentExpireYeartextbox,GetData.getDataFromExcel(Excel,SheetName,Row,5));
	BaseTest.l1.getWebElement(billingObjects.billingPage_paymentCVVTextbox).sendKeys(GetData.getDataFromExcel(Excel,SheetName,Row,6));

}

public void Shippingfieldsclear() throws Exception
{
	l1.getWebElement(shippingObjects.ShippingPage_FN_TextBox).clear();
	l1.getWebElement(shippingObjects.ShippingPage_LN_TextBox).clear();
	l1.getWebElement(shippingObjects.ShippingPage_Address1_TextBox).clear();
	l1.getWebElement(shippingObjects.ShippingPage_Address2_TextBox).clear();
	l1.getWebElement(shippingObjects.ShippingPage_City_TextBox).clear();
	l1.getWebElement(shippingObjects.ShippingPage_ZipCode_TextBox).clear();
}

public void Billingfieldsclear() throws Exception
{
	l1.getWebElement(billingObjects.billingPage_FN_TextBox).clear();
	l1.getWebElement(billingObjects.billingPage_LN_TextBox).clear();
	l1.getWebElement(billingObjects.billingPage_Address1_TextBox).clear();
	l1.getWebElement(billingObjects.billingPage_Address2_TextBox).clear();
	l1.getWebElement(billingObjects.billingPage_City_TextBox).clear();
	l1.getWebElement(billingObjects.billingPage_ZipCode_TextBox).clear();
}


}
